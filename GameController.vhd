LIBRARY ieee;
USE ieee.std_logic_1164.all;

entity GameController is
port
	(
		------------------------	Clock Input	 	------------------------
		clk	: 	in	STD_LOGIC;											--	XX? MHz
		-- CLOCKTAP	: 	out	STD_LOGIC;											
		
		------------------------	Key Input	-----------------------
		-- UP => 001
		-- DOWN => 010
		-- LEFT => 011
		-- RIGHT => 100
		-- ENTER => 101
		-- ESC => 110
		-- U => 111
		KEY 	:		in	STD_LOGIC_VECTOR (2 downto 0);		--	Pushbutton[3:0]
		IS_KEY :		in STD_LOGIC									-- Rises when key is pressed
	);
end;

architecture struct of GameController is
	--Up counter for score game 
	COMPONENT upcount IS	
	PORT ( Clock, Resetn, E : IN STD_LOGIC ;
			Q : OUT STD_LOGIC_VECTOR (11 DOWNTO 0)) ;
	END COMPONENT;
	
	--each tile of the game map is represented by a register of 4bits	
	COMPONENT reg8 IS
	PORT ( D  : IN   STD_LOGIC_VECTOR(3 DOWNTO 0) ;
			Resetn, Clock: IN STD_LOGIC ;
			Q  : OUT  STD_LOGIC_VECTOR(3 DOWNTO 0) ) ;
	END COMPONENT ;
	
	--the number to input in the tile
	SIGNAL number_tile : std_LOGIC_VECTOR(3 downto 0);
	--reset the tile
	SIGNAL reset_tile : std_LOGIC;
	
	BEGIN
	
	tile: 
   for I in 0 to 15 generate
      regx : reg8 port map
        (number_tile(3 downto 0), reset_tile, clk);
   end generate tile;
	
	process (clk, IS_KEY)
	begin
	
	end process;
	
end STRUCT;