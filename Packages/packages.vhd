LIBRARY ieee ;
USE ieee.std_logic_1164.all ;

PACKAGE conv_7seg_package IS
	COMPONENT conv_7seg
		PORT ( digit : IN STD_LOGIC_VECTOR(3 DOWNTO 0) ;
			seg : OUT STD_LOGIC_VECTOR(0 TO 6) ) ;
	END COMPONENT;
END conv_7seg_package;


LIBRARY ieee ;
USE ieee.std_logic_1164.all ;
PACKAGE counter_package IS
	COMPONENT reg8 IS
	PORT ( D  : IN   STD_LOGIC_VECTOR(3 DOWNTO 0) ;
			Resetn, Clock: IN STD_LOGIC ;
			Q  : OUT  STD_LOGIC_VECTOR(3 DOWNTO 0) ) ;
	END COMPONENT ;
	
	COMPONENT upcount IS	
	GENERIC ( k : INTEGER := 12 ) ;
	PORT ( Clock, Resetn, E : IN STD_LOGIC ;
			Q : OUT STD_LOGIC_VECTOR (k-1 DOWNTO 0)) ;
	END COMPONENT;
end counter_package;