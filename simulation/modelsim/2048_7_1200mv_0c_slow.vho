-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 32-bit"
-- VERSION "Version 13.0.1 Build 232 06/12/2013 Service Pack 1 SJ Web Edition"

-- DATE "06/25/2017 15:33:53"

-- 
-- Device: Altera EP3C16U256C7 Package UFBGA256
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY CYCLONEIII;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIII.CYCLONEIII_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	ps2_kbd_test IS
    PORT (
	CLOCK_24 : IN std_logic_vector(1 DOWNTO 0);
	CLOCK_27 : IN std_logic_vector(1 DOWNTO 0);
	CLOCK_50 : IN std_logic;
	KEY : IN std_logic_vector(3 DOWNTO 0);
	SW : IN std_logic_vector(9 DOWNTO 0);
	HEX0 : OUT std_logic_vector(6 DOWNTO 0);
	HEX1 : OUT std_logic_vector(6 DOWNTO 0);
	HEX2 : OUT std_logic_vector(6 DOWNTO 0);
	HEX3 : OUT std_logic_vector(6 DOWNTO 0);
	LEDG : OUT std_logic_vector(7 DOWNTO 0);
	LEDR : OUT std_logic_vector(9 DOWNTO 0);
	PS2_DAT : INOUT std_logic;
	PS2_CLK : INOUT std_logic
	);
END ps2_kbd_test;

-- Design Ports Information
-- CLOCK_24[1]	=>  Location: PIN_D3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- CLOCK_27[0]	=>  Location: PIN_D14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- CLOCK_27[1]	=>  Location: PIN_P3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- CLOCK_50	=>  Location: PIN_B3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- KEY[2]	=>  Location: PIN_G2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- KEY[3]	=>  Location: PIN_N3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[0]	=>  Location: PIN_T6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[1]	=>  Location: PIN_R13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[2]	=>  Location: PIN_F2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[3]	=>  Location: PIN_L3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[4]	=>  Location: PIN_T2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[5]	=>  Location: PIN_G11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[6]	=>  Location: PIN_C14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[7]	=>  Location: PIN_T10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[8]	=>  Location: PIN_A5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[9]	=>  Location: PIN_J1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[0]	=>  Location: PIN_B11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[1]	=>  Location: PIN_B14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[2]	=>  Location: PIN_E9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[3]	=>  Location: PIN_A13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[4]	=>  Location: PIN_C6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[5]	=>  Location: PIN_A14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[6]	=>  Location: PIN_B10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[0]	=>  Location: PIN_D9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[1]	=>  Location: PIN_A15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[2]	=>  Location: PIN_C9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[3]	=>  Location: PIN_F9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[4]	=>  Location: PIN_A11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[5]	=>  Location: PIN_C8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[6]	=>  Location: PIN_A10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[0]	=>  Location: PIN_B5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[1]	=>  Location: PIN_P11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[2]	=>  Location: PIN_M11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[3]	=>  Location: PIN_R10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[4]	=>  Location: PIN_K15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[5]	=>  Location: PIN_K2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[6]	=>  Location: PIN_L8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[0]	=>  Location: PIN_A12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[1]	=>  Location: PIN_N5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[2]	=>  Location: PIN_D8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[3]	=>  Location: PIN_K12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[4]	=>  Location: PIN_E10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[5]	=>  Location: PIN_E11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[6]	=>  Location: PIN_P9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDG[0]	=>  Location: PIN_N15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDG[1]	=>  Location: PIN_R12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDG[2]	=>  Location: PIN_M8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDG[3]	=>  Location: PIN_D11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDG[4]	=>  Location: PIN_L9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDG[5]	=>  Location: PIN_D12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDG[6]	=>  Location: PIN_B12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDG[7]	=>  Location: PIN_B13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[0]	=>  Location: PIN_P8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[1]	=>  Location: PIN_N1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[2]	=>  Location: PIN_T13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[3]	=>  Location: PIN_F3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[4]	=>  Location: PIN_R11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[5]	=>  Location: PIN_P15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[6]	=>  Location: PIN_N6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[7]	=>  Location: PIN_D5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[8]	=>  Location: PIN_B6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[9]	=>  Location: PIN_B1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- PS2_DAT	=>  Location: PIN_F14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- PS2_CLK	=>  Location: PIN_D15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- CLOCK_24[0]	=>  Location: PIN_E2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- KEY[0]	=>  Location: PIN_E1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- KEY[1]	=>  Location: PIN_M16,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF ps2_kbd_test IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_CLOCK_24 : std_logic_vector(1 DOWNTO 0);
SIGNAL ww_CLOCK_27 : std_logic_vector(1 DOWNTO 0);
SIGNAL ww_CLOCK_50 : std_logic;
SIGNAL ww_KEY : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_SW : std_logic_vector(9 DOWNTO 0);
SIGNAL ww_HEX0 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX1 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX2 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX3 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_LEDG : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_LEDR : std_logic_vector(9 DOWNTO 0);
SIGNAL \CLOCK_24[0]~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \CLOCKHZ~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \kbd_ctrl|sigsend~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \KEY[0]~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \kbd_ctrl|ps2_ctrl|count[1]~17_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[7]~30_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~1\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~3\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~4_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~5\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~6_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~7\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~8_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~9\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~10_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~11\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~12_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~13\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~14_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~1\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~3\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~4_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~5\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~6_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~7\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~8_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~9\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~10_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~11\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~12_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~13\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~14_combout\ : std_logic;
SIGNAL \count[1]~22_combout\ : std_logic;
SIGNAL \count[1]~23\ : std_logic;
SIGNAL \count[2]~24_combout\ : std_logic;
SIGNAL \count[2]~25\ : std_logic;
SIGNAL \count[3]~26_combout\ : std_logic;
SIGNAL \count[3]~27\ : std_logic;
SIGNAL \count[4]~28_combout\ : std_logic;
SIGNAL \count[4]~29\ : std_logic;
SIGNAL \count[5]~30_combout\ : std_logic;
SIGNAL \count[5]~31\ : std_logic;
SIGNAL \count[6]~32_combout\ : std_logic;
SIGNAL \count[6]~33\ : std_logic;
SIGNAL \count[7]~34_combout\ : std_logic;
SIGNAL \count[7]~35\ : std_logic;
SIGNAL \count[8]~36_combout\ : std_logic;
SIGNAL \count[8]~37\ : std_logic;
SIGNAL \count[9]~38_combout\ : std_logic;
SIGNAL \count[9]~39\ : std_logic;
SIGNAL \count[10]~40_combout\ : std_logic;
SIGNAL \count[10]~41\ : std_logic;
SIGNAL \count[11]~42_combout\ : std_logic;
SIGNAL \count[11]~43\ : std_logic;
SIGNAL \count[12]~44_combout\ : std_logic;
SIGNAL \count[12]~45\ : std_logic;
SIGNAL \count[13]~46_combout\ : std_logic;
SIGNAL \count[13]~47\ : std_logic;
SIGNAL \count[14]~48_combout\ : std_logic;
SIGNAL \count[14]~49\ : std_logic;
SIGNAL \count[15]~50_combout\ : std_logic;
SIGNAL \count[15]~51\ : std_logic;
SIGNAL \count[16]~52_combout\ : std_logic;
SIGNAL \count[16]~53\ : std_logic;
SIGNAL \count[17]~54_combout\ : std_logic;
SIGNAL \count[17]~55\ : std_logic;
SIGNAL \count[18]~56_combout\ : std_logic;
SIGNAL \count[18]~57\ : std_logic;
SIGNAL \count[19]~58_combout\ : std_logic;
SIGNAL \count[19]~59\ : std_logic;
SIGNAL \count[20]~60_combout\ : std_logic;
SIGNAL \count[20]~61\ : std_logic;
SIGNAL \count[21]~62_combout\ : std_logic;
SIGNAL \count[21]~63\ : std_logic;
SIGNAL \count[22]~64_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[9]~34_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[13]~42_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[15]~46_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[16]~48_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal7~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal14~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal4~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal5~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal19~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|state.RELEASE~q\ : std_logic;
SIGNAL \kbd_ctrl|Equal12~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal8~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|KEY2~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|FROMPS2:count[3]~q\ : std_logic;
SIGNAL \kbd_ctrl|nstate.RELEASE~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Equal3~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|LessThan4~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|ps2_clk~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|LessThan4~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|sigsend~q\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count~14_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|fcount[7]~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|rcount[7]~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sigtrigger~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sigtrigger~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|cmdstate.SENDVAL~q\ : std_logic;
SIGNAL \kbd_ctrl|cmdstate~18_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~16_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|fcount[7]~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~17_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~18_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~16_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|rcount[7]~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~17_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~18_combout\ : std_logic;
SIGNAL \kbd_ctrl|Selector13~0_combout\ : std_logic;
SIGNAL \dir~q\ : std_logic;
SIGNAL \CLOCKHZ~q\ : std_logic;
SIGNAL \kbd_ctrl|Selector13~4_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~19_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~20_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~21_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~22_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~23_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~19_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~20_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~21_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~22_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~23_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|parchecked~1_combout\ : std_logic;
SIGNAL \dir~0_combout\ : std_logic;
SIGNAL \LessThan0~0_combout\ : std_logic;
SIGNAL \LessThan0~1_combout\ : std_logic;
SIGNAL \Equal1~0_combout\ : std_logic;
SIGNAL \LessThan0~2_combout\ : std_logic;
SIGNAL \LessThan0~3_combout\ : std_logic;
SIGNAL \LessThan0~4_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Equal2~3_combout\ : std_logic;
SIGNAL \Equal1~1_combout\ : std_logic;
SIGNAL \Equal1~2_combout\ : std_logic;
SIGNAL \Equal1~3_combout\ : std_logic;
SIGNAL \Equal1~4_combout\ : std_logic;
SIGNAL \Equal1~5_combout\ : std_logic;
SIGNAL \Equal1~6_combout\ : std_logic;
SIGNAL \Equal1~7_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[0]~55_combout\ : std_logic;
SIGNAL \count[0]~66_combout\ : std_logic;
SIGNAL \CLOCK_24[1]~input_o\ : std_logic;
SIGNAL \CLOCK_27[0]~input_o\ : std_logic;
SIGNAL \CLOCK_27[1]~input_o\ : std_logic;
SIGNAL \CLOCK_50~input_o\ : std_logic;
SIGNAL \KEY[2]~input_o\ : std_logic;
SIGNAL \KEY[3]~input_o\ : std_logic;
SIGNAL \SW[0]~input_o\ : std_logic;
SIGNAL \SW[1]~input_o\ : std_logic;
SIGNAL \SW[2]~input_o\ : std_logic;
SIGNAL \SW[3]~input_o\ : std_logic;
SIGNAL \SW[4]~input_o\ : std_logic;
SIGNAL \SW[5]~input_o\ : std_logic;
SIGNAL \SW[6]~input_o\ : std_logic;
SIGNAL \SW[7]~input_o\ : std_logic;
SIGNAL \SW[8]~input_o\ : std_logic;
SIGNAL \SW[9]~input_o\ : std_logic;
SIGNAL \PS2_CLK~input_o\ : std_logic;
SIGNAL \CLOCKHZ~clkctrl_outclk\ : std_logic;
SIGNAL \kbd_ctrl|sigsend~clkctrl_outclk\ : std_logic;
SIGNAL \count[8]~feeder_combout\ : std_logic;
SIGNAL \PS2_DAT~output_o\ : std_logic;
SIGNAL \PS2_CLK~output_o\ : std_logic;
SIGNAL \HEX0[0]~output_o\ : std_logic;
SIGNAL \HEX0[1]~output_o\ : std_logic;
SIGNAL \HEX0[2]~output_o\ : std_logic;
SIGNAL \HEX0[3]~output_o\ : std_logic;
SIGNAL \HEX0[4]~output_o\ : std_logic;
SIGNAL \HEX0[5]~output_o\ : std_logic;
SIGNAL \HEX0[6]~output_o\ : std_logic;
SIGNAL \HEX1[0]~output_o\ : std_logic;
SIGNAL \HEX1[1]~output_o\ : std_logic;
SIGNAL \HEX1[2]~output_o\ : std_logic;
SIGNAL \HEX1[3]~output_o\ : std_logic;
SIGNAL \HEX1[4]~output_o\ : std_logic;
SIGNAL \HEX1[5]~output_o\ : std_logic;
SIGNAL \HEX1[6]~output_o\ : std_logic;
SIGNAL \HEX2[0]~output_o\ : std_logic;
SIGNAL \HEX2[1]~output_o\ : std_logic;
SIGNAL \HEX2[2]~output_o\ : std_logic;
SIGNAL \HEX2[3]~output_o\ : std_logic;
SIGNAL \HEX2[4]~output_o\ : std_logic;
SIGNAL \HEX2[5]~output_o\ : std_logic;
SIGNAL \HEX2[6]~output_o\ : std_logic;
SIGNAL \HEX3[0]~output_o\ : std_logic;
SIGNAL \HEX3[1]~output_o\ : std_logic;
SIGNAL \HEX3[2]~output_o\ : std_logic;
SIGNAL \HEX3[3]~output_o\ : std_logic;
SIGNAL \HEX3[4]~output_o\ : std_logic;
SIGNAL \HEX3[5]~output_o\ : std_logic;
SIGNAL \HEX3[6]~output_o\ : std_logic;
SIGNAL \LEDG[0]~output_o\ : std_logic;
SIGNAL \LEDG[1]~output_o\ : std_logic;
SIGNAL \LEDG[2]~output_o\ : std_logic;
SIGNAL \LEDG[3]~output_o\ : std_logic;
SIGNAL \LEDG[4]~output_o\ : std_logic;
SIGNAL \LEDG[5]~output_o\ : std_logic;
SIGNAL \LEDG[6]~output_o\ : std_logic;
SIGNAL \LEDG[7]~output_o\ : std_logic;
SIGNAL \LEDR[0]~output_o\ : std_logic;
SIGNAL \LEDR[1]~output_o\ : std_logic;
SIGNAL \LEDR[2]~output_o\ : std_logic;
SIGNAL \LEDR[3]~output_o\ : std_logic;
SIGNAL \LEDR[4]~output_o\ : std_logic;
SIGNAL \LEDR[5]~output_o\ : std_logic;
SIGNAL \LEDR[6]~output_o\ : std_logic;
SIGNAL \LEDR[7]~output_o\ : std_logic;
SIGNAL \LEDR[8]~output_o\ : std_logic;
SIGNAL \LEDR[9]~output_o\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sigtrigger~feeder_combout\ : std_logic;
SIGNAL \KEY[0]~input_o\ : std_logic;
SIGNAL \KEY[0]~inputclkctrl_outclk\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sigtrigger~q\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_outclk\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sigsending~feeder_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sigsendend~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|TOPS2:count[0]~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|TOPS2~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[0]~15_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[10]~37\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[11]~38_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[5]~26_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|ps2_clk~3_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|ps2_clk~4_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|ps2_clk~5_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[11]~19_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[0]~16\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[1]~18\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[2]~20_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[2]~21\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[3]~22_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[3]~23\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[4]~24_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[4]~25\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[5]~27\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[6]~28_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[6]~29\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[7]~31\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[8]~32_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[8]~33\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[9]~34_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[9]~35\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[10]~36_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|ps2_clk~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sigclkheld~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sigclkheld~q\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sigsendend~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|TOPS2:count[0]~q\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sigsendend~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sigsendend~q\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|process_2~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sigsending~q\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sigclkreleased~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sigclkreleased~q\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|TOPS2~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|TOPS2:count[2]~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|TOPS2:count[2]~q\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add6~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|TOPS2:count[1]~q\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add3~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|FROMPS2~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~q\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count~12_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~q\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count~13_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~q\ : std_logic;
SIGNAL \PS2_DAT~input_o\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sdata[7]~4_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sdata[7]~5_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|FROMPS2~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Decoder0~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Decoder0~3_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sdata[2]~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Decoder0~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sdata[0]~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Decoder0~5_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sdata[1]~6_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|FROMPS2~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Decoder0~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sdata[4]~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Decoder0~4_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sdata[5]~3_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Decoder0~6_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sdata[3]~7_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|FROMPS2~3_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|parchecked~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|parchecked~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|parchecked~q\ : std_logic;
SIGNAL \KEY[1]~input_o\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|odata_rdy~combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|send_rdy~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[1]~18_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[18]~54_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[1]~19\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[2]~20_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[2]~21\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[3]~22_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[3]~23\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[4]~25\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[5]~26_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[5]~27\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[6]~29\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[7]~31\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[8]~32_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[8]~33\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[9]~35\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[10]~36_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[10]~37\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[11]~38_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[11]~39\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[12]~40_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[12]~41\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[13]~43\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[14]~44_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[14]~45\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[15]~47\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[16]~49\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[17]~50_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[17]~51\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[18]~52_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Equal2~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Equal2~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[6]~28_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[7]~30_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[4]~24_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Equal2~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Equal2~4_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Equal2~5_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|send_rdy~3_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|send_rdy~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|send_rdy~_emulated_q\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|send_rdy~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|Selector9~0_combout\ : std_logic;
SIGNAL \lights~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|newdata~feeder_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal21~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Decoder0~7_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sdata[6]~8_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal21~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal21~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|cmdstate.CLEAR~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|cmdstate.CLEAR~q\ : std_logic;
SIGNAL \kbd_ctrl|sigsending~q\ : std_logic;
SIGNAL \kbd_ctrl|process_0~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|state.DECODE~q\ : std_logic;
SIGNAL \kbd_ctrl|process_13~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|newdata~q\ : std_logic;
SIGNAL \kbd_ctrl|state.EXT1~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|state.EXT1~q\ : std_logic;
SIGNAL \kbd_ctrl|Equal0~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal0~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|state.CODE~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|state.CODE~q\ : std_logic;
SIGNAL \kbd_ctrl|state.CLRDP~feeder_combout\ : std_logic;
SIGNAL \kbd_ctrl|state.CLRDP~q\ : std_logic;
SIGNAL \kbd_ctrl|Equal0~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|nstate.EXT0~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|state.EXT0~q\ : std_logic;
SIGNAL \kbd_ctrl|Selector0~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|Selector0~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|state.IDLE~q\ : std_logic;
SIGNAL \kbd_ctrl|nstate.FETCH~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|state.FETCH~q\ : std_logic;
SIGNAL \kbd_ctrl|process_4~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|relbt~combout\ : std_logic;
SIGNAL \kbd_ctrl|selE0~combout\ : std_logic;
SIGNAL \kbd_ctrl|selbt~combout\ : std_logic;
SIGNAL \kbd_ctrl|KEY0~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal10~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal10~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal17~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|key1code[2]~feeder_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal4~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal4~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal4~3_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal4~5_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal14~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal14~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal14~3_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal14~4_combout\ : std_logic;
SIGNAL \kbd_ctrl|key2en~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|key1en~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal18~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal18~5_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal12~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal12~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal11~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal11~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal11~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|KEY1~6_combout\ : std_logic;
SIGNAL \kbd_ctrl|KEY1~7_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal4~4_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal4~6_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal4~7_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal14~5_combout\ : std_logic;
SIGNAL \kbd_ctrl|KEY1~9_combout\ : std_logic;
SIGNAL \kbd_ctrl|KEY1~8_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal18~4_combout\ : std_logic;
SIGNAL \kbd_ctrl|key2en~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|key2en~3_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal5~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal5~3_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal5~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal5~4_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal5~5_combout\ : std_logic;
SIGNAL \kbd_ctrl|key2en~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|key0en~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal17~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal6~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal6~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal6~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|key0clearn~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|KEY0~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|KEY0~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal14~6_combout\ : std_logic;
SIGNAL \kbd_ctrl|KEY1~10_combout\ : std_logic;
SIGNAL \kbd_ctrl|KEY2~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|KEY2~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|KEY2~3_combout\ : std_logic;
SIGNAL \kbd_ctrl|KEY2~4_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal18~3_combout\ : std_logic;
SIGNAL \kbd_ctrl|KEY2~5_combout\ : std_logic;
SIGNAL \kbd_ctrl|KEY2~6_combout\ : std_logic;
SIGNAL \kbd_ctrl|KEY2~7_combout\ : std_logic;
SIGNAL \kbd_ctrl|KEY2~8_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal19~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal19~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|key_on[2]~feeder_combout\ : std_logic;
SIGNAL \kbd_ctrl|key_on[0]~feeder_combout\ : std_logic;
SIGNAL \Equal0~0_combout\ : std_logic;
SIGNAL \lights~0_combout\ : std_logic;
SIGNAL \lights~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|laststate[0]~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal20~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal20~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|siguplights~feeder_combout\ : std_logic;
SIGNAL \kbd_ctrl|siguplights~q\ : std_logic;
SIGNAL \kbd_ctrl|process_15~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|cmdstate.SEND~q\ : std_logic;
SIGNAL \kbd_ctrl|Selector13~3_combout\ : std_logic;
SIGNAL \kbd_ctrl|cmdstate.WAITACK1~q\ : std_logic;
SIGNAL \kbd_ctrl|Selector11~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|Selector11~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|cmdstate.SETLIGHTS~q\ : std_logic;
SIGNAL \kbd_ctrl|WideOr2~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|Selector13~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|Selector13~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|Selector10~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|cmdstate.WAITACK~q\ : std_logic;
SIGNAL \kbd_ctrl|Selector8~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|Selector8~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|cmdstate.SETCMD~q\ : std_logic;
SIGNAL \kbd_ctrl|hdata[3]~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|ps2_data~6_combout\ : std_logic;
SIGNAL \kbd_ctrl|Selector7~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|Selector6~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|Selector5~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|ps2_data~8_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Mux0~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Mux0~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|ps2_data~7_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|ps2_data~9_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add6~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|TOPS2:count[3]~q\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|ps2_data~10_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|ps2_data~11_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|ps2_data~reg0_q\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|ps2_data~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Equal5~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|ps2_data~3_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|ps2_data~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|ps2_data~en_emulated_q\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|ps2_data~2_combout\ : std_logic;
SIGNAL \CLOCK_24[0]~input_o\ : std_logic;
SIGNAL \CLOCK_24[0]~inputclkctrl_outclk\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|ps2_clk~en_q\ : std_logic;
SIGNAL \hexseg0|Mux6~0_combout\ : std_logic;
SIGNAL \hexseg0|Mux5~0_combout\ : std_logic;
SIGNAL \hexseg0|Mux4~0_combout\ : std_logic;
SIGNAL \hexseg0|Mux3~0_combout\ : std_logic;
SIGNAL \hexseg0|Mux2~0_combout\ : std_logic;
SIGNAL \hexseg0|Mux1~0_combout\ : std_logic;
SIGNAL \hexseg0|Mux0~0_combout\ : std_logic;
SIGNAL \hexseg1|Mux6~0_combout\ : std_logic;
SIGNAL \hexseg1|Mux5~0_combout\ : std_logic;
SIGNAL \hexseg1|Mux4~0_combout\ : std_logic;
SIGNAL \hexseg1|Mux3~0_combout\ : std_logic;
SIGNAL \hexseg1|Mux2~0_combout\ : std_logic;
SIGNAL \hexseg1|Mux1~0_combout\ : std_logic;
SIGNAL \hexseg1|Mux0~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|key_on[1]~feeder_combout\ : std_logic;
SIGNAL lights : std_logic_vector(2 DOWNTO 0);
SIGNAL count : std_logic_vector(22 DOWNTO 0);
SIGNAL \kbd_ctrl|laststate\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \kbd_ctrl|key_on\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \kbd_ctrl|key2code\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \kbd_ctrl|key1code\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \kbd_ctrl|key0code\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \kbd_ctrl|hdata\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \kbd_ctrl|fetchdata\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \kbd_ctrl|ps2_ctrl|sdata\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \kbd_ctrl|ps2_ctrl|rcount\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \kbd_ctrl|ps2_ctrl|countclk\ : std_logic_vector(18 DOWNTO 0);
SIGNAL \kbd_ctrl|ps2_ctrl|fcount\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \kbd_ctrl|ps2_ctrl|hdata\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \kbd_ctrl|ps2_ctrl|count\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \kbd_ctrl|ps2_ctrl|ALT_INV_send_rdy~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|ALT_INV_sigsendend~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ALT_INV_process_13~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|ALT_INV_process_2~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|ALT_INV_ps2_data~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ALT_INV_process_15~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|ALT_INV_FROMPS2~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|ALT_INV_ps2_clk~en_q\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|ALT_INV_sigclkheld~q\ : std_logic;
SIGNAL \kbd_ctrl|ALT_INV_process_0~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ALT_INV_KEY2~8_combout\ : std_logic;
SIGNAL \kbd_ctrl|ALT_INV_KEY1~8_combout\ : std_logic;
SIGNAL \kbd_ctrl|ALT_INV_KEY0~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|ALT_INV_key0code\ : std_logic_vector(13 DOWNTO 13);
SIGNAL \hexseg1|ALT_INV_Mux6~0_combout\ : std_logic;
SIGNAL \hexseg0|ALT_INV_Mux6~0_combout\ : std_logic;

BEGIN

ww_CLOCK_24 <= CLOCK_24;
ww_CLOCK_27 <= CLOCK_27;
ww_CLOCK_50 <= CLOCK_50;
ww_KEY <= KEY;
ww_SW <= SW;
HEX0 <= ww_HEX0;
HEX1 <= ww_HEX1;
HEX2 <= ww_HEX2;
HEX3 <= ww_HEX3;
LEDG <= ww_LEDG;
LEDR <= ww_LEDR;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\CLOCK_24[0]~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \CLOCK_24[0]~input_o\);

\kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \kbd_ctrl|ps2_ctrl|sigtrigger~q\);

\CLOCKHZ~clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \CLOCKHZ~q\);

\kbd_ctrl|sigsend~clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \kbd_ctrl|sigsend~q\);

\KEY[0]~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \KEY[0]~input_o\);
\kbd_ctrl|ps2_ctrl|ALT_INV_send_rdy~0_combout\ <= NOT \kbd_ctrl|ps2_ctrl|send_rdy~0_combout\;
\kbd_ctrl|ps2_ctrl|ALT_INV_sigsendend~0_combout\ <= NOT \kbd_ctrl|ps2_ctrl|sigsendend~0_combout\;
\kbd_ctrl|ALT_INV_process_13~0_combout\ <= NOT \kbd_ctrl|process_13~0_combout\;
\kbd_ctrl|ps2_ctrl|ALT_INV_process_2~0_combout\ <= NOT \kbd_ctrl|ps2_ctrl|process_2~0_combout\;
\kbd_ctrl|ps2_ctrl|ALT_INV_ps2_data~0_combout\ <= NOT \kbd_ctrl|ps2_ctrl|ps2_data~0_combout\;
\kbd_ctrl|ALT_INV_process_15~0_combout\ <= NOT \kbd_ctrl|process_15~0_combout\;
\kbd_ctrl|ps2_ctrl|ALT_INV_FROMPS2~1_combout\ <= NOT \kbd_ctrl|ps2_ctrl|FROMPS2~1_combout\;
\kbd_ctrl|ps2_ctrl|ALT_INV_ps2_clk~en_q\ <= NOT \kbd_ctrl|ps2_ctrl|ps2_clk~en_q\;
\kbd_ctrl|ps2_ctrl|ALT_INV_sigclkheld~q\ <= NOT \kbd_ctrl|ps2_ctrl|sigclkheld~q\;
\kbd_ctrl|ALT_INV_process_0~0_combout\ <= NOT \kbd_ctrl|process_0~0_combout\;
\kbd_ctrl|ALT_INV_KEY2~8_combout\ <= NOT \kbd_ctrl|KEY2~8_combout\;
\kbd_ctrl|ALT_INV_KEY1~8_combout\ <= NOT \kbd_ctrl|KEY1~8_combout\;
\kbd_ctrl|ALT_INV_KEY0~2_combout\ <= NOT \kbd_ctrl|KEY0~2_combout\;
\kbd_ctrl|ALT_INV_key0code\(13) <= NOT \kbd_ctrl|key0code\(13);
\hexseg1|ALT_INV_Mux6~0_combout\ <= NOT \hexseg1|Mux6~0_combout\;
\hexseg0|ALT_INV_Mux6~0_combout\ <= NOT \hexseg0|Mux6~0_combout\;

-- Location: FF_X33_Y24_N7
\kbd_ctrl|ps2_ctrl|count[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|count[1]~17_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_process_2~0_combout\,
	ena => \kbd_ctrl|ps2_ctrl|count[11]~19_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|count\(1));

-- Location: FF_X33_Y24_N19
\kbd_ctrl|ps2_ctrl|count[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|count[7]~30_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_process_2~0_combout\,
	ena => \kbd_ctrl|ps2_ctrl|count[11]~19_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|count\(7));

-- Location: LCCOMB_X33_Y24_N6
\kbd_ctrl|ps2_ctrl|count[1]~17\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|count[1]~17_combout\ = (\kbd_ctrl|ps2_ctrl|count\(1) & (!\kbd_ctrl|ps2_ctrl|count[0]~16\)) # (!\kbd_ctrl|ps2_ctrl|count\(1) & ((\kbd_ctrl|ps2_ctrl|count[0]~16\) # (GND)))
-- \kbd_ctrl|ps2_ctrl|count[1]~18\ = CARRY((!\kbd_ctrl|ps2_ctrl|count[0]~16\) # (!\kbd_ctrl|ps2_ctrl|count\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|count\(1),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|count[0]~16\,
	combout => \kbd_ctrl|ps2_ctrl|count[1]~17_combout\,
	cout => \kbd_ctrl|ps2_ctrl|count[1]~18\);

-- Location: LCCOMB_X33_Y24_N18
\kbd_ctrl|ps2_ctrl|count[7]~30\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|count[7]~30_combout\ = (\kbd_ctrl|ps2_ctrl|count\(7) & (!\kbd_ctrl|ps2_ctrl|count[6]~29\)) # (!\kbd_ctrl|ps2_ctrl|count\(7) & ((\kbd_ctrl|ps2_ctrl|count[6]~29\) # (GND)))
-- \kbd_ctrl|ps2_ctrl|count[7]~31\ = CARRY((!\kbd_ctrl|ps2_ctrl|count[6]~29\) # (!\kbd_ctrl|ps2_ctrl|count\(7)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|count\(7),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|count[6]~29\,
	combout => \kbd_ctrl|ps2_ctrl|count[7]~30_combout\,
	cout => \kbd_ctrl|ps2_ctrl|count[7]~31\);

-- Location: LCCOMB_X40_Y18_N0
\kbd_ctrl|ps2_ctrl|Add0~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add0~0_combout\ = \kbd_ctrl|ps2_ctrl|fcount\(0) $ (VCC)
-- \kbd_ctrl|ps2_ctrl|Add0~1\ = CARRY(\kbd_ctrl|ps2_ctrl|fcount\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|fcount\(0),
	datad => VCC,
	combout => \kbd_ctrl|ps2_ctrl|Add0~0_combout\,
	cout => \kbd_ctrl|ps2_ctrl|Add0~1\);

-- Location: LCCOMB_X40_Y18_N2
\kbd_ctrl|ps2_ctrl|Add0~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add0~2_combout\ = (\kbd_ctrl|ps2_ctrl|fcount\(1) & (!\kbd_ctrl|ps2_ctrl|Add0~1\)) # (!\kbd_ctrl|ps2_ctrl|fcount\(1) & ((\kbd_ctrl|ps2_ctrl|Add0~1\) # (GND)))
-- \kbd_ctrl|ps2_ctrl|Add0~3\ = CARRY((!\kbd_ctrl|ps2_ctrl|Add0~1\) # (!\kbd_ctrl|ps2_ctrl|fcount\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|fcount\(1),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|Add0~1\,
	combout => \kbd_ctrl|ps2_ctrl|Add0~2_combout\,
	cout => \kbd_ctrl|ps2_ctrl|Add0~3\);

-- Location: LCCOMB_X40_Y18_N4
\kbd_ctrl|ps2_ctrl|Add0~4\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add0~4_combout\ = (\kbd_ctrl|ps2_ctrl|fcount\(2) & (\kbd_ctrl|ps2_ctrl|Add0~3\ $ (GND))) # (!\kbd_ctrl|ps2_ctrl|fcount\(2) & (!\kbd_ctrl|ps2_ctrl|Add0~3\ & VCC))
-- \kbd_ctrl|ps2_ctrl|Add0~5\ = CARRY((\kbd_ctrl|ps2_ctrl|fcount\(2) & !\kbd_ctrl|ps2_ctrl|Add0~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|fcount\(2),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|Add0~3\,
	combout => \kbd_ctrl|ps2_ctrl|Add0~4_combout\,
	cout => \kbd_ctrl|ps2_ctrl|Add0~5\);

-- Location: LCCOMB_X40_Y18_N6
\kbd_ctrl|ps2_ctrl|Add0~6\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add0~6_combout\ = (\kbd_ctrl|ps2_ctrl|fcount\(3) & (!\kbd_ctrl|ps2_ctrl|Add0~5\)) # (!\kbd_ctrl|ps2_ctrl|fcount\(3) & ((\kbd_ctrl|ps2_ctrl|Add0~5\) # (GND)))
-- \kbd_ctrl|ps2_ctrl|Add0~7\ = CARRY((!\kbd_ctrl|ps2_ctrl|Add0~5\) # (!\kbd_ctrl|ps2_ctrl|fcount\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|fcount\(3),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|Add0~5\,
	combout => \kbd_ctrl|ps2_ctrl|Add0~6_combout\,
	cout => \kbd_ctrl|ps2_ctrl|Add0~7\);

-- Location: LCCOMB_X40_Y18_N8
\kbd_ctrl|ps2_ctrl|Add0~8\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add0~8_combout\ = (\kbd_ctrl|ps2_ctrl|fcount\(4) & (\kbd_ctrl|ps2_ctrl|Add0~7\ $ (GND))) # (!\kbd_ctrl|ps2_ctrl|fcount\(4) & (!\kbd_ctrl|ps2_ctrl|Add0~7\ & VCC))
-- \kbd_ctrl|ps2_ctrl|Add0~9\ = CARRY((\kbd_ctrl|ps2_ctrl|fcount\(4) & !\kbd_ctrl|ps2_ctrl|Add0~7\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|fcount\(4),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|Add0~7\,
	combout => \kbd_ctrl|ps2_ctrl|Add0~8_combout\,
	cout => \kbd_ctrl|ps2_ctrl|Add0~9\);

-- Location: LCCOMB_X40_Y18_N10
\kbd_ctrl|ps2_ctrl|Add0~10\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add0~10_combout\ = (\kbd_ctrl|ps2_ctrl|fcount\(5) & (!\kbd_ctrl|ps2_ctrl|Add0~9\)) # (!\kbd_ctrl|ps2_ctrl|fcount\(5) & ((\kbd_ctrl|ps2_ctrl|Add0~9\) # (GND)))
-- \kbd_ctrl|ps2_ctrl|Add0~11\ = CARRY((!\kbd_ctrl|ps2_ctrl|Add0~9\) # (!\kbd_ctrl|ps2_ctrl|fcount\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|fcount\(5),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|Add0~9\,
	combout => \kbd_ctrl|ps2_ctrl|Add0~10_combout\,
	cout => \kbd_ctrl|ps2_ctrl|Add0~11\);

-- Location: LCCOMB_X40_Y18_N12
\kbd_ctrl|ps2_ctrl|Add0~12\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add0~12_combout\ = (\kbd_ctrl|ps2_ctrl|fcount\(6) & (\kbd_ctrl|ps2_ctrl|Add0~11\ $ (GND))) # (!\kbd_ctrl|ps2_ctrl|fcount\(6) & (!\kbd_ctrl|ps2_ctrl|Add0~11\ & VCC))
-- \kbd_ctrl|ps2_ctrl|Add0~13\ = CARRY((\kbd_ctrl|ps2_ctrl|fcount\(6) & !\kbd_ctrl|ps2_ctrl|Add0~11\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|fcount\(6),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|Add0~11\,
	combout => \kbd_ctrl|ps2_ctrl|Add0~12_combout\,
	cout => \kbd_ctrl|ps2_ctrl|Add0~13\);

-- Location: LCCOMB_X40_Y18_N14
\kbd_ctrl|ps2_ctrl|Add0~14\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add0~14_combout\ = \kbd_ctrl|ps2_ctrl|Add0~13\ $ (\kbd_ctrl|ps2_ctrl|fcount\(7))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \kbd_ctrl|ps2_ctrl|fcount\(7),
	cin => \kbd_ctrl|ps2_ctrl|Add0~13\,
	combout => \kbd_ctrl|ps2_ctrl|Add0~14_combout\);

-- Location: LCCOMB_X38_Y18_N4
\kbd_ctrl|ps2_ctrl|Add1~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add1~0_combout\ = \kbd_ctrl|ps2_ctrl|rcount\(0) $ (VCC)
-- \kbd_ctrl|ps2_ctrl|Add1~1\ = CARRY(\kbd_ctrl|ps2_ctrl|rcount\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|rcount\(0),
	datad => VCC,
	combout => \kbd_ctrl|ps2_ctrl|Add1~0_combout\,
	cout => \kbd_ctrl|ps2_ctrl|Add1~1\);

-- Location: LCCOMB_X38_Y18_N6
\kbd_ctrl|ps2_ctrl|Add1~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add1~2_combout\ = (\kbd_ctrl|ps2_ctrl|rcount\(1) & (!\kbd_ctrl|ps2_ctrl|Add1~1\)) # (!\kbd_ctrl|ps2_ctrl|rcount\(1) & ((\kbd_ctrl|ps2_ctrl|Add1~1\) # (GND)))
-- \kbd_ctrl|ps2_ctrl|Add1~3\ = CARRY((!\kbd_ctrl|ps2_ctrl|Add1~1\) # (!\kbd_ctrl|ps2_ctrl|rcount\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|rcount\(1),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|Add1~1\,
	combout => \kbd_ctrl|ps2_ctrl|Add1~2_combout\,
	cout => \kbd_ctrl|ps2_ctrl|Add1~3\);

-- Location: LCCOMB_X38_Y18_N8
\kbd_ctrl|ps2_ctrl|Add1~4\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add1~4_combout\ = (\kbd_ctrl|ps2_ctrl|rcount\(2) & (\kbd_ctrl|ps2_ctrl|Add1~3\ $ (GND))) # (!\kbd_ctrl|ps2_ctrl|rcount\(2) & (!\kbd_ctrl|ps2_ctrl|Add1~3\ & VCC))
-- \kbd_ctrl|ps2_ctrl|Add1~5\ = CARRY((\kbd_ctrl|ps2_ctrl|rcount\(2) & !\kbd_ctrl|ps2_ctrl|Add1~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|rcount\(2),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|Add1~3\,
	combout => \kbd_ctrl|ps2_ctrl|Add1~4_combout\,
	cout => \kbd_ctrl|ps2_ctrl|Add1~5\);

-- Location: LCCOMB_X38_Y18_N10
\kbd_ctrl|ps2_ctrl|Add1~6\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add1~6_combout\ = (\kbd_ctrl|ps2_ctrl|rcount\(3) & (!\kbd_ctrl|ps2_ctrl|Add1~5\)) # (!\kbd_ctrl|ps2_ctrl|rcount\(3) & ((\kbd_ctrl|ps2_ctrl|Add1~5\) # (GND)))
-- \kbd_ctrl|ps2_ctrl|Add1~7\ = CARRY((!\kbd_ctrl|ps2_ctrl|Add1~5\) # (!\kbd_ctrl|ps2_ctrl|rcount\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|rcount\(3),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|Add1~5\,
	combout => \kbd_ctrl|ps2_ctrl|Add1~6_combout\,
	cout => \kbd_ctrl|ps2_ctrl|Add1~7\);

-- Location: LCCOMB_X38_Y18_N12
\kbd_ctrl|ps2_ctrl|Add1~8\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add1~8_combout\ = (\kbd_ctrl|ps2_ctrl|rcount\(4) & (\kbd_ctrl|ps2_ctrl|Add1~7\ $ (GND))) # (!\kbd_ctrl|ps2_ctrl|rcount\(4) & (!\kbd_ctrl|ps2_ctrl|Add1~7\ & VCC))
-- \kbd_ctrl|ps2_ctrl|Add1~9\ = CARRY((\kbd_ctrl|ps2_ctrl|rcount\(4) & !\kbd_ctrl|ps2_ctrl|Add1~7\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|rcount\(4),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|Add1~7\,
	combout => \kbd_ctrl|ps2_ctrl|Add1~8_combout\,
	cout => \kbd_ctrl|ps2_ctrl|Add1~9\);

-- Location: LCCOMB_X38_Y18_N14
\kbd_ctrl|ps2_ctrl|Add1~10\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add1~10_combout\ = (\kbd_ctrl|ps2_ctrl|rcount\(5) & (!\kbd_ctrl|ps2_ctrl|Add1~9\)) # (!\kbd_ctrl|ps2_ctrl|rcount\(5) & ((\kbd_ctrl|ps2_ctrl|Add1~9\) # (GND)))
-- \kbd_ctrl|ps2_ctrl|Add1~11\ = CARRY((!\kbd_ctrl|ps2_ctrl|Add1~9\) # (!\kbd_ctrl|ps2_ctrl|rcount\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|rcount\(5),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|Add1~9\,
	combout => \kbd_ctrl|ps2_ctrl|Add1~10_combout\,
	cout => \kbd_ctrl|ps2_ctrl|Add1~11\);

-- Location: LCCOMB_X38_Y18_N16
\kbd_ctrl|ps2_ctrl|Add1~12\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add1~12_combout\ = (\kbd_ctrl|ps2_ctrl|rcount\(6) & (\kbd_ctrl|ps2_ctrl|Add1~11\ $ (GND))) # (!\kbd_ctrl|ps2_ctrl|rcount\(6) & (!\kbd_ctrl|ps2_ctrl|Add1~11\ & VCC))
-- \kbd_ctrl|ps2_ctrl|Add1~13\ = CARRY((\kbd_ctrl|ps2_ctrl|rcount\(6) & !\kbd_ctrl|ps2_ctrl|Add1~11\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|rcount\(6),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|Add1~11\,
	combout => \kbd_ctrl|ps2_ctrl|Add1~12_combout\,
	cout => \kbd_ctrl|ps2_ctrl|Add1~13\);

-- Location: LCCOMB_X38_Y18_N18
\kbd_ctrl|ps2_ctrl|Add1~14\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add1~14_combout\ = \kbd_ctrl|ps2_ctrl|Add1~13\ $ (\kbd_ctrl|ps2_ctrl|rcount\(7))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \kbd_ctrl|ps2_ctrl|rcount\(7),
	cin => \kbd_ctrl|ps2_ctrl|Add1~13\,
	combout => \kbd_ctrl|ps2_ctrl|Add1~14_combout\);

-- Location: FF_X38_Y15_N11
\count[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \count[17]~54_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => count(17));

-- Location: FF_X38_Y15_N9
\count[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \count[16]~52_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => count(16));

-- Location: FF_X38_Y15_N7
\count[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \count[15]~50_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => count(15));

-- Location: FF_X38_Y15_N13
\count[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \count[18]~56_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => count(18));

-- Location: FF_X38_Y15_N5
\count[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \count[14]~48_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => count(14));

-- Location: FF_X38_Y15_N3
\count[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \count[13]~46_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => count(13));

-- Location: FF_X39_Y15_N25
\count[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \count[8]~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => count(8));

-- Location: FF_X38_Y16_N27
\count[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \count[9]~38_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => count(9));

-- Location: FF_X38_Y16_N29
\count[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \count[10]~40_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => count(10));

-- Location: FF_X38_Y16_N31
\count[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \count[11]~42_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => count(11));

-- Location: FF_X38_Y15_N1
\count[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \count[12]~44_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => count(12));

-- Location: FF_X38_Y15_N21
\count[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \count[22]~64_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => count(22));

-- Location: FF_X38_Y15_N15
\count[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \count[19]~58_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => count(19));

-- Location: FF_X38_Y15_N17
\count[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \count[20]~60_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => count(20));

-- Location: FF_X38_Y15_N19
\count[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \count[21]~62_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => count(21));

-- Location: FF_X31_Y25_N31
\kbd_ctrl|ps2_ctrl|countclk[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|countclk[9]~34_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_FROMPS2~1_combout\,
	ena => \kbd_ctrl|ps2_ctrl|countclk[18]~54_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|countclk\(9));

-- Location: FF_X31_Y24_N7
\kbd_ctrl|ps2_ctrl|countclk[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|countclk[13]~42_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_FROMPS2~1_combout\,
	ena => \kbd_ctrl|ps2_ctrl|countclk[18]~54_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|countclk\(13));

-- Location: FF_X31_Y24_N11
\kbd_ctrl|ps2_ctrl|countclk[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|countclk[15]~46_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_FROMPS2~1_combout\,
	ena => \kbd_ctrl|ps2_ctrl|countclk[18]~54_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|countclk\(15));

-- Location: FF_X31_Y24_N13
\kbd_ctrl|ps2_ctrl|countclk[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|countclk[16]~48_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_FROMPS2~1_combout\,
	ena => \kbd_ctrl|ps2_ctrl|countclk[18]~54_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|countclk\(16));

-- Location: FF_X38_Y16_N23
\count[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \count[7]~34_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => count(7));

-- Location: FF_X38_Y16_N11
\count[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \count[1]~22_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => count(1));

-- Location: FF_X38_Y16_N13
\count[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \count[2]~24_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => count(2));

-- Location: FF_X38_Y16_N15
\count[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \count[3]~26_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => count(3));

-- Location: FF_X38_Y16_N17
\count[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \count[4]~28_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => count(4));

-- Location: FF_X38_Y16_N19
\count[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \count[5]~30_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => count(5));

-- Location: FF_X38_Y16_N21
\count[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \count[6]~32_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => count(6));

-- Location: LCCOMB_X38_Y16_N10
\count[1]~22\ : cycloneiii_lcell_comb
-- Equation(s):
-- \count[1]~22_combout\ = (count(1) & (count(0) $ (VCC))) # (!count(1) & (count(0) & VCC))
-- \count[1]~23\ = CARRY((count(1) & count(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110011010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => count(1),
	datab => count(0),
	datad => VCC,
	combout => \count[1]~22_combout\,
	cout => \count[1]~23\);

-- Location: LCCOMB_X38_Y16_N12
\count[2]~24\ : cycloneiii_lcell_comb
-- Equation(s):
-- \count[2]~24_combout\ = (count(2) & (!\count[1]~23\)) # (!count(2) & ((\count[1]~23\) # (GND)))
-- \count[2]~25\ = CARRY((!\count[1]~23\) # (!count(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => count(2),
	datad => VCC,
	cin => \count[1]~23\,
	combout => \count[2]~24_combout\,
	cout => \count[2]~25\);

-- Location: LCCOMB_X38_Y16_N14
\count[3]~26\ : cycloneiii_lcell_comb
-- Equation(s):
-- \count[3]~26_combout\ = (count(3) & (\count[2]~25\ $ (GND))) # (!count(3) & (!\count[2]~25\ & VCC))
-- \count[3]~27\ = CARRY((count(3) & !\count[2]~25\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => count(3),
	datad => VCC,
	cin => \count[2]~25\,
	combout => \count[3]~26_combout\,
	cout => \count[3]~27\);

-- Location: LCCOMB_X38_Y16_N16
\count[4]~28\ : cycloneiii_lcell_comb
-- Equation(s):
-- \count[4]~28_combout\ = (count(4) & (!\count[3]~27\)) # (!count(4) & ((\count[3]~27\) # (GND)))
-- \count[4]~29\ = CARRY((!\count[3]~27\) # (!count(4)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => count(4),
	datad => VCC,
	cin => \count[3]~27\,
	combout => \count[4]~28_combout\,
	cout => \count[4]~29\);

-- Location: LCCOMB_X38_Y16_N18
\count[5]~30\ : cycloneiii_lcell_comb
-- Equation(s):
-- \count[5]~30_combout\ = (count(5) & (\count[4]~29\ $ (GND))) # (!count(5) & (!\count[4]~29\ & VCC))
-- \count[5]~31\ = CARRY((count(5) & !\count[4]~29\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => count(5),
	datad => VCC,
	cin => \count[4]~29\,
	combout => \count[5]~30_combout\,
	cout => \count[5]~31\);

-- Location: LCCOMB_X38_Y16_N20
\count[6]~32\ : cycloneiii_lcell_comb
-- Equation(s):
-- \count[6]~32_combout\ = (count(6) & (!\count[5]~31\)) # (!count(6) & ((\count[5]~31\) # (GND)))
-- \count[6]~33\ = CARRY((!\count[5]~31\) # (!count(6)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => count(6),
	datad => VCC,
	cin => \count[5]~31\,
	combout => \count[6]~32_combout\,
	cout => \count[6]~33\);

-- Location: LCCOMB_X38_Y16_N22
\count[7]~34\ : cycloneiii_lcell_comb
-- Equation(s):
-- \count[7]~34_combout\ = (count(7) & (\count[6]~33\ $ (GND))) # (!count(7) & (!\count[6]~33\ & VCC))
-- \count[7]~35\ = CARRY((count(7) & !\count[6]~33\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => count(7),
	datad => VCC,
	cin => \count[6]~33\,
	combout => \count[7]~34_combout\,
	cout => \count[7]~35\);

-- Location: LCCOMB_X38_Y16_N24
\count[8]~36\ : cycloneiii_lcell_comb
-- Equation(s):
-- \count[8]~36_combout\ = (count(8) & (!\count[7]~35\)) # (!count(8) & ((\count[7]~35\) # (GND)))
-- \count[8]~37\ = CARRY((!\count[7]~35\) # (!count(8)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => count(8),
	datad => VCC,
	cin => \count[7]~35\,
	combout => \count[8]~36_combout\,
	cout => \count[8]~37\);

-- Location: LCCOMB_X38_Y16_N26
\count[9]~38\ : cycloneiii_lcell_comb
-- Equation(s):
-- \count[9]~38_combout\ = (\count[8]~37\ & (count(9) & (!\Equal1~7_combout\ & VCC))) # (!\count[8]~37\ & ((((count(9) & !\Equal1~7_combout\)))))
-- \count[9]~39\ = CARRY((count(9) & (!\Equal1~7_combout\ & !\count[8]~37\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010110100000010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => count(9),
	datab => \Equal1~7_combout\,
	datad => VCC,
	cin => \count[8]~37\,
	combout => \count[9]~38_combout\,
	cout => \count[9]~39\);

-- Location: LCCOMB_X38_Y16_N28
\count[10]~40\ : cycloneiii_lcell_comb
-- Equation(s):
-- \count[10]~40_combout\ = (\count[9]~39\ & (((\Equal1~7_combout\)) # (!count(10)))) # (!\count[9]~39\ & (((count(10) & !\Equal1~7_combout\)) # (GND)))
-- \count[10]~41\ = CARRY(((\Equal1~7_combout\) # (!\count[9]~39\)) # (!count(10)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101001011011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => count(10),
	datab => \Equal1~7_combout\,
	datad => VCC,
	cin => \count[9]~39\,
	combout => \count[10]~40_combout\,
	cout => \count[10]~41\);

-- Location: LCCOMB_X38_Y16_N30
\count[11]~42\ : cycloneiii_lcell_comb
-- Equation(s):
-- \count[11]~42_combout\ = (\count[10]~41\ & (count(11) & (!\Equal1~7_combout\ & VCC))) # (!\count[10]~41\ & ((((count(11) & !\Equal1~7_combout\)))))
-- \count[11]~43\ = CARRY((count(11) & (!\Equal1~7_combout\ & !\count[10]~41\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010110100000010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => count(11),
	datab => \Equal1~7_combout\,
	datad => VCC,
	cin => \count[10]~41\,
	combout => \count[11]~42_combout\,
	cout => \count[11]~43\);

-- Location: LCCOMB_X38_Y15_N0
\count[12]~44\ : cycloneiii_lcell_comb
-- Equation(s):
-- \count[12]~44_combout\ = (\count[11]~43\ & ((\Equal1~7_combout\) # ((!count(12))))) # (!\count[11]~43\ & (((!\Equal1~7_combout\ & count(12))) # (GND)))
-- \count[12]~45\ = CARRY((\Equal1~7_combout\) # ((!\count[11]~43\) # (!count(12))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011010010111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Equal1~7_combout\,
	datab => count(12),
	datad => VCC,
	cin => \count[11]~43\,
	combout => \count[12]~44_combout\,
	cout => \count[12]~45\);

-- Location: LCCOMB_X38_Y15_N2
\count[13]~46\ : cycloneiii_lcell_comb
-- Equation(s):
-- \count[13]~46_combout\ = (\count[12]~45\ & (count(13) & (!\Equal1~7_combout\ & VCC))) # (!\count[12]~45\ & ((((count(13) & !\Equal1~7_combout\)))))
-- \count[13]~47\ = CARRY((count(13) & (!\Equal1~7_combout\ & !\count[12]~45\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010110100000010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => count(13),
	datab => \Equal1~7_combout\,
	datad => VCC,
	cin => \count[12]~45\,
	combout => \count[13]~46_combout\,
	cout => \count[13]~47\);

-- Location: LCCOMB_X38_Y15_N4
\count[14]~48\ : cycloneiii_lcell_comb
-- Equation(s):
-- \count[14]~48_combout\ = (count(14) & (!\count[13]~47\)) # (!count(14) & ((\count[13]~47\) # (GND)))
-- \count[14]~49\ = CARRY((!\count[13]~47\) # (!count(14)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => count(14),
	datad => VCC,
	cin => \count[13]~47\,
	combout => \count[14]~48_combout\,
	cout => \count[14]~49\);

-- Location: LCCOMB_X38_Y15_N6
\count[15]~50\ : cycloneiii_lcell_comb
-- Equation(s):
-- \count[15]~50_combout\ = (count(15) & (\count[14]~49\ $ (GND))) # (!count(15) & (!\count[14]~49\ & VCC))
-- \count[15]~51\ = CARRY((count(15) & !\count[14]~49\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => count(15),
	datad => VCC,
	cin => \count[14]~49\,
	combout => \count[15]~50_combout\,
	cout => \count[15]~51\);

-- Location: LCCOMB_X38_Y15_N8
\count[16]~52\ : cycloneiii_lcell_comb
-- Equation(s):
-- \count[16]~52_combout\ = (\count[15]~51\ & (((\Equal1~7_combout\)) # (!count(16)))) # (!\count[15]~51\ & (((count(16) & !\Equal1~7_combout\)) # (GND)))
-- \count[16]~53\ = CARRY(((\Equal1~7_combout\) # (!\count[15]~51\)) # (!count(16)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101001011011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => count(16),
	datab => \Equal1~7_combout\,
	datad => VCC,
	cin => \count[15]~51\,
	combout => \count[16]~52_combout\,
	cout => \count[16]~53\);

-- Location: LCCOMB_X38_Y15_N10
\count[17]~54\ : cycloneiii_lcell_comb
-- Equation(s):
-- \count[17]~54_combout\ = (count(17) & (\count[16]~53\ $ (GND))) # (!count(17) & (!\count[16]~53\ & VCC))
-- \count[17]~55\ = CARRY((count(17) & !\count[16]~53\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => count(17),
	datad => VCC,
	cin => \count[16]~53\,
	combout => \count[17]~54_combout\,
	cout => \count[17]~55\);

-- Location: LCCOMB_X38_Y15_N12
\count[18]~56\ : cycloneiii_lcell_comb
-- Equation(s):
-- \count[18]~56_combout\ = (count(18) & (!\count[17]~55\)) # (!count(18) & ((\count[17]~55\) # (GND)))
-- \count[18]~57\ = CARRY((!\count[17]~55\) # (!count(18)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => count(18),
	datad => VCC,
	cin => \count[17]~55\,
	combout => \count[18]~56_combout\,
	cout => \count[18]~57\);

-- Location: LCCOMB_X38_Y15_N14
\count[19]~58\ : cycloneiii_lcell_comb
-- Equation(s):
-- \count[19]~58_combout\ = (\count[18]~57\ & (count(19) & (!\Equal1~7_combout\ & VCC))) # (!\count[18]~57\ & ((((count(19) & !\Equal1~7_combout\)))))
-- \count[19]~59\ = CARRY((count(19) & (!\Equal1~7_combout\ & !\count[18]~57\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010110100000010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => count(19),
	datab => \Equal1~7_combout\,
	datad => VCC,
	cin => \count[18]~57\,
	combout => \count[19]~58_combout\,
	cout => \count[19]~59\);

-- Location: LCCOMB_X38_Y15_N16
\count[20]~60\ : cycloneiii_lcell_comb
-- Equation(s):
-- \count[20]~60_combout\ = (count(20) & (!\count[19]~59\)) # (!count(20) & ((\count[19]~59\) # (GND)))
-- \count[20]~61\ = CARRY((!\count[19]~59\) # (!count(20)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => count(20),
	datad => VCC,
	cin => \count[19]~59\,
	combout => \count[20]~60_combout\,
	cout => \count[20]~61\);

-- Location: LCCOMB_X38_Y15_N18
\count[21]~62\ : cycloneiii_lcell_comb
-- Equation(s):
-- \count[21]~62_combout\ = (count(21) & (\count[20]~61\ $ (GND))) # (!count(21) & (!\count[20]~61\ & VCC))
-- \count[21]~63\ = CARRY((count(21) & !\count[20]~61\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => count(21),
	datad => VCC,
	cin => \count[20]~61\,
	combout => \count[21]~62_combout\,
	cout => \count[21]~63\);

-- Location: LCCOMB_X38_Y15_N20
\count[22]~64\ : cycloneiii_lcell_comb
-- Equation(s):
-- \count[22]~64_combout\ = \count[21]~63\ $ (((count(22) & !\Equal1~7_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000001011010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => count(22),
	datad => \Equal1~7_combout\,
	cin => \count[21]~63\,
	combout => \count[22]~64_combout\);

-- Location: LCCOMB_X31_Y25_N30
\kbd_ctrl|ps2_ctrl|countclk[9]~34\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|countclk[9]~34_combout\ = (\kbd_ctrl|ps2_ctrl|countclk\(9) & (\kbd_ctrl|ps2_ctrl|countclk[8]~33\ $ (GND))) # (!\kbd_ctrl|ps2_ctrl|countclk\(9) & (!\kbd_ctrl|ps2_ctrl|countclk[8]~33\ & VCC))
-- \kbd_ctrl|ps2_ctrl|countclk[9]~35\ = CARRY((\kbd_ctrl|ps2_ctrl|countclk\(9) & !\kbd_ctrl|ps2_ctrl|countclk[8]~33\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|countclk\(9),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|countclk[8]~33\,
	combout => \kbd_ctrl|ps2_ctrl|countclk[9]~34_combout\,
	cout => \kbd_ctrl|ps2_ctrl|countclk[9]~35\);

-- Location: LCCOMB_X31_Y24_N6
\kbd_ctrl|ps2_ctrl|countclk[13]~42\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|countclk[13]~42_combout\ = (\kbd_ctrl|ps2_ctrl|countclk\(13) & (\kbd_ctrl|ps2_ctrl|countclk[12]~41\ $ (GND))) # (!\kbd_ctrl|ps2_ctrl|countclk\(13) & (!\kbd_ctrl|ps2_ctrl|countclk[12]~41\ & VCC))
-- \kbd_ctrl|ps2_ctrl|countclk[13]~43\ = CARRY((\kbd_ctrl|ps2_ctrl|countclk\(13) & !\kbd_ctrl|ps2_ctrl|countclk[12]~41\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|countclk\(13),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|countclk[12]~41\,
	combout => \kbd_ctrl|ps2_ctrl|countclk[13]~42_combout\,
	cout => \kbd_ctrl|ps2_ctrl|countclk[13]~43\);

-- Location: LCCOMB_X31_Y24_N10
\kbd_ctrl|ps2_ctrl|countclk[15]~46\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|countclk[15]~46_combout\ = (\kbd_ctrl|ps2_ctrl|countclk\(15) & (\kbd_ctrl|ps2_ctrl|countclk[14]~45\ $ (GND))) # (!\kbd_ctrl|ps2_ctrl|countclk\(15) & (!\kbd_ctrl|ps2_ctrl|countclk[14]~45\ & VCC))
-- \kbd_ctrl|ps2_ctrl|countclk[15]~47\ = CARRY((\kbd_ctrl|ps2_ctrl|countclk\(15) & !\kbd_ctrl|ps2_ctrl|countclk[14]~45\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|countclk\(15),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|countclk[14]~45\,
	combout => \kbd_ctrl|ps2_ctrl|countclk[15]~46_combout\,
	cout => \kbd_ctrl|ps2_ctrl|countclk[15]~47\);

-- Location: LCCOMB_X31_Y24_N12
\kbd_ctrl|ps2_ctrl|countclk[16]~48\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|countclk[16]~48_combout\ = (\kbd_ctrl|ps2_ctrl|countclk\(16) & (!\kbd_ctrl|ps2_ctrl|countclk[15]~47\)) # (!\kbd_ctrl|ps2_ctrl|countclk\(16) & ((\kbd_ctrl|ps2_ctrl|countclk[15]~47\) # (GND)))
-- \kbd_ctrl|ps2_ctrl|countclk[16]~49\ = CARRY((!\kbd_ctrl|ps2_ctrl|countclk[15]~47\) # (!\kbd_ctrl|ps2_ctrl|countclk\(16)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|countclk\(16),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|countclk[15]~47\,
	combout => \kbd_ctrl|ps2_ctrl|countclk[16]~48_combout\,
	cout => \kbd_ctrl|ps2_ctrl|countclk[16]~49\);

-- Location: LCCOMB_X28_Y26_N30
\kbd_ctrl|Equal7~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal7~0_combout\ = ((!\kbd_ctrl|key0code\(13)) # (!\kbd_ctrl|key0code\(4))) # (!\kbd_ctrl|key0code\(1))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(1),
	datab => \kbd_ctrl|key0code\(4),
	datad => \kbd_ctrl|key0code\(13),
	combout => \kbd_ctrl|Equal7~0_combout\);

-- Location: LCCOMB_X29_Y26_N26
\kbd_ctrl|Equal14~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal14~1_combout\ = (\kbd_ctrl|fetchdata\(2) & (\kbd_ctrl|key0code\(2) & (\kbd_ctrl|key0code\(3) $ (!\kbd_ctrl|fetchdata\(3))))) # (!\kbd_ctrl|fetchdata\(2) & (!\kbd_ctrl|key0code\(2) & (\kbd_ctrl|key0code\(3) $ (!\kbd_ctrl|fetchdata\(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000001001000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|fetchdata\(2),
	datab => \kbd_ctrl|key0code\(3),
	datac => \kbd_ctrl|fetchdata\(3),
	datad => \kbd_ctrl|key0code\(2),
	combout => \kbd_ctrl|Equal14~1_combout\);

-- Location: LCCOMB_X30_Y26_N6
\kbd_ctrl|Equal4~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal4~0_combout\ = \kbd_ctrl|key1code\(13) $ (\kbd_ctrl|selE0~combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key1code\(13),
	datad => \kbd_ctrl|selE0~combout\,
	combout => \kbd_ctrl|Equal4~0_combout\);

-- Location: LCCOMB_X31_Y26_N2
\kbd_ctrl|Equal5~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal5~1_combout\ = (\kbd_ctrl|key2code\(1) & (\kbd_ctrl|fetchdata\(1) & (\kbd_ctrl|key2code\(0) $ (!\kbd_ctrl|fetchdata\(0))))) # (!\kbd_ctrl|key2code\(1) & (!\kbd_ctrl|fetchdata\(1) & (\kbd_ctrl|key2code\(0) $ (!\kbd_ctrl|fetchdata\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000010000100001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key2code\(1),
	datab => \kbd_ctrl|key2code\(0),
	datac => \kbd_ctrl|fetchdata\(1),
	datad => \kbd_ctrl|fetchdata\(0),
	combout => \kbd_ctrl|Equal5~1_combout\);

-- Location: FF_X32_Y26_N27
\kbd_ctrl|key2code[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	asdata => \kbd_ctrl|fetchdata\(7),
	clrn => \kbd_ctrl|ALT_INV_KEY2~8_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key2en~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|key2code\(7));

-- Location: LCCOMB_X31_Y26_N0
\kbd_ctrl|Equal19~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal19~1_combout\ = (!\kbd_ctrl|key2code\(1) & (!\kbd_ctrl|key2code\(3) & (!\kbd_ctrl|key2code\(0) & !\kbd_ctrl|key2code\(6))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key2code\(1),
	datab => \kbd_ctrl|key2code\(3),
	datac => \kbd_ctrl|key2code\(0),
	datad => \kbd_ctrl|key2code\(6),
	combout => \kbd_ctrl|Equal19~1_combout\);

-- Location: FF_X32_Y26_N19
\kbd_ctrl|state.RELEASE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|nstate.RELEASE~0_combout\,
	clrn => \kbd_ctrl|ALT_INV_process_0~0_combout\,
	ena => \KEY[1]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|state.RELEASE~q\);

-- Location: LCCOMB_X30_Y26_N12
\kbd_ctrl|Equal12~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal12~1_combout\ = (\kbd_ctrl|key1code\(1)) # (((!\kbd_ctrl|key1code\(3)) # (!\kbd_ctrl|key1code\(4))) # (!\kbd_ctrl|key1code\(13)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key1code\(1),
	datab => \kbd_ctrl|key1code\(13),
	datac => \kbd_ctrl|key1code\(4),
	datad => \kbd_ctrl|key1code\(3),
	combout => \kbd_ctrl|Equal12~1_combout\);

-- Location: LCCOMB_X30_Y26_N14
\kbd_ctrl|Equal8~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal8~0_combout\ = ((!\kbd_ctrl|key1code\(1)) # (!\kbd_ctrl|key1code\(13))) # (!\kbd_ctrl|key1code\(4))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key1code\(4),
	datab => \kbd_ctrl|key1code\(13),
	datad => \kbd_ctrl|key1code\(1),
	combout => \kbd_ctrl|Equal8~0_combout\);

-- Location: LCCOMB_X32_Y26_N10
\kbd_ctrl|KEY2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|KEY2~0_combout\ = (\kbd_ctrl|key2code\(13) & (\kbd_ctrl|KEY0~0_combout\ & (\kbd_ctrl|key2code\(4) & \kbd_ctrl|Equal19~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key2code\(13),
	datab => \kbd_ctrl|KEY0~0_combout\,
	datac => \kbd_ctrl|key2code\(4),
	datad => \kbd_ctrl|Equal19~0_combout\,
	combout => \kbd_ctrl|KEY2~0_combout\);

-- Location: FF_X33_Y27_N11
\kbd_ctrl|ps2_ctrl|FROMPS2:count[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|count~14_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_FROMPS2~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|FROMPS2:count[3]~q\);

-- Location: LCCOMB_X32_Y26_N18
\kbd_ctrl|nstate.RELEASE~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|nstate.RELEASE~0_combout\ = (\kbd_ctrl|fetchdata\(4) & (\kbd_ctrl|state.DECODE~q\ & \kbd_ctrl|Equal0~2_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|fetchdata\(4),
	datac => \kbd_ctrl|state.DECODE~q\,
	datad => \kbd_ctrl|Equal0~2_combout\,
	combout => \kbd_ctrl|nstate.RELEASE~0_combout\);

-- Location: LCCOMB_X35_Y24_N22
\kbd_ctrl|ps2_ctrl|Equal3~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Equal3~0_combout\ = (!\kbd_ctrl|ps2_ctrl|TOPS2:count[0]~q\ & (!\kbd_ctrl|ps2_ctrl|TOPS2:count[2]~q\ & (\kbd_ctrl|ps2_ctrl|TOPS2:count[3]~q\ & !\kbd_ctrl|ps2_ctrl|TOPS2:count[1]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|TOPS2:count[0]~q\,
	datab => \kbd_ctrl|ps2_ctrl|TOPS2:count[2]~q\,
	datac => \kbd_ctrl|ps2_ctrl|TOPS2:count[3]~q\,
	datad => \kbd_ctrl|ps2_ctrl|TOPS2:count[1]~q\,
	combout => \kbd_ctrl|ps2_ctrl|Equal3~0_combout\);

-- Location: LCCOMB_X32_Y24_N26
\kbd_ctrl|ps2_ctrl|LessThan4~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|LessThan4~0_combout\ = ((!\kbd_ctrl|ps2_ctrl|count\(2) & (!\kbd_ctrl|ps2_ctrl|count\(3) & !\kbd_ctrl|ps2_ctrl|count\(1)))) # (!\kbd_ctrl|ps2_ctrl|count\(4))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100011111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|count\(2),
	datab => \kbd_ctrl|ps2_ctrl|count\(3),
	datac => \kbd_ctrl|ps2_ctrl|count\(4),
	datad => \kbd_ctrl|ps2_ctrl|count\(1),
	combout => \kbd_ctrl|ps2_ctrl|LessThan4~0_combout\);

-- Location: LCCOMB_X33_Y24_N30
\kbd_ctrl|ps2_ctrl|ps2_clk~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|ps2_clk~1_combout\ = (\kbd_ctrl|ps2_ctrl|count\(8) & \kbd_ctrl|ps2_ctrl|count\(7))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|count\(8),
	datad => \kbd_ctrl|ps2_ctrl|count\(7),
	combout => \kbd_ctrl|ps2_ctrl|ps2_clk~1_combout\);

-- Location: LCCOMB_X32_Y24_N0
\kbd_ctrl|ps2_ctrl|LessThan4~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|LessThan4~1_combout\ = ((!\kbd_ctrl|ps2_ctrl|count\(5) & (\kbd_ctrl|ps2_ctrl|LessThan4~0_combout\ & !\kbd_ctrl|ps2_ctrl|count\(6)))) # (!\kbd_ctrl|ps2_ctrl|ps2_clk~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010101110101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|ps2_clk~1_combout\,
	datab => \kbd_ctrl|ps2_ctrl|count\(5),
	datac => \kbd_ctrl|ps2_ctrl|LessThan4~0_combout\,
	datad => \kbd_ctrl|ps2_ctrl|count\(6),
	combout => \kbd_ctrl|ps2_ctrl|LessThan4~1_combout\);

-- Location: FF_X33_Y26_N17
\kbd_ctrl|sigsend\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|cmdstate~18_combout\,
	clrn => \kbd_ctrl|ALT_INV_process_15~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|sigsend~q\);

-- Location: LCCOMB_X33_Y27_N10
\kbd_ctrl|ps2_ctrl|count~14\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|count~14_combout\ = (\kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~q\ & (\kbd_ctrl|ps2_ctrl|FROMPS2:count[3]~q\ $ (((\kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~q\ & \kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~q\))))) # (!\kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~q\ & 
-- (\kbd_ctrl|ps2_ctrl|FROMPS2:count[3]~q\ & ((\kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~q\) # (!\kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100010110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~q\,
	datab => \kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~q\,
	datac => \kbd_ctrl|ps2_ctrl|FROMPS2:count[3]~q\,
	datad => \kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~q\,
	combout => \kbd_ctrl|ps2_ctrl|count~14_combout\);

-- Location: FF_X40_Y18_N29
\kbd_ctrl|ps2_ctrl|fcount[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|Add0~16_combout\,
	clrn => \KEY[0]~inputclkctrl_outclk\,
	ena => \kbd_ctrl|ps2_ctrl|fcount[7]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|fcount\(7));

-- Location: FF_X40_Y18_N27
\kbd_ctrl|ps2_ctrl|fcount[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|Add0~17_combout\,
	clrn => \KEY[0]~inputclkctrl_outclk\,
	ena => \kbd_ctrl|ps2_ctrl|fcount[7]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|fcount\(6));

-- Location: FF_X40_Y18_N21
\kbd_ctrl|ps2_ctrl|fcount[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|Add0~18_combout\,
	clrn => \KEY[0]~inputclkctrl_outclk\,
	ena => \kbd_ctrl|ps2_ctrl|fcount[7]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|fcount\(5));

-- Location: LCCOMB_X40_Y18_N18
\kbd_ctrl|ps2_ctrl|fcount[7]~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|fcount[7]~0_combout\ = (!\PS2_CLK~input_o\ & (\kbd_ctrl|ps2_ctrl|fcount\(7) & ((\kbd_ctrl|ps2_ctrl|fcount\(5)) # (\kbd_ctrl|ps2_ctrl|fcount\(6)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \PS2_CLK~input_o\,
	datab => \kbd_ctrl|ps2_ctrl|fcount\(5),
	datac => \kbd_ctrl|ps2_ctrl|fcount\(6),
	datad => \kbd_ctrl|ps2_ctrl|fcount\(7),
	combout => \kbd_ctrl|ps2_ctrl|fcount[7]~0_combout\);

-- Location: FF_X38_Y18_N25
\kbd_ctrl|ps2_ctrl|rcount[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|Add1~16_combout\,
	clrn => \KEY[0]~inputclkctrl_outclk\,
	ena => \kbd_ctrl|ps2_ctrl|rcount[7]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|rcount\(7));

-- Location: FF_X38_Y18_N27
\kbd_ctrl|ps2_ctrl|rcount[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|Add1~17_combout\,
	clrn => \KEY[0]~inputclkctrl_outclk\,
	ena => \kbd_ctrl|ps2_ctrl|rcount[7]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|rcount\(6));

-- Location: FF_X38_Y18_N29
\kbd_ctrl|ps2_ctrl|rcount[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|Add1~18_combout\,
	clrn => \KEY[0]~inputclkctrl_outclk\,
	ena => \kbd_ctrl|ps2_ctrl|rcount[7]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|rcount\(5));

-- Location: LCCOMB_X39_Y18_N28
\kbd_ctrl|ps2_ctrl|rcount[7]~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|rcount[7]~0_combout\ = (\PS2_CLK~input_o\ & (\kbd_ctrl|ps2_ctrl|rcount\(7) & ((\kbd_ctrl|ps2_ctrl|rcount\(6)) # (\kbd_ctrl|ps2_ctrl|rcount\(5)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \PS2_CLK~input_o\,
	datab => \kbd_ctrl|ps2_ctrl|rcount\(7),
	datac => \kbd_ctrl|ps2_ctrl|rcount\(6),
	datad => \kbd_ctrl|ps2_ctrl|rcount\(5),
	combout => \kbd_ctrl|ps2_ctrl|rcount[7]~0_combout\);

-- Location: LCCOMB_X39_Y18_N30
\kbd_ctrl|ps2_ctrl|sigtrigger~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|sigtrigger~0_combout\ = (\KEY[1]~input_o\ & ((\kbd_ctrl|ps2_ctrl|rcount[7]~0_combout\) # (\kbd_ctrl|ps2_ctrl|fcount[7]~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \KEY[1]~input_o\,
	datac => \kbd_ctrl|ps2_ctrl|rcount[7]~0_combout\,
	datad => \kbd_ctrl|ps2_ctrl|fcount[7]~0_combout\,
	combout => \kbd_ctrl|ps2_ctrl|sigtrigger~0_combout\);

-- Location: LCCOMB_X39_Y18_N22
\kbd_ctrl|ps2_ctrl|sigtrigger~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|sigtrigger~1_combout\ = (\kbd_ctrl|ps2_ctrl|sigtrigger~0_combout\ & ((!\PS2_CLK~input_o\))) # (!\kbd_ctrl|ps2_ctrl|sigtrigger~0_combout\ & (\kbd_ctrl|ps2_ctrl|sigtrigger~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|sigtrigger~q\,
	datac => \PS2_CLK~input_o\,
	datad => \kbd_ctrl|ps2_ctrl|sigtrigger~0_combout\,
	combout => \kbd_ctrl|ps2_ctrl|sigtrigger~1_combout\);

-- Location: FF_X33_Y26_N7
\kbd_ctrl|cmdstate.SENDVAL\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|Selector13~4_combout\,
	clrn => \kbd_ctrl|ALT_INV_process_15~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|cmdstate.SENDVAL~q\);

-- Location: LCCOMB_X33_Y26_N16
\kbd_ctrl|cmdstate~18\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|cmdstate~18_combout\ = (\kbd_ctrl|cmdstate.SEND~q\) # (\kbd_ctrl|cmdstate.SENDVAL~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|cmdstate.SEND~q\,
	datad => \kbd_ctrl|cmdstate.SENDVAL~q\,
	combout => \kbd_ctrl|cmdstate~18_combout\);

-- Location: FF_X40_Y18_N17
\kbd_ctrl|ps2_ctrl|fcount[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|Add0~19_combout\,
	clrn => \KEY[0]~inputclkctrl_outclk\,
	ena => \kbd_ctrl|ps2_ctrl|fcount[7]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|fcount\(4));

-- Location: FF_X40_Y18_N23
\kbd_ctrl|ps2_ctrl|fcount[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|Add0~20_combout\,
	clrn => \KEY[0]~inputclkctrl_outclk\,
	ena => \kbd_ctrl|ps2_ctrl|fcount[7]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|fcount\(3));

-- Location: FF_X39_Y18_N17
\kbd_ctrl|ps2_ctrl|fcount[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|Add0~21_combout\,
	clrn => \KEY[0]~inputclkctrl_outclk\,
	ena => \kbd_ctrl|ps2_ctrl|fcount[7]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|fcount\(2));

-- Location: FF_X39_Y18_N7
\kbd_ctrl|ps2_ctrl|fcount[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|Add0~22_combout\,
	clrn => \KEY[0]~inputclkctrl_outclk\,
	ena => \kbd_ctrl|ps2_ctrl|fcount[7]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|fcount\(1));

-- Location: FF_X40_Y18_N25
\kbd_ctrl|ps2_ctrl|fcount[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|Add0~23_combout\,
	clrn => \KEY[0]~inputclkctrl_outclk\,
	ena => \kbd_ctrl|ps2_ctrl|fcount[7]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|fcount\(0));

-- Location: LCCOMB_X40_Y18_N28
\kbd_ctrl|ps2_ctrl|Add0~16\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add0~16_combout\ = (!\PS2_CLK~input_o\ & \kbd_ctrl|ps2_ctrl|Add0~14_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \PS2_CLK~input_o\,
	datac => \kbd_ctrl|ps2_ctrl|Add0~14_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Add0~16_combout\);

-- Location: LCCOMB_X40_Y18_N30
\kbd_ctrl|ps2_ctrl|fcount[7]~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|fcount[7]~1_combout\ = (\KEY[1]~input_o\ & !\kbd_ctrl|ps2_ctrl|fcount[7]~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \KEY[1]~input_o\,
	datad => \kbd_ctrl|ps2_ctrl|fcount[7]~0_combout\,
	combout => \kbd_ctrl|ps2_ctrl|fcount[7]~1_combout\);

-- Location: LCCOMB_X40_Y18_N26
\kbd_ctrl|ps2_ctrl|Add0~17\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add0~17_combout\ = (!\PS2_CLK~input_o\ & \kbd_ctrl|ps2_ctrl|Add0~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \PS2_CLK~input_o\,
	datad => \kbd_ctrl|ps2_ctrl|Add0~12_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Add0~17_combout\);

-- Location: LCCOMB_X40_Y18_N20
\kbd_ctrl|ps2_ctrl|Add0~18\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add0~18_combout\ = (!\PS2_CLK~input_o\ & \kbd_ctrl|ps2_ctrl|Add0~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \PS2_CLK~input_o\,
	datad => \kbd_ctrl|ps2_ctrl|Add0~10_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Add0~18_combout\);

-- Location: FF_X38_Y18_N3
\kbd_ctrl|ps2_ctrl|rcount[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|Add1~19_combout\,
	clrn => \KEY[0]~inputclkctrl_outclk\,
	ena => \kbd_ctrl|ps2_ctrl|rcount[7]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|rcount\(4));

-- Location: FF_X38_Y18_N21
\kbd_ctrl|ps2_ctrl|rcount[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|Add1~20_combout\,
	clrn => \KEY[0]~inputclkctrl_outclk\,
	ena => \kbd_ctrl|ps2_ctrl|rcount[7]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|rcount\(3));

-- Location: FF_X38_Y18_N23
\kbd_ctrl|ps2_ctrl|rcount[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|Add1~21_combout\,
	clrn => \KEY[0]~inputclkctrl_outclk\,
	ena => \kbd_ctrl|ps2_ctrl|rcount[7]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|rcount\(2));

-- Location: FF_X38_Y18_N1
\kbd_ctrl|ps2_ctrl|rcount[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|Add1~22_combout\,
	clrn => \KEY[0]~inputclkctrl_outclk\,
	ena => \kbd_ctrl|ps2_ctrl|rcount[7]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|rcount\(1));

-- Location: FF_X38_Y18_N31
\kbd_ctrl|ps2_ctrl|rcount[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|Add1~23_combout\,
	clrn => \KEY[0]~inputclkctrl_outclk\,
	ena => \kbd_ctrl|ps2_ctrl|rcount[7]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|rcount\(0));

-- Location: LCCOMB_X38_Y18_N24
\kbd_ctrl|ps2_ctrl|Add1~16\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add1~16_combout\ = (\PS2_CLK~input_o\ & \kbd_ctrl|ps2_ctrl|Add1~14_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \PS2_CLK~input_o\,
	datad => \kbd_ctrl|ps2_ctrl|Add1~14_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Add1~16_combout\);

-- Location: LCCOMB_X39_Y18_N20
\kbd_ctrl|ps2_ctrl|rcount[7]~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|rcount[7]~1_combout\ = (\KEY[1]~input_o\ & !\kbd_ctrl|ps2_ctrl|rcount[7]~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \KEY[1]~input_o\,
	datad => \kbd_ctrl|ps2_ctrl|rcount[7]~0_combout\,
	combout => \kbd_ctrl|ps2_ctrl|rcount[7]~1_combout\);

-- Location: LCCOMB_X38_Y18_N26
\kbd_ctrl|ps2_ctrl|Add1~17\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add1~17_combout\ = (\PS2_CLK~input_o\ & \kbd_ctrl|ps2_ctrl|Add1~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \PS2_CLK~input_o\,
	datad => \kbd_ctrl|ps2_ctrl|Add1~12_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Add1~17_combout\);

-- Location: LCCOMB_X38_Y18_N28
\kbd_ctrl|ps2_ctrl|Add1~18\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add1~18_combout\ = (\PS2_CLK~input_o\ & \kbd_ctrl|ps2_ctrl|Add1~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \PS2_CLK~input_o\,
	datac => \kbd_ctrl|ps2_ctrl|Add1~10_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Add1~18_combout\);

-- Location: LCCOMB_X35_Y26_N24
\kbd_ctrl|Selector13~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Selector13~0_combout\ = (!\kbd_ctrl|cmdstate.SENDVAL~q\ & !\kbd_ctrl|cmdstate.CLEAR~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \kbd_ctrl|cmdstate.SENDVAL~q\,
	datad => \kbd_ctrl|cmdstate.CLEAR~q\,
	combout => \kbd_ctrl|Selector13~0_combout\);

-- Location: FF_X30_Y27_N23
dir : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCKHZ~clkctrl_outclk\,
	d => \dir~0_combout\,
	clrn => \KEY[0]~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dir~q\);

-- Location: FF_X40_Y15_N7
CLOCKHZ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \LessThan0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \CLOCKHZ~q\);

-- Location: LCCOMB_X33_Y26_N6
\kbd_ctrl|Selector13~4\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Selector13~4_combout\ = (\kbd_ctrl|cmdstate.SETLIGHTS~q\ & \kbd_ctrl|ps2_ctrl|send_rdy~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|cmdstate.SETLIGHTS~q\,
	datac => \kbd_ctrl|ps2_ctrl|send_rdy~2_combout\,
	combout => \kbd_ctrl|Selector13~4_combout\);

-- Location: LCCOMB_X40_Y18_N16
\kbd_ctrl|ps2_ctrl|Add0~19\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add0~19_combout\ = (!\PS2_CLK~input_o\ & \kbd_ctrl|ps2_ctrl|Add0~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \PS2_CLK~input_o\,
	datac => \kbd_ctrl|ps2_ctrl|Add0~8_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Add0~19_combout\);

-- Location: LCCOMB_X40_Y18_N22
\kbd_ctrl|ps2_ctrl|Add0~20\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add0~20_combout\ = (!\PS2_CLK~input_o\ & \kbd_ctrl|ps2_ctrl|Add0~6_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \PS2_CLK~input_o\,
	datad => \kbd_ctrl|ps2_ctrl|Add0~6_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Add0~20_combout\);

-- Location: LCCOMB_X39_Y18_N16
\kbd_ctrl|ps2_ctrl|Add0~21\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add0~21_combout\ = (!\PS2_CLK~input_o\ & \kbd_ctrl|ps2_ctrl|Add0~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \PS2_CLK~input_o\,
	datad => \kbd_ctrl|ps2_ctrl|Add0~4_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Add0~21_combout\);

-- Location: LCCOMB_X39_Y18_N6
\kbd_ctrl|ps2_ctrl|Add0~22\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add0~22_combout\ = (!\PS2_CLK~input_o\ & \kbd_ctrl|ps2_ctrl|Add0~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \PS2_CLK~input_o\,
	datad => \kbd_ctrl|ps2_ctrl|Add0~2_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Add0~22_combout\);

-- Location: LCCOMB_X40_Y18_N24
\kbd_ctrl|ps2_ctrl|Add0~23\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add0~23_combout\ = (\kbd_ctrl|ps2_ctrl|Add0~0_combout\ & !\PS2_CLK~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|Add0~0_combout\,
	datac => \PS2_CLK~input_o\,
	combout => \kbd_ctrl|ps2_ctrl|Add0~23_combout\);

-- Location: LCCOMB_X38_Y18_N2
\kbd_ctrl|ps2_ctrl|Add1~19\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add1~19_combout\ = (\PS2_CLK~input_o\ & \kbd_ctrl|ps2_ctrl|Add1~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \PS2_CLK~input_o\,
	datad => \kbd_ctrl|ps2_ctrl|Add1~8_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Add1~19_combout\);

-- Location: LCCOMB_X38_Y18_N20
\kbd_ctrl|ps2_ctrl|Add1~20\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add1~20_combout\ = (\PS2_CLK~input_o\ & \kbd_ctrl|ps2_ctrl|Add1~6_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \PS2_CLK~input_o\,
	datad => \kbd_ctrl|ps2_ctrl|Add1~6_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Add1~20_combout\);

-- Location: LCCOMB_X38_Y18_N22
\kbd_ctrl|ps2_ctrl|Add1~21\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add1~21_combout\ = (\PS2_CLK~input_o\ & \kbd_ctrl|ps2_ctrl|Add1~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \PS2_CLK~input_o\,
	datac => \kbd_ctrl|ps2_ctrl|Add1~4_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Add1~21_combout\);

-- Location: LCCOMB_X38_Y18_N0
\kbd_ctrl|ps2_ctrl|Add1~22\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add1~22_combout\ = (\PS2_CLK~input_o\ & \kbd_ctrl|ps2_ctrl|Add1~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \PS2_CLK~input_o\,
	datad => \kbd_ctrl|ps2_ctrl|Add1~2_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Add1~22_combout\);

-- Location: LCCOMB_X38_Y18_N30
\kbd_ctrl|ps2_ctrl|Add1~23\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add1~23_combout\ = (\PS2_CLK~input_o\ & \kbd_ctrl|ps2_ctrl|Add1~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \PS2_CLK~input_o\,
	datad => \kbd_ctrl|ps2_ctrl|Add1~0_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Add1~23_combout\);

-- Location: LCCOMB_X33_Y27_N12
\kbd_ctrl|ps2_ctrl|parchecked~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|parchecked~1_combout\ = (\kbd_ctrl|ps2_ctrl|FROMPS2:count[3]~q\ & (!\kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~q\ & (\kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~q\ $ (\kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|FROMPS2:count[3]~q\,
	datab => \kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~q\,
	datac => \kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~q\,
	datad => \kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~q\,
	combout => \kbd_ctrl|ps2_ctrl|parchecked~1_combout\);

-- Location: LCCOMB_X30_Y27_N22
\dir~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \dir~0_combout\ = (lights(2)) # ((lights(0) & \dir~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => lights(0),
	datac => \dir~q\,
	datad => lights(2),
	combout => \dir~0_combout\);

-- Location: LCCOMB_X37_Y15_N20
\LessThan0~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \LessThan0~0_combout\ = ((!count(17) & (!count(16) & !count(15)))) # (!count(18))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100011111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => count(17),
	datab => count(16),
	datac => count(18),
	datad => count(15),
	combout => \LessThan0~0_combout\);

-- Location: LCCOMB_X38_Y15_N26
\LessThan0~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \LessThan0~1_combout\ = (!count(16) & (!count(13) & (!count(17) & !count(14))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => count(16),
	datab => count(13),
	datac => count(17),
	datad => count(14),
	combout => \LessThan0~1_combout\);

-- Location: LCCOMB_X38_Y16_N8
\Equal1~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \Equal1~0_combout\ = (count(11) & (count(12) & (count(10) & count(9))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => count(11),
	datab => count(12),
	datac => count(10),
	datad => count(9),
	combout => \Equal1~0_combout\);

-- Location: LCCOMB_X38_Y15_N28
\LessThan0~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \LessThan0~2_combout\ = (\LessThan0~0_combout\) # ((\LessThan0~1_combout\ & ((!count(8)) # (!\Equal1~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111101110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal1~0_combout\,
	datab => count(8),
	datac => \LessThan0~1_combout\,
	datad => \LessThan0~0_combout\,
	combout => \LessThan0~2_combout\);

-- Location: LCCOMB_X39_Y15_N18
\LessThan0~3\ : cycloneiii_lcell_comb
-- Equation(s):
-- \LessThan0~3_combout\ = (!count(19) & (!count(20) & !count(22)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => count(19),
	datac => count(20),
	datad => count(22),
	combout => \LessThan0~3_combout\);

-- Location: LCCOMB_X40_Y15_N6
\LessThan0~4\ : cycloneiii_lcell_comb
-- Equation(s):
-- \LessThan0~4_combout\ = (count(21) & (\LessThan0~3_combout\ & ((\LessThan0~2_combout\)))) # (!count(21) & (((\LessThan0~3_combout\ & \LessThan0~2_combout\)) # (!count(22))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110100000101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => count(21),
	datab => \LessThan0~3_combout\,
	datac => count(22),
	datad => \LessThan0~2_combout\,
	combout => \LessThan0~4_combout\);

-- Location: FF_X31_Y25_N13
\kbd_ctrl|ps2_ctrl|countclk[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|countclk[0]~55_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_FROMPS2~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|countclk\(0));

-- Location: LCCOMB_X31_Y24_N22
\kbd_ctrl|ps2_ctrl|Equal2~3\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Equal2~3_combout\ = (!\kbd_ctrl|ps2_ctrl|countclk\(15) & (!\kbd_ctrl|ps2_ctrl|countclk\(12) & (!\kbd_ctrl|ps2_ctrl|countclk\(14) & !\kbd_ctrl|ps2_ctrl|countclk\(13))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|countclk\(15),
	datab => \kbd_ctrl|ps2_ctrl|countclk\(12),
	datac => \kbd_ctrl|ps2_ctrl|countclk\(14),
	datad => \kbd_ctrl|ps2_ctrl|countclk\(13),
	combout => \kbd_ctrl|ps2_ctrl|Equal2~3_combout\);

-- Location: FF_X37_Y15_N7
\count[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \count[0]~66_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => count(0));

-- Location: LCCOMB_X38_Y15_N22
\Equal1~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \Equal1~1_combout\ = (!count(17) & (!count(14) & (!count(0) & !count(8))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => count(17),
	datab => count(14),
	datac => count(0),
	datad => count(8),
	combout => \Equal1~1_combout\);

-- Location: LCCOMB_X38_Y15_N24
\Equal1~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \Equal1~2_combout\ = (!count(15) & (count(13) & (count(16) & !count(18))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => count(15),
	datab => count(13),
	datac => count(16),
	datad => count(18),
	combout => \Equal1~2_combout\);

-- Location: LCCOMB_X38_Y15_N30
\Equal1~3\ : cycloneiii_lcell_comb
-- Equation(s):
-- \Equal1~3_combout\ = (count(22) & (!count(20) & (count(19) & !count(21))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => count(22),
	datab => count(20),
	datac => count(19),
	datad => count(21),
	combout => \Equal1~3_combout\);

-- Location: LCCOMB_X38_Y16_N2
\Equal1~4\ : cycloneiii_lcell_comb
-- Equation(s):
-- \Equal1~4_combout\ = (\Equal1~2_combout\ & (\Equal1~0_combout\ & (\Equal1~3_combout\ & \Equal1~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal1~2_combout\,
	datab => \Equal1~0_combout\,
	datac => \Equal1~3_combout\,
	datad => \Equal1~1_combout\,
	combout => \Equal1~4_combout\);

-- Location: LCCOMB_X38_Y16_N0
\Equal1~5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \Equal1~5_combout\ = (!count(1) & (!count(3) & (!count(7) & !count(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => count(1),
	datab => count(3),
	datac => count(7),
	datad => count(2),
	combout => \Equal1~5_combout\);

-- Location: LCCOMB_X38_Y16_N6
\Equal1~6\ : cycloneiii_lcell_comb
-- Equation(s):
-- \Equal1~6_combout\ = (!count(6) & (!count(5) & !count(4)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => count(6),
	datac => count(5),
	datad => count(4),
	combout => \Equal1~6_combout\);

-- Location: LCCOMB_X38_Y16_N4
\Equal1~7\ : cycloneiii_lcell_comb
-- Equation(s):
-- \Equal1~7_combout\ = (\Equal1~6_combout\ & (\Equal1~5_combout\ & \Equal1~4_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal1~6_combout\,
	datab => \Equal1~5_combout\,
	datad => \Equal1~4_combout\,
	combout => \Equal1~7_combout\);

-- Location: LCCOMB_X31_Y25_N12
\kbd_ctrl|ps2_ctrl|countclk[0]~55\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|countclk[0]~55_combout\ = \kbd_ctrl|ps2_ctrl|countclk\(0) $ (((!\kbd_ctrl|ps2_ctrl|sigsending~q\ & !\kbd_ctrl|ps2_ctrl|Equal2~5_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|sigsending~q\,
	datac => \kbd_ctrl|ps2_ctrl|countclk\(0),
	datad => \kbd_ctrl|ps2_ctrl|Equal2~5_combout\,
	combout => \kbd_ctrl|ps2_ctrl|countclk[0]~55_combout\);

-- Location: LCCOMB_X37_Y15_N6
\count[0]~66\ : cycloneiii_lcell_comb
-- Equation(s):
-- \count[0]~66_combout\ = !count(0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => count(0),
	combout => \count[0]~66_combout\);

-- Location: IOIBUF_X41_Y24_N1
\PS2_CLK~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => PS2_CLK,
	o => \PS2_CLK~input_o\);

-- Location: CLKCTRL_G5
\CLOCKHZ~clkctrl\ : cycloneiii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \CLOCKHZ~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \CLOCKHZ~clkctrl_outclk\);

-- Location: CLKCTRL_G11
\kbd_ctrl|sigsend~clkctrl\ : cycloneiii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \kbd_ctrl|sigsend~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \kbd_ctrl|sigsend~clkctrl_outclk\);

-- Location: LCCOMB_X39_Y15_N24
\count[8]~feeder\ : cycloneiii_lcell_comb
-- Equation(s):
-- \count[8]~feeder_combout\ = \count[8]~36_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \count[8]~36_combout\,
	combout => \count[8]~feeder_combout\);

-- Location: IOOBUF_X41_Y23_N2
\PS2_DAT~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \kbd_ctrl|ps2_ctrl|ps2_data~reg0_q\,
	oe => \kbd_ctrl|ps2_ctrl|ps2_data~2_combout\,
	devoe => ww_devoe,
	o => \PS2_DAT~output_o\);

-- Location: IOOBUF_X41_Y24_N2
\PS2_CLK~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => \kbd_ctrl|ps2_ctrl|ALT_INV_ps2_clk~en_q\,
	oe => VCC,
	devoe => ww_devoe,
	o => \PS2_CLK~output_o\);

-- Location: IOOBUF_X30_Y29_N23
\HEX0[0]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \hexseg0|ALT_INV_Mux6~0_combout\,
	devoe => ww_devoe,
	o => \HEX0[0]~output_o\);

-- Location: IOOBUF_X35_Y29_N9
\HEX0[1]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \hexseg0|Mux5~0_combout\,
	devoe => ww_devoe,
	o => \HEX0[1]~output_o\);

-- Location: IOOBUF_X21_Y29_N9
\HEX0[2]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \hexseg0|Mux4~0_combout\,
	devoe => ww_devoe,
	o => \HEX0[2]~output_o\);

-- Location: IOOBUF_X28_Y29_N9
\HEX0[3]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \hexseg0|Mux3~0_combout\,
	devoe => ww_devoe,
	o => \HEX0[3]~output_o\);

-- Location: IOOBUF_X11_Y29_N30
\HEX0[4]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \hexseg0|Mux2~0_combout\,
	devoe => ww_devoe,
	o => \HEX0[4]~output_o\);

-- Location: IOOBUF_X35_Y29_N2
\HEX0[5]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \hexseg0|Mux1~0_combout\,
	devoe => ww_devoe,
	o => \HEX0[5]~output_o\);

-- Location: IOOBUF_X26_Y29_N30
\HEX0[6]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \hexseg0|Mux0~0_combout\,
	devoe => ww_devoe,
	o => \HEX0[6]~output_o\);

-- Location: IOOBUF_X23_Y29_N9
\HEX1[0]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \hexseg1|ALT_INV_Mux6~0_combout\,
	devoe => ww_devoe,
	o => \HEX1[0]~output_o\);

-- Location: IOOBUF_X28_Y29_N16
\HEX1[1]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \hexseg1|Mux5~0_combout\,
	devoe => ww_devoe,
	o => \HEX1[1]~output_o\);

-- Location: IOOBUF_X23_Y29_N2
\HEX1[2]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \hexseg1|Mux4~0_combout\,
	devoe => ww_devoe,
	o => \HEX1[2]~output_o\);

-- Location: IOOBUF_X26_Y29_N16
\HEX1[3]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \hexseg1|Mux3~0_combout\,
	devoe => ww_devoe,
	o => \HEX1[3]~output_o\);

-- Location: IOOBUF_X30_Y29_N16
\HEX1[4]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \hexseg1|Mux2~0_combout\,
	devoe => ww_devoe,
	o => \HEX1[4]~output_o\);

-- Location: IOOBUF_X14_Y29_N2
\HEX1[5]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \hexseg1|Mux1~0_combout\,
	devoe => ww_devoe,
	o => \HEX1[5]~output_o\);

-- Location: IOOBUF_X26_Y29_N23
\HEX1[6]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \hexseg1|Mux0~0_combout\,
	devoe => ww_devoe,
	o => \HEX1[6]~output_o\);

-- Location: IOOBUF_X5_Y29_N16
\HEX2[0]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => \HEX2[0]~output_o\);

-- Location: IOOBUF_X37_Y0_N30
\HEX2[1]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \HEX2[1]~output_o\);

-- Location: IOOBUF_X39_Y0_N23
\HEX2[2]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \HEX2[2]~output_o\);

-- Location: IOOBUF_X26_Y0_N30
\HEX2[3]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \HEX2[3]~output_o\);

-- Location: IOOBUF_X41_Y13_N23
\HEX2[4]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \HEX2[4]~output_o\);

-- Location: IOOBUF_X0_Y6_N16
\HEX2[5]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \HEX2[5]~output_o\);

-- Location: IOOBUF_X19_Y0_N30
\HEX2[6]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \HEX2[6]~output_o\);

-- Location: IOOBUF_X32_Y29_N23
\HEX3[0]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \kbd_ctrl|ALT_INV_key0code\(13),
	devoe => ww_devoe,
	o => \HEX3[0]~output_o\);

-- Location: IOOBUF_X7_Y0_N23
\HEX3[1]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \HEX3[1]~output_o\);

-- Location: IOOBUF_X14_Y29_N9
\HEX3[2]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \HEX3[2]~output_o\);

-- Location: IOOBUF_X41_Y2_N16
\HEX3[3]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \HEX3[3]~output_o\);

-- Location: IOOBUF_X32_Y29_N9
\HEX3[4]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \kbd_ctrl|key0code\(13),
	devoe => ww_devoe,
	o => \HEX3[4]~output_o\);

-- Location: IOOBUF_X32_Y29_N2
\HEX3[5]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \kbd_ctrl|key0code\(13),
	devoe => ww_devoe,
	o => \HEX3[5]~output_o\);

-- Location: IOOBUF_X30_Y0_N23
\HEX3[6]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \HEX3[6]~output_o\);

-- Location: IOOBUF_X41_Y5_N2
\LEDG[0]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LEDG[0]~output_o\);

-- Location: IOOBUF_X26_Y0_N2
\LEDG[1]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LEDG[1]~output_o\);

-- Location: IOOBUF_X19_Y0_N9
\LEDG[2]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LEDG[2]~output_o\);

-- Location: IOOBUF_X39_Y29_N30
\LEDG[3]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LEDG[3]~output_o\);

-- Location: IOOBUF_X23_Y0_N23
\LEDG[4]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LEDG[4]~output_o\);

-- Location: IOOBUF_X37_Y29_N2
\LEDG[5]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \kbd_ctrl|key_on\(0),
	devoe => ww_devoe,
	o => \LEDG[5]~output_o\);

-- Location: IOOBUF_X32_Y29_N30
\LEDG[6]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \kbd_ctrl|key_on\(1),
	devoe => ww_devoe,
	o => \LEDG[6]~output_o\);

-- Location: IOOBUF_X37_Y29_N23
\LEDG[7]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \kbd_ctrl|key_on\(2),
	devoe => ww_devoe,
	o => \LEDG[7]~output_o\);

-- Location: IOOBUF_X21_Y0_N30
\LEDR[0]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LEDR[0]~output_o\);

-- Location: IOOBUF_X0_Y5_N16
\LEDR[1]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LEDR[1]~output_o\);

-- Location: IOOBUF_X30_Y0_N2
\LEDR[2]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LEDR[2]~output_o\);

-- Location: IOOBUF_X0_Y25_N16
\LEDR[3]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LEDR[3]~output_o\);

-- Location: IOOBUF_X26_Y0_N16
\LEDR[4]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LEDR[4]~output_o\);

-- Location: IOOBUF_X41_Y3_N23
\LEDR[5]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LEDR[5]~output_o\);

-- Location: IOOBUF_X7_Y0_N16
\LEDR[6]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LEDR[6]~output_o\);

-- Location: IOOBUF_X3_Y29_N23
\LEDR[7]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LEDR[7]~output_o\);

-- Location: IOOBUF_X9_Y29_N9
\LEDR[8]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LEDR[8]~output_o\);

-- Location: IOOBUF_X0_Y26_N16
\LEDR[9]~output\ : cycloneiii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LEDR[9]~output_o\);

-- Location: LCCOMB_X39_Y18_N0
\kbd_ctrl|ps2_ctrl|sigtrigger~feeder\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|sigtrigger~feeder_combout\ = \kbd_ctrl|ps2_ctrl|sigtrigger~1_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|sigtrigger~1_combout\,
	combout => \kbd_ctrl|ps2_ctrl|sigtrigger~feeder_combout\);

-- Location: IOIBUF_X0_Y14_N8
\KEY[0]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(0),
	o => \KEY[0]~input_o\);

-- Location: CLKCTRL_G2
\KEY[0]~inputclkctrl\ : cycloneiii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \KEY[0]~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \KEY[0]~inputclkctrl_outclk\);

-- Location: FF_X39_Y18_N1
\kbd_ctrl|ps2_ctrl|sigtrigger\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|sigtrigger~feeder_combout\,
	clrn => \KEY[0]~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|sigtrigger~q\);

-- Location: CLKCTRL_G8
\kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl\ : cycloneiii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_outclk\);

-- Location: LCCOMB_X32_Y24_N28
\kbd_ctrl|ps2_ctrl|sigsending~feeder\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|sigsending~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	combout => \kbd_ctrl|ps2_ctrl|sigsending~feeder_combout\);

-- Location: LCCOMB_X35_Y24_N18
\kbd_ctrl|ps2_ctrl|sigsendend~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|sigsendend~1_combout\ = (\kbd_ctrl|ps2_ctrl|TOPS2:count[2]~q\ & (\kbd_ctrl|ps2_ctrl|sigsendend~q\)) # (!\kbd_ctrl|ps2_ctrl|TOPS2:count[2]~q\ & ((\kbd_ctrl|ps2_ctrl|TOPS2:count[1]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|sigsendend~q\,
	datac => \kbd_ctrl|ps2_ctrl|TOPS2:count[1]~q\,
	datad => \kbd_ctrl|ps2_ctrl|TOPS2:count[2]~q\,
	combout => \kbd_ctrl|ps2_ctrl|sigsendend~1_combout\);

-- Location: LCCOMB_X35_Y24_N26
\kbd_ctrl|ps2_ctrl|TOPS2:count[0]~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|TOPS2:count[0]~0_combout\ = !\kbd_ctrl|ps2_ctrl|TOPS2:count[0]~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \kbd_ctrl|ps2_ctrl|TOPS2:count[0]~q\,
	combout => \kbd_ctrl|ps2_ctrl|TOPS2:count[0]~0_combout\);

-- Location: LCCOMB_X32_Y24_N16
\kbd_ctrl|ps2_ctrl|TOPS2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|TOPS2~0_combout\ = (\KEY[0]~input_o\ & \kbd_ctrl|ps2_ctrl|sigsending~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \KEY[0]~input_o\,
	datad => \kbd_ctrl|ps2_ctrl|sigsending~q\,
	combout => \kbd_ctrl|ps2_ctrl|TOPS2~0_combout\);

-- Location: LCCOMB_X33_Y24_N4
\kbd_ctrl|ps2_ctrl|count[0]~15\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|count[0]~15_combout\ = \kbd_ctrl|ps2_ctrl|count\(0) $ (VCC)
-- \kbd_ctrl|ps2_ctrl|count[0]~16\ = CARRY(\kbd_ctrl|ps2_ctrl|count\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|count\(0),
	datad => VCC,
	combout => \kbd_ctrl|ps2_ctrl|count[0]~15_combout\,
	cout => \kbd_ctrl|ps2_ctrl|count[0]~16\);

-- Location: LCCOMB_X33_Y24_N24
\kbd_ctrl|ps2_ctrl|count[10]~36\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|count[10]~36_combout\ = (\kbd_ctrl|ps2_ctrl|count\(10) & (\kbd_ctrl|ps2_ctrl|count[9]~35\ $ (GND))) # (!\kbd_ctrl|ps2_ctrl|count\(10) & (!\kbd_ctrl|ps2_ctrl|count[9]~35\ & VCC))
-- \kbd_ctrl|ps2_ctrl|count[10]~37\ = CARRY((\kbd_ctrl|ps2_ctrl|count\(10) & !\kbd_ctrl|ps2_ctrl|count[9]~35\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|count\(10),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|count[9]~35\,
	combout => \kbd_ctrl|ps2_ctrl|count[10]~36_combout\,
	cout => \kbd_ctrl|ps2_ctrl|count[10]~37\);

-- Location: LCCOMB_X33_Y24_N26
\kbd_ctrl|ps2_ctrl|count[11]~38\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|count[11]~38_combout\ = \kbd_ctrl|ps2_ctrl|count\(11) $ (\kbd_ctrl|ps2_ctrl|count[10]~37\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|count\(11),
	cin => \kbd_ctrl|ps2_ctrl|count[10]~37\,
	combout => \kbd_ctrl|ps2_ctrl|count[11]~38_combout\);

-- Location: FF_X33_Y24_N27
\kbd_ctrl|ps2_ctrl|count[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|count[11]~38_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_process_2~0_combout\,
	ena => \kbd_ctrl|ps2_ctrl|count[11]~19_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|count\(11));

-- Location: LCCOMB_X33_Y24_N14
\kbd_ctrl|ps2_ctrl|count[5]~26\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|count[5]~26_combout\ = (\kbd_ctrl|ps2_ctrl|count\(5) & (!\kbd_ctrl|ps2_ctrl|count[4]~25\)) # (!\kbd_ctrl|ps2_ctrl|count\(5) & ((\kbd_ctrl|ps2_ctrl|count[4]~25\) # (GND)))
-- \kbd_ctrl|ps2_ctrl|count[5]~27\ = CARRY((!\kbd_ctrl|ps2_ctrl|count[4]~25\) # (!\kbd_ctrl|ps2_ctrl|count\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|count\(5),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|count[4]~25\,
	combout => \kbd_ctrl|ps2_ctrl|count[5]~26_combout\,
	cout => \kbd_ctrl|ps2_ctrl|count[5]~27\);

-- Location: FF_X33_Y24_N15
\kbd_ctrl|ps2_ctrl|count[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|count[5]~26_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_process_2~0_combout\,
	ena => \kbd_ctrl|ps2_ctrl|count[11]~19_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|count\(5));

-- Location: LCCOMB_X33_Y24_N0
\kbd_ctrl|ps2_ctrl|ps2_clk~3\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|ps2_clk~3_combout\ = (!\kbd_ctrl|ps2_ctrl|count\(3) & (!\kbd_ctrl|ps2_ctrl|count\(2) & (!\kbd_ctrl|ps2_ctrl|count\(5) & !\kbd_ctrl|ps2_ctrl|count\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|count\(3),
	datab => \kbd_ctrl|ps2_ctrl|count\(2),
	datac => \kbd_ctrl|ps2_ctrl|count\(5),
	datad => \kbd_ctrl|ps2_ctrl|count\(4),
	combout => \kbd_ctrl|ps2_ctrl|ps2_clk~3_combout\);

-- Location: LCCOMB_X32_Y24_N6
\kbd_ctrl|ps2_ctrl|ps2_clk~4\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|ps2_clk~4_combout\ = (\kbd_ctrl|ps2_ctrl|ps2_clk~2_combout\ & (((\kbd_ctrl|ps2_ctrl|ps2_clk~3_combout\) # (!\kbd_ctrl|ps2_ctrl|count\(6))) # (!\kbd_ctrl|ps2_ctrl|ps2_clk~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111011100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|ps2_clk~1_combout\,
	datab => \kbd_ctrl|ps2_ctrl|count\(6),
	datac => \kbd_ctrl|ps2_ctrl|ps2_clk~3_combout\,
	datad => \kbd_ctrl|ps2_ctrl|ps2_clk~2_combout\,
	combout => \kbd_ctrl|ps2_ctrl|ps2_clk~4_combout\);

-- Location: LCCOMB_X32_Y24_N14
\kbd_ctrl|ps2_ctrl|ps2_clk~5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|ps2_clk~5_combout\ = (\kbd_ctrl|ps2_ctrl|ps2_clk~4_combout\) # (!\kbd_ctrl|ps2_ctrl|count\(11))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \kbd_ctrl|ps2_ctrl|count\(11),
	datad => \kbd_ctrl|ps2_ctrl|ps2_clk~4_combout\,
	combout => \kbd_ctrl|ps2_ctrl|ps2_clk~5_combout\);

-- Location: LCCOMB_X33_Y24_N2
\kbd_ctrl|ps2_ctrl|count[11]~19\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|count[11]~19_combout\ = (\kbd_ctrl|ps2_ctrl|sigsending~q\ & ((\kbd_ctrl|ps2_ctrl|ps2_clk~5_combout\) # ((\kbd_ctrl|ps2_ctrl|LessThan4~1_combout\ & \kbd_ctrl|ps2_ctrl|ps2_clk~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|LessThan4~1_combout\,
	datab => \kbd_ctrl|ps2_ctrl|ps2_clk~2_combout\,
	datac => \kbd_ctrl|ps2_ctrl|sigsending~q\,
	datad => \kbd_ctrl|ps2_ctrl|ps2_clk~5_combout\,
	combout => \kbd_ctrl|ps2_ctrl|count[11]~19_combout\);

-- Location: FF_X33_Y24_N5
\kbd_ctrl|ps2_ctrl|count[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|count[0]~15_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_process_2~0_combout\,
	ena => \kbd_ctrl|ps2_ctrl|count[11]~19_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|count\(0));

-- Location: LCCOMB_X33_Y24_N8
\kbd_ctrl|ps2_ctrl|count[2]~20\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|count[2]~20_combout\ = (\kbd_ctrl|ps2_ctrl|count\(2) & (\kbd_ctrl|ps2_ctrl|count[1]~18\ $ (GND))) # (!\kbd_ctrl|ps2_ctrl|count\(2) & (!\kbd_ctrl|ps2_ctrl|count[1]~18\ & VCC))
-- \kbd_ctrl|ps2_ctrl|count[2]~21\ = CARRY((\kbd_ctrl|ps2_ctrl|count\(2) & !\kbd_ctrl|ps2_ctrl|count[1]~18\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|count\(2),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|count[1]~18\,
	combout => \kbd_ctrl|ps2_ctrl|count[2]~20_combout\,
	cout => \kbd_ctrl|ps2_ctrl|count[2]~21\);

-- Location: FF_X33_Y24_N9
\kbd_ctrl|ps2_ctrl|count[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|count[2]~20_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_process_2~0_combout\,
	ena => \kbd_ctrl|ps2_ctrl|count[11]~19_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|count\(2));

-- Location: LCCOMB_X33_Y24_N10
\kbd_ctrl|ps2_ctrl|count[3]~22\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|count[3]~22_combout\ = (\kbd_ctrl|ps2_ctrl|count\(3) & (!\kbd_ctrl|ps2_ctrl|count[2]~21\)) # (!\kbd_ctrl|ps2_ctrl|count\(3) & ((\kbd_ctrl|ps2_ctrl|count[2]~21\) # (GND)))
-- \kbd_ctrl|ps2_ctrl|count[3]~23\ = CARRY((!\kbd_ctrl|ps2_ctrl|count[2]~21\) # (!\kbd_ctrl|ps2_ctrl|count\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|count\(3),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|count[2]~21\,
	combout => \kbd_ctrl|ps2_ctrl|count[3]~22_combout\,
	cout => \kbd_ctrl|ps2_ctrl|count[3]~23\);

-- Location: FF_X33_Y24_N11
\kbd_ctrl|ps2_ctrl|count[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|count[3]~22_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_process_2~0_combout\,
	ena => \kbd_ctrl|ps2_ctrl|count[11]~19_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|count\(3));

-- Location: LCCOMB_X33_Y24_N12
\kbd_ctrl|ps2_ctrl|count[4]~24\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|count[4]~24_combout\ = (\kbd_ctrl|ps2_ctrl|count\(4) & (\kbd_ctrl|ps2_ctrl|count[3]~23\ $ (GND))) # (!\kbd_ctrl|ps2_ctrl|count\(4) & (!\kbd_ctrl|ps2_ctrl|count[3]~23\ & VCC))
-- \kbd_ctrl|ps2_ctrl|count[4]~25\ = CARRY((\kbd_ctrl|ps2_ctrl|count\(4) & !\kbd_ctrl|ps2_ctrl|count[3]~23\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|count\(4),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|count[3]~23\,
	combout => \kbd_ctrl|ps2_ctrl|count[4]~24_combout\,
	cout => \kbd_ctrl|ps2_ctrl|count[4]~25\);

-- Location: FF_X33_Y24_N13
\kbd_ctrl|ps2_ctrl|count[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|count[4]~24_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_process_2~0_combout\,
	ena => \kbd_ctrl|ps2_ctrl|count[11]~19_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|count\(4));

-- Location: LCCOMB_X33_Y24_N16
\kbd_ctrl|ps2_ctrl|count[6]~28\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|count[6]~28_combout\ = (\kbd_ctrl|ps2_ctrl|count\(6) & (\kbd_ctrl|ps2_ctrl|count[5]~27\ $ (GND))) # (!\kbd_ctrl|ps2_ctrl|count\(6) & (!\kbd_ctrl|ps2_ctrl|count[5]~27\ & VCC))
-- \kbd_ctrl|ps2_ctrl|count[6]~29\ = CARRY((\kbd_ctrl|ps2_ctrl|count\(6) & !\kbd_ctrl|ps2_ctrl|count[5]~27\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|count\(6),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|count[5]~27\,
	combout => \kbd_ctrl|ps2_ctrl|count[6]~28_combout\,
	cout => \kbd_ctrl|ps2_ctrl|count[6]~29\);

-- Location: FF_X33_Y24_N17
\kbd_ctrl|ps2_ctrl|count[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|count[6]~28_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_process_2~0_combout\,
	ena => \kbd_ctrl|ps2_ctrl|count[11]~19_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|count\(6));

-- Location: LCCOMB_X33_Y24_N20
\kbd_ctrl|ps2_ctrl|count[8]~32\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|count[8]~32_combout\ = (\kbd_ctrl|ps2_ctrl|count\(8) & (\kbd_ctrl|ps2_ctrl|count[7]~31\ $ (GND))) # (!\kbd_ctrl|ps2_ctrl|count\(8) & (!\kbd_ctrl|ps2_ctrl|count[7]~31\ & VCC))
-- \kbd_ctrl|ps2_ctrl|count[8]~33\ = CARRY((\kbd_ctrl|ps2_ctrl|count\(8) & !\kbd_ctrl|ps2_ctrl|count[7]~31\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|count\(8),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|count[7]~31\,
	combout => \kbd_ctrl|ps2_ctrl|count[8]~32_combout\,
	cout => \kbd_ctrl|ps2_ctrl|count[8]~33\);

-- Location: FF_X33_Y24_N21
\kbd_ctrl|ps2_ctrl|count[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|count[8]~32_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_process_2~0_combout\,
	ena => \kbd_ctrl|ps2_ctrl|count[11]~19_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|count\(8));

-- Location: LCCOMB_X33_Y24_N22
\kbd_ctrl|ps2_ctrl|count[9]~34\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|count[9]~34_combout\ = (\kbd_ctrl|ps2_ctrl|count\(9) & (!\kbd_ctrl|ps2_ctrl|count[8]~33\)) # (!\kbd_ctrl|ps2_ctrl|count\(9) & ((\kbd_ctrl|ps2_ctrl|count[8]~33\) # (GND)))
-- \kbd_ctrl|ps2_ctrl|count[9]~35\ = CARRY((!\kbd_ctrl|ps2_ctrl|count[8]~33\) # (!\kbd_ctrl|ps2_ctrl|count\(9)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|count\(9),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|count[8]~33\,
	combout => \kbd_ctrl|ps2_ctrl|count[9]~34_combout\,
	cout => \kbd_ctrl|ps2_ctrl|count[9]~35\);

-- Location: FF_X33_Y24_N23
\kbd_ctrl|ps2_ctrl|count[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|count[9]~34_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_process_2~0_combout\,
	ena => \kbd_ctrl|ps2_ctrl|count[11]~19_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|count\(9));

-- Location: FF_X33_Y24_N25
\kbd_ctrl|ps2_ctrl|count[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|count[10]~36_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_process_2~0_combout\,
	ena => \kbd_ctrl|ps2_ctrl|count[11]~19_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|count\(10));

-- Location: LCCOMB_X33_Y24_N28
\kbd_ctrl|ps2_ctrl|ps2_clk~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|ps2_clk~2_combout\ = (!\kbd_ctrl|ps2_ctrl|count\(10) & !\kbd_ctrl|ps2_ctrl|count\(9))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|count\(10),
	datad => \kbd_ctrl|ps2_ctrl|count\(9),
	combout => \kbd_ctrl|ps2_ctrl|ps2_clk~2_combout\);

-- Location: LCCOMB_X32_Y24_N4
\kbd_ctrl|ps2_ctrl|sigclkheld~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|sigclkheld~0_combout\ = (\kbd_ctrl|ps2_ctrl|count\(11) & (\kbd_ctrl|ps2_ctrl|ps2_clk~4_combout\ & ((!\kbd_ctrl|ps2_ctrl|ps2_clk~2_combout\) # (!\kbd_ctrl|ps2_ctrl|LessThan4~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|LessThan4~1_combout\,
	datab => \kbd_ctrl|ps2_ctrl|ps2_clk~2_combout\,
	datac => \kbd_ctrl|ps2_ctrl|count\(11),
	datad => \kbd_ctrl|ps2_ctrl|ps2_clk~4_combout\,
	combout => \kbd_ctrl|ps2_ctrl|sigclkheld~0_combout\);

-- Location: FF_X32_Y24_N5
\kbd_ctrl|ps2_ctrl|sigclkheld\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|sigclkheld~0_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_process_2~0_combout\,
	ena => \kbd_ctrl|ps2_ctrl|sigsending~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|sigclkheld~q\);

-- Location: LCCOMB_X32_Y24_N24
\kbd_ctrl|ps2_ctrl|sigsendend~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|sigsendend~0_combout\ = (\kbd_ctrl|ps2_ctrl|sigclkheld~q\) # (!\kbd_ctrl|ps2_ctrl|TOPS2~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001111110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|TOPS2~0_combout\,
	datac => \kbd_ctrl|ps2_ctrl|sigclkheld~q\,
	combout => \kbd_ctrl|ps2_ctrl|sigsendend~0_combout\);

-- Location: FF_X35_Y24_N27
\kbd_ctrl|ps2_ctrl|TOPS2:count[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|TOPS2:count[0]~0_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_sigsendend~0_combout\,
	ena => \kbd_ctrl|ps2_ctrl|TOPS2~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|TOPS2:count[0]~q\);

-- Location: LCCOMB_X35_Y24_N28
\kbd_ctrl|ps2_ctrl|sigsendend~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|sigsendend~2_combout\ = (\kbd_ctrl|ps2_ctrl|TOPS2:count[3]~q\ & (\kbd_ctrl|ps2_ctrl|sigsendend~1_combout\ & ((\kbd_ctrl|ps2_ctrl|sigsendend~q\) # (!\kbd_ctrl|ps2_ctrl|TOPS2:count[0]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|TOPS2:count[3]~q\,
	datab => \kbd_ctrl|ps2_ctrl|sigsendend~1_combout\,
	datac => \kbd_ctrl|ps2_ctrl|sigsendend~q\,
	datad => \kbd_ctrl|ps2_ctrl|TOPS2:count[0]~q\,
	combout => \kbd_ctrl|ps2_ctrl|sigsendend~2_combout\);

-- Location: FF_X35_Y24_N29
\kbd_ctrl|ps2_ctrl|sigsendend\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|sigsendend~2_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_sigsendend~0_combout\,
	ena => \kbd_ctrl|ps2_ctrl|TOPS2~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|sigsendend~q\);

-- Location: LCCOMB_X35_Y24_N2
\kbd_ctrl|ps2_ctrl|process_2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|process_2~0_combout\ = (\kbd_ctrl|ps2_ctrl|sigsendend~q\) # (!\KEY[0]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111101010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \KEY[0]~input_o\,
	datad => \kbd_ctrl|ps2_ctrl|sigsendend~q\,
	combout => \kbd_ctrl|ps2_ctrl|process_2~0_combout\);

-- Location: FF_X32_Y24_N29
\kbd_ctrl|ps2_ctrl|sigsending\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \kbd_ctrl|sigsend~clkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|sigsending~feeder_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_process_2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|sigsending~q\);

-- Location: LCCOMB_X32_Y24_N12
\kbd_ctrl|ps2_ctrl|sigclkreleased~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|sigclkreleased~0_combout\ = ((\kbd_ctrl|ps2_ctrl|ps2_clk~4_combout\) # ((\kbd_ctrl|ps2_ctrl|LessThan4~1_combout\ & \kbd_ctrl|ps2_ctrl|ps2_clk~2_combout\))) # (!\kbd_ctrl|ps2_ctrl|count\(11))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|LessThan4~1_combout\,
	datab => \kbd_ctrl|ps2_ctrl|ps2_clk~2_combout\,
	datac => \kbd_ctrl|ps2_ctrl|count\(11),
	datad => \kbd_ctrl|ps2_ctrl|ps2_clk~4_combout\,
	combout => \kbd_ctrl|ps2_ctrl|sigclkreleased~0_combout\);

-- Location: FF_X32_Y24_N13
\kbd_ctrl|ps2_ctrl|sigclkreleased\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|sigclkreleased~0_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_process_2~0_combout\,
	ena => \kbd_ctrl|ps2_ctrl|sigsending~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|sigclkreleased~q\);

-- Location: LCCOMB_X32_Y24_N2
\kbd_ctrl|ps2_ctrl|TOPS2~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|TOPS2~1_combout\ = (\kbd_ctrl|ps2_ctrl|sigsending~q\ & !\kbd_ctrl|ps2_ctrl|sigclkreleased~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|sigsending~q\,
	datad => \kbd_ctrl|ps2_ctrl|sigclkreleased~q\,
	combout => \kbd_ctrl|ps2_ctrl|TOPS2~1_combout\);

-- Location: LCCOMB_X35_Y24_N12
\kbd_ctrl|ps2_ctrl|TOPS2:count[2]~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|TOPS2:count[2]~0_combout\ = \kbd_ctrl|ps2_ctrl|TOPS2:count[2]~q\ $ (((\kbd_ctrl|ps2_ctrl|TOPS2:count[0]~q\ & (\kbd_ctrl|ps2_ctrl|TOPS2:count[1]~q\ & \kbd_ctrl|ps2_ctrl|TOPS2~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|TOPS2:count[0]~q\,
	datab => \kbd_ctrl|ps2_ctrl|TOPS2:count[1]~q\,
	datac => \kbd_ctrl|ps2_ctrl|TOPS2:count[2]~q\,
	datad => \kbd_ctrl|ps2_ctrl|TOPS2~1_combout\,
	combout => \kbd_ctrl|ps2_ctrl|TOPS2:count[2]~0_combout\);

-- Location: FF_X35_Y24_N13
\kbd_ctrl|ps2_ctrl|TOPS2:count[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|TOPS2:count[2]~0_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_sigsendend~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|TOPS2:count[2]~q\);

-- Location: LCCOMB_X35_Y24_N8
\kbd_ctrl|ps2_ctrl|Add6~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add6~0_combout\ = (\kbd_ctrl|ps2_ctrl|TOPS2:count[1]~q\ & (!\kbd_ctrl|ps2_ctrl|TOPS2:count[0]~q\ & ((\kbd_ctrl|ps2_ctrl|TOPS2:count[2]~q\) # (!\kbd_ctrl|ps2_ctrl|TOPS2:count[3]~q\)))) # (!\kbd_ctrl|ps2_ctrl|TOPS2:count[1]~q\ & 
-- (((\kbd_ctrl|ps2_ctrl|TOPS2:count[0]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|TOPS2:count[3]~q\,
	datab => \kbd_ctrl|ps2_ctrl|TOPS2:count[2]~q\,
	datac => \kbd_ctrl|ps2_ctrl|TOPS2:count[1]~q\,
	datad => \kbd_ctrl|ps2_ctrl|TOPS2:count[0]~q\,
	combout => \kbd_ctrl|ps2_ctrl|Add6~0_combout\);

-- Location: FF_X35_Y24_N9
\kbd_ctrl|ps2_ctrl|TOPS2:count[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|Add6~0_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_sigsendend~0_combout\,
	ena => \kbd_ctrl|ps2_ctrl|TOPS2~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|TOPS2:count[1]~q\);

-- Location: LCCOMB_X33_Y27_N8
\kbd_ctrl|ps2_ctrl|Add3~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add3~0_combout\ = \kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~q\ $ (((\kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~q\ & \kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100001111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~q\,
	datab => \kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~q\,
	datac => \kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~q\,
	combout => \kbd_ctrl|ps2_ctrl|Add3~0_combout\);

-- Location: LCCOMB_X32_Y24_N18
\kbd_ctrl|ps2_ctrl|FROMPS2~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|FROMPS2~1_combout\ = (\kbd_ctrl|ps2_ctrl|sigsending~q\) # (!\KEY[0]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \KEY[0]~input_o\,
	datad => \kbd_ctrl|ps2_ctrl|sigsending~q\,
	combout => \kbd_ctrl|ps2_ctrl|FROMPS2~1_combout\);

-- Location: FF_X33_Y27_N9
\kbd_ctrl|ps2_ctrl|FROMPS2:count[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|Add3~0_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_FROMPS2~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~q\);

-- Location: LCCOMB_X33_Y27_N14
\kbd_ctrl|ps2_ctrl|count~12\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|count~12_combout\ = (\kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~q\ & (!\kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~q\ & ((\kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~q\) # (!\kbd_ctrl|ps2_ctrl|FROMPS2:count[3]~q\)))) # (!\kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~q\ 
-- & (((\kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|FROMPS2:count[3]~q\,
	datab => \kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~q\,
	datac => \kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~q\,
	datad => \kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~q\,
	combout => \kbd_ctrl|ps2_ctrl|count~12_combout\);

-- Location: FF_X33_Y27_N15
\kbd_ctrl|ps2_ctrl|FROMPS2:count[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|count~12_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_FROMPS2~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~q\);

-- Location: LCCOMB_X33_Y27_N0
\kbd_ctrl|ps2_ctrl|count~13\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|count~13_combout\ = (!\kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~q\ & (((\kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~q\) # (!\kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~q\)) # (!\kbd_ctrl|ps2_ctrl|FROMPS2:count[3]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|FROMPS2:count[3]~q\,
	datab => \kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~q\,
	datac => \kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~q\,
	datad => \kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~q\,
	combout => \kbd_ctrl|ps2_ctrl|count~13_combout\);

-- Location: FF_X33_Y27_N1
\kbd_ctrl|ps2_ctrl|FROMPS2:count[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|count~13_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_FROMPS2~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~q\);

-- Location: IOIBUF_X41_Y23_N1
\PS2_DAT~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => PS2_DAT,
	o => \PS2_DAT~input_o\);

-- Location: LCCOMB_X33_Y27_N28
\kbd_ctrl|ps2_ctrl|sdata[7]~4\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|sdata[7]~4_combout\ = (\kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~q\) # ((\kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~q\) # ((\kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~q\) # (\kbd_ctrl|ps2_ctrl|sigsending~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~q\,
	datab => \kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~q\,
	datac => \kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~q\,
	datad => \kbd_ctrl|ps2_ctrl|sigsending~q\,
	combout => \kbd_ctrl|ps2_ctrl|sdata[7]~4_combout\);

-- Location: LCCOMB_X33_Y27_N22
\kbd_ctrl|ps2_ctrl|sdata[7]~5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|sdata[7]~5_combout\ = (\kbd_ctrl|ps2_ctrl|FROMPS2~0_combout\ & ((\kbd_ctrl|ps2_ctrl|sdata[7]~4_combout\ & ((\kbd_ctrl|ps2_ctrl|sdata\(7)))) # (!\kbd_ctrl|ps2_ctrl|sdata[7]~4_combout\ & (\PS2_DAT~input_o\)))) # 
-- (!\kbd_ctrl|ps2_ctrl|FROMPS2~0_combout\ & (((\kbd_ctrl|ps2_ctrl|sdata\(7)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|FROMPS2~0_combout\,
	datab => \PS2_DAT~input_o\,
	datac => \kbd_ctrl|ps2_ctrl|sdata\(7),
	datad => \kbd_ctrl|ps2_ctrl|sdata[7]~4_combout\,
	combout => \kbd_ctrl|ps2_ctrl|sdata[7]~5_combout\);

-- Location: FF_X33_Y27_N23
\kbd_ctrl|ps2_ctrl|sdata[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|sdata[7]~5_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_FROMPS2~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|sdata\(7));

-- Location: LCCOMB_X33_Y27_N4
\kbd_ctrl|ps2_ctrl|FROMPS2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|FROMPS2~0_combout\ = \kbd_ctrl|ps2_ctrl|FROMPS2:count[3]~q\ $ (((\kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~q\) # ((\kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~q\) # (\kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010101010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|FROMPS2:count[3]~q\,
	datab => \kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~q\,
	datac => \kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~q\,
	datad => \kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~q\,
	combout => \kbd_ctrl|ps2_ctrl|FROMPS2~0_combout\);

-- Location: LCCOMB_X32_Y27_N20
\kbd_ctrl|ps2_ctrl|Decoder0~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Decoder0~0_combout\ = (\kbd_ctrl|ps2_ctrl|sigsending~q\) # (!\kbd_ctrl|ps2_ctrl|FROMPS2~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \kbd_ctrl|ps2_ctrl|sigsending~q\,
	datad => \kbd_ctrl|ps2_ctrl|FROMPS2~0_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Decoder0~0_combout\);

-- Location: LCCOMB_X32_Y27_N18
\kbd_ctrl|ps2_ctrl|Decoder0~3\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Decoder0~3_combout\ = (\kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~q\ & (\kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~q\ & (!\kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~q\ & !\kbd_ctrl|ps2_ctrl|Decoder0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~q\,
	datab => \kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~q\,
	datac => \kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~q\,
	datad => \kbd_ctrl|ps2_ctrl|Decoder0~0_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Decoder0~3_combout\);

-- Location: LCCOMB_X32_Y27_N14
\kbd_ctrl|ps2_ctrl|sdata[2]~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|sdata[2]~2_combout\ = (\kbd_ctrl|ps2_ctrl|Decoder0~3_combout\ & (\PS2_DAT~input_o\)) # (!\kbd_ctrl|ps2_ctrl|Decoder0~3_combout\ & ((\kbd_ctrl|ps2_ctrl|sdata\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \PS2_DAT~input_o\,
	datac => \kbd_ctrl|ps2_ctrl|sdata\(2),
	datad => \kbd_ctrl|ps2_ctrl|Decoder0~3_combout\,
	combout => \kbd_ctrl|ps2_ctrl|sdata[2]~2_combout\);

-- Location: FF_X32_Y27_N15
\kbd_ctrl|ps2_ctrl|sdata[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|sdata[2]~2_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_FROMPS2~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|sdata\(2));

-- Location: LCCOMB_X32_Y27_N26
\kbd_ctrl|ps2_ctrl|Decoder0~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Decoder0~1_combout\ = (\kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~q\ & (!\kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~q\ & (!\kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~q\ & !\kbd_ctrl|ps2_ctrl|Decoder0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~q\,
	datab => \kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~q\,
	datac => \kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~q\,
	datad => \kbd_ctrl|ps2_ctrl|Decoder0~0_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Decoder0~1_combout\);

-- Location: LCCOMB_X32_Y27_N4
\kbd_ctrl|ps2_ctrl|sdata[0]~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|sdata[0]~0_combout\ = (\kbd_ctrl|ps2_ctrl|Decoder0~1_combout\ & (\PS2_DAT~input_o\)) # (!\kbd_ctrl|ps2_ctrl|Decoder0~1_combout\ & ((\kbd_ctrl|ps2_ctrl|sdata\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \PS2_DAT~input_o\,
	datac => \kbd_ctrl|ps2_ctrl|sdata\(0),
	datad => \kbd_ctrl|ps2_ctrl|Decoder0~1_combout\,
	combout => \kbd_ctrl|ps2_ctrl|sdata[0]~0_combout\);

-- Location: FF_X32_Y27_N5
\kbd_ctrl|ps2_ctrl|sdata[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|sdata[0]~0_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_FROMPS2~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|sdata\(0));

-- Location: LCCOMB_X32_Y27_N16
\kbd_ctrl|ps2_ctrl|Decoder0~5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Decoder0~5_combout\ = (!\kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~q\ & (\kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~q\ & (!\kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~q\ & !\kbd_ctrl|ps2_ctrl|Decoder0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~q\,
	datab => \kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~q\,
	datac => \kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~q\,
	datad => \kbd_ctrl|ps2_ctrl|Decoder0~0_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Decoder0~5_combout\);

-- Location: LCCOMB_X32_Y27_N8
\kbd_ctrl|ps2_ctrl|sdata[1]~6\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|sdata[1]~6_combout\ = (\kbd_ctrl|ps2_ctrl|Decoder0~5_combout\ & (\PS2_DAT~input_o\)) # (!\kbd_ctrl|ps2_ctrl|Decoder0~5_combout\ & ((\kbd_ctrl|ps2_ctrl|sdata\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \PS2_DAT~input_o\,
	datac => \kbd_ctrl|ps2_ctrl|sdata\(1),
	datad => \kbd_ctrl|ps2_ctrl|Decoder0~5_combout\,
	combout => \kbd_ctrl|ps2_ctrl|sdata[1]~6_combout\);

-- Location: FF_X32_Y27_N9
\kbd_ctrl|ps2_ctrl|sdata[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|sdata[1]~6_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_FROMPS2~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|sdata\(1));

-- Location: LCCOMB_X32_Y27_N6
\kbd_ctrl|ps2_ctrl|FROMPS2~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|FROMPS2~2_combout\ = \PS2_DAT~input_o\ $ (\kbd_ctrl|ps2_ctrl|sdata\(2) $ (\kbd_ctrl|ps2_ctrl|sdata\(0) $ (\kbd_ctrl|ps2_ctrl|sdata\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \PS2_DAT~input_o\,
	datab => \kbd_ctrl|ps2_ctrl|sdata\(2),
	datac => \kbd_ctrl|ps2_ctrl|sdata\(0),
	datad => \kbd_ctrl|ps2_ctrl|sdata\(1),
	combout => \kbd_ctrl|ps2_ctrl|FROMPS2~2_combout\);

-- Location: LCCOMB_X32_Y27_N12
\kbd_ctrl|ps2_ctrl|Decoder0~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Decoder0~2_combout\ = (\kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~q\ & (\kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~q\ & (!\kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~q\ & !\kbd_ctrl|ps2_ctrl|Decoder0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~q\,
	datab => \kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~q\,
	datac => \kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~q\,
	datad => \kbd_ctrl|ps2_ctrl|Decoder0~0_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Decoder0~2_combout\);

-- Location: LCCOMB_X33_Y27_N16
\kbd_ctrl|ps2_ctrl|sdata[4]~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|sdata[4]~1_combout\ = (\kbd_ctrl|ps2_ctrl|Decoder0~2_combout\ & (\PS2_DAT~input_o\)) # (!\kbd_ctrl|ps2_ctrl|Decoder0~2_combout\ & ((\kbd_ctrl|ps2_ctrl|sdata\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \PS2_DAT~input_o\,
	datac => \kbd_ctrl|ps2_ctrl|sdata\(4),
	datad => \kbd_ctrl|ps2_ctrl|Decoder0~2_combout\,
	combout => \kbd_ctrl|ps2_ctrl|sdata[4]~1_combout\);

-- Location: FF_X33_Y27_N17
\kbd_ctrl|ps2_ctrl|sdata[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|sdata[4]~1_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_FROMPS2~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|sdata\(4));

-- Location: LCCOMB_X33_Y27_N2
\kbd_ctrl|ps2_ctrl|Decoder0~4\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Decoder0~4_combout\ = (\kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~q\ & (\kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~q\ & (!\kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~q\ & !\kbd_ctrl|ps2_ctrl|Decoder0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~q\,
	datab => \kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~q\,
	datac => \kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~q\,
	datad => \kbd_ctrl|ps2_ctrl|Decoder0~0_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Decoder0~4_combout\);

-- Location: LCCOMB_X33_Y27_N18
\kbd_ctrl|ps2_ctrl|sdata[5]~3\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|sdata[5]~3_combout\ = (\kbd_ctrl|ps2_ctrl|Decoder0~4_combout\ & (\PS2_DAT~input_o\)) # (!\kbd_ctrl|ps2_ctrl|Decoder0~4_combout\ & ((\kbd_ctrl|ps2_ctrl|sdata\(5))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \PS2_DAT~input_o\,
	datac => \kbd_ctrl|ps2_ctrl|sdata\(5),
	datad => \kbd_ctrl|ps2_ctrl|Decoder0~4_combout\,
	combout => \kbd_ctrl|ps2_ctrl|sdata[5]~3_combout\);

-- Location: FF_X33_Y27_N19
\kbd_ctrl|ps2_ctrl|sdata[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|sdata[5]~3_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_FROMPS2~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|sdata\(5));

-- Location: LCCOMB_X32_Y27_N2
\kbd_ctrl|ps2_ctrl|Decoder0~6\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Decoder0~6_combout\ = (!\kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~q\ & (!\kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~q\ & (\kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~q\ & !\kbd_ctrl|ps2_ctrl|Decoder0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~q\,
	datab => \kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~q\,
	datac => \kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~q\,
	datad => \kbd_ctrl|ps2_ctrl|Decoder0~0_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Decoder0~6_combout\);

-- Location: LCCOMB_X32_Y27_N10
\kbd_ctrl|ps2_ctrl|sdata[3]~7\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|sdata[3]~7_combout\ = (\kbd_ctrl|ps2_ctrl|Decoder0~6_combout\ & (\PS2_DAT~input_o\)) # (!\kbd_ctrl|ps2_ctrl|Decoder0~6_combout\ & ((\kbd_ctrl|ps2_ctrl|sdata\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \PS2_DAT~input_o\,
	datac => \kbd_ctrl|ps2_ctrl|sdata\(3),
	datad => \kbd_ctrl|ps2_ctrl|Decoder0~6_combout\,
	combout => \kbd_ctrl|ps2_ctrl|sdata[3]~7_combout\);

-- Location: FF_X32_Y27_N11
\kbd_ctrl|ps2_ctrl|sdata[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|sdata[3]~7_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_FROMPS2~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|sdata\(3));

-- Location: LCCOMB_X33_Y27_N20
\kbd_ctrl|ps2_ctrl|FROMPS2~3\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|FROMPS2~3_combout\ = \kbd_ctrl|ps2_ctrl|sdata\(6) $ (\kbd_ctrl|ps2_ctrl|sdata\(4) $ (\kbd_ctrl|ps2_ctrl|sdata\(5) $ (\kbd_ctrl|ps2_ctrl|sdata\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|sdata\(6),
	datab => \kbd_ctrl|ps2_ctrl|sdata\(4),
	datac => \kbd_ctrl|ps2_ctrl|sdata\(5),
	datad => \kbd_ctrl|ps2_ctrl|sdata\(3),
	combout => \kbd_ctrl|ps2_ctrl|FROMPS2~3_combout\);

-- Location: LCCOMB_X33_Y27_N24
\kbd_ctrl|ps2_ctrl|parchecked~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|parchecked~0_combout\ = \kbd_ctrl|ps2_ctrl|sdata\(7) $ (\kbd_ctrl|ps2_ctrl|FROMPS2~2_combout\ $ (\kbd_ctrl|ps2_ctrl|FROMPS2~3_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|sdata\(7),
	datac => \kbd_ctrl|ps2_ctrl|FROMPS2~2_combout\,
	datad => \kbd_ctrl|ps2_ctrl|FROMPS2~3_combout\,
	combout => \kbd_ctrl|ps2_ctrl|parchecked~0_combout\);

-- Location: LCCOMB_X33_Y27_N30
\kbd_ctrl|ps2_ctrl|parchecked~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|parchecked~2_combout\ = (\kbd_ctrl|ps2_ctrl|parchecked~1_combout\ & (\kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~q\ & ((\kbd_ctrl|ps2_ctrl|parchecked~0_combout\)))) # (!\kbd_ctrl|ps2_ctrl|parchecked~1_combout\ & 
-- (((\kbd_ctrl|ps2_ctrl|parchecked~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|parchecked~1_combout\,
	datab => \kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~q\,
	datac => \kbd_ctrl|ps2_ctrl|parchecked~q\,
	datad => \kbd_ctrl|ps2_ctrl|parchecked~0_combout\,
	combout => \kbd_ctrl|ps2_ctrl|parchecked~2_combout\);

-- Location: FF_X33_Y27_N31
\kbd_ctrl|ps2_ctrl|parchecked\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \kbd_ctrl|ps2_ctrl|sigtrigger~q\,
	d => \kbd_ctrl|ps2_ctrl|parchecked~2_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_FROMPS2~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|parchecked~q\);

-- Location: IOIBUF_X41_Y15_N22
\KEY[1]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(1),
	o => \KEY[1]~input_o\);

-- Location: LCCOMB_X35_Y26_N22
\kbd_ctrl|ps2_ctrl|odata_rdy\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|odata_rdy~combout\ = LCELL((\kbd_ctrl|ps2_ctrl|parchecked~q\ & \KEY[1]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \kbd_ctrl|ps2_ctrl|parchecked~q\,
	datad => \KEY[1]~input_o\,
	combout => \kbd_ctrl|ps2_ctrl|odata_rdy~combout\);

-- Location: LCCOMB_X32_Y24_N10
\kbd_ctrl|ps2_ctrl|send_rdy~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|send_rdy~1_combout\ = (!\kbd_ctrl|ps2_ctrl|TOPS2~0_combout\ & ((\kbd_ctrl|ps2_ctrl|send_rdy~1_combout\) # (!\KEY[0]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \KEY[0]~input_o\,
	datac => \kbd_ctrl|ps2_ctrl|TOPS2~0_combout\,
	datad => \kbd_ctrl|ps2_ctrl|send_rdy~1_combout\,
	combout => \kbd_ctrl|ps2_ctrl|send_rdy~1_combout\);

-- Location: LCCOMB_X31_Y25_N14
\kbd_ctrl|ps2_ctrl|countclk[1]~18\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|countclk[1]~18_combout\ = (\kbd_ctrl|ps2_ctrl|countclk\(0) & (\kbd_ctrl|ps2_ctrl|countclk\(1) $ (VCC))) # (!\kbd_ctrl|ps2_ctrl|countclk\(0) & (\kbd_ctrl|ps2_ctrl|countclk\(1) & VCC))
-- \kbd_ctrl|ps2_ctrl|countclk[1]~19\ = CARRY((\kbd_ctrl|ps2_ctrl|countclk\(0) & \kbd_ctrl|ps2_ctrl|countclk\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110011010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|countclk\(0),
	datab => \kbd_ctrl|ps2_ctrl|countclk\(1),
	datad => VCC,
	combout => \kbd_ctrl|ps2_ctrl|countclk[1]~18_combout\,
	cout => \kbd_ctrl|ps2_ctrl|countclk[1]~19\);

-- Location: LCCOMB_X31_Y24_N24
\kbd_ctrl|ps2_ctrl|countclk[18]~54\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|countclk[18]~54_combout\ = (!\kbd_ctrl|ps2_ctrl|sigsending~q\ & !\kbd_ctrl|ps2_ctrl|Equal2~5_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|sigsending~q\,
	datad => \kbd_ctrl|ps2_ctrl|Equal2~5_combout\,
	combout => \kbd_ctrl|ps2_ctrl|countclk[18]~54_combout\);

-- Location: FF_X31_Y25_N15
\kbd_ctrl|ps2_ctrl|countclk[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|countclk[1]~18_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_FROMPS2~1_combout\,
	ena => \kbd_ctrl|ps2_ctrl|countclk[18]~54_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|countclk\(1));

-- Location: LCCOMB_X31_Y25_N16
\kbd_ctrl|ps2_ctrl|countclk[2]~20\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|countclk[2]~20_combout\ = (\kbd_ctrl|ps2_ctrl|countclk\(2) & (!\kbd_ctrl|ps2_ctrl|countclk[1]~19\)) # (!\kbd_ctrl|ps2_ctrl|countclk\(2) & ((\kbd_ctrl|ps2_ctrl|countclk[1]~19\) # (GND)))
-- \kbd_ctrl|ps2_ctrl|countclk[2]~21\ = CARRY((!\kbd_ctrl|ps2_ctrl|countclk[1]~19\) # (!\kbd_ctrl|ps2_ctrl|countclk\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|countclk\(2),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|countclk[1]~19\,
	combout => \kbd_ctrl|ps2_ctrl|countclk[2]~20_combout\,
	cout => \kbd_ctrl|ps2_ctrl|countclk[2]~21\);

-- Location: FF_X31_Y25_N17
\kbd_ctrl|ps2_ctrl|countclk[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|countclk[2]~20_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_FROMPS2~1_combout\,
	ena => \kbd_ctrl|ps2_ctrl|countclk[18]~54_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|countclk\(2));

-- Location: LCCOMB_X31_Y25_N18
\kbd_ctrl|ps2_ctrl|countclk[3]~22\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|countclk[3]~22_combout\ = (\kbd_ctrl|ps2_ctrl|countclk\(3) & (\kbd_ctrl|ps2_ctrl|countclk[2]~21\ $ (GND))) # (!\kbd_ctrl|ps2_ctrl|countclk\(3) & (!\kbd_ctrl|ps2_ctrl|countclk[2]~21\ & VCC))
-- \kbd_ctrl|ps2_ctrl|countclk[3]~23\ = CARRY((\kbd_ctrl|ps2_ctrl|countclk\(3) & !\kbd_ctrl|ps2_ctrl|countclk[2]~21\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|countclk\(3),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|countclk[2]~21\,
	combout => \kbd_ctrl|ps2_ctrl|countclk[3]~22_combout\,
	cout => \kbd_ctrl|ps2_ctrl|countclk[3]~23\);

-- Location: FF_X31_Y25_N19
\kbd_ctrl|ps2_ctrl|countclk[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|countclk[3]~22_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_FROMPS2~1_combout\,
	ena => \kbd_ctrl|ps2_ctrl|countclk[18]~54_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|countclk\(3));

-- Location: LCCOMB_X31_Y25_N20
\kbd_ctrl|ps2_ctrl|countclk[4]~24\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|countclk[4]~24_combout\ = (\kbd_ctrl|ps2_ctrl|countclk\(4) & (!\kbd_ctrl|ps2_ctrl|countclk[3]~23\)) # (!\kbd_ctrl|ps2_ctrl|countclk\(4) & ((\kbd_ctrl|ps2_ctrl|countclk[3]~23\) # (GND)))
-- \kbd_ctrl|ps2_ctrl|countclk[4]~25\ = CARRY((!\kbd_ctrl|ps2_ctrl|countclk[3]~23\) # (!\kbd_ctrl|ps2_ctrl|countclk\(4)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|countclk\(4),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|countclk[3]~23\,
	combout => \kbd_ctrl|ps2_ctrl|countclk[4]~24_combout\,
	cout => \kbd_ctrl|ps2_ctrl|countclk[4]~25\);

-- Location: LCCOMB_X31_Y25_N22
\kbd_ctrl|ps2_ctrl|countclk[5]~26\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|countclk[5]~26_combout\ = (\kbd_ctrl|ps2_ctrl|countclk\(5) & (\kbd_ctrl|ps2_ctrl|countclk[4]~25\ $ (GND))) # (!\kbd_ctrl|ps2_ctrl|countclk\(5) & (!\kbd_ctrl|ps2_ctrl|countclk[4]~25\ & VCC))
-- \kbd_ctrl|ps2_ctrl|countclk[5]~27\ = CARRY((\kbd_ctrl|ps2_ctrl|countclk\(5) & !\kbd_ctrl|ps2_ctrl|countclk[4]~25\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|countclk\(5),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|countclk[4]~25\,
	combout => \kbd_ctrl|ps2_ctrl|countclk[5]~26_combout\,
	cout => \kbd_ctrl|ps2_ctrl|countclk[5]~27\);

-- Location: FF_X31_Y25_N23
\kbd_ctrl|ps2_ctrl|countclk[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|countclk[5]~26_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_FROMPS2~1_combout\,
	ena => \kbd_ctrl|ps2_ctrl|countclk[18]~54_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|countclk\(5));

-- Location: LCCOMB_X31_Y25_N24
\kbd_ctrl|ps2_ctrl|countclk[6]~28\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|countclk[6]~28_combout\ = (\kbd_ctrl|ps2_ctrl|countclk\(6) & (!\kbd_ctrl|ps2_ctrl|countclk[5]~27\)) # (!\kbd_ctrl|ps2_ctrl|countclk\(6) & ((\kbd_ctrl|ps2_ctrl|countclk[5]~27\) # (GND)))
-- \kbd_ctrl|ps2_ctrl|countclk[6]~29\ = CARRY((!\kbd_ctrl|ps2_ctrl|countclk[5]~27\) # (!\kbd_ctrl|ps2_ctrl|countclk\(6)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|countclk\(6),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|countclk[5]~27\,
	combout => \kbd_ctrl|ps2_ctrl|countclk[6]~28_combout\,
	cout => \kbd_ctrl|ps2_ctrl|countclk[6]~29\);

-- Location: LCCOMB_X31_Y25_N26
\kbd_ctrl|ps2_ctrl|countclk[7]~30\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|countclk[7]~30_combout\ = (\kbd_ctrl|ps2_ctrl|countclk\(7) & (\kbd_ctrl|ps2_ctrl|countclk[6]~29\ $ (GND))) # (!\kbd_ctrl|ps2_ctrl|countclk\(7) & (!\kbd_ctrl|ps2_ctrl|countclk[6]~29\ & VCC))
-- \kbd_ctrl|ps2_ctrl|countclk[7]~31\ = CARRY((\kbd_ctrl|ps2_ctrl|countclk\(7) & !\kbd_ctrl|ps2_ctrl|countclk[6]~29\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|countclk\(7),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|countclk[6]~29\,
	combout => \kbd_ctrl|ps2_ctrl|countclk[7]~30_combout\,
	cout => \kbd_ctrl|ps2_ctrl|countclk[7]~31\);

-- Location: LCCOMB_X31_Y25_N28
\kbd_ctrl|ps2_ctrl|countclk[8]~32\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|countclk[8]~32_combout\ = (\kbd_ctrl|ps2_ctrl|countclk\(8) & (!\kbd_ctrl|ps2_ctrl|countclk[7]~31\)) # (!\kbd_ctrl|ps2_ctrl|countclk\(8) & ((\kbd_ctrl|ps2_ctrl|countclk[7]~31\) # (GND)))
-- \kbd_ctrl|ps2_ctrl|countclk[8]~33\ = CARRY((!\kbd_ctrl|ps2_ctrl|countclk[7]~31\) # (!\kbd_ctrl|ps2_ctrl|countclk\(8)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|countclk\(8),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|countclk[7]~31\,
	combout => \kbd_ctrl|ps2_ctrl|countclk[8]~32_combout\,
	cout => \kbd_ctrl|ps2_ctrl|countclk[8]~33\);

-- Location: FF_X31_Y25_N29
\kbd_ctrl|ps2_ctrl|countclk[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|countclk[8]~32_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_FROMPS2~1_combout\,
	ena => \kbd_ctrl|ps2_ctrl|countclk[18]~54_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|countclk\(8));

-- Location: LCCOMB_X31_Y24_N0
\kbd_ctrl|ps2_ctrl|countclk[10]~36\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|countclk[10]~36_combout\ = (\kbd_ctrl|ps2_ctrl|countclk\(10) & (!\kbd_ctrl|ps2_ctrl|countclk[9]~35\)) # (!\kbd_ctrl|ps2_ctrl|countclk\(10) & ((\kbd_ctrl|ps2_ctrl|countclk[9]~35\) # (GND)))
-- \kbd_ctrl|ps2_ctrl|countclk[10]~37\ = CARRY((!\kbd_ctrl|ps2_ctrl|countclk[9]~35\) # (!\kbd_ctrl|ps2_ctrl|countclk\(10)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|countclk\(10),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|countclk[9]~35\,
	combout => \kbd_ctrl|ps2_ctrl|countclk[10]~36_combout\,
	cout => \kbd_ctrl|ps2_ctrl|countclk[10]~37\);

-- Location: FF_X31_Y24_N1
\kbd_ctrl|ps2_ctrl|countclk[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|countclk[10]~36_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_FROMPS2~1_combout\,
	ena => \kbd_ctrl|ps2_ctrl|countclk[18]~54_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|countclk\(10));

-- Location: LCCOMB_X31_Y24_N2
\kbd_ctrl|ps2_ctrl|countclk[11]~38\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|countclk[11]~38_combout\ = (\kbd_ctrl|ps2_ctrl|countclk\(11) & (\kbd_ctrl|ps2_ctrl|countclk[10]~37\ $ (GND))) # (!\kbd_ctrl|ps2_ctrl|countclk\(11) & (!\kbd_ctrl|ps2_ctrl|countclk[10]~37\ & VCC))
-- \kbd_ctrl|ps2_ctrl|countclk[11]~39\ = CARRY((\kbd_ctrl|ps2_ctrl|countclk\(11) & !\kbd_ctrl|ps2_ctrl|countclk[10]~37\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|countclk\(11),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|countclk[10]~37\,
	combout => \kbd_ctrl|ps2_ctrl|countclk[11]~38_combout\,
	cout => \kbd_ctrl|ps2_ctrl|countclk[11]~39\);

-- Location: FF_X31_Y24_N3
\kbd_ctrl|ps2_ctrl|countclk[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|countclk[11]~38_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_FROMPS2~1_combout\,
	ena => \kbd_ctrl|ps2_ctrl|countclk[18]~54_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|countclk\(11));

-- Location: LCCOMB_X31_Y24_N4
\kbd_ctrl|ps2_ctrl|countclk[12]~40\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|countclk[12]~40_combout\ = (\kbd_ctrl|ps2_ctrl|countclk\(12) & (!\kbd_ctrl|ps2_ctrl|countclk[11]~39\)) # (!\kbd_ctrl|ps2_ctrl|countclk\(12) & ((\kbd_ctrl|ps2_ctrl|countclk[11]~39\) # (GND)))
-- \kbd_ctrl|ps2_ctrl|countclk[12]~41\ = CARRY((!\kbd_ctrl|ps2_ctrl|countclk[11]~39\) # (!\kbd_ctrl|ps2_ctrl|countclk\(12)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|countclk\(12),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|countclk[11]~39\,
	combout => \kbd_ctrl|ps2_ctrl|countclk[12]~40_combout\,
	cout => \kbd_ctrl|ps2_ctrl|countclk[12]~41\);

-- Location: FF_X31_Y24_N5
\kbd_ctrl|ps2_ctrl|countclk[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|countclk[12]~40_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_FROMPS2~1_combout\,
	ena => \kbd_ctrl|ps2_ctrl|countclk[18]~54_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|countclk\(12));

-- Location: LCCOMB_X31_Y24_N8
\kbd_ctrl|ps2_ctrl|countclk[14]~44\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|countclk[14]~44_combout\ = (\kbd_ctrl|ps2_ctrl|countclk\(14) & (!\kbd_ctrl|ps2_ctrl|countclk[13]~43\)) # (!\kbd_ctrl|ps2_ctrl|countclk\(14) & ((\kbd_ctrl|ps2_ctrl|countclk[13]~43\) # (GND)))
-- \kbd_ctrl|ps2_ctrl|countclk[14]~45\ = CARRY((!\kbd_ctrl|ps2_ctrl|countclk[13]~43\) # (!\kbd_ctrl|ps2_ctrl|countclk\(14)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|countclk\(14),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|countclk[13]~43\,
	combout => \kbd_ctrl|ps2_ctrl|countclk[14]~44_combout\,
	cout => \kbd_ctrl|ps2_ctrl|countclk[14]~45\);

-- Location: FF_X31_Y24_N9
\kbd_ctrl|ps2_ctrl|countclk[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|countclk[14]~44_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_FROMPS2~1_combout\,
	ena => \kbd_ctrl|ps2_ctrl|countclk[18]~54_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|countclk\(14));

-- Location: LCCOMB_X31_Y24_N14
\kbd_ctrl|ps2_ctrl|countclk[17]~50\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|countclk[17]~50_combout\ = (\kbd_ctrl|ps2_ctrl|countclk\(17) & (\kbd_ctrl|ps2_ctrl|countclk[16]~49\ $ (GND))) # (!\kbd_ctrl|ps2_ctrl|countclk\(17) & (!\kbd_ctrl|ps2_ctrl|countclk[16]~49\ & VCC))
-- \kbd_ctrl|ps2_ctrl|countclk[17]~51\ = CARRY((\kbd_ctrl|ps2_ctrl|countclk\(17) & !\kbd_ctrl|ps2_ctrl|countclk[16]~49\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|countclk\(17),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|countclk[16]~49\,
	combout => \kbd_ctrl|ps2_ctrl|countclk[17]~50_combout\,
	cout => \kbd_ctrl|ps2_ctrl|countclk[17]~51\);

-- Location: FF_X31_Y24_N15
\kbd_ctrl|ps2_ctrl|countclk[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|countclk[17]~50_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_FROMPS2~1_combout\,
	ena => \kbd_ctrl|ps2_ctrl|countclk[18]~54_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|countclk\(17));

-- Location: LCCOMB_X31_Y24_N16
\kbd_ctrl|ps2_ctrl|countclk[18]~52\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|countclk[18]~52_combout\ = \kbd_ctrl|ps2_ctrl|countclk[17]~51\ $ (\kbd_ctrl|ps2_ctrl|countclk\(18))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \kbd_ctrl|ps2_ctrl|countclk\(18),
	cin => \kbd_ctrl|ps2_ctrl|countclk[17]~51\,
	combout => \kbd_ctrl|ps2_ctrl|countclk[18]~52_combout\);

-- Location: FF_X31_Y24_N17
\kbd_ctrl|ps2_ctrl|countclk[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|countclk[18]~52_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_FROMPS2~1_combout\,
	ena => \kbd_ctrl|ps2_ctrl|countclk[18]~54_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|countclk\(18));

-- Location: LCCOMB_X31_Y24_N28
\kbd_ctrl|ps2_ctrl|Equal2~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Equal2~2_combout\ = (\kbd_ctrl|ps2_ctrl|countclk\(9) & (\kbd_ctrl|ps2_ctrl|countclk\(10) & (!\kbd_ctrl|ps2_ctrl|countclk\(11) & \kbd_ctrl|ps2_ctrl|countclk\(8))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|countclk\(9),
	datab => \kbd_ctrl|ps2_ctrl|countclk\(10),
	datac => \kbd_ctrl|ps2_ctrl|countclk\(11),
	datad => \kbd_ctrl|ps2_ctrl|countclk\(8),
	combout => \kbd_ctrl|ps2_ctrl|Equal2~2_combout\);

-- Location: LCCOMB_X31_Y25_N2
\kbd_ctrl|ps2_ctrl|Equal2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Equal2~0_combout\ = (!\kbd_ctrl|ps2_ctrl|countclk\(0) & (!\kbd_ctrl|ps2_ctrl|countclk\(3) & (!\kbd_ctrl|ps2_ctrl|countclk\(1) & !\kbd_ctrl|ps2_ctrl|countclk\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|countclk\(0),
	datab => \kbd_ctrl|ps2_ctrl|countclk\(3),
	datac => \kbd_ctrl|ps2_ctrl|countclk\(1),
	datad => \kbd_ctrl|ps2_ctrl|countclk\(2),
	combout => \kbd_ctrl|ps2_ctrl|Equal2~0_combout\);

-- Location: FF_X31_Y25_N25
\kbd_ctrl|ps2_ctrl|countclk[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|countclk[6]~28_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_FROMPS2~1_combout\,
	ena => \kbd_ctrl|ps2_ctrl|countclk[18]~54_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|countclk\(6));

-- Location: FF_X31_Y25_N27
\kbd_ctrl|ps2_ctrl|countclk[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|countclk[7]~30_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_FROMPS2~1_combout\,
	ena => \kbd_ctrl|ps2_ctrl|countclk[18]~54_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|countclk\(7));

-- Location: FF_X31_Y25_N21
\kbd_ctrl|ps2_ctrl|countclk[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|countclk[4]~24_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_FROMPS2~1_combout\,
	ena => \kbd_ctrl|ps2_ctrl|countclk[18]~54_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|countclk\(4));

-- Location: LCCOMB_X31_Y25_N8
\kbd_ctrl|ps2_ctrl|Equal2~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Equal2~1_combout\ = (!\kbd_ctrl|ps2_ctrl|countclk\(5) & (\kbd_ctrl|ps2_ctrl|countclk\(6) & (!\kbd_ctrl|ps2_ctrl|countclk\(7) & !\kbd_ctrl|ps2_ctrl|countclk\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|countclk\(5),
	datab => \kbd_ctrl|ps2_ctrl|countclk\(6),
	datac => \kbd_ctrl|ps2_ctrl|countclk\(7),
	datad => \kbd_ctrl|ps2_ctrl|countclk\(4),
	combout => \kbd_ctrl|ps2_ctrl|Equal2~1_combout\);

-- Location: LCCOMB_X31_Y24_N20
\kbd_ctrl|ps2_ctrl|Equal2~4\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Equal2~4_combout\ = (\kbd_ctrl|ps2_ctrl|Equal2~3_combout\ & (\kbd_ctrl|ps2_ctrl|Equal2~2_combout\ & (\kbd_ctrl|ps2_ctrl|Equal2~0_combout\ & \kbd_ctrl|ps2_ctrl|Equal2~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|Equal2~3_combout\,
	datab => \kbd_ctrl|ps2_ctrl|Equal2~2_combout\,
	datac => \kbd_ctrl|ps2_ctrl|Equal2~0_combout\,
	datad => \kbd_ctrl|ps2_ctrl|Equal2~1_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Equal2~4_combout\);

-- Location: LCCOMB_X31_Y24_N18
\kbd_ctrl|ps2_ctrl|Equal2~5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Equal2~5_combout\ = (!\kbd_ctrl|ps2_ctrl|countclk\(16) & (\kbd_ctrl|ps2_ctrl|countclk\(18) & (!\kbd_ctrl|ps2_ctrl|countclk\(17) & \kbd_ctrl|ps2_ctrl|Equal2~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|countclk\(16),
	datab => \kbd_ctrl|ps2_ctrl|countclk\(18),
	datac => \kbd_ctrl|ps2_ctrl|countclk\(17),
	datad => \kbd_ctrl|ps2_ctrl|Equal2~4_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Equal2~5_combout\);

-- Location: LCCOMB_X31_Y24_N26
\kbd_ctrl|ps2_ctrl|send_rdy~3\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|send_rdy~3_combout\ = \kbd_ctrl|ps2_ctrl|send_rdy~1_combout\ $ (((\kbd_ctrl|ps2_ctrl|Equal2~5_combout\) # (\kbd_ctrl|ps2_ctrl|send_rdy~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|send_rdy~1_combout\,
	datac => \kbd_ctrl|ps2_ctrl|Equal2~5_combout\,
	datad => \kbd_ctrl|ps2_ctrl|send_rdy~2_combout\,
	combout => \kbd_ctrl|ps2_ctrl|send_rdy~3_combout\);

-- Location: LCCOMB_X32_Y24_N8
\kbd_ctrl|ps2_ctrl|send_rdy~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|send_rdy~0_combout\ = (\kbd_ctrl|ps2_ctrl|TOPS2~0_combout\) # (!\KEY[0]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \KEY[0]~input_o\,
	datad => \kbd_ctrl|ps2_ctrl|TOPS2~0_combout\,
	combout => \kbd_ctrl|ps2_ctrl|send_rdy~0_combout\);

-- Location: FF_X31_Y24_N27
\kbd_ctrl|ps2_ctrl|send_rdy~_emulated\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|send_rdy~3_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_send_rdy~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|send_rdy~_emulated_q\);

-- Location: LCCOMB_X32_Y24_N22
\kbd_ctrl|ps2_ctrl|send_rdy~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|send_rdy~2_combout\ = (!\kbd_ctrl|ps2_ctrl|TOPS2~0_combout\ & ((\kbd_ctrl|ps2_ctrl|send_rdy~1_combout\ $ (\kbd_ctrl|ps2_ctrl|send_rdy~_emulated_q\)) # (!\KEY[0]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001111011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|send_rdy~1_combout\,
	datab => \KEY[0]~input_o\,
	datac => \kbd_ctrl|ps2_ctrl|send_rdy~_emulated_q\,
	datad => \kbd_ctrl|ps2_ctrl|TOPS2~0_combout\,
	combout => \kbd_ctrl|ps2_ctrl|send_rdy~2_combout\);

-- Location: LCCOMB_X33_Y26_N20
\kbd_ctrl|Selector9~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Selector9~0_combout\ = (!\kbd_ctrl|cmdstate.SETLIGHTS~q\ & (!\kbd_ctrl|cmdstate.SETCMD~q\ & \kbd_ctrl|ps2_ctrl|send_rdy~2_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|cmdstate.SETLIGHTS~q\,
	datab => \kbd_ctrl|cmdstate.SETCMD~q\,
	datac => \kbd_ctrl|ps2_ctrl|send_rdy~2_combout\,
	combout => \kbd_ctrl|Selector9~0_combout\);

-- Location: LCCOMB_X30_Y27_N6
\lights~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \lights~1_combout\ = ((!lights(2) & ((!lights(0)) # (!\dir~q\)))) # (!lights(1))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001001111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dir~q\,
	datab => lights(2),
	datac => lights(0),
	datad => lights(1),
	combout => \lights~1_combout\);

-- Location: LCCOMB_X33_Y26_N30
\kbd_ctrl|newdata~feeder\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|newdata~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	combout => \kbd_ctrl|newdata~feeder_combout\);

-- Location: LCCOMB_X32_Y27_N28
\kbd_ctrl|Equal21~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal21~0_combout\ = (\kbd_ctrl|ps2_ctrl|sdata\(0)) # (((!\kbd_ctrl|ps2_ctrl|sdata\(3)) # (!\kbd_ctrl|ps2_ctrl|sdata\(1))) # (!\kbd_ctrl|ps2_ctrl|sdata\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|sdata\(0),
	datab => \kbd_ctrl|ps2_ctrl|sdata\(2),
	datac => \kbd_ctrl|ps2_ctrl|sdata\(1),
	datad => \kbd_ctrl|ps2_ctrl|sdata\(3),
	combout => \kbd_ctrl|Equal21~0_combout\);

-- Location: LCCOMB_X32_Y27_N24
\kbd_ctrl|ps2_ctrl|Decoder0~7\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Decoder0~7_combout\ = (\kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~q\ & (\kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~q\ & (\kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~q\ & !\kbd_ctrl|ps2_ctrl|Decoder0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~q\,
	datab => \kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~q\,
	datac => \kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~q\,
	datad => \kbd_ctrl|ps2_ctrl|Decoder0~0_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Decoder0~7_combout\);

-- Location: LCCOMB_X33_Y27_N26
\kbd_ctrl|ps2_ctrl|sdata[6]~8\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|sdata[6]~8_combout\ = (\kbd_ctrl|ps2_ctrl|Decoder0~7_combout\ & (\PS2_DAT~input_o\)) # (!\kbd_ctrl|ps2_ctrl|Decoder0~7_combout\ & ((\kbd_ctrl|ps2_ctrl|sdata\(6))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \PS2_DAT~input_o\,
	datac => \kbd_ctrl|ps2_ctrl|sdata\(6),
	datad => \kbd_ctrl|ps2_ctrl|Decoder0~7_combout\,
	combout => \kbd_ctrl|ps2_ctrl|sdata[6]~8_combout\);

-- Location: FF_X33_Y27_N27
\kbd_ctrl|ps2_ctrl|sdata[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|sdata[6]~8_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_FROMPS2~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|sdata\(6));

-- Location: LCCOMB_X33_Y27_N6
\kbd_ctrl|Equal21~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal21~1_combout\ = (((!\kbd_ctrl|ps2_ctrl|sdata\(4)) # (!\kbd_ctrl|ps2_ctrl|sdata\(6))) # (!\kbd_ctrl|ps2_ctrl|sdata\(5))) # (!\kbd_ctrl|ps2_ctrl|sdata\(7))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|sdata\(7),
	datab => \kbd_ctrl|ps2_ctrl|sdata\(5),
	datac => \kbd_ctrl|ps2_ctrl|sdata\(6),
	datad => \kbd_ctrl|ps2_ctrl|sdata\(4),
	combout => \kbd_ctrl|Equal21~1_combout\);

-- Location: LCCOMB_X33_Y26_N2
\kbd_ctrl|Equal21~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal21~2_combout\ = (\kbd_ctrl|Equal21~0_combout\) # (\kbd_ctrl|Equal21~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \kbd_ctrl|Equal21~0_combout\,
	datad => \kbd_ctrl|Equal21~1_combout\,
	combout => \kbd_ctrl|Equal21~2_combout\);

-- Location: LCCOMB_X33_Y26_N14
\kbd_ctrl|cmdstate.CLEAR~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|cmdstate.CLEAR~0_combout\ = (\kbd_ctrl|cmdstate.CLEAR~q\) # ((\kbd_ctrl|cmdstate.WAITACK1~q\ & (\kbd_ctrl|ps2_ctrl|odata_rdy~combout\ & \kbd_ctrl|Equal21~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|cmdstate.WAITACK1~q\,
	datab => \kbd_ctrl|ps2_ctrl|odata_rdy~combout\,
	datac => \kbd_ctrl|cmdstate.CLEAR~q\,
	datad => \kbd_ctrl|Equal21~2_combout\,
	combout => \kbd_ctrl|cmdstate.CLEAR~0_combout\);

-- Location: FF_X33_Y26_N15
\kbd_ctrl|cmdstate.CLEAR\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|cmdstate.CLEAR~0_combout\,
	clrn => \kbd_ctrl|ALT_INV_process_15~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|cmdstate.CLEAR~q\);

-- Location: FF_X33_Y26_N23
\kbd_ctrl|sigsending\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	asdata => \kbd_ctrl|cmdstate.CLEAR~q\,
	clrn => \kbd_ctrl|ALT_INV_process_15~0_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|sigsending~q\);

-- Location: LCCOMB_X33_Y26_N22
\kbd_ctrl|process_0~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|process_0~0_combout\ = (!\KEY[0]~input_o\) # (!\kbd_ctrl|sigsending~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \kbd_ctrl|sigsending~q\,
	datad => \KEY[0]~input_o\,
	combout => \kbd_ctrl|process_0~0_combout\);

-- Location: FF_X32_Y26_N15
\kbd_ctrl|state.DECODE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	asdata => \kbd_ctrl|state.FETCH~q\,
	clrn => \kbd_ctrl|ALT_INV_process_0~0_combout\,
	sload => VCC,
	ena => \KEY[1]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|state.DECODE~q\);

-- Location: LCCOMB_X33_Y26_N8
\kbd_ctrl|process_13~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|process_13~0_combout\ = ((\kbd_ctrl|state.DECODE~q\) # (!\kbd_ctrl|sigsending~q\)) # (!\KEY[0]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \KEY[0]~input_o\,
	datac => \kbd_ctrl|sigsending~q\,
	datad => \kbd_ctrl|state.DECODE~q\,
	combout => \kbd_ctrl|process_13~0_combout\);

-- Location: FF_X33_Y26_N31
\kbd_ctrl|newdata\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \kbd_ctrl|ps2_ctrl|odata_rdy~combout\,
	d => \kbd_ctrl|newdata~feeder_combout\,
	clrn => \kbd_ctrl|ALT_INV_process_13~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|newdata~q\);

-- Location: FF_X29_Y26_N23
\kbd_ctrl|fetchdata[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	asdata => \kbd_ctrl|ps2_ctrl|sdata\(0),
	clrn => \KEY[0]~inputclkctrl_outclk\,
	sload => VCC,
	ena => \kbd_ctrl|state.FETCH~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|fetchdata\(0));

-- Location: FF_X29_Y26_N1
\kbd_ctrl|fetchdata[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	asdata => \kbd_ctrl|ps2_ctrl|sdata\(4),
	clrn => \KEY[0]~inputclkctrl_outclk\,
	sload => VCC,
	ena => \kbd_ctrl|state.FETCH~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|fetchdata\(4));

-- Location: LCCOMB_X35_Y26_N20
\kbd_ctrl|state.EXT1~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|state.EXT1~0_combout\ = (\kbd_ctrl|Equal0~1_combout\ & (\kbd_ctrl|fetchdata\(0) & (\kbd_ctrl|state.DECODE~q\ & !\kbd_ctrl|fetchdata\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|Equal0~1_combout\,
	datab => \kbd_ctrl|fetchdata\(0),
	datac => \kbd_ctrl|state.DECODE~q\,
	datad => \kbd_ctrl|fetchdata\(4),
	combout => \kbd_ctrl|state.EXT1~0_combout\);

-- Location: FF_X35_Y26_N21
\kbd_ctrl|state.EXT1\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|state.EXT1~0_combout\,
	clrn => \kbd_ctrl|ALT_INV_process_0~0_combout\,
	ena => \KEY[1]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|state.EXT1~q\);

-- Location: FF_X29_Y26_N15
\kbd_ctrl|fetchdata[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	asdata => \kbd_ctrl|ps2_ctrl|sdata\(7),
	clrn => \KEY[0]~inputclkctrl_outclk\,
	sload => VCC,
	ena => \kbd_ctrl|state.FETCH~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|fetchdata\(7));

-- Location: FF_X29_Y26_N27
\kbd_ctrl|fetchdata[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	asdata => \kbd_ctrl|ps2_ctrl|sdata\(3),
	clrn => \KEY[0]~inputclkctrl_outclk\,
	sload => VCC,
	ena => \kbd_ctrl|state.FETCH~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|fetchdata\(3));

-- Location: FF_X29_Y26_N13
\kbd_ctrl|fetchdata[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	asdata => \kbd_ctrl|ps2_ctrl|sdata\(6),
	clrn => \KEY[0]~inputclkctrl_outclk\,
	sload => VCC,
	ena => \kbd_ctrl|state.FETCH~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|fetchdata\(6));

-- Location: FF_X29_Y26_N7
\kbd_ctrl|fetchdata[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	asdata => \kbd_ctrl|ps2_ctrl|sdata\(2),
	clrn => \KEY[0]~inputclkctrl_outclk\,
	sload => VCC,
	ena => \kbd_ctrl|state.FETCH~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|fetchdata\(2));

-- Location: LCCOMB_X29_Y26_N12
\kbd_ctrl|Equal0~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal0~0_combout\ = (!\kbd_ctrl|fetchdata\(1) & (!\kbd_ctrl|fetchdata\(3) & (\kbd_ctrl|fetchdata\(6) & !\kbd_ctrl|fetchdata\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|fetchdata\(1),
	datab => \kbd_ctrl|fetchdata\(3),
	datac => \kbd_ctrl|fetchdata\(6),
	datad => \kbd_ctrl|fetchdata\(2),
	combout => \kbd_ctrl|Equal0~0_combout\);

-- Location: LCCOMB_X32_Y26_N14
\kbd_ctrl|Equal0~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal0~1_combout\ = (\kbd_ctrl|fetchdata\(5) & (\kbd_ctrl|fetchdata\(7) & \kbd_ctrl|Equal0~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|fetchdata\(5),
	datab => \kbd_ctrl|fetchdata\(7),
	datad => \kbd_ctrl|Equal0~0_combout\,
	combout => \kbd_ctrl|Equal0~1_combout\);

-- Location: LCCOMB_X32_Y26_N0
\kbd_ctrl|state.CODE~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|state.CODE~0_combout\ = (\kbd_ctrl|state.DECODE~q\ & (((\kbd_ctrl|fetchdata\(0) & \kbd_ctrl|fetchdata\(4))) # (!\kbd_ctrl|Equal0~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|fetchdata\(0),
	datab => \kbd_ctrl|Equal0~1_combout\,
	datac => \kbd_ctrl|state.DECODE~q\,
	datad => \kbd_ctrl|fetchdata\(4),
	combout => \kbd_ctrl|state.CODE~0_combout\);

-- Location: FF_X32_Y26_N1
\kbd_ctrl|state.CODE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|state.CODE~0_combout\,
	clrn => \kbd_ctrl|ALT_INV_process_0~0_combout\,
	ena => \KEY[1]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|state.CODE~q\);

-- Location: LCCOMB_X32_Y25_N8
\kbd_ctrl|state.CLRDP~feeder\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|state.CLRDP~feeder_combout\ = \kbd_ctrl|state.CODE~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \kbd_ctrl|state.CODE~q\,
	combout => \kbd_ctrl|state.CLRDP~feeder_combout\);

-- Location: FF_X32_Y25_N9
\kbd_ctrl|state.CLRDP\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|state.CLRDP~feeder_combout\,
	clrn => \kbd_ctrl|ALT_INV_process_0~0_combout\,
	ena => \KEY[1]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|state.CLRDP~q\);

-- Location: LCCOMB_X29_Y26_N14
\kbd_ctrl|Equal0~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal0~2_combout\ = (!\kbd_ctrl|fetchdata\(0) & (\kbd_ctrl|fetchdata\(5) & (\kbd_ctrl|fetchdata\(7) & \kbd_ctrl|Equal0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|fetchdata\(0),
	datab => \kbd_ctrl|fetchdata\(5),
	datac => \kbd_ctrl|fetchdata\(7),
	datad => \kbd_ctrl|Equal0~0_combout\,
	combout => \kbd_ctrl|Equal0~2_combout\);

-- Location: LCCOMB_X35_Y26_N30
\kbd_ctrl|nstate.EXT0~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|nstate.EXT0~0_combout\ = (!\kbd_ctrl|fetchdata\(4) & (\kbd_ctrl|state.DECODE~q\ & \kbd_ctrl|Equal0~2_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|fetchdata\(4),
	datac => \kbd_ctrl|state.DECODE~q\,
	datad => \kbd_ctrl|Equal0~2_combout\,
	combout => \kbd_ctrl|nstate.EXT0~0_combout\);

-- Location: FF_X35_Y26_N31
\kbd_ctrl|state.EXT0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|nstate.EXT0~0_combout\,
	clrn => \kbd_ctrl|ALT_INV_process_0~0_combout\,
	ena => \KEY[1]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|state.EXT0~q\);

-- Location: LCCOMB_X32_Y25_N10
\kbd_ctrl|Selector0~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Selector0~0_combout\ = (\kbd_ctrl|state.RELEASE~q\) # ((\kbd_ctrl|state.EXT0~q\) # ((!\kbd_ctrl|state.IDLE~q\ & !\kbd_ctrl|newdata~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110101011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|state.RELEASE~q\,
	datab => \kbd_ctrl|state.IDLE~q\,
	datac => \kbd_ctrl|newdata~q\,
	datad => \kbd_ctrl|state.EXT0~q\,
	combout => \kbd_ctrl|Selector0~0_combout\);

-- Location: LCCOMB_X32_Y25_N0
\kbd_ctrl|Selector0~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Selector0~1_combout\ = (!\kbd_ctrl|state.EXT1~q\ & (!\kbd_ctrl|state.CLRDP~q\ & !\kbd_ctrl|Selector0~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|state.EXT1~q\,
	datac => \kbd_ctrl|state.CLRDP~q\,
	datad => \kbd_ctrl|Selector0~0_combout\,
	combout => \kbd_ctrl|Selector0~1_combout\);

-- Location: FF_X32_Y25_N1
\kbd_ctrl|state.IDLE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|Selector0~1_combout\,
	clrn => \kbd_ctrl|ALT_INV_process_0~0_combout\,
	ena => \KEY[1]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|state.IDLE~q\);

-- Location: LCCOMB_X35_Y26_N14
\kbd_ctrl|nstate.FETCH~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|nstate.FETCH~0_combout\ = (\kbd_ctrl|newdata~q\ & !\kbd_ctrl|state.IDLE~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \kbd_ctrl|newdata~q\,
	datad => \kbd_ctrl|state.IDLE~q\,
	combout => \kbd_ctrl|nstate.FETCH~0_combout\);

-- Location: FF_X35_Y26_N15
\kbd_ctrl|state.FETCH\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|nstate.FETCH~0_combout\,
	clrn => \kbd_ctrl|ALT_INV_process_0~0_combout\,
	ena => \KEY[1]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|state.FETCH~q\);

-- Location: FF_X29_Y26_N5
\kbd_ctrl|fetchdata[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	asdata => \kbd_ctrl|ps2_ctrl|sdata\(5),
	clrn => \KEY[0]~inputclkctrl_outclk\,
	sload => VCC,
	ena => \kbd_ctrl|state.FETCH~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|fetchdata\(5));

-- Location: LCCOMB_X32_Y25_N22
\kbd_ctrl|process_4~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|process_4~0_combout\ = (\kbd_ctrl|state.CLRDP~q\) # (!\KEY[0]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111111001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|state.CLRDP~q\,
	datac => \KEY[0]~input_o\,
	combout => \kbd_ctrl|process_4~0_combout\);

-- Location: LCCOMB_X32_Y25_N6
\kbd_ctrl|relbt\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|relbt~combout\ = (!\kbd_ctrl|process_4~0_combout\ & ((\kbd_ctrl|state.RELEASE~q\) # (\kbd_ctrl|relbt~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|state.RELEASE~q\,
	datac => \kbd_ctrl|process_4~0_combout\,
	datad => \kbd_ctrl|relbt~combout\,
	combout => \kbd_ctrl|relbt~combout\);

-- Location: LCCOMB_X32_Y25_N28
\kbd_ctrl|selE0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|selE0~combout\ = (!\kbd_ctrl|process_4~0_combout\ & ((\kbd_ctrl|state.EXT0~q\) # (\kbd_ctrl|selE0~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|state.EXT0~q\,
	datac => \kbd_ctrl|process_4~0_combout\,
	datad => \kbd_ctrl|selE0~combout\,
	combout => \kbd_ctrl|selE0~combout\);

-- Location: LCCOMB_X32_Y25_N12
\kbd_ctrl|selbt\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|selbt~combout\ = (!\kbd_ctrl|process_4~0_combout\ & ((\kbd_ctrl|state.CODE~q\) # (\kbd_ctrl|selbt~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|state.CODE~q\,
	datac => \kbd_ctrl|process_4~0_combout\,
	datad => \kbd_ctrl|selbt~combout\,
	combout => \kbd_ctrl|selbt~combout\);

-- Location: LCCOMB_X29_Y26_N22
\kbd_ctrl|KEY0~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|KEY0~0_combout\ = (\kbd_ctrl|relbt~combout\ & \kbd_ctrl|selbt~combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|relbt~combout\,
	datad => \kbd_ctrl|selbt~combout\,
	combout => \kbd_ctrl|KEY0~0_combout\);

-- Location: FF_X29_Y26_N17
\kbd_ctrl|fetchdata[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	asdata => \kbd_ctrl|ps2_ctrl|sdata\(1),
	clrn => \KEY[0]~inputclkctrl_outclk\,
	sload => VCC,
	ena => \kbd_ctrl|state.FETCH~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|fetchdata\(1));

-- Location: LCCOMB_X29_Y26_N18
\kbd_ctrl|Equal10~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal10~0_combout\ = (\kbd_ctrl|fetchdata\(6) & (\kbd_ctrl|fetchdata\(3) & (\kbd_ctrl|fetchdata\(0) & !\kbd_ctrl|fetchdata\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|fetchdata\(6),
	datab => \kbd_ctrl|fetchdata\(3),
	datac => \kbd_ctrl|fetchdata\(0),
	datad => \kbd_ctrl|fetchdata\(1),
	combout => \kbd_ctrl|Equal10~0_combout\);

-- Location: LCCOMB_X29_Y26_N0
\kbd_ctrl|Equal10~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal10~1_combout\ = (\kbd_ctrl|Equal6~0_combout\ & (!\kbd_ctrl|selE0~combout\ & \kbd_ctrl|Equal10~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|Equal6~0_combout\,
	datab => \kbd_ctrl|selE0~combout\,
	datad => \kbd_ctrl|Equal10~0_combout\,
	combout => \kbd_ctrl|Equal10~1_combout\);

-- Location: FF_X28_Y26_N23
\kbd_ctrl|key0code[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	asdata => \kbd_ctrl|fetchdata\(1),
	clrn => \kbd_ctrl|ALT_INV_KEY0~2_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key0en~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|key0code\(1));

-- Location: FF_X28_Y26_N13
\kbd_ctrl|key0code[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	asdata => \kbd_ctrl|fetchdata\(4),
	clrn => \kbd_ctrl|ALT_INV_KEY0~2_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key0en~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|key0code\(4));

-- Location: LCCOMB_X28_Y26_N22
\kbd_ctrl|Equal17~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal17~1_combout\ = (\kbd_ctrl|key0code\(13)) # (((\kbd_ctrl|key0code\(1)) # (\kbd_ctrl|key0code\(4))) # (!\kbd_ctrl|Equal17~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(13),
	datab => \kbd_ctrl|Equal17~0_combout\,
	datac => \kbd_ctrl|key0code\(1),
	datad => \kbd_ctrl|key0code\(4),
	combout => \kbd_ctrl|Equal17~1_combout\);

-- Location: FF_X30_Y26_N5
\kbd_ctrl|key1code[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	asdata => \kbd_ctrl|fetchdata\(3),
	clrn => \kbd_ctrl|ALT_INV_KEY1~8_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key1en~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|key1code\(3));

-- Location: LCCOMB_X31_Y26_N24
\kbd_ctrl|key1code[2]~feeder\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|key1code[2]~feeder_combout\ = \kbd_ctrl|fetchdata\(2)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \kbd_ctrl|fetchdata\(2),
	combout => \kbd_ctrl|key1code[2]~feeder_combout\);

-- Location: FF_X31_Y26_N25
\kbd_ctrl|key1code[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|key1code[2]~feeder_combout\,
	clrn => \kbd_ctrl|ALT_INV_KEY1~8_combout\,
	ena => \kbd_ctrl|key1en~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|key1code\(2));

-- Location: LCCOMB_X30_Y26_N4
\kbd_ctrl|Equal4~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal4~2_combout\ = (\kbd_ctrl|fetchdata\(3) & (\kbd_ctrl|key1code\(3) & (\kbd_ctrl|fetchdata\(2) $ (!\kbd_ctrl|key1code\(2))))) # (!\kbd_ctrl|fetchdata\(3) & (!\kbd_ctrl|key1code\(3) & (\kbd_ctrl|fetchdata\(2) $ (!\kbd_ctrl|key1code\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000010000100001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|fetchdata\(3),
	datab => \kbd_ctrl|fetchdata\(2),
	datac => \kbd_ctrl|key1code\(3),
	datad => \kbd_ctrl|key1code\(2),
	combout => \kbd_ctrl|Equal4~2_combout\);

-- Location: FF_X30_Y26_N3
\kbd_ctrl|key1code[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	asdata => \kbd_ctrl|fetchdata\(0),
	clrn => \kbd_ctrl|ALT_INV_KEY1~8_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key1en~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|key1code\(0));

-- Location: LCCOMB_X30_Y26_N8
\kbd_ctrl|Equal4~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal4~1_combout\ = (\kbd_ctrl|fetchdata\(1) & (\kbd_ctrl|key1code\(1) & (\kbd_ctrl|key1code\(0) $ (!\kbd_ctrl|fetchdata\(0))))) # (!\kbd_ctrl|fetchdata\(1) & (!\kbd_ctrl|key1code\(1) & (\kbd_ctrl|key1code\(0) $ (!\kbd_ctrl|fetchdata\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000010000100001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|fetchdata\(1),
	datab => \kbd_ctrl|key1code\(0),
	datac => \kbd_ctrl|key1code\(1),
	datad => \kbd_ctrl|fetchdata\(0),
	combout => \kbd_ctrl|Equal4~1_combout\);

-- Location: FF_X30_Y26_N13
\kbd_ctrl|key1code[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	asdata => \kbd_ctrl|fetchdata\(4),
	clrn => \kbd_ctrl|ALT_INV_KEY1~8_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key1en~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|key1code\(4));

-- Location: LCCOMB_X30_Y26_N10
\kbd_ctrl|Equal4~3\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal4~3_combout\ = (\kbd_ctrl|fetchdata\(4) & (\kbd_ctrl|key1code\(4) & (\kbd_ctrl|fetchdata\(5) $ (!\kbd_ctrl|key1code\(5))))) # (!\kbd_ctrl|fetchdata\(4) & (!\kbd_ctrl|key1code\(4) & (\kbd_ctrl|fetchdata\(5) $ (!\kbd_ctrl|key1code\(5)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000001001000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|fetchdata\(4),
	datab => \kbd_ctrl|fetchdata\(5),
	datac => \kbd_ctrl|key1code\(5),
	datad => \kbd_ctrl|key1code\(4),
	combout => \kbd_ctrl|Equal4~3_combout\);

-- Location: LCCOMB_X30_Y26_N22
\kbd_ctrl|Equal4~5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal4~5_combout\ = (\kbd_ctrl|Equal4~4_combout\ & (\kbd_ctrl|Equal4~2_combout\ & (\kbd_ctrl|Equal4~1_combout\ & \kbd_ctrl|Equal4~3_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|Equal4~4_combout\,
	datab => \kbd_ctrl|Equal4~2_combout\,
	datac => \kbd_ctrl|Equal4~1_combout\,
	datad => \kbd_ctrl|Equal4~3_combout\,
	combout => \kbd_ctrl|Equal4~5_combout\);

-- Location: FF_X28_Y26_N17
\kbd_ctrl|key0code[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	asdata => \kbd_ctrl|fetchdata\(0),
	clrn => \kbd_ctrl|ALT_INV_KEY0~2_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key0en~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|key0code\(0));

-- Location: LCCOMB_X29_Y26_N8
\kbd_ctrl|Equal14~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal14~0_combout\ = (\kbd_ctrl|fetchdata\(0) & (\kbd_ctrl|key0code\(0) & (\kbd_ctrl|fetchdata\(1) $ (!\kbd_ctrl|key0code\(1))))) # (!\kbd_ctrl|fetchdata\(0) & (!\kbd_ctrl|key0code\(0) & (\kbd_ctrl|fetchdata\(1) $ (!\kbd_ctrl|key0code\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000010000100001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|fetchdata\(0),
	datab => \kbd_ctrl|fetchdata\(1),
	datac => \kbd_ctrl|key0code\(0),
	datad => \kbd_ctrl|key0code\(1),
	combout => \kbd_ctrl|Equal14~0_combout\);

-- Location: LCCOMB_X29_Y26_N4
\kbd_ctrl|Equal14~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal14~2_combout\ = (\kbd_ctrl|key0code\(5) & (\kbd_ctrl|fetchdata\(5) & (\kbd_ctrl|fetchdata\(4) $ (!\kbd_ctrl|key0code\(4))))) # (!\kbd_ctrl|key0code\(5) & (!\kbd_ctrl|fetchdata\(5) & (\kbd_ctrl|fetchdata\(4) $ (!\kbd_ctrl|key0code\(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000010000100001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(5),
	datab => \kbd_ctrl|fetchdata\(4),
	datac => \kbd_ctrl|fetchdata\(5),
	datad => \kbd_ctrl|key0code\(4),
	combout => \kbd_ctrl|Equal14~2_combout\);

-- Location: FF_X28_Y26_N7
\kbd_ctrl|key0code[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	asdata => \kbd_ctrl|fetchdata\(7),
	clrn => \kbd_ctrl|ALT_INV_KEY0~2_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key0en~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|key0code\(7));

-- Location: LCCOMB_X29_Y26_N2
\kbd_ctrl|Equal14~3\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal14~3_combout\ = (\kbd_ctrl|fetchdata\(6) & (\kbd_ctrl|key0code\(6) & (\kbd_ctrl|fetchdata\(7) $ (!\kbd_ctrl|key0code\(7))))) # (!\kbd_ctrl|fetchdata\(6) & (!\kbd_ctrl|key0code\(6) & (\kbd_ctrl|fetchdata\(7) $ (!\kbd_ctrl|key0code\(7)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000001001000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|fetchdata\(6),
	datab => \kbd_ctrl|fetchdata\(7),
	datac => \kbd_ctrl|key0code\(7),
	datad => \kbd_ctrl|key0code\(6),
	combout => \kbd_ctrl|Equal14~3_combout\);

-- Location: LCCOMB_X29_Y26_N24
\kbd_ctrl|Equal14~4\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal14~4_combout\ = (\kbd_ctrl|Equal14~1_combout\ & (\kbd_ctrl|Equal14~0_combout\ & (\kbd_ctrl|Equal14~2_combout\ & \kbd_ctrl|Equal14~3_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|Equal14~1_combout\,
	datab => \kbd_ctrl|Equal14~0_combout\,
	datac => \kbd_ctrl|Equal14~2_combout\,
	datad => \kbd_ctrl|Equal14~3_combout\,
	combout => \kbd_ctrl|Equal14~4_combout\);

-- Location: LCCOMB_X30_Y26_N28
\kbd_ctrl|key2en~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|key2en~0_combout\ = (\kbd_ctrl|Equal4~0_combout\ & ((\kbd_ctrl|Equal14~6_combout\) # ((!\kbd_ctrl|Equal14~4_combout\)))) # (!\kbd_ctrl|Equal4~0_combout\ & (!\kbd_ctrl|Equal4~5_combout\ & ((\kbd_ctrl|Equal14~6_combout\) # 
-- (!\kbd_ctrl|Equal14~4_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000110010101111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|Equal4~0_combout\,
	datab => \kbd_ctrl|Equal14~6_combout\,
	datac => \kbd_ctrl|Equal4~5_combout\,
	datad => \kbd_ctrl|Equal14~4_combout\,
	combout => \kbd_ctrl|key2en~0_combout\);

-- Location: LCCOMB_X30_Y26_N16
\kbd_ctrl|key1en~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|key1en~0_combout\ = (!\kbd_ctrl|Equal18~4_combout\ & (\kbd_ctrl|Equal17~1_combout\ & (\kbd_ctrl|key2en~0_combout\ & \kbd_ctrl|key2en~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|Equal18~4_combout\,
	datab => \kbd_ctrl|Equal17~1_combout\,
	datac => \kbd_ctrl|key2en~0_combout\,
	datad => \kbd_ctrl|key2en~1_combout\,
	combout => \kbd_ctrl|key1en~0_combout\);

-- Location: FF_X30_Y26_N11
\kbd_ctrl|key1code[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	asdata => \kbd_ctrl|fetchdata\(5),
	clrn => \kbd_ctrl|ALT_INV_KEY1~8_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key1en~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|key1code\(5));

-- Location: LCCOMB_X30_Y26_N2
\kbd_ctrl|Equal18~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal18~2_combout\ = (!\kbd_ctrl|key1code\(7) & (!\kbd_ctrl|key1code\(5) & (!\kbd_ctrl|key1code\(0) & !\kbd_ctrl|key1code\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key1code\(7),
	datab => \kbd_ctrl|key1code\(5),
	datac => \kbd_ctrl|key1code\(0),
	datad => \kbd_ctrl|key1code\(2),
	combout => \kbd_ctrl|Equal18~2_combout\);

-- Location: LCCOMB_X30_Y26_N18
\kbd_ctrl|Equal18~5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal18~5_combout\ = (!\kbd_ctrl|key1code\(6) & (\kbd_ctrl|Equal18~2_combout\ & !\kbd_ctrl|key1code\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key1code\(6),
	datac => \kbd_ctrl|Equal18~2_combout\,
	datad => \kbd_ctrl|key1code\(3),
	combout => \kbd_ctrl|Equal18~5_combout\);

-- Location: FF_X30_Y26_N7
\kbd_ctrl|key1code[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	asdata => \kbd_ctrl|fetchdata\(6),
	clrn => \kbd_ctrl|ALT_INV_KEY1~8_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key1en~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|key1code\(6));

-- Location: LCCOMB_X30_Y26_N30
\kbd_ctrl|Equal12~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal12~0_combout\ = (\kbd_ctrl|key1code\(7)) # (((\kbd_ctrl|key1code\(5)) # (\kbd_ctrl|key1code\(2))) # (!\kbd_ctrl|key1code\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key1code\(7),
	datab => \kbd_ctrl|key1code\(0),
	datac => \kbd_ctrl|key1code\(5),
	datad => \kbd_ctrl|key1code\(2),
	combout => \kbd_ctrl|Equal12~0_combout\);

-- Location: LCCOMB_X31_Y26_N14
\kbd_ctrl|Equal12~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal12~2_combout\ = (\kbd_ctrl|Equal12~1_combout\) # ((\kbd_ctrl|Equal12~0_combout\) # (!\kbd_ctrl|key1code\(6)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110101111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|Equal12~1_combout\,
	datac => \kbd_ctrl|key1code\(6),
	datad => \kbd_ctrl|Equal12~0_combout\,
	combout => \kbd_ctrl|Equal12~2_combout\);

-- Location: FF_X28_Y26_N31
\kbd_ctrl|key0code[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	asdata => \kbd_ctrl|fetchdata\(3),
	clrn => \kbd_ctrl|ALT_INV_KEY0~2_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key0en~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|key0code\(3));

-- Location: LCCOMB_X28_Y26_N8
\kbd_ctrl|Equal11~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal11~0_combout\ = (((\kbd_ctrl|key0code\(1)) # (!\kbd_ctrl|key0code\(3))) # (!\kbd_ctrl|key0code\(4))) # (!\kbd_ctrl|key0code\(13))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111011111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(13),
	datab => \kbd_ctrl|key0code\(4),
	datac => \kbd_ctrl|key0code\(1),
	datad => \kbd_ctrl|key0code\(3),
	combout => \kbd_ctrl|Equal11~0_combout\);

-- Location: FF_X28_Y26_N15
\kbd_ctrl|key0code[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	asdata => \kbd_ctrl|fetchdata\(5),
	clrn => \kbd_ctrl|ALT_INV_KEY0~2_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key0en~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|key0code\(5));

-- Location: LCCOMB_X28_Y26_N16
\kbd_ctrl|Equal11~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal11~1_combout\ = (!\kbd_ctrl|key0code\(2) & (!\kbd_ctrl|key0code\(5) & !\kbd_ctrl|key0code\(7)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000010001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(2),
	datab => \kbd_ctrl|key0code\(5),
	datad => \kbd_ctrl|key0code\(7),
	combout => \kbd_ctrl|Equal11~1_combout\);

-- Location: LCCOMB_X28_Y26_N18
\kbd_ctrl|Equal11~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal11~2_combout\ = (((\kbd_ctrl|Equal11~0_combout\) # (!\kbd_ctrl|Equal11~1_combout\)) # (!\kbd_ctrl|key0code\(6))) # (!\kbd_ctrl|key0code\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111011111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(0),
	datab => \kbd_ctrl|key0code\(6),
	datac => \kbd_ctrl|Equal11~0_combout\,
	datad => \kbd_ctrl|Equal11~1_combout\,
	combout => \kbd_ctrl|Equal11~2_combout\);

-- Location: LCCOMB_X31_Y26_N4
\kbd_ctrl|KEY1~6\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|KEY1~6_combout\ = (!\kbd_ctrl|Equal6~2_combout\ & (\kbd_ctrl|Equal10~1_combout\ & (!\kbd_ctrl|Equal12~2_combout\ & \kbd_ctrl|Equal11~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|Equal6~2_combout\,
	datab => \kbd_ctrl|Equal10~1_combout\,
	datac => \kbd_ctrl|Equal12~2_combout\,
	datad => \kbd_ctrl|Equal11~2_combout\,
	combout => \kbd_ctrl|KEY1~6_combout\);

-- Location: LCCOMB_X31_Y26_N26
\kbd_ctrl|KEY1~7\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|KEY1~7_combout\ = (\kbd_ctrl|KEY1~6_combout\) # ((!\kbd_ctrl|Equal8~0_combout\ & (\kbd_ctrl|Equal18~5_combout\ & \kbd_ctrl|key0clearn~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|Equal8~0_combout\,
	datab => \kbd_ctrl|Equal18~5_combout\,
	datac => \kbd_ctrl|KEY1~6_combout\,
	datad => \kbd_ctrl|key0clearn~0_combout\,
	combout => \kbd_ctrl|KEY1~7_combout\);

-- Location: FF_X30_Y26_N25
\kbd_ctrl|key1code[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	asdata => \kbd_ctrl|selE0~combout\,
	clrn => \kbd_ctrl|ALT_INV_KEY1~8_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key1en~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|key1code\(13));

-- Location: FF_X30_Y26_N15
\kbd_ctrl|key1code[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	asdata => \kbd_ctrl|fetchdata\(7),
	clrn => \kbd_ctrl|ALT_INV_KEY1~8_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key1en~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|key1code\(7));

-- Location: LCCOMB_X30_Y26_N20
\kbd_ctrl|Equal4~4\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal4~4_combout\ = (\kbd_ctrl|fetchdata\(6) & (\kbd_ctrl|key1code\(6) & (\kbd_ctrl|key1code\(7) $ (!\kbd_ctrl|fetchdata\(7))))) # (!\kbd_ctrl|fetchdata\(6) & (!\kbd_ctrl|key1code\(6) & (\kbd_ctrl|key1code\(7) $ (!\kbd_ctrl|fetchdata\(7)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001000000001001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|fetchdata\(6),
	datab => \kbd_ctrl|key1code\(6),
	datac => \kbd_ctrl|key1code\(7),
	datad => \kbd_ctrl|fetchdata\(7),
	combout => \kbd_ctrl|Equal4~4_combout\);

-- Location: LCCOMB_X30_Y26_N24
\kbd_ctrl|Equal4~6\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal4~6_combout\ = (\kbd_ctrl|Equal4~4_combout\ & (\kbd_ctrl|selE0~combout\ $ (!\kbd_ctrl|key1code\(13))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|selE0~combout\,
	datac => \kbd_ctrl|key1code\(13),
	datad => \kbd_ctrl|Equal4~4_combout\,
	combout => \kbd_ctrl|Equal4~6_combout\);

-- Location: LCCOMB_X30_Y26_N0
\kbd_ctrl|Equal4~7\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal4~7_combout\ = (\kbd_ctrl|Equal4~3_combout\ & (\kbd_ctrl|Equal4~2_combout\ & (\kbd_ctrl|Equal4~1_combout\ & \kbd_ctrl|Equal4~6_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|Equal4~3_combout\,
	datab => \kbd_ctrl|Equal4~2_combout\,
	datac => \kbd_ctrl|Equal4~1_combout\,
	datad => \kbd_ctrl|Equal4~6_combout\,
	combout => \kbd_ctrl|Equal4~7_combout\);

-- Location: LCCOMB_X32_Y26_N4
\kbd_ctrl|Equal14~5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal14~5_combout\ = (\kbd_ctrl|Equal14~4_combout\ & (\kbd_ctrl|key0code\(13) $ (!\kbd_ctrl|selE0~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(13),
	datac => \kbd_ctrl|selE0~combout\,
	datad => \kbd_ctrl|Equal14~4_combout\,
	combout => \kbd_ctrl|Equal14~5_combout\);

-- Location: LCCOMB_X31_Y26_N10
\kbd_ctrl|KEY1~9\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|KEY1~9_combout\ = (\kbd_ctrl|relbt~combout\ & (\kbd_ctrl|selbt~combout\ & (\kbd_ctrl|Equal4~7_combout\ & !\kbd_ctrl|Equal14~5_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|relbt~combout\,
	datab => \kbd_ctrl|selbt~combout\,
	datac => \kbd_ctrl|Equal4~7_combout\,
	datad => \kbd_ctrl|Equal14~5_combout\,
	combout => \kbd_ctrl|KEY1~9_combout\);

-- Location: LCCOMB_X31_Y26_N12
\kbd_ctrl|KEY1~8\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|KEY1~8_combout\ = ((\kbd_ctrl|KEY1~9_combout\) # ((\kbd_ctrl|KEY0~0_combout\ & \kbd_ctrl|KEY1~7_combout\))) # (!\KEY[0]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \KEY[0]~input_o\,
	datab => \kbd_ctrl|KEY0~0_combout\,
	datac => \kbd_ctrl|KEY1~7_combout\,
	datad => \kbd_ctrl|KEY1~9_combout\,
	combout => \kbd_ctrl|KEY1~8_combout\);

-- Location: FF_X30_Y26_N9
\kbd_ctrl|key1code[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	asdata => \kbd_ctrl|fetchdata\(1),
	clrn => \kbd_ctrl|ALT_INV_KEY1~8_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key1en~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|key1code\(1));

-- Location: LCCOMB_X30_Y26_N26
\kbd_ctrl|Equal18~4\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal18~4_combout\ = (\kbd_ctrl|key1code\(4)) # ((\kbd_ctrl|key1code\(1)) # ((\kbd_ctrl|key1code\(13)) # (!\kbd_ctrl|Equal18~5_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key1code\(4),
	datab => \kbd_ctrl|key1code\(1),
	datac => \kbd_ctrl|key1code\(13),
	datad => \kbd_ctrl|Equal18~5_combout\,
	combout => \kbd_ctrl|Equal18~4_combout\);

-- Location: LCCOMB_X32_Y26_N6
\kbd_ctrl|key2en~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|key2en~2_combout\ = (!\kbd_ctrl|Equal14~5_combout\ & (!\kbd_ctrl|Equal4~7_combout\ & \kbd_ctrl|key2en~1_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|Equal14~5_combout\,
	datac => \kbd_ctrl|Equal4~7_combout\,
	datad => \kbd_ctrl|key2en~1_combout\,
	combout => \kbd_ctrl|key2en~2_combout\);

-- Location: LCCOMB_X32_Y26_N8
\kbd_ctrl|key2en~3\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|key2en~3_combout\ = (\kbd_ctrl|Equal17~1_combout\ & (!\kbd_ctrl|Equal19~2_combout\ & (\kbd_ctrl|Equal18~4_combout\ & \kbd_ctrl|key2en~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|Equal17~1_combout\,
	datab => \kbd_ctrl|Equal19~2_combout\,
	datac => \kbd_ctrl|Equal18~4_combout\,
	datad => \kbd_ctrl|key2en~2_combout\,
	combout => \kbd_ctrl|key2en~3_combout\);

-- Location: FF_X32_Y26_N5
\kbd_ctrl|key2code[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	asdata => \kbd_ctrl|selE0~combout\,
	clrn => \kbd_ctrl|ALT_INV_KEY2~8_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key2en~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|key2code\(13));

-- Location: LCCOMB_X32_Y26_N26
\kbd_ctrl|Equal5~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal5~0_combout\ = \kbd_ctrl|selE0~combout\ $ (\kbd_ctrl|key2code\(13))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|selE0~combout\,
	datad => \kbd_ctrl|key2code\(13),
	combout => \kbd_ctrl|Equal5~0_combout\);

-- Location: LCCOMB_X32_Y26_N28
\kbd_ctrl|Equal5~3\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal5~3_combout\ = (\kbd_ctrl|key2code\(4) & (\kbd_ctrl|fetchdata\(4) & (\kbd_ctrl|fetchdata\(5) $ (!\kbd_ctrl|key2code\(5))))) # (!\kbd_ctrl|key2code\(4) & (!\kbd_ctrl|fetchdata\(4) & (\kbd_ctrl|fetchdata\(5) $ (!\kbd_ctrl|key2code\(5)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000001001000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key2code\(4),
	datab => \kbd_ctrl|fetchdata\(5),
	datac => \kbd_ctrl|key2code\(5),
	datad => \kbd_ctrl|fetchdata\(4),
	combout => \kbd_ctrl|Equal5~3_combout\);

-- Location: FF_X32_Y26_N23
\kbd_ctrl|key2code[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	asdata => \kbd_ctrl|fetchdata\(2),
	clrn => \kbd_ctrl|ALT_INV_KEY2~8_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key2en~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|key2code\(2));

-- Location: LCCOMB_X32_Y26_N22
\kbd_ctrl|Equal5~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal5~2_combout\ = (\kbd_ctrl|key2code\(3) & (\kbd_ctrl|fetchdata\(3) & (\kbd_ctrl|fetchdata\(2) $ (!\kbd_ctrl|key2code\(2))))) # (!\kbd_ctrl|key2code\(3) & (!\kbd_ctrl|fetchdata\(3) & (\kbd_ctrl|fetchdata\(2) $ (!\kbd_ctrl|key2code\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000001001000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key2code\(3),
	datab => \kbd_ctrl|fetchdata\(2),
	datac => \kbd_ctrl|key2code\(2),
	datad => \kbd_ctrl|fetchdata\(3),
	combout => \kbd_ctrl|Equal5~2_combout\);

-- Location: FF_X32_Y26_N17
\kbd_ctrl|key2code[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	asdata => \kbd_ctrl|fetchdata\(6),
	clrn => \kbd_ctrl|ALT_INV_KEY2~8_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key2en~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|key2code\(6));

-- Location: LCCOMB_X32_Y26_N16
\kbd_ctrl|Equal5~4\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal5~4_combout\ = (\kbd_ctrl|key2code\(7) & (\kbd_ctrl|fetchdata\(7) & (\kbd_ctrl|fetchdata\(6) $ (!\kbd_ctrl|key2code\(6))))) # (!\kbd_ctrl|key2code\(7) & (!\kbd_ctrl|fetchdata\(7) & (\kbd_ctrl|fetchdata\(6) $ (!\kbd_ctrl|key2code\(6)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000001001000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key2code\(7),
	datab => \kbd_ctrl|fetchdata\(6),
	datac => \kbd_ctrl|key2code\(6),
	datad => \kbd_ctrl|fetchdata\(7),
	combout => \kbd_ctrl|Equal5~4_combout\);

-- Location: LCCOMB_X32_Y26_N30
\kbd_ctrl|Equal5~5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal5~5_combout\ = (\kbd_ctrl|Equal5~1_combout\ & (\kbd_ctrl|Equal5~3_combout\ & (\kbd_ctrl|Equal5~2_combout\ & \kbd_ctrl|Equal5~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|Equal5~1_combout\,
	datab => \kbd_ctrl|Equal5~3_combout\,
	datac => \kbd_ctrl|Equal5~2_combout\,
	datad => \kbd_ctrl|Equal5~4_combout\,
	combout => \kbd_ctrl|Equal5~5_combout\);

-- Location: LCCOMB_X32_Y26_N12
\kbd_ctrl|key2en~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|key2en~1_combout\ = (\kbd_ctrl|selbt~combout\ & (!\kbd_ctrl|relbt~combout\ & ((\kbd_ctrl|Equal5~0_combout\) # (!\kbd_ctrl|Equal5~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|selbt~combout\,
	datab => \kbd_ctrl|Equal5~0_combout\,
	datac => \kbd_ctrl|Equal5~5_combout\,
	datad => \kbd_ctrl|relbt~combout\,
	combout => \kbd_ctrl|key2en~1_combout\);

-- Location: LCCOMB_X28_Y26_N2
\kbd_ctrl|key0en~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|key0en~0_combout\ = (!\kbd_ctrl|Equal17~1_combout\ & (\kbd_ctrl|key2en~1_combout\ & \kbd_ctrl|key2en~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|Equal17~1_combout\,
	datac => \kbd_ctrl|key2en~1_combout\,
	datad => \kbd_ctrl|key2en~0_combout\,
	combout => \kbd_ctrl|key0en~0_combout\);

-- Location: FF_X28_Y26_N25
\kbd_ctrl|key0code[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	asdata => \kbd_ctrl|fetchdata\(6),
	clrn => \kbd_ctrl|ALT_INV_KEY0~2_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key0en~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|key0code\(6));

-- Location: LCCOMB_X28_Y26_N0
\kbd_ctrl|Equal17~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal17~0_combout\ = (!\kbd_ctrl|key0code\(3) & (!\kbd_ctrl|key0code\(6) & (!\kbd_ctrl|key0code\(0) & \kbd_ctrl|Equal11~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(3),
	datab => \kbd_ctrl|key0code\(6),
	datac => \kbd_ctrl|key0code\(0),
	datad => \kbd_ctrl|Equal11~1_combout\,
	combout => \kbd_ctrl|Equal17~0_combout\);

-- Location: LCCOMB_X29_Y26_N30
\kbd_ctrl|Equal6~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal6~1_combout\ = (!\kbd_ctrl|fetchdata\(6) & (!\kbd_ctrl|fetchdata\(3) & (!\kbd_ctrl|fetchdata\(0) & \kbd_ctrl|fetchdata\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|fetchdata\(6),
	datab => \kbd_ctrl|fetchdata\(3),
	datac => \kbd_ctrl|fetchdata\(0),
	datad => \kbd_ctrl|fetchdata\(1),
	combout => \kbd_ctrl|Equal6~1_combout\);

-- Location: LCCOMB_X29_Y26_N6
\kbd_ctrl|Equal6~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal6~0_combout\ = (!\kbd_ctrl|fetchdata\(5) & (!\kbd_ctrl|fetchdata\(7) & (!\kbd_ctrl|fetchdata\(2) & \kbd_ctrl|fetchdata\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|fetchdata\(5),
	datab => \kbd_ctrl|fetchdata\(7),
	datac => \kbd_ctrl|fetchdata\(2),
	datad => \kbd_ctrl|fetchdata\(4),
	combout => \kbd_ctrl|Equal6~0_combout\);

-- Location: LCCOMB_X29_Y26_N28
\kbd_ctrl|Equal6~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal6~2_combout\ = (!\kbd_ctrl|selE0~combout\ & (\kbd_ctrl|Equal6~1_combout\ & \kbd_ctrl|Equal6~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|selE0~combout\,
	datac => \kbd_ctrl|Equal6~1_combout\,
	datad => \kbd_ctrl|Equal6~0_combout\,
	combout => \kbd_ctrl|Equal6~2_combout\);

-- Location: LCCOMB_X28_Y26_N10
\kbd_ctrl|key0clearn~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|key0clearn~0_combout\ = (\kbd_ctrl|Equal6~2_combout\ & ((\kbd_ctrl|Equal7~0_combout\) # (!\kbd_ctrl|Equal17~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011101100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|Equal7~0_combout\,
	datab => \kbd_ctrl|Equal17~0_combout\,
	datad => \kbd_ctrl|Equal6~2_combout\,
	combout => \kbd_ctrl|key0clearn~0_combout\);

-- Location: LCCOMB_X28_Y26_N26
\kbd_ctrl|KEY0~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|KEY0~1_combout\ = (!\kbd_ctrl|key0clearn~0_combout\ & ((\kbd_ctrl|Equal6~2_combout\) # ((!\kbd_ctrl|Equal11~2_combout\ & \kbd_ctrl|Equal10~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|Equal11~2_combout\,
	datab => \kbd_ctrl|Equal10~1_combout\,
	datac => \kbd_ctrl|key0clearn~0_combout\,
	datad => \kbd_ctrl|Equal6~2_combout\,
	combout => \kbd_ctrl|KEY0~1_combout\);

-- Location: LCCOMB_X28_Y26_N20
\kbd_ctrl|KEY0~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|KEY0~2_combout\ = ((\kbd_ctrl|KEY0~0_combout\ & ((\kbd_ctrl|KEY0~1_combout\) # (\kbd_ctrl|Equal14~5_combout\)))) # (!\KEY[0]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110111010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \KEY[0]~input_o\,
	datab => \kbd_ctrl|KEY0~0_combout\,
	datac => \kbd_ctrl|KEY0~1_combout\,
	datad => \kbd_ctrl|Equal14~5_combout\,
	combout => \kbd_ctrl|KEY0~2_combout\);

-- Location: FF_X28_Y26_N11
\kbd_ctrl|key0code[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	asdata => \kbd_ctrl|selE0~combout\,
	clrn => \kbd_ctrl|ALT_INV_KEY0~2_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key0en~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|key0code\(13));

-- Location: LCCOMB_X29_Y26_N16
\kbd_ctrl|Equal14~6\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal14~6_combout\ = \kbd_ctrl|selE0~combout\ $ (\kbd_ctrl|key0code\(13))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|selE0~combout\,
	datad => \kbd_ctrl|key0code\(13),
	combout => \kbd_ctrl|Equal14~6_combout\);

-- Location: LCCOMB_X29_Y26_N10
\kbd_ctrl|KEY1~10\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|KEY1~10_combout\ = (\kbd_ctrl|selbt~combout\ & (\kbd_ctrl|relbt~combout\ & ((\kbd_ctrl|Equal14~6_combout\) # (!\kbd_ctrl|Equal14~4_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|selbt~combout\,
	datab => \kbd_ctrl|relbt~combout\,
	datac => \kbd_ctrl|Equal14~6_combout\,
	datad => \kbd_ctrl|Equal14~4_combout\,
	combout => \kbd_ctrl|KEY1~10_combout\);

-- Location: FF_X32_Y26_N21
\kbd_ctrl|key2code[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	asdata => \kbd_ctrl|fetchdata\(3),
	clrn => \kbd_ctrl|ALT_INV_KEY2~8_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key2en~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|key2code\(3));

-- Location: FF_X31_Y26_N29
\kbd_ctrl|key2code[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	asdata => \kbd_ctrl|fetchdata\(0),
	clrn => \kbd_ctrl|ALT_INV_KEY2~8_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key2en~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|key2code\(0));

-- Location: LCCOMB_X31_Y26_N30
\kbd_ctrl|KEY2~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|KEY2~1_combout\ = (!\kbd_ctrl|key2code\(1) & (\kbd_ctrl|key2code\(3) & (\kbd_ctrl|key2code\(0) & \kbd_ctrl|key2code\(6))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key2code\(1),
	datab => \kbd_ctrl|key2code\(3),
	datac => \kbd_ctrl|key2code\(0),
	datad => \kbd_ctrl|key2code\(6),
	combout => \kbd_ctrl|KEY2~1_combout\);

-- Location: LCCOMB_X31_Y26_N20
\kbd_ctrl|KEY2~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|KEY2~2_combout\ = (\kbd_ctrl|KEY2~1_combout\ & ((\kbd_ctrl|Equal12~1_combout\) # ((\kbd_ctrl|Equal12~0_combout\) # (!\kbd_ctrl|key1code\(6)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|Equal12~1_combout\,
	datab => \kbd_ctrl|key1code\(6),
	datac => \kbd_ctrl|KEY2~1_combout\,
	datad => \kbd_ctrl|Equal12~0_combout\,
	combout => \kbd_ctrl|KEY2~2_combout\);

-- Location: LCCOMB_X31_Y26_N18
\kbd_ctrl|KEY2~3\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|KEY2~3_combout\ = (!\kbd_ctrl|Equal6~2_combout\ & (\kbd_ctrl|KEY2~2_combout\ & (\kbd_ctrl|Equal10~1_combout\ & \kbd_ctrl|Equal11~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|Equal6~2_combout\,
	datab => \kbd_ctrl|KEY2~2_combout\,
	datac => \kbd_ctrl|Equal10~1_combout\,
	datad => \kbd_ctrl|Equal11~2_combout\,
	combout => \kbd_ctrl|KEY2~3_combout\);

-- Location: FF_X31_Y26_N7
\kbd_ctrl|key2code[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	asdata => \kbd_ctrl|fetchdata\(1),
	clrn => \kbd_ctrl|ALT_INV_KEY2~8_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key2en~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|key2code\(1));

-- Location: LCCOMB_X31_Y26_N28
\kbd_ctrl|KEY2~4\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|KEY2~4_combout\ = (!\kbd_ctrl|key2code\(3) & (!\kbd_ctrl|key2code\(6) & (!\kbd_ctrl|key2code\(0) & \kbd_ctrl|key2code\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key2code\(3),
	datab => \kbd_ctrl|key2code\(6),
	datac => \kbd_ctrl|key2code\(0),
	datad => \kbd_ctrl|key2code\(1),
	combout => \kbd_ctrl|KEY2~4_combout\);

-- Location: LCCOMB_X31_Y26_N6
\kbd_ctrl|Equal18~3\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal18~3_combout\ = (!\kbd_ctrl|key1code\(3) & !\kbd_ctrl|key1code\(6))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|key1code\(3),
	datad => \kbd_ctrl|key1code\(6),
	combout => \kbd_ctrl|Equal18~3_combout\);

-- Location: LCCOMB_X31_Y26_N8
\kbd_ctrl|KEY2~5\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|KEY2~5_combout\ = (\kbd_ctrl|KEY2~4_combout\ & ((\kbd_ctrl|Equal8~0_combout\) # ((!\kbd_ctrl|Equal18~2_combout\) # (!\kbd_ctrl|Equal18~3_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000110011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|Equal8~0_combout\,
	datab => \kbd_ctrl|KEY2~4_combout\,
	datac => \kbd_ctrl|Equal18~3_combout\,
	datad => \kbd_ctrl|Equal18~2_combout\,
	combout => \kbd_ctrl|KEY2~5_combout\);

-- Location: LCCOMB_X31_Y26_N22
\kbd_ctrl|KEY2~6\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|KEY2~6_combout\ = (\kbd_ctrl|KEY2~0_combout\ & ((\kbd_ctrl|KEY2~3_combout\) # ((\kbd_ctrl|KEY2~5_combout\ & \kbd_ctrl|key0clearn~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|KEY2~0_combout\,
	datab => \kbd_ctrl|KEY2~3_combout\,
	datac => \kbd_ctrl|KEY2~5_combout\,
	datad => \kbd_ctrl|key0clearn~0_combout\,
	combout => \kbd_ctrl|KEY2~6_combout\);

-- Location: LCCOMB_X32_Y26_N24
\kbd_ctrl|KEY2~7\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|KEY2~7_combout\ = (!\kbd_ctrl|Equal5~0_combout\ & (\kbd_ctrl|Equal5~5_combout\ & ((\kbd_ctrl|Equal4~0_combout\) # (!\kbd_ctrl|Equal4~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|Equal4~0_combout\,
	datab => \kbd_ctrl|Equal5~0_combout\,
	datac => \kbd_ctrl|Equal5~5_combout\,
	datad => \kbd_ctrl|Equal4~5_combout\,
	combout => \kbd_ctrl|KEY2~7_combout\);

-- Location: LCCOMB_X31_Y26_N16
\kbd_ctrl|KEY2~8\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|KEY2~8_combout\ = ((\kbd_ctrl|KEY2~6_combout\) # ((\kbd_ctrl|KEY1~10_combout\ & \kbd_ctrl|KEY2~7_combout\))) # (!\KEY[0]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110111110101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \KEY[0]~input_o\,
	datab => \kbd_ctrl|KEY1~10_combout\,
	datac => \kbd_ctrl|KEY2~6_combout\,
	datad => \kbd_ctrl|KEY2~7_combout\,
	combout => \kbd_ctrl|KEY2~8_combout\);

-- Location: FF_X32_Y26_N29
\kbd_ctrl|key2code[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	asdata => \kbd_ctrl|fetchdata\(5),
	clrn => \kbd_ctrl|ALT_INV_KEY2~8_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key2en~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|key2code\(5));

-- Location: LCCOMB_X32_Y26_N20
\kbd_ctrl|Equal19~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal19~0_combout\ = (!\kbd_ctrl|key2code\(7) & (!\kbd_ctrl|key2code\(5) & !\kbd_ctrl|key2code\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000010001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key2code\(7),
	datab => \kbd_ctrl|key2code\(5),
	datad => \kbd_ctrl|key2code\(2),
	combout => \kbd_ctrl|Equal19~0_combout\);

-- Location: FF_X32_Y26_N11
\kbd_ctrl|key2code[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	asdata => \kbd_ctrl|fetchdata\(4),
	clrn => \kbd_ctrl|ALT_INV_KEY2~8_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key2en~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|key2code\(4));

-- Location: LCCOMB_X32_Y26_N2
\kbd_ctrl|Equal19~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal19~2_combout\ = (((\kbd_ctrl|key2code\(4)) # (\kbd_ctrl|key2code\(13))) # (!\kbd_ctrl|Equal19~0_combout\)) # (!\kbd_ctrl|Equal19~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|Equal19~1_combout\,
	datab => \kbd_ctrl|Equal19~0_combout\,
	datac => \kbd_ctrl|key2code\(4),
	datad => \kbd_ctrl|key2code\(13),
	combout => \kbd_ctrl|Equal19~2_combout\);

-- Location: LCCOMB_X31_Y27_N8
\kbd_ctrl|key_on[2]~feeder\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|key_on[2]~feeder_combout\ = \kbd_ctrl|Equal19~2_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \kbd_ctrl|Equal19~2_combout\,
	combout => \kbd_ctrl|key_on[2]~feeder_combout\);

-- Location: FF_X31_Y27_N9
\kbd_ctrl|key_on[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|key_on[2]~feeder_combout\,
	clrn => \KEY[0]~inputclkctrl_outclk\,
	ena => \kbd_ctrl|state.CLRDP~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|key_on\(2));

-- Location: LCCOMB_X31_Y27_N24
\kbd_ctrl|key_on[0]~feeder\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|key_on[0]~feeder_combout\ = \kbd_ctrl|Equal17~1_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \kbd_ctrl|Equal17~1_combout\,
	combout => \kbd_ctrl|key_on[0]~feeder_combout\);

-- Location: FF_X31_Y27_N25
\kbd_ctrl|key_on[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|key_on[0]~feeder_combout\,
	clrn => \KEY[0]~inputclkctrl_outclk\,
	ena => \kbd_ctrl|state.CLRDP~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|key_on\(0));

-- Location: LCCOMB_X31_Y27_N0
\Equal0~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \Equal0~0_combout\ = (!\kbd_ctrl|key_on\(1) & (!\kbd_ctrl|key_on\(2) & !\kbd_ctrl|key_on\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key_on\(1),
	datac => \kbd_ctrl|key_on\(2),
	datad => \kbd_ctrl|key_on\(0),
	combout => \Equal0~0_combout\);

-- Location: FF_X30_Y27_N7
\lights[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCKHZ~clkctrl_outclk\,
	d => \lights~1_combout\,
	clrn => \KEY[0]~inputclkctrl_outclk\,
	ena => \Equal0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => lights(0));

-- Location: LCCOMB_X30_Y27_N16
\lights~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \lights~0_combout\ = (lights(2) & (((!lights(0))))) # (!lights(2) & (lights(1) & ((!lights(0)) # (!\dir~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010011111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dir~q\,
	datab => lights(1),
	datac => lights(2),
	datad => lights(0),
	combout => \lights~0_combout\);

-- Location: FF_X30_Y27_N17
\lights[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCKHZ~clkctrl_outclk\,
	d => \lights~0_combout\,
	clrn => \KEY[0]~inputclkctrl_outclk\,
	ena => \Equal0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => lights(2));

-- Location: LCCOMB_X30_Y27_N24
\lights~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \lights~2_combout\ = (lights(2)) # (!lights(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => lights(2),
	datad => lights(0),
	combout => \lights~2_combout\);

-- Location: FF_X30_Y27_N25
\lights[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCKHZ~clkctrl_outclk\,
	d => \lights~2_combout\,
	clrn => \KEY[0]~inputclkctrl_outclk\,
	ena => \Equal0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => lights(1));

-- Location: FF_X31_Y27_N23
\kbd_ctrl|laststate[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	asdata => lights(1),
	sload => VCC,
	ena => \kbd_ctrl|Equal20~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|laststate\(2));

-- Location: FF_X31_Y27_N19
\kbd_ctrl|laststate[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	asdata => lights(2),
	sload => VCC,
	ena => \kbd_ctrl|Equal20~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|laststate\(1));

-- Location: LCCOMB_X31_Y27_N28
\kbd_ctrl|laststate[0]~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|laststate[0]~0_combout\ = !lights(0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => lights(0),
	combout => \kbd_ctrl|laststate[0]~0_combout\);

-- Location: FF_X31_Y27_N29
\kbd_ctrl|laststate[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|laststate[0]~0_combout\,
	ena => \kbd_ctrl|Equal20~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|laststate\(0));

-- Location: LCCOMB_X31_Y27_N18
\kbd_ctrl|Equal20~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal20~0_combout\ = (lights(2) & (\kbd_ctrl|laststate\(1) & (lights(0) $ (\kbd_ctrl|laststate\(0))))) # (!lights(2) & (!\kbd_ctrl|laststate\(1) & (lights(0) $ (\kbd_ctrl|laststate\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000110000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => lights(2),
	datab => lights(0),
	datac => \kbd_ctrl|laststate\(1),
	datad => \kbd_ctrl|laststate\(0),
	combout => \kbd_ctrl|Equal20~0_combout\);

-- Location: LCCOMB_X31_Y27_N22
\kbd_ctrl|Equal20~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal20~1_combout\ = (lights(1) $ (\kbd_ctrl|laststate\(2))) # (!\kbd_ctrl|Equal20~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => lights(1),
	datac => \kbd_ctrl|laststate\(2),
	datad => \kbd_ctrl|Equal20~0_combout\,
	combout => \kbd_ctrl|Equal20~1_combout\);

-- Location: LCCOMB_X32_Y27_N30
\kbd_ctrl|siguplights~feeder\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|siguplights~feeder_combout\ = \kbd_ctrl|Equal20~1_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \kbd_ctrl|Equal20~1_combout\,
	combout => \kbd_ctrl|siguplights~feeder_combout\);

-- Location: FF_X32_Y27_N31
\kbd_ctrl|siguplights\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|siguplights~feeder_combout\,
	clrn => \kbd_ctrl|ALT_INV_process_0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|siguplights~q\);

-- Location: LCCOMB_X33_Y26_N26
\kbd_ctrl|process_15~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|process_15~0_combout\ = ((\kbd_ctrl|siguplights~q\) # (!\KEY[1]~input_o\)) # (!\KEY[0]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \KEY[0]~input_o\,
	datac => \KEY[1]~input_o\,
	datad => \kbd_ctrl|siguplights~q\,
	combout => \kbd_ctrl|process_15~0_combout\);

-- Location: FF_X33_Y26_N21
\kbd_ctrl|cmdstate.SEND\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|Selector9~0_combout\,
	clrn => \kbd_ctrl|ALT_INV_process_15~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|cmdstate.SEND~q\);

-- Location: LCCOMB_X33_Y26_N4
\kbd_ctrl|Selector13~3\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Selector13~3_combout\ = (\kbd_ctrl|cmdstate.SENDVAL~q\) # ((!\kbd_ctrl|cmdstate.SEND~q\ & (\kbd_ctrl|cmdstate.WAITACK1~q\ & !\kbd_ctrl|Selector13~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|cmdstate.SENDVAL~q\,
	datab => \kbd_ctrl|cmdstate.SEND~q\,
	datac => \kbd_ctrl|cmdstate.WAITACK1~q\,
	datad => \kbd_ctrl|Selector13~2_combout\,
	combout => \kbd_ctrl|Selector13~3_combout\);

-- Location: FF_X33_Y26_N5
\kbd_ctrl|cmdstate.WAITACK1\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|Selector13~3_combout\,
	clrn => \kbd_ctrl|ALT_INV_process_15~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|cmdstate.WAITACK1~q\);

-- Location: LCCOMB_X33_Y26_N18
\kbd_ctrl|Selector11~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Selector11~0_combout\ = (\kbd_ctrl|ps2_ctrl|odata_rdy~combout\ & ((\kbd_ctrl|Equal21~2_combout\ & (\kbd_ctrl|cmdstate.WAITACK~q\)) # (!\kbd_ctrl|Equal21~2_combout\ & ((\kbd_ctrl|cmdstate.WAITACK1~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|cmdstate.WAITACK~q\,
	datab => \kbd_ctrl|cmdstate.WAITACK1~q\,
	datac => \kbd_ctrl|ps2_ctrl|odata_rdy~combout\,
	datad => \kbd_ctrl|Equal21~2_combout\,
	combout => \kbd_ctrl|Selector11~0_combout\);

-- Location: LCCOMB_X33_Y26_N10
\kbd_ctrl|Selector11~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Selector11~1_combout\ = (\kbd_ctrl|Selector11~0_combout\) # ((!\kbd_ctrl|ps2_ctrl|send_rdy~2_combout\ & \kbd_ctrl|cmdstate.SETLIGHTS~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111101010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|send_rdy~2_combout\,
	datac => \kbd_ctrl|cmdstate.SETLIGHTS~q\,
	datad => \kbd_ctrl|Selector11~0_combout\,
	combout => \kbd_ctrl|Selector11~1_combout\);

-- Location: FF_X33_Y26_N11
\kbd_ctrl|cmdstate.SETLIGHTS\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|Selector11~1_combout\,
	clrn => \kbd_ctrl|ALT_INV_process_15~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|cmdstate.SETLIGHTS~q\);

-- Location: LCCOMB_X37_Y24_N28
\kbd_ctrl|WideOr2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|WideOr2~0_combout\ = (\kbd_ctrl|cmdstate.SETLIGHTS~q\) # (!\kbd_ctrl|cmdstate.SETCMD~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \kbd_ctrl|cmdstate.SETCMD~q\,
	datad => \kbd_ctrl|cmdstate.SETLIGHTS~q\,
	combout => \kbd_ctrl|WideOr2~0_combout\);

-- Location: LCCOMB_X33_Y26_N28
\kbd_ctrl|Selector13~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Selector13~1_combout\ = (\kbd_ctrl|ps2_ctrl|odata_rdy~combout\ & ((\kbd_ctrl|cmdstate.WAITACK~q\) # (\kbd_ctrl|cmdstate.WAITACK1~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|cmdstate.WAITACK~q\,
	datac => \kbd_ctrl|cmdstate.WAITACK1~q\,
	datad => \kbd_ctrl|ps2_ctrl|odata_rdy~combout\,
	combout => \kbd_ctrl|Selector13~1_combout\);

-- Location: LCCOMB_X33_Y26_N0
\kbd_ctrl|Selector13~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Selector13~2_combout\ = (\kbd_ctrl|Selector13~0_combout\ & ((\kbd_ctrl|Selector13~1_combout\) # ((\kbd_ctrl|WideOr2~0_combout\ & \kbd_ctrl|ps2_ctrl|send_rdy~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|Selector13~0_combout\,
	datab => \kbd_ctrl|WideOr2~0_combout\,
	datac => \kbd_ctrl|ps2_ctrl|send_rdy~2_combout\,
	datad => \kbd_ctrl|Selector13~1_combout\,
	combout => \kbd_ctrl|Selector13~2_combout\);

-- Location: LCCOMB_X33_Y26_N12
\kbd_ctrl|Selector10~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Selector10~0_combout\ = (!\kbd_ctrl|cmdstate.SENDVAL~q\ & ((\kbd_ctrl|cmdstate.SEND~q\) # ((\kbd_ctrl|cmdstate.WAITACK~q\ & !\kbd_ctrl|Selector13~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010001010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|cmdstate.SENDVAL~q\,
	datab => \kbd_ctrl|cmdstate.SEND~q\,
	datac => \kbd_ctrl|cmdstate.WAITACK~q\,
	datad => \kbd_ctrl|Selector13~2_combout\,
	combout => \kbd_ctrl|Selector10~0_combout\);

-- Location: FF_X33_Y26_N13
\kbd_ctrl|cmdstate.WAITACK\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|Selector10~0_combout\,
	clrn => \kbd_ctrl|ALT_INV_process_15~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|cmdstate.WAITACK~q\);

-- Location: LCCOMB_X35_Y26_N26
\kbd_ctrl|Selector8~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Selector8~0_combout\ = (\kbd_ctrl|ps2_ctrl|odata_rdy~combout\ & \kbd_ctrl|cmdstate.WAITACK~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \kbd_ctrl|ps2_ctrl|odata_rdy~combout\,
	datad => \kbd_ctrl|cmdstate.WAITACK~q\,
	combout => \kbd_ctrl|Selector8~0_combout\);

-- Location: LCCOMB_X33_Y26_N24
\kbd_ctrl|Selector8~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Selector8~1_combout\ = (\kbd_ctrl|ps2_ctrl|send_rdy~2_combout\ & (((\kbd_ctrl|Equal21~2_combout\)) # (!\kbd_ctrl|Selector8~0_combout\))) # (!\kbd_ctrl|ps2_ctrl|send_rdy~2_combout\ & (\kbd_ctrl|cmdstate.SETCMD~q\ & ((\kbd_ctrl|Equal21~2_combout\) 
-- # (!\kbd_ctrl|Selector8~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|send_rdy~2_combout\,
	datab => \kbd_ctrl|Selector8~0_combout\,
	datac => \kbd_ctrl|cmdstate.SETCMD~q\,
	datad => \kbd_ctrl|Equal21~2_combout\,
	combout => \kbd_ctrl|Selector8~1_combout\);

-- Location: FF_X33_Y26_N25
\kbd_ctrl|cmdstate.SETCMD\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|Selector8~1_combout\,
	clrn => \kbd_ctrl|ALT_INV_process_15~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|cmdstate.SETCMD~q\);

-- Location: LCCOMB_X37_Y24_N8
\kbd_ctrl|hdata[3]~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|hdata[3]~0_combout\ = !\kbd_ctrl|cmdstate.SETCMD~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \kbd_ctrl|cmdstate.SETCMD~q\,
	combout => \kbd_ctrl|hdata[3]~0_combout\);

-- Location: FF_X37_Y24_N9
\kbd_ctrl|hdata[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|hdata[3]~0_combout\,
	ena => \kbd_ctrl|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|hdata\(3));

-- Location: FF_X36_Y24_N31
\kbd_ctrl|ps2_ctrl|hdata[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \kbd_ctrl|sigsend~clkctrl_outclk\,
	asdata => \kbd_ctrl|hdata\(3),
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_process_2~0_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|hdata\(3));

-- Location: LCCOMB_X36_Y24_N30
\kbd_ctrl|ps2_ctrl|ps2_data~6\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|ps2_data~6_combout\ = (\kbd_ctrl|ps2_ctrl|hdata\(3) & (\kbd_ctrl|ps2_ctrl|TOPS2:count[2]~q\ & ((\kbd_ctrl|ps2_ctrl|TOPS2:count[0]~q\) # (\kbd_ctrl|ps2_ctrl|TOPS2:count[1]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|TOPS2:count[0]~q\,
	datab => \kbd_ctrl|ps2_ctrl|TOPS2:count[1]~q\,
	datac => \kbd_ctrl|ps2_ctrl|hdata\(3),
	datad => \kbd_ctrl|ps2_ctrl|TOPS2:count[2]~q\,
	combout => \kbd_ctrl|ps2_ctrl|ps2_data~6_combout\);

-- Location: LCCOMB_X37_Y24_N10
\kbd_ctrl|Selector7~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Selector7~0_combout\ = (!\kbd_ctrl|cmdstate.SETLIGHTS~q\) # (!lights(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => lights(0),
	datad => \kbd_ctrl|cmdstate.SETLIGHTS~q\,
	combout => \kbd_ctrl|Selector7~0_combout\);

-- Location: FF_X37_Y24_N11
\kbd_ctrl|hdata[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|Selector7~0_combout\,
	ena => \kbd_ctrl|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|hdata\(0));

-- Location: FF_X36_Y24_N21
\kbd_ctrl|ps2_ctrl|hdata[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \kbd_ctrl|sigsend~clkctrl_outclk\,
	asdata => \kbd_ctrl|hdata\(0),
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_process_2~0_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|hdata\(0));

-- Location: LCCOMB_X37_Y24_N12
\kbd_ctrl|Selector6~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Selector6~0_combout\ = (lights(2) & \kbd_ctrl|cmdstate.SETLIGHTS~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => lights(2),
	datad => \kbd_ctrl|cmdstate.SETLIGHTS~q\,
	combout => \kbd_ctrl|Selector6~0_combout\);

-- Location: FF_X37_Y24_N13
\kbd_ctrl|hdata[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|Selector6~0_combout\,
	ena => \kbd_ctrl|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|hdata\(1));

-- Location: FF_X36_Y24_N27
\kbd_ctrl|ps2_ctrl|hdata[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \kbd_ctrl|sigsend~clkctrl_outclk\,
	asdata => \kbd_ctrl|hdata\(1),
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_process_2~0_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|hdata\(1));

-- Location: LCCOMB_X37_Y24_N14
\kbd_ctrl|Selector5~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Selector5~0_combout\ = (lights(1)) # (!\kbd_ctrl|cmdstate.SETLIGHTS~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => lights(1),
	datad => \kbd_ctrl|cmdstate.SETLIGHTS~q\,
	combout => \kbd_ctrl|Selector5~0_combout\);

-- Location: FF_X37_Y24_N15
\kbd_ctrl|hdata[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|Selector5~0_combout\,
	ena => \kbd_ctrl|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|hdata\(2));

-- Location: FF_X36_Y24_N29
\kbd_ctrl|ps2_ctrl|hdata[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \kbd_ctrl|sigsend~clkctrl_outclk\,
	asdata => \kbd_ctrl|hdata\(2),
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_process_2~0_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|hdata\(2));

-- Location: LCCOMB_X36_Y24_N26
\kbd_ctrl|ps2_ctrl|ps2_data~8\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|ps2_data~8_combout\ = \kbd_ctrl|ps2_ctrl|hdata\(0) $ (\kbd_ctrl|ps2_ctrl|hdata\(1) $ (\kbd_ctrl|ps2_ctrl|hdata\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|hdata\(0),
	datac => \kbd_ctrl|ps2_ctrl|hdata\(1),
	datad => \kbd_ctrl|ps2_ctrl|hdata\(2),
	combout => \kbd_ctrl|ps2_ctrl|ps2_data~8_combout\);

-- Location: LCCOMB_X36_Y24_N2
\kbd_ctrl|ps2_ctrl|Mux0~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Mux0~0_combout\ = (\kbd_ctrl|ps2_ctrl|TOPS2:count[0]~q\ & ((\kbd_ctrl|ps2_ctrl|TOPS2:count[1]~q\) # ((\kbd_ctrl|ps2_ctrl|hdata\(1))))) # (!\kbd_ctrl|ps2_ctrl|TOPS2:count[0]~q\ & (!\kbd_ctrl|ps2_ctrl|TOPS2:count[1]~q\ & 
-- ((\kbd_ctrl|ps2_ctrl|hdata\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100110101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|TOPS2:count[0]~q\,
	datab => \kbd_ctrl|ps2_ctrl|TOPS2:count[1]~q\,
	datac => \kbd_ctrl|ps2_ctrl|hdata\(1),
	datad => \kbd_ctrl|ps2_ctrl|hdata\(0),
	combout => \kbd_ctrl|ps2_ctrl|Mux0~0_combout\);

-- Location: LCCOMB_X36_Y24_N24
\kbd_ctrl|ps2_ctrl|Mux0~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Mux0~1_combout\ = (\kbd_ctrl|ps2_ctrl|TOPS2:count[1]~q\ & ((\kbd_ctrl|ps2_ctrl|Mux0~0_combout\ & ((\kbd_ctrl|ps2_ctrl|hdata\(3)))) # (!\kbd_ctrl|ps2_ctrl|Mux0~0_combout\ & (\kbd_ctrl|ps2_ctrl|hdata\(2))))) # 
-- (!\kbd_ctrl|ps2_ctrl|TOPS2:count[1]~q\ & (((\kbd_ctrl|ps2_ctrl|Mux0~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|TOPS2:count[1]~q\,
	datab => \kbd_ctrl|ps2_ctrl|hdata\(2),
	datac => \kbd_ctrl|ps2_ctrl|hdata\(3),
	datad => \kbd_ctrl|ps2_ctrl|Mux0~0_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Mux0~1_combout\);

-- Location: LCCOMB_X36_Y24_N18
\kbd_ctrl|ps2_ctrl|ps2_data~7\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|ps2_data~7_combout\ = (!\kbd_ctrl|ps2_ctrl|TOPS2:count[2]~q\ & \kbd_ctrl|ps2_ctrl|Mux0~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|TOPS2:count[2]~q\,
	datad => \kbd_ctrl|ps2_ctrl|Mux0~1_combout\,
	combout => \kbd_ctrl|ps2_ctrl|ps2_data~7_combout\);

-- Location: LCCOMB_X36_Y24_N16
\kbd_ctrl|ps2_ctrl|ps2_data~9\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|ps2_data~9_combout\ = (\kbd_ctrl|ps2_ctrl|ps2_data~6_combout\) # ((\kbd_ctrl|ps2_ctrl|Equal3~0_combout\ & (!\kbd_ctrl|ps2_ctrl|ps2_data~8_combout\)) # (!\kbd_ctrl|ps2_ctrl|Equal3~0_combout\ & ((\kbd_ctrl|ps2_ctrl|ps2_data~7_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101111111001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|Equal3~0_combout\,
	datab => \kbd_ctrl|ps2_ctrl|ps2_data~6_combout\,
	datac => \kbd_ctrl|ps2_ctrl|ps2_data~8_combout\,
	datad => \kbd_ctrl|ps2_ctrl|ps2_data~7_combout\,
	combout => \kbd_ctrl|ps2_ctrl|ps2_data~9_combout\);

-- Location: LCCOMB_X35_Y24_N16
\kbd_ctrl|ps2_ctrl|Add6~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add6~1_combout\ = (\kbd_ctrl|ps2_ctrl|TOPS2:count[0]~q\ & (\kbd_ctrl|ps2_ctrl|TOPS2:count[3]~q\ $ (((\kbd_ctrl|ps2_ctrl|TOPS2:count[2]~q\ & \kbd_ctrl|ps2_ctrl|TOPS2:count[1]~q\))))) # (!\kbd_ctrl|ps2_ctrl|TOPS2:count[0]~q\ & 
-- (\kbd_ctrl|ps2_ctrl|TOPS2:count[3]~q\ & ((\kbd_ctrl|ps2_ctrl|TOPS2:count[2]~q\) # (!\kbd_ctrl|ps2_ctrl|TOPS2:count[1]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|TOPS2:count[0]~q\,
	datab => \kbd_ctrl|ps2_ctrl|TOPS2:count[2]~q\,
	datac => \kbd_ctrl|ps2_ctrl|TOPS2:count[3]~q\,
	datad => \kbd_ctrl|ps2_ctrl|TOPS2:count[1]~q\,
	combout => \kbd_ctrl|ps2_ctrl|Add6~1_combout\);

-- Location: FF_X33_Y24_N29
\kbd_ctrl|ps2_ctrl|TOPS2:count[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_outclk\,
	asdata => \kbd_ctrl|ps2_ctrl|Add6~1_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_sigsendend~0_combout\,
	sload => VCC,
	ena => \kbd_ctrl|ps2_ctrl|TOPS2~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|TOPS2:count[3]~q\);

-- Location: LCCOMB_X35_Y24_N4
\kbd_ctrl|ps2_ctrl|ps2_data~10\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|ps2_data~10_combout\ = (!\kbd_ctrl|ps2_ctrl|TOPS2:count[2]~q\ & ((!\kbd_ctrl|ps2_ctrl|TOPS2:count[0]~q\) # (!\kbd_ctrl|ps2_ctrl|TOPS2:count[1]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|TOPS2:count[1]~q\,
	datac => \kbd_ctrl|ps2_ctrl|TOPS2:count[0]~q\,
	datad => \kbd_ctrl|ps2_ctrl|TOPS2:count[2]~q\,
	combout => \kbd_ctrl|ps2_ctrl|ps2_data~10_combout\);

-- Location: LCCOMB_X32_Y24_N30
\kbd_ctrl|ps2_ctrl|ps2_data~11\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|ps2_data~11_combout\ = (!\kbd_ctrl|ps2_ctrl|sigclkreleased~q\ & (\kbd_ctrl|ps2_ctrl|sigsending~q\ & ((\kbd_ctrl|ps2_ctrl|ps2_data~10_combout\) # (!\kbd_ctrl|ps2_ctrl|TOPS2:count[3]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|sigclkreleased~q\,
	datab => \kbd_ctrl|ps2_ctrl|TOPS2:count[3]~q\,
	datac => \kbd_ctrl|ps2_ctrl|sigsending~q\,
	datad => \kbd_ctrl|ps2_ctrl|ps2_data~10_combout\,
	combout => \kbd_ctrl|ps2_ctrl|ps2_data~11_combout\);

-- Location: FF_X36_Y24_N17
\kbd_ctrl|ps2_ctrl|ps2_data~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|ps2_data~9_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_sigclkheld~q\,
	ena => \kbd_ctrl|ps2_ctrl|ps2_data~11_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|ps2_data~reg0_q\);

-- Location: LCCOMB_X33_Y23_N28
\kbd_ctrl|ps2_ctrl|ps2_data~1\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|ps2_data~1_combout\ = (\kbd_ctrl|ps2_ctrl|TOPS2~0_combout\ & ((\kbd_ctrl|ps2_ctrl|sigclkheld~q\) # (\kbd_ctrl|ps2_ctrl|ps2_data~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|sigclkheld~q\,
	datac => \kbd_ctrl|ps2_ctrl|TOPS2~0_combout\,
	datad => \kbd_ctrl|ps2_ctrl|ps2_data~1_combout\,
	combout => \kbd_ctrl|ps2_ctrl|ps2_data~1_combout\);

-- Location: LCCOMB_X35_Y24_N30
\kbd_ctrl|ps2_ctrl|Equal5~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Equal5~0_combout\ = (\kbd_ctrl|ps2_ctrl|TOPS2:count[3]~q\ & !\kbd_ctrl|ps2_ctrl|TOPS2:count[2]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|TOPS2:count[3]~q\,
	datac => \kbd_ctrl|ps2_ctrl|TOPS2:count[2]~q\,
	combout => \kbd_ctrl|ps2_ctrl|Equal5~0_combout\);

-- Location: LCCOMB_X33_Y23_N0
\kbd_ctrl|ps2_ctrl|ps2_data~3\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|ps2_data~3_combout\ = \kbd_ctrl|ps2_ctrl|ps2_data~1_combout\ $ (((\kbd_ctrl|ps2_ctrl|TOPS2:count[0]~q\ $ (!\kbd_ctrl|ps2_ctrl|TOPS2:count[1]~q\)) # (!\kbd_ctrl|ps2_ctrl|Equal5~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110001110010011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|TOPS2:count[0]~q\,
	datab => \kbd_ctrl|ps2_ctrl|ps2_data~1_combout\,
	datac => \kbd_ctrl|ps2_ctrl|Equal5~0_combout\,
	datad => \kbd_ctrl|ps2_ctrl|TOPS2:count[1]~q\,
	combout => \kbd_ctrl|ps2_ctrl|ps2_data~3_combout\);

-- Location: LCCOMB_X32_Y24_N20
\kbd_ctrl|ps2_ctrl|ps2_data~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|ps2_data~0_combout\ = (\kbd_ctrl|ps2_ctrl|sigclkheld~q\) # (!\kbd_ctrl|ps2_ctrl|TOPS2~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \kbd_ctrl|ps2_ctrl|sigclkheld~q\,
	datad => \kbd_ctrl|ps2_ctrl|TOPS2~0_combout\,
	combout => \kbd_ctrl|ps2_ctrl|ps2_data~0_combout\);

-- Location: FF_X33_Y23_N1
\kbd_ctrl|ps2_ctrl|ps2_data~en_emulated\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|ps2_data~3_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_ps2_data~0_combout\,
	ena => \kbd_ctrl|ps2_ctrl|ps2_data~11_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|ps2_data~en_emulated_q\);

-- Location: LCCOMB_X33_Y23_N10
\kbd_ctrl|ps2_ctrl|ps2_data~2\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|ps2_data~2_combout\ = (\kbd_ctrl|ps2_ctrl|TOPS2~0_combout\ & ((\kbd_ctrl|ps2_ctrl|sigclkheld~q\) # (\kbd_ctrl|ps2_ctrl|ps2_data~1_combout\ $ (\kbd_ctrl|ps2_ctrl|ps2_data~en_emulated_q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011000011100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|sigclkheld~q\,
	datab => \kbd_ctrl|ps2_ctrl|ps2_data~1_combout\,
	datac => \kbd_ctrl|ps2_ctrl|TOPS2~0_combout\,
	datad => \kbd_ctrl|ps2_ctrl|ps2_data~en_emulated_q\,
	combout => \kbd_ctrl|ps2_ctrl|ps2_data~2_combout\);

-- Location: IOIBUF_X0_Y14_N1
\CLOCK_24[0]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_CLOCK_24(0),
	o => \CLOCK_24[0]~input_o\);

-- Location: CLKCTRL_G4
\CLOCK_24[0]~inputclkctrl\ : cycloneiii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \CLOCK_24[0]~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \CLOCK_24[0]~inputclkctrl_outclk\);

-- Location: FF_X32_Y24_N15
\kbd_ctrl|ps2_ctrl|ps2_clk~en\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|ps2_ctrl|ps2_clk~5_combout\,
	clrn => \kbd_ctrl|ps2_ctrl|ALT_INV_process_2~0_combout\,
	ena => \kbd_ctrl|ps2_ctrl|sigsending~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|ps2_ctrl|ps2_clk~en_q\);

-- Location: FF_X28_Y26_N29
\kbd_ctrl|key0code[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	asdata => \kbd_ctrl|fetchdata\(2),
	clrn => \kbd_ctrl|ALT_INV_KEY0~2_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key0en~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|key0code\(2));

-- Location: LCCOMB_X29_Y25_N0
\hexseg0|Mux6~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \hexseg0|Mux6~0_combout\ = (\kbd_ctrl|key0code\(3)) # ((\kbd_ctrl|key0code\(2) & ((!\kbd_ctrl|key0code\(0)) # (!\kbd_ctrl|key0code\(1)))) # (!\kbd_ctrl|key0code\(2) & (\kbd_ctrl|key0code\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111101101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(2),
	datab => \kbd_ctrl|key0code\(1),
	datac => \kbd_ctrl|key0code\(0),
	datad => \kbd_ctrl|key0code\(3),
	combout => \hexseg0|Mux6~0_combout\);

-- Location: LCCOMB_X29_Y25_N26
\hexseg0|Mux5~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \hexseg0|Mux5~0_combout\ = (!\kbd_ctrl|key0code\(3) & ((\kbd_ctrl|key0code\(2) & (\kbd_ctrl|key0code\(1) & \kbd_ctrl|key0code\(0))) # (!\kbd_ctrl|key0code\(2) & ((\kbd_ctrl|key0code\(1)) # (\kbd_ctrl|key0code\(0))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(2),
	datab => \kbd_ctrl|key0code\(1),
	datac => \kbd_ctrl|key0code\(0),
	datad => \kbd_ctrl|key0code\(3),
	combout => \hexseg0|Mux5~0_combout\);

-- Location: LCCOMB_X29_Y26_N20
\hexseg0|Mux4~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \hexseg0|Mux4~0_combout\ = (\kbd_ctrl|key0code\(1) & (!\kbd_ctrl|key0code\(3) & (\kbd_ctrl|key0code\(0)))) # (!\kbd_ctrl|key0code\(1) & ((\kbd_ctrl|key0code\(2) & (!\kbd_ctrl|key0code\(3))) # (!\kbd_ctrl|key0code\(2) & ((\kbd_ctrl|key0code\(0))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000101110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(3),
	datab => \kbd_ctrl|key0code\(1),
	datac => \kbd_ctrl|key0code\(0),
	datad => \kbd_ctrl|key0code\(2),
	combout => \hexseg0|Mux4~0_combout\);

-- Location: LCCOMB_X29_Y25_N8
\hexseg0|Mux3~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \hexseg0|Mux3~0_combout\ = (!\kbd_ctrl|key0code\(3) & ((\kbd_ctrl|key0code\(2) & (\kbd_ctrl|key0code\(1) $ (!\kbd_ctrl|key0code\(0)))) # (!\kbd_ctrl|key0code\(2) & (!\kbd_ctrl|key0code\(1) & \kbd_ctrl|key0code\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(2),
	datab => \kbd_ctrl|key0code\(1),
	datac => \kbd_ctrl|key0code\(0),
	datad => \kbd_ctrl|key0code\(3),
	combout => \hexseg0|Mux3~0_combout\);

-- Location: LCCOMB_X29_Y25_N6
\hexseg0|Mux2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \hexseg0|Mux2~0_combout\ = (\kbd_ctrl|key0code\(2) & (((\kbd_ctrl|key0code\(3))))) # (!\kbd_ctrl|key0code\(2) & (\kbd_ctrl|key0code\(1) & ((\kbd_ctrl|key0code\(3)) # (!\kbd_ctrl|key0code\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(2),
	datab => \kbd_ctrl|key0code\(1),
	datac => \kbd_ctrl|key0code\(0),
	datad => \kbd_ctrl|key0code\(3),
	combout => \hexseg0|Mux2~0_combout\);

-- Location: LCCOMB_X29_Y25_N28
\hexseg0|Mux1~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \hexseg0|Mux1~0_combout\ = (\kbd_ctrl|key0code\(2) & ((\kbd_ctrl|key0code\(3)) # (\kbd_ctrl|key0code\(1) $ (\kbd_ctrl|key0code\(0))))) # (!\kbd_ctrl|key0code\(2) & (\kbd_ctrl|key0code\(1) & ((\kbd_ctrl|key0code\(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(2),
	datab => \kbd_ctrl|key0code\(1),
	datac => \kbd_ctrl|key0code\(0),
	datad => \kbd_ctrl|key0code\(3),
	combout => \hexseg0|Mux1~0_combout\);

-- Location: LCCOMB_X28_Y26_N28
\hexseg0|Mux0~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \hexseg0|Mux0~0_combout\ = (!\kbd_ctrl|key0code\(1) & (!\kbd_ctrl|key0code\(3) & (\kbd_ctrl|key0code\(0) $ (\kbd_ctrl|key0code\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(1),
	datab => \kbd_ctrl|key0code\(0),
	datac => \kbd_ctrl|key0code\(2),
	datad => \kbd_ctrl|key0code\(3),
	combout => \hexseg0|Mux0~0_combout\);

-- Location: LCCOMB_X28_Y26_N6
\hexseg1|Mux6~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \hexseg1|Mux6~0_combout\ = (\kbd_ctrl|key0code\(7)) # ((\kbd_ctrl|key0code\(6) & ((!\kbd_ctrl|key0code\(4)) # (!\kbd_ctrl|key0code\(5)))) # (!\kbd_ctrl|key0code\(6) & (\kbd_ctrl|key0code\(5))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111011011111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(6),
	datab => \kbd_ctrl|key0code\(5),
	datac => \kbd_ctrl|key0code\(7),
	datad => \kbd_ctrl|key0code\(4),
	combout => \hexseg1|Mux6~0_combout\);

-- Location: LCCOMB_X29_Y25_N2
\hexseg1|Mux5~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \hexseg1|Mux5~0_combout\ = (!\kbd_ctrl|key0code\(7) & ((\kbd_ctrl|key0code\(5) & ((\kbd_ctrl|key0code\(4)) # (!\kbd_ctrl|key0code\(6)))) # (!\kbd_ctrl|key0code\(5) & (!\kbd_ctrl|key0code\(6) & \kbd_ctrl|key0code\(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(5),
	datab => \kbd_ctrl|key0code\(6),
	datac => \kbd_ctrl|key0code\(4),
	datad => \kbd_ctrl|key0code\(7),
	combout => \hexseg1|Mux5~0_combout\);

-- Location: LCCOMB_X28_Y26_N14
\hexseg1|Mux4~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \hexseg1|Mux4~0_combout\ = (\kbd_ctrl|key0code\(5) & (\kbd_ctrl|key0code\(4) & ((!\kbd_ctrl|key0code\(7))))) # (!\kbd_ctrl|key0code\(5) & ((\kbd_ctrl|key0code\(6) & ((!\kbd_ctrl|key0code\(7)))) # (!\kbd_ctrl|key0code\(6) & (\kbd_ctrl|key0code\(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001010101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(4),
	datab => \kbd_ctrl|key0code\(6),
	datac => \kbd_ctrl|key0code\(5),
	datad => \kbd_ctrl|key0code\(7),
	combout => \hexseg1|Mux4~0_combout\);

-- Location: LCCOMB_X28_Y26_N24
\hexseg1|Mux3~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \hexseg1|Mux3~0_combout\ = (!\kbd_ctrl|key0code\(7) & ((\kbd_ctrl|key0code\(5) & (\kbd_ctrl|key0code\(6) & \kbd_ctrl|key0code\(4))) # (!\kbd_ctrl|key0code\(5) & (\kbd_ctrl|key0code\(6) $ (\kbd_ctrl|key0code\(4))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000100010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(7),
	datab => \kbd_ctrl|key0code\(5),
	datac => \kbd_ctrl|key0code\(6),
	datad => \kbd_ctrl|key0code\(4),
	combout => \hexseg1|Mux3~0_combout\);

-- Location: LCCOMB_X29_Y25_N12
\hexseg1|Mux2~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \hexseg1|Mux2~0_combout\ = (\kbd_ctrl|key0code\(6) & (((\kbd_ctrl|key0code\(7))))) # (!\kbd_ctrl|key0code\(6) & (\kbd_ctrl|key0code\(5) & ((\kbd_ctrl|key0code\(7)) # (!\kbd_ctrl|key0code\(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(5),
	datab => \kbd_ctrl|key0code\(6),
	datac => \kbd_ctrl|key0code\(4),
	datad => \kbd_ctrl|key0code\(7),
	combout => \hexseg1|Mux2~0_combout\);

-- Location: LCCOMB_X28_Y26_N12
\hexseg1|Mux1~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \hexseg1|Mux1~0_combout\ = (\kbd_ctrl|key0code\(6) & ((\kbd_ctrl|key0code\(7)) # (\kbd_ctrl|key0code\(5) $ (\kbd_ctrl|key0code\(4))))) # (!\kbd_ctrl|key0code\(6) & (\kbd_ctrl|key0code\(5) & ((\kbd_ctrl|key0code\(7)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(6),
	datab => \kbd_ctrl|key0code\(5),
	datac => \kbd_ctrl|key0code\(4),
	datad => \kbd_ctrl|key0code\(7),
	combout => \hexseg1|Mux1~0_combout\);

-- Location: LCCOMB_X28_Y26_N4
\hexseg1|Mux0~0\ : cycloneiii_lcell_comb
-- Equation(s):
-- \hexseg1|Mux0~0_combout\ = (!\kbd_ctrl|key0code\(5) & (!\kbd_ctrl|key0code\(7) & (\kbd_ctrl|key0code\(6) $ (\kbd_ctrl|key0code\(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(6),
	datab => \kbd_ctrl|key0code\(4),
	datac => \kbd_ctrl|key0code\(5),
	datad => \kbd_ctrl|key0code\(7),
	combout => \hexseg1|Mux0~0_combout\);

-- Location: LCCOMB_X31_Y27_N30
\kbd_ctrl|key_on[1]~feeder\ : cycloneiii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|key_on[1]~feeder_combout\ = \kbd_ctrl|Equal18~4_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \kbd_ctrl|Equal18~4_combout\,
	combout => \kbd_ctrl|key_on[1]~feeder_combout\);

-- Location: FF_X31_Y27_N31
\kbd_ctrl|key_on[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \CLOCK_24[0]~inputclkctrl_outclk\,
	d => \kbd_ctrl|key_on[1]~feeder_combout\,
	clrn => \KEY[0]~inputclkctrl_outclk\,
	ena => \kbd_ctrl|state.CLRDP~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \kbd_ctrl|key_on\(1));

-- Location: IOIBUF_X1_Y29_N29
\CLOCK_24[1]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_CLOCK_24(1),
	o => \CLOCK_24[1]~input_o\);

-- Location: IOIBUF_X39_Y29_N8
\CLOCK_27[0]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_CLOCK_27(0),
	o => \CLOCK_27[0]~input_o\);

-- Location: IOIBUF_X3_Y0_N29
\CLOCK_27[1]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_CLOCK_27(1),
	o => \CLOCK_27[1]~input_o\);

-- Location: IOIBUF_X1_Y29_N1
\CLOCK_50~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_CLOCK_50,
	o => \CLOCK_50~input_o\);

-- Location: IOIBUF_X0_Y21_N1
\KEY[2]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(2),
	o => \KEY[2]~input_o\);

-- Location: IOIBUF_X1_Y0_N1
\KEY[3]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(3),
	o => \KEY[3]~input_o\);

-- Location: IOIBUF_X16_Y0_N22
\SW[0]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(0),
	o => \SW[0]~input_o\);

-- Location: IOIBUF_X30_Y0_N8
\SW[1]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(1),
	o => \SW[1]~input_o\);

-- Location: IOIBUF_X0_Y22_N15
\SW[2]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(2),
	o => \SW[2]~input_o\);

-- Location: IOIBUF_X0_Y11_N8
\SW[3]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(3),
	o => \SW[3]~input_o\);

-- Location: IOIBUF_X5_Y0_N8
\SW[4]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(4),
	o => \SW[4]~input_o\);

-- Location: IOIBUF_X41_Y26_N1
\SW[5]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(5),
	o => \SW[5]~input_o\);

-- Location: IOIBUF_X39_Y29_N1
\SW[6]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(6),
	o => \SW[6]~input_o\);

-- Location: IOIBUF_X26_Y0_N22
\SW[7]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(7),
	o => \SW[7]~input_o\);

-- Location: IOIBUF_X5_Y29_N1
\SW[8]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(8),
	o => \SW[8]~input_o\);

-- Location: IOIBUF_X0_Y13_N22
\SW[9]~input\ : cycloneiii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(9),
	o => \SW[9]~input_o\);

ww_HEX0(0) <= \HEX0[0]~output_o\;

ww_HEX0(1) <= \HEX0[1]~output_o\;

ww_HEX0(2) <= \HEX0[2]~output_o\;

ww_HEX0(3) <= \HEX0[3]~output_o\;

ww_HEX0(4) <= \HEX0[4]~output_o\;

ww_HEX0(5) <= \HEX0[5]~output_o\;

ww_HEX0(6) <= \HEX0[6]~output_o\;

ww_HEX1(0) <= \HEX1[0]~output_o\;

ww_HEX1(1) <= \HEX1[1]~output_o\;

ww_HEX1(2) <= \HEX1[2]~output_o\;

ww_HEX1(3) <= \HEX1[3]~output_o\;

ww_HEX1(4) <= \HEX1[4]~output_o\;

ww_HEX1(5) <= \HEX1[5]~output_o\;

ww_HEX1(6) <= \HEX1[6]~output_o\;

ww_HEX2(0) <= \HEX2[0]~output_o\;

ww_HEX2(1) <= \HEX2[1]~output_o\;

ww_HEX2(2) <= \HEX2[2]~output_o\;

ww_HEX2(3) <= \HEX2[3]~output_o\;

ww_HEX2(4) <= \HEX2[4]~output_o\;

ww_HEX2(5) <= \HEX2[5]~output_o\;

ww_HEX2(6) <= \HEX2[6]~output_o\;

ww_HEX3(0) <= \HEX3[0]~output_o\;

ww_HEX3(1) <= \HEX3[1]~output_o\;

ww_HEX3(2) <= \HEX3[2]~output_o\;

ww_HEX3(3) <= \HEX3[3]~output_o\;

ww_HEX3(4) <= \HEX3[4]~output_o\;

ww_HEX3(5) <= \HEX3[5]~output_o\;

ww_HEX3(6) <= \HEX3[6]~output_o\;

ww_LEDG(0) <= \LEDG[0]~output_o\;

ww_LEDG(1) <= \LEDG[1]~output_o\;

ww_LEDG(2) <= \LEDG[2]~output_o\;

ww_LEDG(3) <= \LEDG[3]~output_o\;

ww_LEDG(4) <= \LEDG[4]~output_o\;

ww_LEDG(5) <= \LEDG[5]~output_o\;

ww_LEDG(6) <= \LEDG[6]~output_o\;

ww_LEDG(7) <= \LEDG[7]~output_o\;

ww_LEDR(0) <= \LEDR[0]~output_o\;

ww_LEDR(1) <= \LEDR[1]~output_o\;

ww_LEDR(2) <= \LEDR[2]~output_o\;

ww_LEDR(3) <= \LEDR[3]~output_o\;

ww_LEDR(4) <= \LEDR[4]~output_o\;

ww_LEDR(5) <= \LEDR[5]~output_o\;

ww_LEDR(6) <= \LEDR[6]~output_o\;

ww_LEDR(7) <= \LEDR[7]~output_o\;

ww_LEDR(8) <= \LEDR[8]~output_o\;

ww_LEDR(9) <= \LEDR[9]~output_o\;

PS2_DAT <= \PS2_DAT~output_o\;

PS2_CLK <= \PS2_CLK~output_o\;
END structure;


