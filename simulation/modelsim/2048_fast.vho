-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 32-bit"
-- VERSION "Version 13.0.1 Build 232 06/12/2013 Service Pack 1 SJ Web Edition"

-- DATE "06/27/2017 16:36:51"

-- 
-- Device: Altera EP2C20F484C6 Package FBGA484
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY CYCLONEII;
LIBRARY IEEE;
USE CYCLONEII.CYCLONEII_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	InputController IS
    PORT (
	CLOCK_24 : IN std_logic_vector(1 DOWNTO 0);
	CLOCK_27 : IN std_logic_vector(1 DOWNTO 0);
	CLOCK_50 : IN std_logic;
	KEY : IN std_logic_vector(3 DOWNTO 0);
	SW : IN std_logic_vector(9 DOWNTO 0);
	HEX0 : OUT std_logic_vector(6 DOWNTO 0);
	HEX1 : OUT std_logic_vector(6 DOWNTO 0);
	HEX2 : OUT std_logic_vector(6 DOWNTO 0);
	HEX3 : OUT std_logic_vector(6 DOWNTO 0);
	KEY_PRESS : OUT std_logic_vector(2 DOWNTO 0);
	IS_PRESSED : OUT std_logic;
	LEDG : OUT std_logic_vector(7 DOWNTO 0);
	LEDR : OUT std_logic_vector(9 DOWNTO 0);
	PS2_DAT : INOUT std_logic;
	PS2_CLK : INOUT std_logic
	);
END InputController;

-- Design Ports Information
-- PS2_DAT	=>  Location: PIN_J14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- PS2_CLK	=>  Location: PIN_H15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- CLOCK_24[1]	=>  Location: PIN_A12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- CLOCK_27[0]	=>  Location: PIN_D12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- CLOCK_27[1]	=>  Location: PIN_E12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- CLOCK_50	=>  Location: PIN_L1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- KEY[2]	=>  Location: PIN_T22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- KEY[3]	=>  Location: PIN_T21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[0]	=>  Location: PIN_L22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[1]	=>  Location: PIN_L21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[2]	=>  Location: PIN_M22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[3]	=>  Location: PIN_V12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[4]	=>  Location: PIN_W12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[5]	=>  Location: PIN_U12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[6]	=>  Location: PIN_U11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[7]	=>  Location: PIN_M2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[8]	=>  Location: PIN_M1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[9]	=>  Location: PIN_L2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- HEX0[0]	=>  Location: PIN_J2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[1]	=>  Location: PIN_J1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[2]	=>  Location: PIN_H2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[3]	=>  Location: PIN_H1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[4]	=>  Location: PIN_F2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[5]	=>  Location: PIN_F1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[6]	=>  Location: PIN_E2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[0]	=>  Location: PIN_E1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[1]	=>  Location: PIN_H6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[2]	=>  Location: PIN_H5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[3]	=>  Location: PIN_H4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[4]	=>  Location: PIN_G3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[5]	=>  Location: PIN_D2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[6]	=>  Location: PIN_D1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[0]	=>  Location: PIN_G5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[1]	=>  Location: PIN_G6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[2]	=>  Location: PIN_C2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[3]	=>  Location: PIN_C1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[4]	=>  Location: PIN_E3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[5]	=>  Location: PIN_E4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[6]	=>  Location: PIN_D3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[0]	=>  Location: PIN_F4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[1]	=>  Location: PIN_D5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[2]	=>  Location: PIN_D6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[3]	=>  Location: PIN_J4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[4]	=>  Location: PIN_L8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[5]	=>  Location: PIN_F3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[6]	=>  Location: PIN_D4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- KEY_PRESS[0]	=>  Location: PIN_E11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- KEY_PRESS[1]	=>  Location: PIN_D11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- KEY_PRESS[2]	=>  Location: PIN_AA10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- IS_PRESSED	=>  Location: PIN_P15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDG[0]	=>  Location: PIN_U22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDG[1]	=>  Location: PIN_U21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDG[2]	=>  Location: PIN_V22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDG[3]	=>  Location: PIN_V21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDG[4]	=>  Location: PIN_W22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDG[5]	=>  Location: PIN_W21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDG[6]	=>  Location: PIN_Y22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDG[7]	=>  Location: PIN_Y21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[0]	=>  Location: PIN_R20,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[1]	=>  Location: PIN_R19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[2]	=>  Location: PIN_U19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[3]	=>  Location: PIN_Y19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[4]	=>  Location: PIN_T18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[5]	=>  Location: PIN_V19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[6]	=>  Location: PIN_Y18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[7]	=>  Location: PIN_U18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[8]	=>  Location: PIN_R18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[9]	=>  Location: PIN_R17,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- KEY[1]	=>  Location: PIN_R21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- CLOCK_24[0]	=>  Location: PIN_B12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- KEY[0]	=>  Location: PIN_R22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default


ARCHITECTURE structure OF InputController IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_CLOCK_24 : std_logic_vector(1 DOWNTO 0);
SIGNAL ww_CLOCK_27 : std_logic_vector(1 DOWNTO 0);
SIGNAL ww_CLOCK_50 : std_logic;
SIGNAL ww_KEY : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_SW : std_logic_vector(9 DOWNTO 0);
SIGNAL ww_HEX0 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX1 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX2 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_HEX3 : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_KEY_PRESS : std_logic_vector(2 DOWNTO 0);
SIGNAL ww_IS_PRESSED : std_logic;
SIGNAL ww_LEDG : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_LEDR : std_logic_vector(9 DOWNTO 0);
SIGNAL \kbd_ctrl|sigsend~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \CLOCKHZ~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \CLOCK_24[0]~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \kbd_ctrl|ps2_ctrl|count[6]~27_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~4_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~8_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~13\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~14_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~4_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~6_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~8_combout\ : std_logic;
SIGNAL \count[1]~22_combout\ : std_logic;
SIGNAL \count[1]~23\ : std_logic;
SIGNAL \count[2]~24_combout\ : std_logic;
SIGNAL \count[2]~25\ : std_logic;
SIGNAL \count[3]~26_combout\ : std_logic;
SIGNAL \count[3]~27\ : std_logic;
SIGNAL \count[4]~28_combout\ : std_logic;
SIGNAL \count[4]~29\ : std_logic;
SIGNAL \count[5]~30_combout\ : std_logic;
SIGNAL \count[5]~31\ : std_logic;
SIGNAL \count[6]~32_combout\ : std_logic;
SIGNAL \count[6]~33\ : std_logic;
SIGNAL \count[7]~34_combout\ : std_logic;
SIGNAL \count[7]~35\ : std_logic;
SIGNAL \count[8]~36_combout\ : std_logic;
SIGNAL \count[8]~37\ : std_logic;
SIGNAL \count[9]~38_combout\ : std_logic;
SIGNAL \count[9]~39\ : std_logic;
SIGNAL \count[10]~40_combout\ : std_logic;
SIGNAL \count[10]~41\ : std_logic;
SIGNAL \count[11]~42_combout\ : std_logic;
SIGNAL \count[11]~43\ : std_logic;
SIGNAL \count[12]~44_combout\ : std_logic;
SIGNAL \count[12]~45\ : std_logic;
SIGNAL \count[13]~46_combout\ : std_logic;
SIGNAL \count[13]~47\ : std_logic;
SIGNAL \count[14]~48_combout\ : std_logic;
SIGNAL \count[14]~49\ : std_logic;
SIGNAL \count[15]~50_combout\ : std_logic;
SIGNAL \count[15]~51\ : std_logic;
SIGNAL \count[16]~52_combout\ : std_logic;
SIGNAL \count[16]~53\ : std_logic;
SIGNAL \count[17]~54_combout\ : std_logic;
SIGNAL \count[17]~55\ : std_logic;
SIGNAL \count[18]~56_combout\ : std_logic;
SIGNAL \count[18]~57\ : std_logic;
SIGNAL \count[19]~58_combout\ : std_logic;
SIGNAL \count[19]~59\ : std_logic;
SIGNAL \count[20]~60_combout\ : std_logic;
SIGNAL \count[20]~61\ : std_logic;
SIGNAL \count[21]~62_combout\ : std_logic;
SIGNAL \count[21]~63\ : std_logic;
SIGNAL \count[22]~64_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[4]~24_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[9]~34_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[15]~46_combout\ : std_logic;
SIGNAL \Equal0~3_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal14~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal11~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|key0clearn~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal4~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal5~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal18~3_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal19~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal12~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal12~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|KEY2~3_combout\ : std_logic;
SIGNAL \kbd_ctrl|KEY2~4_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|FROMPS2:count[3]~regout\ : std_logic;
SIGNAL \kbd_ctrl|Equal0~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|ps2_data~10_combout\ : std_logic;
SIGNAL \kbd_ctrl|sigsend~regout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count~14_combout\ : std_logic;
SIGNAL \kbd_ctrl|state.EXT1~regout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sigsendend~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|cmdstate.SEND~regout\ : std_logic;
SIGNAL \kbd_ctrl|cmdstate~18_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~16_combout\ : std_logic;
SIGNAL \kbd_ctrl|state.EXT1~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|Selector13~0_combout\ : std_logic;
SIGNAL \CLOCKHZ~regout\ : std_logic;
SIGNAL \kbd_ctrl|Selector9~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~19_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~21_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~19_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~20_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~21_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~23_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|parchecked~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|Selector8~0_combout\ : std_logic;
SIGNAL \LessThan0~0_combout\ : std_logic;
SIGNAL \LessThan0~1_combout\ : std_logic;
SIGNAL \Equal8~0_combout\ : std_logic;
SIGNAL \LessThan0~2_combout\ : std_logic;
SIGNAL \LessThan0~3_combout\ : std_logic;
SIGNAL \LessThan0~4_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Equal2~3_combout\ : std_logic;
SIGNAL \Equal8~1_combout\ : std_logic;
SIGNAL \Equal8~2_combout\ : std_logic;
SIGNAL \Equal8~3_combout\ : std_logic;
SIGNAL \Equal8~4_combout\ : std_logic;
SIGNAL \Equal8~5_combout\ : std_logic;
SIGNAL \Equal8~6_combout\ : std_logic;
SIGNAL \Equal8~7_combout\ : std_logic;
SIGNAL \count[0]~66_combout\ : std_logic;
SIGNAL \kbd_ctrl|sigsend~clkctrl_outclk\ : std_logic;
SIGNAL \CLOCKHZ~clkctrl_outclk\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[9]~feeder_combout\ : std_logic;
SIGNAL \PS2_DAT~0\ : std_logic;
SIGNAL \PS2_CLK~0\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~23_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|fcount[7]~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~1\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~22_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~3\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~5\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~6_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~20_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~7\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~9\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~10_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~18_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~11\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~12_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add0~17_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|fcount[7]~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~1\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~22_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|rcount[7]~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~3\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~5\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~7\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~9\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~11\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~12_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~17_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~10_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~18_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~13\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~14_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add1~16_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|rcount[7]~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sigtrigger~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sigtrigger~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sigtrigger~regout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_outclk\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|TOPS2:count[0]~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sigsending~feeder_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sigsendend~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[0]~15_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[8]~31_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[2]~19_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|ps2_clk~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|ps2_clk~3_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|ps2_clk~4_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[4]~23_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[1]~17_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|LessThan4~6_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|LessThan4~3_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|LessThan4~22_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[11]~35_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[0]~16\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[1]~18\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[2]~20\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[3]~21_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[3]~22\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[4]~24\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[5]~25_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[5]~26\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[6]~28\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[7]~29_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[7]~30\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[8]~32\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[9]~33_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[9]~34\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[10]~36_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[10]~37\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count[11]~38_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sigclkreleased~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sigclkreleased~regout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|TOPS2~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sigsendend~regout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|process_2~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sigsending~regout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|TOPS2~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sigclkheld~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sigclkheld~regout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sigsendend~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|TOPS2:count[0]~regout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|TOPS2:count[2]~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|TOPS2:count[2]~regout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add6~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|TOPS2:count[1]~regout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add6~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|TOPS2:count[3]~regout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Equal3~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|send_rdy~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[0]~55_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|FROMPS2~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[1]~19\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[2]~21\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[3]~22_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[12]~41\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[13]~43\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[14]~44_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[14]~45\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[15]~47\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[16]~49\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[17]~50_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[17]~51\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[18]~52_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[16]~48_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Equal2~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[18]~54_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[3]~23\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[4]~25\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[5]~26_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[5]~27\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[6]~29\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[7]~30_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[7]~31\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[8]~32_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[8]~33\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[9]~35\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[10]~36_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[10]~37\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[11]~38_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[11]~39\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[12]~40_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[13]~42_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Equal2~4_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[6]~28_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Equal2~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[1]~18_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|countclk[2]~20_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Equal2~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Equal2~5_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|send_rdy~3_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|send_rdy~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|send_rdy~_emulated_regout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|send_rdy~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count~12_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~regout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|count~13_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~regout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Add3~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~regout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|FROMPS2~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Decoder0~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Decoder0~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sdata[0]~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Decoder0~3_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sdata[3]~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Decoder0~4_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sdata[2]~3_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal21~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Decoder0~7_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sdata[7]~6_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Decoder0~6_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sdata[4]~5_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Decoder0~5_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sdata[5]~4_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal21~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal21~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|Selector8~1_combout\ : std_logic;
SIGNAL \dir~0_combout\ : std_logic;
SIGNAL \dir~regout\ : std_logic;
SIGNAL \lights~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Decoder0~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sdata[1]~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|FROMPS2~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|FROMPS2~3_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|parchecked~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|parchecked~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|parchecked~regout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|odata_rdy~combout\ : std_logic;
SIGNAL \kbd_ctrl|cmdstate.CLEAR~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|cmdstate.CLEAR~regout\ : std_logic;
SIGNAL \kbd_ctrl|sigsending~regout\ : std_logic;
SIGNAL \kbd_ctrl|process_0~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|state.DECODE~regout\ : std_logic;
SIGNAL \kbd_ctrl|state.CODE~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|state.CODE~regout\ : std_logic;
SIGNAL \kbd_ctrl|state.CLRDP~regout\ : std_logic;
SIGNAL \kbd_ctrl|Equal0~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal0~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|nstate.EXT0~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|state.EXT0~regout\ : std_logic;
SIGNAL \kbd_ctrl|nstate.RELEASE~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|state.RELEASE~regout\ : std_logic;
SIGNAL \kbd_ctrl|newdata~feeder_combout\ : std_logic;
SIGNAL \kbd_ctrl|process_13~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|newdata~regout\ : std_logic;
SIGNAL \kbd_ctrl|Selector0~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|Selector0~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|state.IDLE~regout\ : std_logic;
SIGNAL \kbd_ctrl|nstate.FETCH~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|state.FETCH~regout\ : std_logic;
SIGNAL \kbd_ctrl|process_4~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|relbt~combout\ : std_logic;
SIGNAL \kbd_ctrl|selbt~combout\ : std_logic;
SIGNAL \kbd_ctrl|KEY0~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|selE0~combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal17~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|key2en~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|key1en~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal8~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal18~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal8~3_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal6~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Decoder0~8_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|sdata[6]~7_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal10~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal10~1_combout\ : std_logic;
SIGNAL \Equal2~0_combout\ : std_logic;
SIGNAL \Equal4~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal6~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal6~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|KEY1~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|KEY1~3_combout\ : std_logic;
SIGNAL \kbd_ctrl|KEY1~5_combout\ : std_logic;
SIGNAL \kbd_ctrl|KEY1~4_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal4~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal4~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal4~3_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal4~4_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal4~5_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal5~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal5~3_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal5~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal5~4_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal5~5_combout\ : std_logic;
SIGNAL \kbd_ctrl|key2en~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|key0en~2_combout\ : std_logic;
SIGNAL \Equal2~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|key0clearn~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|KEY0~3_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal14~3_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal14~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal14~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal14~4_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal14~5_combout\ : std_logic;
SIGNAL \kbd_ctrl|KEY0~5_combout\ : std_logic;
SIGNAL \kbd_ctrl|KEY0~4_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal17~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|key2en~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal19~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|KEY2~6_combout\ : std_logic;
SIGNAL \kbd_ctrl|KEY2~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal12~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|KEY2~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|KEY2~5_combout\ : std_logic;
SIGNAL \kbd_ctrl|KEY2~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|KEY2~7_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal19~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|key_on[2]~feeder_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal18~4_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal18~5_combout\ : std_logic;
SIGNAL \kbd_ctrl|key_on[1]~feeder_combout\ : std_logic;
SIGNAL \Equal7~0_combout\ : std_logic;
SIGNAL \lights~2_combout\ : std_logic;
SIGNAL \lights~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|laststate[0]~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal20~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|Equal20~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|siguplights~regout\ : std_logic;
SIGNAL \kbd_ctrl|process_15~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|cmdstate.SETCMD~regout\ : std_logic;
SIGNAL \kbd_ctrl|hdata[3]~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|Selector13~4_combout\ : std_logic;
SIGNAL \kbd_ctrl|cmdstate.SENDVAL~regout\ : std_logic;
SIGNAL \kbd_ctrl|Selector13~3_combout\ : std_logic;
SIGNAL \kbd_ctrl|cmdstate.WAITACK1~regout\ : std_logic;
SIGNAL \kbd_ctrl|Selector13~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|Selector13~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|Selector10~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|cmdstate.WAITACK~regout\ : std_logic;
SIGNAL \kbd_ctrl|Selector11~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|Selector11~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|cmdstate.SETLIGHTS~regout\ : std_logic;
SIGNAL \kbd_ctrl|WideOr2~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|hdata[3]~feeder_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|ps2_data~8_combout\ : std_logic;
SIGNAL \kbd_ctrl|Selector5~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|hdata[2]~feeder_combout\ : std_logic;
SIGNAL \kbd_ctrl|Selector6~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|hdata[1]~feeder_combout\ : std_logic;
SIGNAL \kbd_ctrl|Selector7~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|hdata[0]~feeder_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Mux0~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Mux0~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|ps2_data~9_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|ps2_data~11_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|ps2_data~12_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|ps2_data~13_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|ps2_data~reg0_regout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|ps2_data~1_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|Equal5~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|ps2_data~3_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|ps2_data~0_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|ps2_data~en_emulated_regout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|ps2_data~2_combout\ : std_logic;
SIGNAL \CLOCK_24[0]~clkctrl_outclk\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|ps2_clk~2_combout\ : std_logic;
SIGNAL \kbd_ctrl|ps2_ctrl|ps2_clk~en_regout\ : std_logic;
SIGNAL \hexseg0|Mux6~0_combout\ : std_logic;
SIGNAL \hexseg0|Mux5~0_combout\ : std_logic;
SIGNAL \hexseg0|Mux4~0_combout\ : std_logic;
SIGNAL \hexseg0|Mux3~0_combout\ : std_logic;
SIGNAL \hexseg0|Mux2~0_combout\ : std_logic;
SIGNAL \hexseg0|Mux1~0_combout\ : std_logic;
SIGNAL \hexseg0|Mux0~0_combout\ : std_logic;
SIGNAL \hexseg1|Mux6~0_combout\ : std_logic;
SIGNAL \hexseg1|Mux5~0_combout\ : std_logic;
SIGNAL \hexseg1|Mux4~0_combout\ : std_logic;
SIGNAL \hexseg1|Mux3~0_combout\ : std_logic;
SIGNAL \hexseg1|Mux2~0_combout\ : std_logic;
SIGNAL \hexseg1|Mux1~0_combout\ : std_logic;
SIGNAL \hexseg1|Mux0~0_combout\ : std_logic;
SIGNAL \Equal0~1_combout\ : std_logic;
SIGNAL \Equal0~0_combout\ : std_logic;
SIGNAL \Equal0~2_combout\ : std_logic;
SIGNAL \Equal2~2_combout\ : std_logic;
SIGNAL \Equal3~0_combout\ : std_logic;
SIGNAL \Equal6~0_combout\ : std_logic;
SIGNAL \Equal4~1_combout\ : std_logic;
SIGNAL \KEY_PRESS~0_combout\ : std_logic;
SIGNAL \KEY_PRESS~2_combout\ : std_logic;
SIGNAL \Equal5~0_combout\ : std_logic;
SIGNAL \KEY_PRESS~1_combout\ : std_logic;
SIGNAL \KEY_PRESS~3_combout\ : std_logic;
SIGNAL \KEY_PRESS~4_combout\ : std_logic;
SIGNAL \KEY_PRESS~5_combout\ : std_logic;
SIGNAL lights : std_logic_vector(2 DOWNTO 0);
SIGNAL count : std_logic_vector(22 DOWNTO 0);
SIGNAL \kbd_ctrl|laststate\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \kbd_ctrl|key_on\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \kbd_ctrl|key2code\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \kbd_ctrl|key1code\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \kbd_ctrl|key0code\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \kbd_ctrl|hdata\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \kbd_ctrl|fetchdata\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \kbd_ctrl|ps2_ctrl|sdata\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \kbd_ctrl|ps2_ctrl|rcount\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \kbd_ctrl|ps2_ctrl|hdata\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \kbd_ctrl|ps2_ctrl|fcount\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \kbd_ctrl|ps2_ctrl|countclk\ : std_logic_vector(18 DOWNTO 0);
SIGNAL \kbd_ctrl|ps2_ctrl|count\ : std_logic_vector(11 DOWNTO 0);
SIGNAL \KEY~combout\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \CLOCK_24~combout\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \ALT_INV_KEY~combout\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \kbd_ctrl|ps2_ctrl|ALT_INV_ps2_clk~en_regout\ : std_logic;
SIGNAL \kbd_ctrl|ALT_INV_key0code\ : std_logic_vector(13 DOWNTO 13);
SIGNAL \hexseg1|ALT_INV_Mux0~0_combout\ : std_logic;
SIGNAL \hexseg0|ALT_INV_Mux0~0_combout\ : std_logic;

BEGIN

ww_CLOCK_24 <= CLOCK_24;
ww_CLOCK_27 <= CLOCK_27;
ww_CLOCK_50 <= CLOCK_50;
ww_KEY <= KEY;
ww_SW <= SW;
HEX0 <= ww_HEX0;
HEX1 <= ww_HEX1;
HEX2 <= ww_HEX2;
HEX3 <= ww_HEX3;
KEY_PRESS <= ww_KEY_PRESS;
IS_PRESSED <= ww_IS_PRESSED;
LEDG <= ww_LEDG;
LEDR <= ww_LEDR;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\kbd_ctrl|sigsend~clkctrl_INCLK_bus\ <= (gnd & gnd & gnd & \kbd_ctrl|sigsend~regout\);

\kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_INCLK_bus\ <= (gnd & gnd & gnd & \kbd_ctrl|ps2_ctrl|sigtrigger~regout\);

\CLOCKHZ~clkctrl_INCLK_bus\ <= (gnd & gnd & gnd & \CLOCKHZ~regout\);

\CLOCK_24[0]~clkctrl_INCLK_bus\ <= (gnd & gnd & gnd & \CLOCK_24~combout\(0));
\ALT_INV_KEY~combout\(0) <= NOT \KEY~combout\(0);
\kbd_ctrl|ps2_ctrl|ALT_INV_ps2_clk~en_regout\ <= NOT \kbd_ctrl|ps2_ctrl|ps2_clk~en_regout\;
\kbd_ctrl|ALT_INV_key0code\(13) <= NOT \kbd_ctrl|key0code\(13);
\hexseg1|ALT_INV_Mux0~0_combout\ <= NOT \hexseg1|Mux0~0_combout\;
\hexseg0|ALT_INV_Mux0~0_combout\ <= NOT \hexseg0|Mux0~0_combout\;

-- Location: LCFF_X31_Y18_N21
\kbd_ctrl|ps2_ctrl|count[6]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|count[6]~27_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|process_2~0_combout\,
	ena => \kbd_ctrl|ps2_ctrl|count[11]~35_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|count\(6));

-- Location: LCCOMB_X31_Y18_N20
\kbd_ctrl|ps2_ctrl|count[6]~27\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|count[6]~27_combout\ = (\kbd_ctrl|ps2_ctrl|count\(6) & (\kbd_ctrl|ps2_ctrl|count[5]~26\ $ (GND))) # (!\kbd_ctrl|ps2_ctrl|count\(6) & (!\kbd_ctrl|ps2_ctrl|count[5]~26\ & VCC))
-- \kbd_ctrl|ps2_ctrl|count[6]~28\ = CARRY((\kbd_ctrl|ps2_ctrl|count\(6) & !\kbd_ctrl|ps2_ctrl|count[5]~26\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|count\(6),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|count[5]~26\,
	combout => \kbd_ctrl|ps2_ctrl|count[6]~27_combout\,
	cout => \kbd_ctrl|ps2_ctrl|count[6]~28\);

-- Location: LCCOMB_X37_Y16_N16
\kbd_ctrl|ps2_ctrl|Add0~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add0~4_combout\ = (\kbd_ctrl|ps2_ctrl|fcount\(2) & (\kbd_ctrl|ps2_ctrl|Add0~3\ $ (GND))) # (!\kbd_ctrl|ps2_ctrl|fcount\(2) & (!\kbd_ctrl|ps2_ctrl|Add0~3\ & VCC))
-- \kbd_ctrl|ps2_ctrl|Add0~5\ = CARRY((\kbd_ctrl|ps2_ctrl|fcount\(2) & !\kbd_ctrl|ps2_ctrl|Add0~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|fcount\(2),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|Add0~3\,
	combout => \kbd_ctrl|ps2_ctrl|Add0~4_combout\,
	cout => \kbd_ctrl|ps2_ctrl|Add0~5\);

-- Location: LCCOMB_X37_Y16_N20
\kbd_ctrl|ps2_ctrl|Add0~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add0~8_combout\ = (\kbd_ctrl|ps2_ctrl|fcount\(4) & (\kbd_ctrl|ps2_ctrl|Add0~7\ $ (GND))) # (!\kbd_ctrl|ps2_ctrl|fcount\(4) & (!\kbd_ctrl|ps2_ctrl|Add0~7\ & VCC))
-- \kbd_ctrl|ps2_ctrl|Add0~9\ = CARRY((\kbd_ctrl|ps2_ctrl|fcount\(4) & !\kbd_ctrl|ps2_ctrl|Add0~7\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|fcount\(4),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|Add0~7\,
	combout => \kbd_ctrl|ps2_ctrl|Add0~8_combout\,
	cout => \kbd_ctrl|ps2_ctrl|Add0~9\);

-- Location: LCCOMB_X37_Y16_N24
\kbd_ctrl|ps2_ctrl|Add0~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add0~12_combout\ = (\kbd_ctrl|ps2_ctrl|fcount\(6) & (\kbd_ctrl|ps2_ctrl|Add0~11\ $ (GND))) # (!\kbd_ctrl|ps2_ctrl|fcount\(6) & (!\kbd_ctrl|ps2_ctrl|Add0~11\ & VCC))
-- \kbd_ctrl|ps2_ctrl|Add0~13\ = CARRY((\kbd_ctrl|ps2_ctrl|fcount\(6) & !\kbd_ctrl|ps2_ctrl|Add0~11\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|fcount\(6),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|Add0~11\,
	combout => \kbd_ctrl|ps2_ctrl|Add0~12_combout\,
	cout => \kbd_ctrl|ps2_ctrl|Add0~13\);

-- Location: LCCOMB_X37_Y16_N26
\kbd_ctrl|ps2_ctrl|Add0~14\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add0~14_combout\ = \kbd_ctrl|ps2_ctrl|fcount\(7) $ (\kbd_ctrl|ps2_ctrl|Add0~13\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|fcount\(7),
	cin => \kbd_ctrl|ps2_ctrl|Add0~13\,
	combout => \kbd_ctrl|ps2_ctrl|Add0~14_combout\);

-- Location: LCCOMB_X39_Y16_N0
\kbd_ctrl|ps2_ctrl|Add1~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add1~0_combout\ = \kbd_ctrl|ps2_ctrl|rcount\(0) $ (VCC)
-- \kbd_ctrl|ps2_ctrl|Add1~1\ = CARRY(\kbd_ctrl|ps2_ctrl|rcount\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|rcount\(0),
	datad => VCC,
	combout => \kbd_ctrl|ps2_ctrl|Add1~0_combout\,
	cout => \kbd_ctrl|ps2_ctrl|Add1~1\);

-- Location: LCCOMB_X39_Y16_N4
\kbd_ctrl|ps2_ctrl|Add1~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add1~4_combout\ = (\kbd_ctrl|ps2_ctrl|rcount\(2) & (\kbd_ctrl|ps2_ctrl|Add1~3\ $ (GND))) # (!\kbd_ctrl|ps2_ctrl|rcount\(2) & (!\kbd_ctrl|ps2_ctrl|Add1~3\ & VCC))
-- \kbd_ctrl|ps2_ctrl|Add1~5\ = CARRY((\kbd_ctrl|ps2_ctrl|rcount\(2) & !\kbd_ctrl|ps2_ctrl|Add1~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|rcount\(2),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|Add1~3\,
	combout => \kbd_ctrl|ps2_ctrl|Add1~4_combout\,
	cout => \kbd_ctrl|ps2_ctrl|Add1~5\);

-- Location: LCCOMB_X39_Y16_N6
\kbd_ctrl|ps2_ctrl|Add1~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add1~6_combout\ = (\kbd_ctrl|ps2_ctrl|rcount\(3) & (!\kbd_ctrl|ps2_ctrl|Add1~5\)) # (!\kbd_ctrl|ps2_ctrl|rcount\(3) & ((\kbd_ctrl|ps2_ctrl|Add1~5\) # (GND)))
-- \kbd_ctrl|ps2_ctrl|Add1~7\ = CARRY((!\kbd_ctrl|ps2_ctrl|Add1~5\) # (!\kbd_ctrl|ps2_ctrl|rcount\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|rcount\(3),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|Add1~5\,
	combout => \kbd_ctrl|ps2_ctrl|Add1~6_combout\,
	cout => \kbd_ctrl|ps2_ctrl|Add1~7\);

-- Location: LCCOMB_X39_Y16_N8
\kbd_ctrl|ps2_ctrl|Add1~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add1~8_combout\ = (\kbd_ctrl|ps2_ctrl|rcount\(4) & (\kbd_ctrl|ps2_ctrl|Add1~7\ $ (GND))) # (!\kbd_ctrl|ps2_ctrl|rcount\(4) & (!\kbd_ctrl|ps2_ctrl|Add1~7\ & VCC))
-- \kbd_ctrl|ps2_ctrl|Add1~9\ = CARRY((\kbd_ctrl|ps2_ctrl|rcount\(4) & !\kbd_ctrl|ps2_ctrl|Add1~7\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|rcount\(4),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|Add1~7\,
	combout => \kbd_ctrl|ps2_ctrl|Add1~8_combout\,
	cout => \kbd_ctrl|ps2_ctrl|Add1~9\);

-- Location: LCFF_X10_Y24_N11
\count[17]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \count[17]~54_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => count(17));

-- Location: LCFF_X10_Y24_N9
\count[16]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \count[16]~52_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => count(16));

-- Location: LCFF_X10_Y24_N7
\count[15]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \count[15]~50_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => count(15));

-- Location: LCFF_X10_Y24_N13
\count[18]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \count[18]~56_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => count(18));

-- Location: LCFF_X10_Y24_N5
\count[14]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \count[14]~48_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => count(14));

-- Location: LCFF_X10_Y24_N3
\count[13]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \count[13]~46_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => count(13));

-- Location: LCFF_X10_Y25_N25
\count[8]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \count[8]~36_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => count(8));

-- Location: LCFF_X10_Y25_N27
\count[9]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \count[9]~38_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => count(9));

-- Location: LCFF_X10_Y25_N29
\count[10]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \count[10]~40_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => count(10));

-- Location: LCFF_X10_Y25_N31
\count[11]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \count[11]~42_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => count(11));

-- Location: LCFF_X10_Y24_N1
\count[12]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \count[12]~44_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => count(12));

-- Location: LCFF_X10_Y24_N21
\count[22]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \count[22]~64_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => count(22));

-- Location: LCFF_X10_Y24_N15
\count[19]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \count[19]~58_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => count(19));

-- Location: LCFF_X10_Y24_N17
\count[20]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \count[20]~60_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => count(20));

-- Location: LCFF_X10_Y24_N19
\count[21]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \count[21]~62_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => count(21));

-- Location: LCFF_X29_Y19_N21
\kbd_ctrl|ps2_ctrl|countclk[4]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|countclk[4]~24_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|FROMPS2~1_combout\,
	ena => \kbd_ctrl|ps2_ctrl|countclk[18]~54_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|countclk\(4));

-- Location: LCFF_X29_Y18_N21
\kbd_ctrl|ps2_ctrl|countclk[9]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|countclk[9]~feeder_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|FROMPS2~1_combout\,
	ena => \kbd_ctrl|ps2_ctrl|countclk[18]~54_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|countclk\(9));

-- Location: LCFF_X29_Y18_N11
\kbd_ctrl|ps2_ctrl|countclk[15]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|countclk[15]~46_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|FROMPS2~1_combout\,
	ena => \kbd_ctrl|ps2_ctrl|countclk[18]~54_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|countclk\(15));

-- Location: LCFF_X10_Y25_N23
\count[7]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \count[7]~34_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => count(7));

-- Location: LCFF_X10_Y25_N11
\count[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \count[1]~22_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => count(1));

-- Location: LCFF_X10_Y25_N13
\count[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \count[2]~24_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => count(2));

-- Location: LCFF_X10_Y25_N15
\count[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \count[3]~26_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => count(3));

-- Location: LCFF_X10_Y25_N17
\count[4]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \count[4]~28_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => count(4));

-- Location: LCFF_X10_Y25_N19
\count[5]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \count[5]~30_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => count(5));

-- Location: LCFF_X10_Y25_N21
\count[6]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \count[6]~32_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => count(6));

-- Location: LCCOMB_X10_Y25_N10
\count[1]~22\ : cycloneii_lcell_comb
-- Equation(s):
-- \count[1]~22_combout\ = (count(1) & (count(0) $ (VCC))) # (!count(1) & (count(0) & VCC))
-- \count[1]~23\ = CARRY((count(1) & count(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110011010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => count(1),
	datab => count(0),
	datad => VCC,
	combout => \count[1]~22_combout\,
	cout => \count[1]~23\);

-- Location: LCCOMB_X10_Y25_N12
\count[2]~24\ : cycloneii_lcell_comb
-- Equation(s):
-- \count[2]~24_combout\ = (count(2) & (!\count[1]~23\)) # (!count(2) & ((\count[1]~23\) # (GND)))
-- \count[2]~25\ = CARRY((!\count[1]~23\) # (!count(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => count(2),
	datad => VCC,
	cin => \count[1]~23\,
	combout => \count[2]~24_combout\,
	cout => \count[2]~25\);

-- Location: LCCOMB_X10_Y25_N14
\count[3]~26\ : cycloneii_lcell_comb
-- Equation(s):
-- \count[3]~26_combout\ = (count(3) & (\count[2]~25\ $ (GND))) # (!count(3) & (!\count[2]~25\ & VCC))
-- \count[3]~27\ = CARRY((count(3) & !\count[2]~25\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => count(3),
	datad => VCC,
	cin => \count[2]~25\,
	combout => \count[3]~26_combout\,
	cout => \count[3]~27\);

-- Location: LCCOMB_X10_Y25_N16
\count[4]~28\ : cycloneii_lcell_comb
-- Equation(s):
-- \count[4]~28_combout\ = (count(4) & (!\count[3]~27\)) # (!count(4) & ((\count[3]~27\) # (GND)))
-- \count[4]~29\ = CARRY((!\count[3]~27\) # (!count(4)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => count(4),
	datad => VCC,
	cin => \count[3]~27\,
	combout => \count[4]~28_combout\,
	cout => \count[4]~29\);

-- Location: LCCOMB_X10_Y25_N18
\count[5]~30\ : cycloneii_lcell_comb
-- Equation(s):
-- \count[5]~30_combout\ = (count(5) & (\count[4]~29\ $ (GND))) # (!count(5) & (!\count[4]~29\ & VCC))
-- \count[5]~31\ = CARRY((count(5) & !\count[4]~29\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => count(5),
	datad => VCC,
	cin => \count[4]~29\,
	combout => \count[5]~30_combout\,
	cout => \count[5]~31\);

-- Location: LCCOMB_X10_Y25_N20
\count[6]~32\ : cycloneii_lcell_comb
-- Equation(s):
-- \count[6]~32_combout\ = (count(6) & (!\count[5]~31\)) # (!count(6) & ((\count[5]~31\) # (GND)))
-- \count[6]~33\ = CARRY((!\count[5]~31\) # (!count(6)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => count(6),
	datad => VCC,
	cin => \count[5]~31\,
	combout => \count[6]~32_combout\,
	cout => \count[6]~33\);

-- Location: LCCOMB_X10_Y25_N22
\count[7]~34\ : cycloneii_lcell_comb
-- Equation(s):
-- \count[7]~34_combout\ = (count(7) & (\count[6]~33\ $ (GND))) # (!count(7) & (!\count[6]~33\ & VCC))
-- \count[7]~35\ = CARRY((count(7) & !\count[6]~33\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => count(7),
	datad => VCC,
	cin => \count[6]~33\,
	combout => \count[7]~34_combout\,
	cout => \count[7]~35\);

-- Location: LCCOMB_X10_Y25_N24
\count[8]~36\ : cycloneii_lcell_comb
-- Equation(s):
-- \count[8]~36_combout\ = (count(8) & (!\count[7]~35\)) # (!count(8) & ((\count[7]~35\) # (GND)))
-- \count[8]~37\ = CARRY((!\count[7]~35\) # (!count(8)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => count(8),
	datad => VCC,
	cin => \count[7]~35\,
	combout => \count[8]~36_combout\,
	cout => \count[8]~37\);

-- Location: LCCOMB_X10_Y25_N26
\count[9]~38\ : cycloneii_lcell_comb
-- Equation(s):
-- \count[9]~38_combout\ = (\count[8]~37\ & (count(9) & (!\Equal8~7_combout\ & VCC))) # (!\count[8]~37\ & ((((count(9) & !\Equal8~7_combout\)))))
-- \count[9]~39\ = CARRY((count(9) & (!\Equal8~7_combout\ & !\count[8]~37\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010110100000010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => count(9),
	datab => \Equal8~7_combout\,
	datad => VCC,
	cin => \count[8]~37\,
	combout => \count[9]~38_combout\,
	cout => \count[9]~39\);

-- Location: LCCOMB_X10_Y25_N28
\count[10]~40\ : cycloneii_lcell_comb
-- Equation(s):
-- \count[10]~40_combout\ = (\count[9]~39\ & (((\Equal8~7_combout\)) # (!count(10)))) # (!\count[9]~39\ & (((count(10) & !\Equal8~7_combout\)) # (GND)))
-- \count[10]~41\ = CARRY(((\Equal8~7_combout\) # (!\count[9]~39\)) # (!count(10)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101001011011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => count(10),
	datab => \Equal8~7_combout\,
	datad => VCC,
	cin => \count[9]~39\,
	combout => \count[10]~40_combout\,
	cout => \count[10]~41\);

-- Location: LCCOMB_X10_Y25_N30
\count[11]~42\ : cycloneii_lcell_comb
-- Equation(s):
-- \count[11]~42_combout\ = (\count[10]~41\ & (count(11) & (!\Equal8~7_combout\ & VCC))) # (!\count[10]~41\ & ((((count(11) & !\Equal8~7_combout\)))))
-- \count[11]~43\ = CARRY((count(11) & (!\Equal8~7_combout\ & !\count[10]~41\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010110100000010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => count(11),
	datab => \Equal8~7_combout\,
	datad => VCC,
	cin => \count[10]~41\,
	combout => \count[11]~42_combout\,
	cout => \count[11]~43\);

-- Location: LCCOMB_X10_Y24_N0
\count[12]~44\ : cycloneii_lcell_comb
-- Equation(s):
-- \count[12]~44_combout\ = (\count[11]~43\ & (((\Equal8~7_combout\)) # (!count(12)))) # (!\count[11]~43\ & (((count(12) & !\Equal8~7_combout\)) # (GND)))
-- \count[12]~45\ = CARRY(((\Equal8~7_combout\) # (!\count[11]~43\)) # (!count(12)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101001011011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => count(12),
	datab => \Equal8~7_combout\,
	datad => VCC,
	cin => \count[11]~43\,
	combout => \count[12]~44_combout\,
	cout => \count[12]~45\);

-- Location: LCCOMB_X10_Y24_N2
\count[13]~46\ : cycloneii_lcell_comb
-- Equation(s):
-- \count[13]~46_combout\ = (\count[12]~45\ & (!\Equal8~7_combout\ & (count(13) & VCC))) # (!\count[12]~45\ & ((((!\Equal8~7_combout\ & count(13))))))
-- \count[13]~47\ = CARRY((!\Equal8~7_combout\ & (count(13) & !\count[12]~45\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100101100000100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Equal8~7_combout\,
	datab => count(13),
	datad => VCC,
	cin => \count[12]~45\,
	combout => \count[13]~46_combout\,
	cout => \count[13]~47\);

-- Location: LCCOMB_X10_Y24_N4
\count[14]~48\ : cycloneii_lcell_comb
-- Equation(s):
-- \count[14]~48_combout\ = (count(14) & (!\count[13]~47\)) # (!count(14) & ((\count[13]~47\) # (GND)))
-- \count[14]~49\ = CARRY((!\count[13]~47\) # (!count(14)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => count(14),
	datad => VCC,
	cin => \count[13]~47\,
	combout => \count[14]~48_combout\,
	cout => \count[14]~49\);

-- Location: LCCOMB_X10_Y24_N6
\count[15]~50\ : cycloneii_lcell_comb
-- Equation(s):
-- \count[15]~50_combout\ = (count(15) & (\count[14]~49\ $ (GND))) # (!count(15) & (!\count[14]~49\ & VCC))
-- \count[15]~51\ = CARRY((count(15) & !\count[14]~49\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => count(15),
	datad => VCC,
	cin => \count[14]~49\,
	combout => \count[15]~50_combout\,
	cout => \count[15]~51\);

-- Location: LCCOMB_X10_Y24_N8
\count[16]~52\ : cycloneii_lcell_comb
-- Equation(s):
-- \count[16]~52_combout\ = (\count[15]~51\ & (((\Equal8~7_combout\)) # (!count(16)))) # (!\count[15]~51\ & (((count(16) & !\Equal8~7_combout\)) # (GND)))
-- \count[16]~53\ = CARRY(((\Equal8~7_combout\) # (!\count[15]~51\)) # (!count(16)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101001011011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => count(16),
	datab => \Equal8~7_combout\,
	datad => VCC,
	cin => \count[15]~51\,
	combout => \count[16]~52_combout\,
	cout => \count[16]~53\);

-- Location: LCCOMB_X10_Y24_N10
\count[17]~54\ : cycloneii_lcell_comb
-- Equation(s):
-- \count[17]~54_combout\ = (count(17) & (\count[16]~53\ $ (GND))) # (!count(17) & (!\count[16]~53\ & VCC))
-- \count[17]~55\ = CARRY((count(17) & !\count[16]~53\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => count(17),
	datad => VCC,
	cin => \count[16]~53\,
	combout => \count[17]~54_combout\,
	cout => \count[17]~55\);

-- Location: LCCOMB_X10_Y24_N12
\count[18]~56\ : cycloneii_lcell_comb
-- Equation(s):
-- \count[18]~56_combout\ = (count(18) & (!\count[17]~55\)) # (!count(18) & ((\count[17]~55\) # (GND)))
-- \count[18]~57\ = CARRY((!\count[17]~55\) # (!count(18)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => count(18),
	datad => VCC,
	cin => \count[17]~55\,
	combout => \count[18]~56_combout\,
	cout => \count[18]~57\);

-- Location: LCCOMB_X10_Y24_N14
\count[19]~58\ : cycloneii_lcell_comb
-- Equation(s):
-- \count[19]~58_combout\ = (\count[18]~57\ & (!\Equal8~7_combout\ & (count(19) & VCC))) # (!\count[18]~57\ & ((((!\Equal8~7_combout\ & count(19))))))
-- \count[19]~59\ = CARRY((!\Equal8~7_combout\ & (count(19) & !\count[18]~57\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100101100000100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Equal8~7_combout\,
	datab => count(19),
	datad => VCC,
	cin => \count[18]~57\,
	combout => \count[19]~58_combout\,
	cout => \count[19]~59\);

-- Location: LCCOMB_X10_Y24_N16
\count[20]~60\ : cycloneii_lcell_comb
-- Equation(s):
-- \count[20]~60_combout\ = (count(20) & (!\count[19]~59\)) # (!count(20) & ((\count[19]~59\) # (GND)))
-- \count[20]~61\ = CARRY((!\count[19]~59\) # (!count(20)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => count(20),
	datad => VCC,
	cin => \count[19]~59\,
	combout => \count[20]~60_combout\,
	cout => \count[20]~61\);

-- Location: LCCOMB_X10_Y24_N18
\count[21]~62\ : cycloneii_lcell_comb
-- Equation(s):
-- \count[21]~62_combout\ = (count(21) & (\count[20]~61\ $ (GND))) # (!count(21) & (!\count[20]~61\ & VCC))
-- \count[21]~63\ = CARRY((count(21) & !\count[20]~61\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => count(21),
	datad => VCC,
	cin => \count[20]~61\,
	combout => \count[21]~62_combout\,
	cout => \count[21]~63\);

-- Location: LCCOMB_X10_Y24_N20
\count[22]~64\ : cycloneii_lcell_comb
-- Equation(s):
-- \count[22]~64_combout\ = \count[21]~63\ $ (((count(22) & !\Equal8~7_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000001011010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => count(22),
	datad => \Equal8~7_combout\,
	cin => \count[21]~63\,
	combout => \count[22]~64_combout\);

-- Location: LCCOMB_X29_Y19_N20
\kbd_ctrl|ps2_ctrl|countclk[4]~24\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|countclk[4]~24_combout\ = (\kbd_ctrl|ps2_ctrl|countclk\(4) & (!\kbd_ctrl|ps2_ctrl|countclk[3]~23\)) # (!\kbd_ctrl|ps2_ctrl|countclk\(4) & ((\kbd_ctrl|ps2_ctrl|countclk[3]~23\) # (GND)))
-- \kbd_ctrl|ps2_ctrl|countclk[4]~25\ = CARRY((!\kbd_ctrl|ps2_ctrl|countclk[3]~23\) # (!\kbd_ctrl|ps2_ctrl|countclk\(4)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|countclk\(4),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|countclk[3]~23\,
	combout => \kbd_ctrl|ps2_ctrl|countclk[4]~24_combout\,
	cout => \kbd_ctrl|ps2_ctrl|countclk[4]~25\);

-- Location: LCCOMB_X29_Y19_N30
\kbd_ctrl|ps2_ctrl|countclk[9]~34\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|countclk[9]~34_combout\ = (\kbd_ctrl|ps2_ctrl|countclk\(9) & (\kbd_ctrl|ps2_ctrl|countclk[8]~33\ $ (GND))) # (!\kbd_ctrl|ps2_ctrl|countclk\(9) & (!\kbd_ctrl|ps2_ctrl|countclk[8]~33\ & VCC))
-- \kbd_ctrl|ps2_ctrl|countclk[9]~35\ = CARRY((\kbd_ctrl|ps2_ctrl|countclk\(9) & !\kbd_ctrl|ps2_ctrl|countclk[8]~33\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|countclk\(9),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|countclk[8]~33\,
	combout => \kbd_ctrl|ps2_ctrl|countclk[9]~34_combout\,
	cout => \kbd_ctrl|ps2_ctrl|countclk[9]~35\);

-- Location: LCCOMB_X29_Y18_N10
\kbd_ctrl|ps2_ctrl|countclk[15]~46\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|countclk[15]~46_combout\ = (\kbd_ctrl|ps2_ctrl|countclk\(15) & (\kbd_ctrl|ps2_ctrl|countclk[14]~45\ $ (GND))) # (!\kbd_ctrl|ps2_ctrl|countclk\(15) & (!\kbd_ctrl|ps2_ctrl|countclk[14]~45\ & VCC))
-- \kbd_ctrl|ps2_ctrl|countclk[15]~47\ = CARRY((\kbd_ctrl|ps2_ctrl|countclk\(15) & !\kbd_ctrl|ps2_ctrl|countclk[14]~45\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|countclk\(15),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|countclk[14]~45\,
	combout => \kbd_ctrl|ps2_ctrl|countclk[15]~46_combout\,
	cout => \kbd_ctrl|ps2_ctrl|countclk[15]~47\);

-- Location: LCCOMB_X22_Y17_N6
\Equal0~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \Equal0~3_combout\ = (!\kbd_ctrl|key0code\(1) & \kbd_ctrl|key0code\(2))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|key0code\(1),
	datad => \kbd_ctrl|key0code\(2),
	combout => \Equal0~3_combout\);

-- Location: LCCOMB_X24_Y17_N20
\kbd_ctrl|Equal14~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal14~1_combout\ = (\kbd_ctrl|fetchdata\(3) & (\kbd_ctrl|key0code\(3) & (\kbd_ctrl|key0code\(2) $ (!\kbd_ctrl|fetchdata\(2))))) # (!\kbd_ctrl|fetchdata\(3) & (!\kbd_ctrl|key0code\(3) & (\kbd_ctrl|key0code\(2) $ (!\kbd_ctrl|fetchdata\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000001001000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|fetchdata\(3),
	datab => \kbd_ctrl|key0code\(2),
	datac => \kbd_ctrl|fetchdata\(2),
	datad => \kbd_ctrl|key0code\(3),
	combout => \kbd_ctrl|Equal14~1_combout\);

-- Location: LCCOMB_X26_Y17_N12
\kbd_ctrl|Equal11~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal11~0_combout\ = (\kbd_ctrl|key0code\(1)) # (((\kbd_ctrl|key0code\(5)) # (!\kbd_ctrl|key0code\(4))) # (!\kbd_ctrl|key0code\(13)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(1),
	datab => \kbd_ctrl|key0code\(13),
	datac => \kbd_ctrl|key0code\(5),
	datad => \kbd_ctrl|key0code\(4),
	combout => \kbd_ctrl|Equal11~0_combout\);

-- Location: LCCOMB_X26_Y17_N10
\kbd_ctrl|key0clearn~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|key0clearn~0_combout\ = (\kbd_ctrl|key0code\(1) & (!\kbd_ctrl|key0code\(6) & (!\kbd_ctrl|key0code\(3) & \kbd_ctrl|key0code\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(1),
	datab => \kbd_ctrl|key0code\(6),
	datac => \kbd_ctrl|key0code\(3),
	datad => \kbd_ctrl|key0code\(4),
	combout => \kbd_ctrl|key0clearn~0_combout\);

-- Location: LCCOMB_X25_Y17_N16
\kbd_ctrl|Equal4~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal4~2_combout\ = (\kbd_ctrl|fetchdata\(5) & (\kbd_ctrl|key1code\(5) & (\kbd_ctrl|key1code\(4) $ (!\kbd_ctrl|fetchdata\(4))))) # (!\kbd_ctrl|fetchdata\(5) & (!\kbd_ctrl|key1code\(5) & (\kbd_ctrl|key1code\(4) $ (!\kbd_ctrl|fetchdata\(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000010000100001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|fetchdata\(5),
	datab => \kbd_ctrl|key1code\(4),
	datac => \kbd_ctrl|key1code\(5),
	datad => \kbd_ctrl|fetchdata\(4),
	combout => \kbd_ctrl|Equal4~2_combout\);

-- Location: LCCOMB_X27_Y17_N28
\kbd_ctrl|Equal5~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal5~1_combout\ = (\kbd_ctrl|key2code\(2) & (\kbd_ctrl|fetchdata\(2) & (\kbd_ctrl|key2code\(3) $ (!\kbd_ctrl|fetchdata\(3))))) # (!\kbd_ctrl|key2code\(2) & (!\kbd_ctrl|fetchdata\(2) & (\kbd_ctrl|key2code\(3) $ (!\kbd_ctrl|fetchdata\(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000010000100001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key2code\(2),
	datab => \kbd_ctrl|key2code\(3),
	datac => \kbd_ctrl|fetchdata\(2),
	datad => \kbd_ctrl|fetchdata\(3),
	combout => \kbd_ctrl|Equal5~1_combout\);

-- Location: LCCOMB_X24_Y17_N22
\kbd_ctrl|Equal18~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal18~3_combout\ = (!\kbd_ctrl|key1code\(3) & !\kbd_ctrl|key1code\(6))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|key1code\(3),
	datad => \kbd_ctrl|key1code\(6),
	combout => \kbd_ctrl|Equal18~3_combout\);

-- Location: LCCOMB_X27_Y17_N0
\kbd_ctrl|Equal19~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal19~1_combout\ = (!\kbd_ctrl|key2code\(1) & (!\kbd_ctrl|key2code\(6) & (!\kbd_ctrl|key2code\(3) & !\kbd_ctrl|key2code\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key2code\(1),
	datab => \kbd_ctrl|key2code\(6),
	datac => \kbd_ctrl|key2code\(3),
	datad => \kbd_ctrl|key2code\(0),
	combout => \kbd_ctrl|Equal19~1_combout\);

-- Location: LCCOMB_X25_Y17_N30
\kbd_ctrl|Equal12~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal12~0_combout\ = (\kbd_ctrl|key1code\(7)) # (((\kbd_ctrl|key1code\(2)) # (\kbd_ctrl|key1code\(5))) # (!\kbd_ctrl|key1code\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key1code\(7),
	datab => \kbd_ctrl|key1code\(0),
	datac => \kbd_ctrl|key1code\(2),
	datad => \kbd_ctrl|key1code\(5),
	combout => \kbd_ctrl|Equal12~0_combout\);

-- Location: LCCOMB_X26_Y17_N6
\kbd_ctrl|Equal12~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal12~2_combout\ = (\kbd_ctrl|Equal12~0_combout\) # ((\kbd_ctrl|Equal12~1_combout\) # (!\kbd_ctrl|key1code\(6)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110111011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|Equal12~0_combout\,
	datab => \kbd_ctrl|key1code\(6),
	datad => \kbd_ctrl|Equal12~1_combout\,
	combout => \kbd_ctrl|Equal12~2_combout\);

-- Location: LCCOMB_X27_Y17_N2
\kbd_ctrl|KEY2~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|KEY2~3_combout\ = (!\kbd_ctrl|key2code\(0) & (!\kbd_ctrl|key2code\(3) & (!\kbd_ctrl|key2code\(6) & \kbd_ctrl|key2code\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key2code\(0),
	datab => \kbd_ctrl|key2code\(3),
	datac => \kbd_ctrl|key2code\(6),
	datad => \kbd_ctrl|key2code\(1),
	combout => \kbd_ctrl|KEY2~3_combout\);

-- Location: LCCOMB_X27_Y17_N12
\kbd_ctrl|KEY2~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|KEY2~4_combout\ = (\kbd_ctrl|KEY2~3_combout\ & ((\kbd_ctrl|Equal8~2_combout\) # ((!\kbd_ctrl|Equal18~2_combout\) # (!\kbd_ctrl|Equal18~3_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|Equal8~2_combout\,
	datab => \kbd_ctrl|Equal18~3_combout\,
	datac => \kbd_ctrl|KEY2~3_combout\,
	datad => \kbd_ctrl|Equal18~2_combout\,
	combout => \kbd_ctrl|KEY2~4_combout\);

-- Location: LCFF_X31_Y19_N25
\kbd_ctrl|ps2_ctrl|FROMPS2:count[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|count~14_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|FROMPS2~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|FROMPS2:count[3]~regout\);

-- Location: LCCOMB_X32_Y17_N8
\kbd_ctrl|Equal0~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal0~1_combout\ = (\kbd_ctrl|fetchdata\(5) & (\kbd_ctrl|Equal0~0_combout\ & \kbd_ctrl|fetchdata\(7)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|fetchdata\(5),
	datac => \kbd_ctrl|Equal0~0_combout\,
	datad => \kbd_ctrl|fetchdata\(7),
	combout => \kbd_ctrl|Equal0~1_combout\);

-- Location: LCCOMB_X33_Y18_N24
\kbd_ctrl|ps2_ctrl|ps2_data~10\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|ps2_data~10_combout\ = \kbd_ctrl|ps2_ctrl|hdata\(0) $ (\kbd_ctrl|ps2_ctrl|hdata\(1) $ (\kbd_ctrl|ps2_ctrl|hdata\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|hdata\(0),
	datac => \kbd_ctrl|ps2_ctrl|hdata\(1),
	datad => \kbd_ctrl|ps2_ctrl|hdata\(2),
	combout => \kbd_ctrl|ps2_ctrl|ps2_data~10_combout\);

-- Location: LCFF_X33_Y17_N11
\kbd_ctrl|sigsend\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|cmdstate~18_combout\,
	aclr => \kbd_ctrl|process_15~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|sigsend~regout\);

-- Location: LCCOMB_X31_Y19_N24
\kbd_ctrl|ps2_ctrl|count~14\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|count~14_combout\ = (\kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~regout\ & ((\kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~regout\ & (\kbd_ctrl|ps2_ctrl|FROMPS2:count[3]~regout\ $ (\kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~regout\))) # 
-- (!\kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~regout\ & (\kbd_ctrl|ps2_ctrl|FROMPS2:count[3]~regout\ & \kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~regout\)))) # (!\kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~regout\ & (((\kbd_ctrl|ps2_ctrl|FROMPS2:count[3]~regout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100011010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~regout\,
	datab => \kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~regout\,
	datac => \kbd_ctrl|ps2_ctrl|FROMPS2:count[3]~regout\,
	datad => \kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~regout\,
	combout => \kbd_ctrl|ps2_ctrl|count~14_combout\);

-- Location: LCFF_X37_Y16_N9
\kbd_ctrl|ps2_ctrl|fcount[7]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|Add0~16_combout\,
	aclr => \ALT_INV_KEY~combout\(0),
	ena => \kbd_ctrl|ps2_ctrl|fcount[7]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|fcount\(7));

-- Location: LCFF_X33_Y17_N29
\kbd_ctrl|state.EXT1\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|state.EXT1~0_combout\,
	aclr => \kbd_ctrl|process_0~0_combout\,
	ena => \KEY~combout\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|state.EXT1~regout\);

-- Location: LCCOMB_X32_Y18_N24
\kbd_ctrl|ps2_ctrl|sigsendend~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|sigsendend~1_combout\ = (\kbd_ctrl|ps2_ctrl|TOPS2:count[2]~regout\ & (\kbd_ctrl|ps2_ctrl|sigsendend~regout\)) # (!\kbd_ctrl|ps2_ctrl|TOPS2:count[2]~regout\ & ((\kbd_ctrl|ps2_ctrl|TOPS2:count[1]~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|sigsendend~regout\,
	datac => \kbd_ctrl|ps2_ctrl|TOPS2:count[2]~regout\,
	datad => \kbd_ctrl|ps2_ctrl|TOPS2:count[1]~regout\,
	combout => \kbd_ctrl|ps2_ctrl|sigsendend~1_combout\);

-- Location: LCFF_X33_Y17_N5
\kbd_ctrl|cmdstate.SEND\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|Selector9~0_combout\,
	aclr => \kbd_ctrl|process_15~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|cmdstate.SEND~regout\);

-- Location: LCCOMB_X33_Y17_N10
\kbd_ctrl|cmdstate~18\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|cmdstate~18_combout\ = (\kbd_ctrl|cmdstate.SEND~regout\) # (\kbd_ctrl|cmdstate.SENDVAL~regout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \kbd_ctrl|cmdstate.SEND~regout\,
	datad => \kbd_ctrl|cmdstate.SENDVAL~regout\,
	combout => \kbd_ctrl|cmdstate~18_combout\);

-- Location: LCFF_X37_Y16_N11
\kbd_ctrl|ps2_ctrl|fcount[4]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|Add0~19_combout\,
	aclr => \ALT_INV_KEY~combout\(0),
	ena => \kbd_ctrl|ps2_ctrl|fcount[7]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|fcount\(4));

-- Location: LCFF_X37_Y16_N7
\kbd_ctrl|ps2_ctrl|fcount[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|Add0~21_combout\,
	aclr => \ALT_INV_KEY~combout\(0),
	ena => \kbd_ctrl|ps2_ctrl|fcount[7]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|fcount\(2));

-- Location: LCCOMB_X37_Y16_N8
\kbd_ctrl|ps2_ctrl|Add0~16\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add0~16_combout\ = (!\PS2_CLK~0\ & \kbd_ctrl|ps2_ctrl|Add0~14_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \PS2_CLK~0\,
	datad => \kbd_ctrl|ps2_ctrl|Add0~14_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Add0~16_combout\);

-- Location: LCFF_X39_Y16_N25
\kbd_ctrl|ps2_ctrl|rcount[4]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|Add1~19_combout\,
	aclr => \ALT_INV_KEY~combout\(0),
	ena => \kbd_ctrl|ps2_ctrl|rcount[7]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|rcount\(4));

-- Location: LCFF_X38_Y16_N15
\kbd_ctrl|ps2_ctrl|rcount[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|Add1~20_combout\,
	aclr => \ALT_INV_KEY~combout\(0),
	ena => \kbd_ctrl|ps2_ctrl|rcount[7]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|rcount\(3));

-- Location: LCFF_X38_Y16_N31
\kbd_ctrl|ps2_ctrl|rcount[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|Add1~21_combout\,
	aclr => \ALT_INV_KEY~combout\(0),
	ena => \kbd_ctrl|ps2_ctrl|rcount[7]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|rcount\(2));

-- Location: LCFF_X39_Y16_N17
\kbd_ctrl|ps2_ctrl|rcount[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|Add1~23_combout\,
	aclr => \ALT_INV_KEY~combout\(0),
	ena => \kbd_ctrl|ps2_ctrl|rcount[7]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|rcount\(0));

-- Location: LCCOMB_X33_Y17_N28
\kbd_ctrl|state.EXT1~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|state.EXT1~0_combout\ = (\kbd_ctrl|Equal0~1_combout\ & (\kbd_ctrl|state.DECODE~regout\ & (!\kbd_ctrl|fetchdata\(4) & \kbd_ctrl|fetchdata\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|Equal0~1_combout\,
	datab => \kbd_ctrl|state.DECODE~regout\,
	datac => \kbd_ctrl|fetchdata\(4),
	datad => \kbd_ctrl|fetchdata\(0),
	combout => \kbd_ctrl|state.EXT1~0_combout\);

-- Location: LCCOMB_X33_Y17_N16
\kbd_ctrl|Selector13~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Selector13~0_combout\ = (!\kbd_ctrl|cmdstate.CLEAR~regout\ & !\kbd_ctrl|cmdstate.SENDVAL~regout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|cmdstate.CLEAR~regout\,
	datad => \kbd_ctrl|cmdstate.SENDVAL~regout\,
	combout => \kbd_ctrl|Selector13~0_combout\);

-- Location: LCFF_X23_Y24_N1
CLOCKHZ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \LessThan0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \CLOCKHZ~regout\);

-- Location: LCCOMB_X33_Y17_N4
\kbd_ctrl|Selector9~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Selector9~0_combout\ = (!\kbd_ctrl|cmdstate.SETCMD~regout\ & (!\kbd_ctrl|cmdstate.SETLIGHTS~regout\ & \kbd_ctrl|ps2_ctrl|send_rdy~2_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|cmdstate.SETCMD~regout\,
	datab => \kbd_ctrl|cmdstate.SETLIGHTS~regout\,
	datad => \kbd_ctrl|ps2_ctrl|send_rdy~2_combout\,
	combout => \kbd_ctrl|Selector9~0_combout\);

-- Location: LCCOMB_X37_Y16_N10
\kbd_ctrl|ps2_ctrl|Add0~19\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add0~19_combout\ = (\kbd_ctrl|ps2_ctrl|Add0~8_combout\ & !\PS2_CLK~0\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \kbd_ctrl|ps2_ctrl|Add0~8_combout\,
	datad => \PS2_CLK~0\,
	combout => \kbd_ctrl|ps2_ctrl|Add0~19_combout\);

-- Location: LCCOMB_X37_Y16_N6
\kbd_ctrl|ps2_ctrl|Add0~21\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add0~21_combout\ = (\kbd_ctrl|ps2_ctrl|Add0~4_combout\ & !\PS2_CLK~0\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \kbd_ctrl|ps2_ctrl|Add0~4_combout\,
	datad => \PS2_CLK~0\,
	combout => \kbd_ctrl|ps2_ctrl|Add0~21_combout\);

-- Location: LCCOMB_X39_Y16_N24
\kbd_ctrl|ps2_ctrl|Add1~19\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add1~19_combout\ = (\PS2_CLK~0\ & \kbd_ctrl|ps2_ctrl|Add1~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \PS2_CLK~0\,
	datac => \kbd_ctrl|ps2_ctrl|Add1~8_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Add1~19_combout\);

-- Location: LCCOMB_X38_Y16_N14
\kbd_ctrl|ps2_ctrl|Add1~20\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add1~20_combout\ = (\PS2_CLK~0\ & \kbd_ctrl|ps2_ctrl|Add1~6_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \PS2_CLK~0\,
	datad => \kbd_ctrl|ps2_ctrl|Add1~6_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Add1~20_combout\);

-- Location: LCCOMB_X38_Y16_N30
\kbd_ctrl|ps2_ctrl|Add1~21\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add1~21_combout\ = (\PS2_CLK~0\ & \kbd_ctrl|ps2_ctrl|Add1~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \PS2_CLK~0\,
	datad => \kbd_ctrl|ps2_ctrl|Add1~4_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Add1~21_combout\);

-- Location: LCCOMB_X39_Y16_N16
\kbd_ctrl|ps2_ctrl|Add1~23\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add1~23_combout\ = (\PS2_CLK~0\ & \kbd_ctrl|ps2_ctrl|Add1~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \PS2_CLK~0\,
	datad => \kbd_ctrl|ps2_ctrl|Add1~0_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Add1~23_combout\);

-- Location: LCCOMB_X31_Y19_N8
\kbd_ctrl|ps2_ctrl|parchecked~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|parchecked~1_combout\ = (\kbd_ctrl|ps2_ctrl|FROMPS2:count[3]~regout\ & (!\kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~regout\ & (\kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~regout\ $ (\kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~regout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|FROMPS2:count[3]~regout\,
	datab => \kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~regout\,
	datac => \kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~regout\,
	datad => \kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~regout\,
	combout => \kbd_ctrl|ps2_ctrl|parchecked~1_combout\);

-- Location: LCCOMB_X32_Y17_N20
\kbd_ctrl|Selector8~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Selector8~0_combout\ = (\kbd_ctrl|cmdstate.WAITACK~regout\ & \kbd_ctrl|ps2_ctrl|odata_rdy~combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|cmdstate.WAITACK~regout\,
	datad => \kbd_ctrl|ps2_ctrl|odata_rdy~combout\,
	combout => \kbd_ctrl|Selector8~0_combout\);

-- Location: LCCOMB_X11_Y24_N12
\LessThan0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \LessThan0~0_combout\ = ((!count(16) & (!count(15) & !count(17)))) # (!count(18))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010101010111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => count(18),
	datab => count(16),
	datac => count(15),
	datad => count(17),
	combout => \LessThan0~0_combout\);

-- Location: LCCOMB_X11_Y24_N14
\LessThan0~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \LessThan0~1_combout\ = (!count(13) & (!count(16) & (!count(14) & !count(17))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => count(13),
	datab => count(16),
	datac => count(14),
	datad => count(17),
	combout => \LessThan0~1_combout\);

-- Location: LCCOMB_X10_Y25_N0
\Equal8~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Equal8~0_combout\ = (count(10) & (count(9) & (count(11) & count(12))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => count(10),
	datab => count(9),
	datac => count(11),
	datad => count(12),
	combout => \Equal8~0_combout\);

-- Location: LCCOMB_X11_Y24_N0
\LessThan0~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \LessThan0~2_combout\ = (\LessThan0~0_combout\) # ((\LessThan0~1_combout\ & ((!count(8)) # (!\Equal8~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111101110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal8~0_combout\,
	datab => count(8),
	datac => \LessThan0~1_combout\,
	datad => \LessThan0~0_combout\,
	combout => \LessThan0~2_combout\);

-- Location: LCCOMB_X11_Y24_N26
\LessThan0~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \LessThan0~3_combout\ = (!count(20) & (!count(22) & !count(19)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => count(20),
	datac => count(22),
	datad => count(19),
	combout => \LessThan0~3_combout\);

-- Location: LCCOMB_X23_Y24_N0
\LessThan0~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \LessThan0~4_combout\ = (\LessThan0~3_combout\ & ((\LessThan0~2_combout\) # ((!count(21) & !count(22))))) # (!\LessThan0~3_combout\ & (!count(21) & (!count(22))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101100000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \LessThan0~3_combout\,
	datab => count(21),
	datac => count(22),
	datad => \LessThan0~2_combout\,
	combout => \LessThan0~4_combout\);

-- Location: LCCOMB_X29_Y18_N24
\kbd_ctrl|ps2_ctrl|Equal2~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Equal2~3_combout\ = (\kbd_ctrl|ps2_ctrl|countclk\(8) & (!\kbd_ctrl|ps2_ctrl|countclk\(11) & (\kbd_ctrl|ps2_ctrl|countclk\(9) & \kbd_ctrl|ps2_ctrl|countclk\(10))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|countclk\(8),
	datab => \kbd_ctrl|ps2_ctrl|countclk\(11),
	datac => \kbd_ctrl|ps2_ctrl|countclk\(9),
	datad => \kbd_ctrl|ps2_ctrl|countclk\(10),
	combout => \kbd_ctrl|ps2_ctrl|Equal2~3_combout\);

-- Location: LCFF_X9_Y24_N21
\count[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \count[0]~66_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => count(0));

-- Location: LCCOMB_X10_Y24_N30
\Equal8~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \Equal8~1_combout\ = (!count(17) & (!count(14) & (!count(0) & !count(8))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => count(17),
	datab => count(14),
	datac => count(0),
	datad => count(8),
	combout => \Equal8~1_combout\);

-- Location: LCCOMB_X10_Y24_N24
\Equal8~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Equal8~2_combout\ = (!count(15) & (count(13) & (count(16) & !count(18))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => count(15),
	datab => count(13),
	datac => count(16),
	datad => count(18),
	combout => \Equal8~2_combout\);

-- Location: LCCOMB_X10_Y24_N22
\Equal8~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \Equal8~3_combout\ = (!count(20) & (count(19) & (count(22) & !count(21))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => count(20),
	datab => count(19),
	datac => count(22),
	datad => count(21),
	combout => \Equal8~3_combout\);

-- Location: LCCOMB_X10_Y24_N28
\Equal8~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \Equal8~4_combout\ = (\Equal8~2_combout\ & (\Equal8~3_combout\ & (\Equal8~1_combout\ & \Equal8~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal8~2_combout\,
	datab => \Equal8~3_combout\,
	datac => \Equal8~1_combout\,
	datad => \Equal8~0_combout\,
	combout => \Equal8~4_combout\);

-- Location: LCCOMB_X10_Y25_N2
\Equal8~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \Equal8~5_combout\ = (!count(1) & (!count(7) & (!count(3) & !count(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => count(1),
	datab => count(7),
	datac => count(3),
	datad => count(2),
	combout => \Equal8~5_combout\);

-- Location: LCCOMB_X10_Y25_N8
\Equal8~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \Equal8~6_combout\ = (!count(4) & (!count(6) & !count(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => count(4),
	datac => count(6),
	datad => count(5),
	combout => \Equal8~6_combout\);

-- Location: LCCOMB_X10_Y24_N26
\Equal8~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \Equal8~7_combout\ = (\Equal8~6_combout\ & (\Equal8~5_combout\ & \Equal8~4_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Equal8~6_combout\,
	datac => \Equal8~5_combout\,
	datad => \Equal8~4_combout\,
	combout => \Equal8~7_combout\);

-- Location: LCCOMB_X9_Y24_N20
\count[0]~66\ : cycloneii_lcell_comb
-- Equation(s):
-- \count[0]~66_combout\ = !count(0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => count(0),
	combout => \count[0]~66_combout\);

-- Location: CLKCTRL_G7
\kbd_ctrl|sigsend~clkctrl\ : cycloneii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \kbd_ctrl|sigsend~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \kbd_ctrl|sigsend~clkctrl_outclk\);

-- Location: CLKCTRL_G8
\CLOCKHZ~clkctrl\ : cycloneii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \CLOCKHZ~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \CLOCKHZ~clkctrl_outclk\);

-- Location: LCCOMB_X29_Y18_N20
\kbd_ctrl|ps2_ctrl|countclk[9]~feeder\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|countclk[9]~feeder_combout\ = \kbd_ctrl|ps2_ctrl|countclk[9]~34_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \kbd_ctrl|ps2_ctrl|countclk[9]~34_combout\,
	combout => \kbd_ctrl|ps2_ctrl|countclk[9]~feeder_combout\);

-- Location: PIN_J14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\PS2_DAT~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \kbd_ctrl|ps2_ctrl|ps2_data~reg0_regout\,
	oe => \kbd_ctrl|ps2_ctrl|ps2_data~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => PS2_DAT,
	combout => \PS2_DAT~0\);

-- Location: PIN_H15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\PS2_CLK~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	open_drain_output => "true",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \kbd_ctrl|ps2_ctrl|ALT_INV_ps2_clk~en_regout\,
	oe => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => PS2_CLK,
	combout => \PS2_CLK~0\);

-- Location: LCCOMB_X37_Y16_N12
\kbd_ctrl|ps2_ctrl|Add0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add0~0_combout\ = \kbd_ctrl|ps2_ctrl|fcount\(0) $ (VCC)
-- \kbd_ctrl|ps2_ctrl|Add0~1\ = CARRY(\kbd_ctrl|ps2_ctrl|fcount\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|fcount\(0),
	datad => VCC,
	combout => \kbd_ctrl|ps2_ctrl|Add0~0_combout\,
	cout => \kbd_ctrl|ps2_ctrl|Add0~1\);

-- Location: LCCOMB_X37_Y16_N2
\kbd_ctrl|ps2_ctrl|Add0~23\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add0~23_combout\ = (!\PS2_CLK~0\ & \kbd_ctrl|ps2_ctrl|Add0~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \PS2_CLK~0\,
	datad => \kbd_ctrl|ps2_ctrl|Add0~0_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Add0~23_combout\);

-- Location: PIN_R22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\KEY[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_KEY(0),
	combout => \KEY~combout\(0));

-- Location: PIN_R21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\KEY[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_KEY(1),
	combout => \KEY~combout\(1));

-- Location: LCCOMB_X38_Y16_N22
\kbd_ctrl|ps2_ctrl|fcount[7]~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|fcount[7]~1_combout\ = (\KEY~combout\(1) & !\kbd_ctrl|ps2_ctrl|fcount[7]~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \KEY~combout\(1),
	datad => \kbd_ctrl|ps2_ctrl|fcount[7]~0_combout\,
	combout => \kbd_ctrl|ps2_ctrl|fcount[7]~1_combout\);

-- Location: LCFF_X37_Y16_N3
\kbd_ctrl|ps2_ctrl|fcount[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|Add0~23_combout\,
	aclr => \ALT_INV_KEY~combout\(0),
	ena => \kbd_ctrl|ps2_ctrl|fcount[7]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|fcount\(0));

-- Location: LCCOMB_X37_Y16_N14
\kbd_ctrl|ps2_ctrl|Add0~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add0~2_combout\ = (\kbd_ctrl|ps2_ctrl|fcount\(1) & (!\kbd_ctrl|ps2_ctrl|Add0~1\)) # (!\kbd_ctrl|ps2_ctrl|fcount\(1) & ((\kbd_ctrl|ps2_ctrl|Add0~1\) # (GND)))
-- \kbd_ctrl|ps2_ctrl|Add0~3\ = CARRY((!\kbd_ctrl|ps2_ctrl|Add0~1\) # (!\kbd_ctrl|ps2_ctrl|fcount\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|fcount\(1),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|Add0~1\,
	combout => \kbd_ctrl|ps2_ctrl|Add0~2_combout\,
	cout => \kbd_ctrl|ps2_ctrl|Add0~3\);

-- Location: LCCOMB_X37_Y16_N4
\kbd_ctrl|ps2_ctrl|Add0~22\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add0~22_combout\ = (\kbd_ctrl|ps2_ctrl|Add0~2_combout\ & !\PS2_CLK~0\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \kbd_ctrl|ps2_ctrl|Add0~2_combout\,
	datad => \PS2_CLK~0\,
	combout => \kbd_ctrl|ps2_ctrl|Add0~22_combout\);

-- Location: LCFF_X37_Y16_N5
\kbd_ctrl|ps2_ctrl|fcount[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|Add0~22_combout\,
	aclr => \ALT_INV_KEY~combout\(0),
	ena => \kbd_ctrl|ps2_ctrl|fcount[7]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|fcount\(1));

-- Location: LCCOMB_X37_Y16_N18
\kbd_ctrl|ps2_ctrl|Add0~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add0~6_combout\ = (\kbd_ctrl|ps2_ctrl|fcount\(3) & (!\kbd_ctrl|ps2_ctrl|Add0~5\)) # (!\kbd_ctrl|ps2_ctrl|fcount\(3) & ((\kbd_ctrl|ps2_ctrl|Add0~5\) # (GND)))
-- \kbd_ctrl|ps2_ctrl|Add0~7\ = CARRY((!\kbd_ctrl|ps2_ctrl|Add0~5\) # (!\kbd_ctrl|ps2_ctrl|fcount\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|fcount\(3),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|Add0~5\,
	combout => \kbd_ctrl|ps2_ctrl|Add0~6_combout\,
	cout => \kbd_ctrl|ps2_ctrl|Add0~7\);

-- Location: LCCOMB_X37_Y16_N0
\kbd_ctrl|ps2_ctrl|Add0~20\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add0~20_combout\ = (!\PS2_CLK~0\ & \kbd_ctrl|ps2_ctrl|Add0~6_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \PS2_CLK~0\,
	datad => \kbd_ctrl|ps2_ctrl|Add0~6_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Add0~20_combout\);

-- Location: LCFF_X37_Y16_N1
\kbd_ctrl|ps2_ctrl|fcount[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|Add0~20_combout\,
	aclr => \ALT_INV_KEY~combout\(0),
	ena => \kbd_ctrl|ps2_ctrl|fcount[7]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|fcount\(3));

-- Location: LCCOMB_X37_Y16_N22
\kbd_ctrl|ps2_ctrl|Add0~10\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add0~10_combout\ = (\kbd_ctrl|ps2_ctrl|fcount\(5) & (!\kbd_ctrl|ps2_ctrl|Add0~9\)) # (!\kbd_ctrl|ps2_ctrl|fcount\(5) & ((\kbd_ctrl|ps2_ctrl|Add0~9\) # (GND)))
-- \kbd_ctrl|ps2_ctrl|Add0~11\ = CARRY((!\kbd_ctrl|ps2_ctrl|Add0~9\) # (!\kbd_ctrl|ps2_ctrl|fcount\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|fcount\(5),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|Add0~9\,
	combout => \kbd_ctrl|ps2_ctrl|Add0~10_combout\,
	cout => \kbd_ctrl|ps2_ctrl|Add0~11\);

-- Location: LCCOMB_X37_Y16_N28
\kbd_ctrl|ps2_ctrl|Add0~18\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add0~18_combout\ = (!\PS2_CLK~0\ & \kbd_ctrl|ps2_ctrl|Add0~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \PS2_CLK~0\,
	datad => \kbd_ctrl|ps2_ctrl|Add0~10_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Add0~18_combout\);

-- Location: LCFF_X37_Y16_N29
\kbd_ctrl|ps2_ctrl|fcount[5]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|Add0~18_combout\,
	aclr => \ALT_INV_KEY~combout\(0),
	ena => \kbd_ctrl|ps2_ctrl|fcount[7]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|fcount\(5));

-- Location: LCCOMB_X37_Y16_N30
\kbd_ctrl|ps2_ctrl|Add0~17\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add0~17_combout\ = (\kbd_ctrl|ps2_ctrl|Add0~12_combout\ & !\PS2_CLK~0\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \kbd_ctrl|ps2_ctrl|Add0~12_combout\,
	datad => \PS2_CLK~0\,
	combout => \kbd_ctrl|ps2_ctrl|Add0~17_combout\);

-- Location: LCFF_X37_Y16_N31
\kbd_ctrl|ps2_ctrl|fcount[6]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|Add0~17_combout\,
	aclr => \ALT_INV_KEY~combout\(0),
	ena => \kbd_ctrl|ps2_ctrl|fcount[7]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|fcount\(6));

-- Location: LCCOMB_X38_Y16_N26
\kbd_ctrl|ps2_ctrl|fcount[7]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|fcount[7]~0_combout\ = (\kbd_ctrl|ps2_ctrl|fcount\(7) & (!\PS2_CLK~0\ & ((\kbd_ctrl|ps2_ctrl|fcount\(6)) # (\kbd_ctrl|ps2_ctrl|fcount\(5)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010001000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|fcount\(7),
	datab => \PS2_CLK~0\,
	datac => \kbd_ctrl|ps2_ctrl|fcount\(6),
	datad => \kbd_ctrl|ps2_ctrl|fcount\(5),
	combout => \kbd_ctrl|ps2_ctrl|fcount[7]~0_combout\);

-- Location: LCCOMB_X39_Y16_N2
\kbd_ctrl|ps2_ctrl|Add1~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add1~2_combout\ = (\kbd_ctrl|ps2_ctrl|rcount\(1) & (!\kbd_ctrl|ps2_ctrl|Add1~1\)) # (!\kbd_ctrl|ps2_ctrl|rcount\(1) & ((\kbd_ctrl|ps2_ctrl|Add1~1\) # (GND)))
-- \kbd_ctrl|ps2_ctrl|Add1~3\ = CARRY((!\kbd_ctrl|ps2_ctrl|Add1~1\) # (!\kbd_ctrl|ps2_ctrl|rcount\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|rcount\(1),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|Add1~1\,
	combout => \kbd_ctrl|ps2_ctrl|Add1~2_combout\,
	cout => \kbd_ctrl|ps2_ctrl|Add1~3\);

-- Location: LCCOMB_X39_Y16_N18
\kbd_ctrl|ps2_ctrl|Add1~22\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add1~22_combout\ = (\PS2_CLK~0\ & \kbd_ctrl|ps2_ctrl|Add1~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \PS2_CLK~0\,
	datad => \kbd_ctrl|ps2_ctrl|Add1~2_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Add1~22_combout\);

-- Location: LCCOMB_X39_Y16_N22
\kbd_ctrl|ps2_ctrl|rcount[7]~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|rcount[7]~1_combout\ = (\KEY~combout\(1) & !\kbd_ctrl|ps2_ctrl|rcount[7]~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \KEY~combout\(1),
	datad => \kbd_ctrl|ps2_ctrl|rcount[7]~0_combout\,
	combout => \kbd_ctrl|ps2_ctrl|rcount[7]~1_combout\);

-- Location: LCFF_X39_Y16_N19
\kbd_ctrl|ps2_ctrl|rcount[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|Add1~22_combout\,
	aclr => \ALT_INV_KEY~combout\(0),
	ena => \kbd_ctrl|ps2_ctrl|rcount[7]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|rcount\(1));

-- Location: LCCOMB_X39_Y16_N10
\kbd_ctrl|ps2_ctrl|Add1~10\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add1~10_combout\ = (\kbd_ctrl|ps2_ctrl|rcount\(5) & (!\kbd_ctrl|ps2_ctrl|Add1~9\)) # (!\kbd_ctrl|ps2_ctrl|rcount\(5) & ((\kbd_ctrl|ps2_ctrl|Add1~9\) # (GND)))
-- \kbd_ctrl|ps2_ctrl|Add1~11\ = CARRY((!\kbd_ctrl|ps2_ctrl|Add1~9\) # (!\kbd_ctrl|ps2_ctrl|rcount\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|rcount\(5),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|Add1~9\,
	combout => \kbd_ctrl|ps2_ctrl|Add1~10_combout\,
	cout => \kbd_ctrl|ps2_ctrl|Add1~11\);

-- Location: LCCOMB_X39_Y16_N12
\kbd_ctrl|ps2_ctrl|Add1~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add1~12_combout\ = (\kbd_ctrl|ps2_ctrl|rcount\(6) & (\kbd_ctrl|ps2_ctrl|Add1~11\ $ (GND))) # (!\kbd_ctrl|ps2_ctrl|rcount\(6) & (!\kbd_ctrl|ps2_ctrl|Add1~11\ & VCC))
-- \kbd_ctrl|ps2_ctrl|Add1~13\ = CARRY((\kbd_ctrl|ps2_ctrl|rcount\(6) & !\kbd_ctrl|ps2_ctrl|Add1~11\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|rcount\(6),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|Add1~11\,
	combout => \kbd_ctrl|ps2_ctrl|Add1~12_combout\,
	cout => \kbd_ctrl|ps2_ctrl|Add1~13\);

-- Location: LCCOMB_X39_Y16_N26
\kbd_ctrl|ps2_ctrl|Add1~17\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add1~17_combout\ = (\PS2_CLK~0\ & \kbd_ctrl|ps2_ctrl|Add1~12_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \PS2_CLK~0\,
	datad => \kbd_ctrl|ps2_ctrl|Add1~12_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Add1~17_combout\);

-- Location: LCFF_X39_Y16_N27
\kbd_ctrl|ps2_ctrl|rcount[6]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|Add1~17_combout\,
	aclr => \ALT_INV_KEY~combout\(0),
	ena => \kbd_ctrl|ps2_ctrl|rcount[7]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|rcount\(6));

-- Location: LCCOMB_X39_Y16_N20
\kbd_ctrl|ps2_ctrl|Add1~18\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add1~18_combout\ = (\PS2_CLK~0\ & \kbd_ctrl|ps2_ctrl|Add1~10_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \PS2_CLK~0\,
	datad => \kbd_ctrl|ps2_ctrl|Add1~10_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Add1~18_combout\);

-- Location: LCFF_X39_Y16_N21
\kbd_ctrl|ps2_ctrl|rcount[5]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|Add1~18_combout\,
	aclr => \ALT_INV_KEY~combout\(0),
	ena => \kbd_ctrl|ps2_ctrl|rcount[7]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|rcount\(5));

-- Location: LCCOMB_X39_Y16_N14
\kbd_ctrl|ps2_ctrl|Add1~14\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add1~14_combout\ = \kbd_ctrl|ps2_ctrl|Add1~13\ $ (\kbd_ctrl|ps2_ctrl|rcount\(7))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \kbd_ctrl|ps2_ctrl|rcount\(7),
	cin => \kbd_ctrl|ps2_ctrl|Add1~13\,
	combout => \kbd_ctrl|ps2_ctrl|Add1~14_combout\);

-- Location: LCCOMB_X39_Y16_N28
\kbd_ctrl|ps2_ctrl|Add1~16\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add1~16_combout\ = (\PS2_CLK~0\ & \kbd_ctrl|ps2_ctrl|Add1~14_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \PS2_CLK~0\,
	datac => \kbd_ctrl|ps2_ctrl|Add1~14_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Add1~16_combout\);

-- Location: LCFF_X39_Y16_N29
\kbd_ctrl|ps2_ctrl|rcount[7]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|Add1~16_combout\,
	aclr => \ALT_INV_KEY~combout\(0),
	ena => \kbd_ctrl|ps2_ctrl|rcount[7]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|rcount\(7));

-- Location: LCCOMB_X39_Y16_N30
\kbd_ctrl|ps2_ctrl|rcount[7]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|rcount[7]~0_combout\ = (\PS2_CLK~0\ & (\kbd_ctrl|ps2_ctrl|rcount\(7) & ((\kbd_ctrl|ps2_ctrl|rcount\(6)) # (\kbd_ctrl|ps2_ctrl|rcount\(5)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \PS2_CLK~0\,
	datab => \kbd_ctrl|ps2_ctrl|rcount\(6),
	datac => \kbd_ctrl|ps2_ctrl|rcount\(5),
	datad => \kbd_ctrl|ps2_ctrl|rcount\(7),
	combout => \kbd_ctrl|ps2_ctrl|rcount[7]~0_combout\);

-- Location: LCCOMB_X38_Y16_N12
\kbd_ctrl|ps2_ctrl|sigtrigger~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|sigtrigger~0_combout\ = (\KEY~combout\(1) & ((\kbd_ctrl|ps2_ctrl|fcount[7]~0_combout\) # (\kbd_ctrl|ps2_ctrl|rcount[7]~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|fcount[7]~0_combout\,
	datac => \KEY~combout\(1),
	datad => \kbd_ctrl|ps2_ctrl|rcount[7]~0_combout\,
	combout => \kbd_ctrl|ps2_ctrl|sigtrigger~0_combout\);

-- Location: LCCOMB_X38_Y16_N18
\kbd_ctrl|ps2_ctrl|sigtrigger~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|sigtrigger~1_combout\ = (\kbd_ctrl|ps2_ctrl|sigtrigger~0_combout\ & ((!\PS2_CLK~0\))) # (!\kbd_ctrl|ps2_ctrl|sigtrigger~0_combout\ & (\kbd_ctrl|ps2_ctrl|sigtrigger~regout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000011111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|sigtrigger~0_combout\,
	datac => \kbd_ctrl|ps2_ctrl|sigtrigger~regout\,
	datad => \PS2_CLK~0\,
	combout => \kbd_ctrl|ps2_ctrl|sigtrigger~1_combout\);

-- Location: LCFF_X38_Y16_N19
\kbd_ctrl|ps2_ctrl|sigtrigger\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|sigtrigger~1_combout\,
	aclr => \ALT_INV_KEY~combout\(0),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|sigtrigger~regout\);

-- Location: CLKCTRL_G4
\kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl\ : cycloneii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_outclk\);

-- Location: LCCOMB_X32_Y18_N0
\kbd_ctrl|ps2_ctrl|TOPS2:count[0]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|TOPS2:count[0]~0_combout\ = !\kbd_ctrl|ps2_ctrl|TOPS2:count[0]~regout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \kbd_ctrl|ps2_ctrl|TOPS2:count[0]~regout\,
	combout => \kbd_ctrl|ps2_ctrl|TOPS2:count[0]~0_combout\);

-- Location: LCCOMB_X30_Y18_N0
\kbd_ctrl|ps2_ctrl|sigsending~feeder\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|sigsending~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	combout => \kbd_ctrl|ps2_ctrl|sigsending~feeder_combout\);

-- Location: LCCOMB_X32_Y18_N22
\kbd_ctrl|ps2_ctrl|sigsendend~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|sigsendend~2_combout\ = (\kbd_ctrl|ps2_ctrl|sigsendend~1_combout\ & (\kbd_ctrl|ps2_ctrl|TOPS2:count[3]~regout\ & ((\kbd_ctrl|ps2_ctrl|sigsendend~regout\) # (!\kbd_ctrl|ps2_ctrl|TOPS2:count[0]~regout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|sigsendend~1_combout\,
	datab => \kbd_ctrl|ps2_ctrl|TOPS2:count[0]~regout\,
	datac => \kbd_ctrl|ps2_ctrl|sigsendend~regout\,
	datad => \kbd_ctrl|ps2_ctrl|TOPS2:count[3]~regout\,
	combout => \kbd_ctrl|ps2_ctrl|sigsendend~2_combout\);

-- Location: LCCOMB_X31_Y18_N8
\kbd_ctrl|ps2_ctrl|count[0]~15\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|count[0]~15_combout\ = \kbd_ctrl|ps2_ctrl|count\(0) $ (VCC)
-- \kbd_ctrl|ps2_ctrl|count[0]~16\ = CARRY(\kbd_ctrl|ps2_ctrl|count\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|count\(0),
	datad => VCC,
	combout => \kbd_ctrl|ps2_ctrl|count[0]~15_combout\,
	cout => \kbd_ctrl|ps2_ctrl|count[0]~16\);

-- Location: LCCOMB_X31_Y18_N24
\kbd_ctrl|ps2_ctrl|count[8]~31\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|count[8]~31_combout\ = (\kbd_ctrl|ps2_ctrl|count\(8) & (\kbd_ctrl|ps2_ctrl|count[7]~30\ $ (GND))) # (!\kbd_ctrl|ps2_ctrl|count\(8) & (!\kbd_ctrl|ps2_ctrl|count[7]~30\ & VCC))
-- \kbd_ctrl|ps2_ctrl|count[8]~32\ = CARRY((\kbd_ctrl|ps2_ctrl|count\(8) & !\kbd_ctrl|ps2_ctrl|count[7]~30\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|count\(8),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|count[7]~30\,
	combout => \kbd_ctrl|ps2_ctrl|count[8]~31_combout\,
	cout => \kbd_ctrl|ps2_ctrl|count[8]~32\);

-- Location: LCFF_X31_Y18_N25
\kbd_ctrl|ps2_ctrl|count[8]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|count[8]~31_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|process_2~0_combout\,
	ena => \kbd_ctrl|ps2_ctrl|count[11]~35_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|count\(8));

-- Location: LCCOMB_X31_Y18_N12
\kbd_ctrl|ps2_ctrl|count[2]~19\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|count[2]~19_combout\ = (\kbd_ctrl|ps2_ctrl|count\(2) & (\kbd_ctrl|ps2_ctrl|count[1]~18\ $ (GND))) # (!\kbd_ctrl|ps2_ctrl|count\(2) & (!\kbd_ctrl|ps2_ctrl|count[1]~18\ & VCC))
-- \kbd_ctrl|ps2_ctrl|count[2]~20\ = CARRY((\kbd_ctrl|ps2_ctrl|count\(2) & !\kbd_ctrl|ps2_ctrl|count[1]~18\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|count\(2),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|count[1]~18\,
	combout => \kbd_ctrl|ps2_ctrl|count[2]~19_combout\,
	cout => \kbd_ctrl|ps2_ctrl|count[2]~20\);

-- Location: LCFF_X31_Y18_N13
\kbd_ctrl|ps2_ctrl|count[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|count[2]~19_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|process_2~0_combout\,
	ena => \kbd_ctrl|ps2_ctrl|count[11]~35_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|count\(2));

-- Location: LCCOMB_X30_Y18_N12
\kbd_ctrl|ps2_ctrl|ps2_clk~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|ps2_clk~1_combout\ = (!\kbd_ctrl|ps2_ctrl|count\(4) & (!\kbd_ctrl|ps2_ctrl|count\(2) & (!\kbd_ctrl|ps2_ctrl|count\(3) & !\kbd_ctrl|ps2_ctrl|count\(5))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|count\(4),
	datab => \kbd_ctrl|ps2_ctrl|count\(2),
	datac => \kbd_ctrl|ps2_ctrl|count\(3),
	datad => \kbd_ctrl|ps2_ctrl|count\(5),
	combout => \kbd_ctrl|ps2_ctrl|ps2_clk~1_combout\);

-- Location: LCCOMB_X30_Y18_N28
\kbd_ctrl|ps2_ctrl|ps2_clk~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|ps2_clk~3_combout\ = (((\kbd_ctrl|ps2_ctrl|ps2_clk~1_combout\) # (!\kbd_ctrl|ps2_ctrl|count\(7))) # (!\kbd_ctrl|ps2_ctrl|count\(8))) # (!\kbd_ctrl|ps2_ctrl|count\(6))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111101111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|count\(6),
	datab => \kbd_ctrl|ps2_ctrl|count\(8),
	datac => \kbd_ctrl|ps2_ctrl|count\(7),
	datad => \kbd_ctrl|ps2_ctrl|ps2_clk~1_combout\,
	combout => \kbd_ctrl|ps2_ctrl|ps2_clk~3_combout\);

-- Location: LCCOMB_X30_Y18_N30
\kbd_ctrl|ps2_ctrl|ps2_clk~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|ps2_clk~4_combout\ = (!\kbd_ctrl|ps2_ctrl|count\(10) & (!\kbd_ctrl|ps2_ctrl|count\(9) & \kbd_ctrl|ps2_ctrl|ps2_clk~3_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|count\(10),
	datac => \kbd_ctrl|ps2_ctrl|count\(9),
	datad => \kbd_ctrl|ps2_ctrl|ps2_clk~3_combout\,
	combout => \kbd_ctrl|ps2_ctrl|ps2_clk~4_combout\);

-- Location: LCCOMB_X31_Y18_N16
\kbd_ctrl|ps2_ctrl|count[4]~23\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|count[4]~23_combout\ = (\kbd_ctrl|ps2_ctrl|count\(4) & (\kbd_ctrl|ps2_ctrl|count[3]~22\ $ (GND))) # (!\kbd_ctrl|ps2_ctrl|count\(4) & (!\kbd_ctrl|ps2_ctrl|count[3]~22\ & VCC))
-- \kbd_ctrl|ps2_ctrl|count[4]~24\ = CARRY((\kbd_ctrl|ps2_ctrl|count\(4) & !\kbd_ctrl|ps2_ctrl|count[3]~22\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|count\(4),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|count[3]~22\,
	combout => \kbd_ctrl|ps2_ctrl|count[4]~23_combout\,
	cout => \kbd_ctrl|ps2_ctrl|count[4]~24\);

-- Location: LCFF_X31_Y18_N17
\kbd_ctrl|ps2_ctrl|count[4]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|count[4]~23_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|process_2~0_combout\,
	ena => \kbd_ctrl|ps2_ctrl|count[11]~35_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|count\(4));

-- Location: LCCOMB_X31_Y18_N10
\kbd_ctrl|ps2_ctrl|count[1]~17\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|count[1]~17_combout\ = (\kbd_ctrl|ps2_ctrl|count\(1) & (!\kbd_ctrl|ps2_ctrl|count[0]~16\)) # (!\kbd_ctrl|ps2_ctrl|count\(1) & ((\kbd_ctrl|ps2_ctrl|count[0]~16\) # (GND)))
-- \kbd_ctrl|ps2_ctrl|count[1]~18\ = CARRY((!\kbd_ctrl|ps2_ctrl|count[0]~16\) # (!\kbd_ctrl|ps2_ctrl|count\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|count\(1),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|count[0]~16\,
	combout => \kbd_ctrl|ps2_ctrl|count[1]~17_combout\,
	cout => \kbd_ctrl|ps2_ctrl|count[1]~18\);

-- Location: LCFF_X31_Y18_N11
\kbd_ctrl|ps2_ctrl|count[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|count[1]~17_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|process_2~0_combout\,
	ena => \kbd_ctrl|ps2_ctrl|count[11]~35_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|count\(1));

-- Location: LCCOMB_X30_Y18_N14
\kbd_ctrl|ps2_ctrl|LessThan4~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|LessThan4~6_combout\ = ((!\kbd_ctrl|ps2_ctrl|count\(2) & (!\kbd_ctrl|ps2_ctrl|count\(3) & !\kbd_ctrl|ps2_ctrl|count\(1)))) # (!\kbd_ctrl|ps2_ctrl|count\(4))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100011111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|count\(2),
	datab => \kbd_ctrl|ps2_ctrl|count\(3),
	datac => \kbd_ctrl|ps2_ctrl|count\(4),
	datad => \kbd_ctrl|ps2_ctrl|count\(1),
	combout => \kbd_ctrl|ps2_ctrl|LessThan4~6_combout\);

-- Location: LCCOMB_X31_Y18_N6
\kbd_ctrl|ps2_ctrl|LessThan4~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|LessThan4~3_combout\ = ((!\kbd_ctrl|ps2_ctrl|count\(6) & (!\kbd_ctrl|ps2_ctrl|count\(5) & \kbd_ctrl|ps2_ctrl|LessThan4~6_combout\))) # (!\kbd_ctrl|ps2_ctrl|count\(8))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|count\(6),
	datab => \kbd_ctrl|ps2_ctrl|count\(5),
	datac => \kbd_ctrl|ps2_ctrl|count\(8),
	datad => \kbd_ctrl|ps2_ctrl|LessThan4~6_combout\,
	combout => \kbd_ctrl|ps2_ctrl|LessThan4~3_combout\);

-- Location: LCCOMB_X31_Y18_N0
\kbd_ctrl|ps2_ctrl|LessThan4~22\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|LessThan4~22_combout\ = (!\kbd_ctrl|ps2_ctrl|count\(10) & (!\kbd_ctrl|ps2_ctrl|count\(9) & ((\kbd_ctrl|ps2_ctrl|LessThan4~3_combout\) # (!\kbd_ctrl|ps2_ctrl|count\(7)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001100000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|count\(7),
	datab => \kbd_ctrl|ps2_ctrl|count\(10),
	datac => \kbd_ctrl|ps2_ctrl|count\(9),
	datad => \kbd_ctrl|ps2_ctrl|LessThan4~3_combout\,
	combout => \kbd_ctrl|ps2_ctrl|LessThan4~22_combout\);

-- Location: LCCOMB_X31_Y18_N2
\kbd_ctrl|ps2_ctrl|count[11]~35\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|count[11]~35_combout\ = (\kbd_ctrl|ps2_ctrl|sigsending~regout\ & (((\kbd_ctrl|ps2_ctrl|ps2_clk~4_combout\) # (\kbd_ctrl|ps2_ctrl|LessThan4~22_combout\)) # (!\kbd_ctrl|ps2_ctrl|count\(11))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|sigsending~regout\,
	datab => \kbd_ctrl|ps2_ctrl|count\(11),
	datac => \kbd_ctrl|ps2_ctrl|ps2_clk~4_combout\,
	datad => \kbd_ctrl|ps2_ctrl|LessThan4~22_combout\,
	combout => \kbd_ctrl|ps2_ctrl|count[11]~35_combout\);

-- Location: LCFF_X31_Y18_N9
\kbd_ctrl|ps2_ctrl|count[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|count[0]~15_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|process_2~0_combout\,
	ena => \kbd_ctrl|ps2_ctrl|count[11]~35_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|count\(0));

-- Location: LCCOMB_X31_Y18_N14
\kbd_ctrl|ps2_ctrl|count[3]~21\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|count[3]~21_combout\ = (\kbd_ctrl|ps2_ctrl|count\(3) & (!\kbd_ctrl|ps2_ctrl|count[2]~20\)) # (!\kbd_ctrl|ps2_ctrl|count\(3) & ((\kbd_ctrl|ps2_ctrl|count[2]~20\) # (GND)))
-- \kbd_ctrl|ps2_ctrl|count[3]~22\ = CARRY((!\kbd_ctrl|ps2_ctrl|count[2]~20\) # (!\kbd_ctrl|ps2_ctrl|count\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|count\(3),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|count[2]~20\,
	combout => \kbd_ctrl|ps2_ctrl|count[3]~21_combout\,
	cout => \kbd_ctrl|ps2_ctrl|count[3]~22\);

-- Location: LCFF_X31_Y18_N15
\kbd_ctrl|ps2_ctrl|count[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|count[3]~21_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|process_2~0_combout\,
	ena => \kbd_ctrl|ps2_ctrl|count[11]~35_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|count\(3));

-- Location: LCCOMB_X31_Y18_N18
\kbd_ctrl|ps2_ctrl|count[5]~25\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|count[5]~25_combout\ = (\kbd_ctrl|ps2_ctrl|count\(5) & (!\kbd_ctrl|ps2_ctrl|count[4]~24\)) # (!\kbd_ctrl|ps2_ctrl|count\(5) & ((\kbd_ctrl|ps2_ctrl|count[4]~24\) # (GND)))
-- \kbd_ctrl|ps2_ctrl|count[5]~26\ = CARRY((!\kbd_ctrl|ps2_ctrl|count[4]~24\) # (!\kbd_ctrl|ps2_ctrl|count\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|count\(5),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|count[4]~24\,
	combout => \kbd_ctrl|ps2_ctrl|count[5]~25_combout\,
	cout => \kbd_ctrl|ps2_ctrl|count[5]~26\);

-- Location: LCFF_X31_Y18_N19
\kbd_ctrl|ps2_ctrl|count[5]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|count[5]~25_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|process_2~0_combout\,
	ena => \kbd_ctrl|ps2_ctrl|count[11]~35_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|count\(5));

-- Location: LCCOMB_X31_Y18_N22
\kbd_ctrl|ps2_ctrl|count[7]~29\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|count[7]~29_combout\ = (\kbd_ctrl|ps2_ctrl|count\(7) & (!\kbd_ctrl|ps2_ctrl|count[6]~28\)) # (!\kbd_ctrl|ps2_ctrl|count\(7) & ((\kbd_ctrl|ps2_ctrl|count[6]~28\) # (GND)))
-- \kbd_ctrl|ps2_ctrl|count[7]~30\ = CARRY((!\kbd_ctrl|ps2_ctrl|count[6]~28\) # (!\kbd_ctrl|ps2_ctrl|count\(7)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|count\(7),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|count[6]~28\,
	combout => \kbd_ctrl|ps2_ctrl|count[7]~29_combout\,
	cout => \kbd_ctrl|ps2_ctrl|count[7]~30\);

-- Location: LCFF_X31_Y18_N23
\kbd_ctrl|ps2_ctrl|count[7]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|count[7]~29_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|process_2~0_combout\,
	ena => \kbd_ctrl|ps2_ctrl|count[11]~35_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|count\(7));

-- Location: LCCOMB_X31_Y18_N26
\kbd_ctrl|ps2_ctrl|count[9]~33\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|count[9]~33_combout\ = (\kbd_ctrl|ps2_ctrl|count\(9) & (!\kbd_ctrl|ps2_ctrl|count[8]~32\)) # (!\kbd_ctrl|ps2_ctrl|count\(9) & ((\kbd_ctrl|ps2_ctrl|count[8]~32\) # (GND)))
-- \kbd_ctrl|ps2_ctrl|count[9]~34\ = CARRY((!\kbd_ctrl|ps2_ctrl|count[8]~32\) # (!\kbd_ctrl|ps2_ctrl|count\(9)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|count\(9),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|count[8]~32\,
	combout => \kbd_ctrl|ps2_ctrl|count[9]~33_combout\,
	cout => \kbd_ctrl|ps2_ctrl|count[9]~34\);

-- Location: LCFF_X31_Y18_N27
\kbd_ctrl|ps2_ctrl|count[9]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|count[9]~33_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|process_2~0_combout\,
	ena => \kbd_ctrl|ps2_ctrl|count[11]~35_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|count\(9));

-- Location: LCCOMB_X31_Y18_N28
\kbd_ctrl|ps2_ctrl|count[10]~36\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|count[10]~36_combout\ = (\kbd_ctrl|ps2_ctrl|count\(10) & (\kbd_ctrl|ps2_ctrl|count[9]~34\ $ (GND))) # (!\kbd_ctrl|ps2_ctrl|count\(10) & (!\kbd_ctrl|ps2_ctrl|count[9]~34\ & VCC))
-- \kbd_ctrl|ps2_ctrl|count[10]~37\ = CARRY((\kbd_ctrl|ps2_ctrl|count\(10) & !\kbd_ctrl|ps2_ctrl|count[9]~34\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|count\(10),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|count[9]~34\,
	combout => \kbd_ctrl|ps2_ctrl|count[10]~36_combout\,
	cout => \kbd_ctrl|ps2_ctrl|count[10]~37\);

-- Location: LCFF_X31_Y18_N29
\kbd_ctrl|ps2_ctrl|count[10]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|count[10]~36_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|process_2~0_combout\,
	ena => \kbd_ctrl|ps2_ctrl|count[11]~35_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|count\(10));

-- Location: LCCOMB_X31_Y18_N30
\kbd_ctrl|ps2_ctrl|count[11]~38\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|count[11]~38_combout\ = \kbd_ctrl|ps2_ctrl|count[10]~37\ $ (\kbd_ctrl|ps2_ctrl|count\(11))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \kbd_ctrl|ps2_ctrl|count\(11),
	cin => \kbd_ctrl|ps2_ctrl|count[10]~37\,
	combout => \kbd_ctrl|ps2_ctrl|count[11]~38_combout\);

-- Location: LCFF_X31_Y18_N31
\kbd_ctrl|ps2_ctrl|count[11]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|count[11]~38_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|process_2~0_combout\,
	ena => \kbd_ctrl|ps2_ctrl|count[11]~35_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|count\(11));

-- Location: LCCOMB_X31_Y18_N4
\kbd_ctrl|ps2_ctrl|sigclkreleased~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|sigclkreleased~0_combout\ = (\kbd_ctrl|ps2_ctrl|ps2_clk~4_combout\) # ((\kbd_ctrl|ps2_ctrl|LessThan4~22_combout\) # (!\kbd_ctrl|ps2_ctrl|count\(11)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111110111011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|ps2_clk~4_combout\,
	datab => \kbd_ctrl|ps2_ctrl|count\(11),
	datad => \kbd_ctrl|ps2_ctrl|LessThan4~22_combout\,
	combout => \kbd_ctrl|ps2_ctrl|sigclkreleased~0_combout\);

-- Location: LCFF_X31_Y18_N5
\kbd_ctrl|ps2_ctrl|sigclkreleased\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|sigclkreleased~0_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|process_2~0_combout\,
	ena => \kbd_ctrl|ps2_ctrl|sigsending~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|sigclkreleased~regout\);

-- Location: LCCOMB_X32_Y18_N10
\kbd_ctrl|ps2_ctrl|TOPS2~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|TOPS2~0_combout\ = (!\kbd_ctrl|ps2_ctrl|sigclkreleased~regout\ & \kbd_ctrl|ps2_ctrl|sigsending~regout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \kbd_ctrl|ps2_ctrl|sigclkreleased~regout\,
	datad => \kbd_ctrl|ps2_ctrl|sigsending~regout\,
	combout => \kbd_ctrl|ps2_ctrl|TOPS2~0_combout\);

-- Location: LCFF_X32_Y18_N23
\kbd_ctrl|ps2_ctrl|sigsendend\ : cycloneii_lcell_ff
PORT MAP (
	clk => \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|sigsendend~2_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|sigsendend~0_combout\,
	ena => \kbd_ctrl|ps2_ctrl|TOPS2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|sigsendend~regout\);

-- Location: LCCOMB_X32_Y18_N12
\kbd_ctrl|ps2_ctrl|process_2~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|process_2~0_combout\ = (\kbd_ctrl|ps2_ctrl|sigsendend~regout\) # (!\KEY~combout\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \KEY~combout\(0),
	datad => \kbd_ctrl|ps2_ctrl|sigsendend~regout\,
	combout => \kbd_ctrl|ps2_ctrl|process_2~0_combout\);

-- Location: LCFF_X30_Y18_N1
\kbd_ctrl|ps2_ctrl|sigsending\ : cycloneii_lcell_ff
PORT MAP (
	clk => \kbd_ctrl|sigsend~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|sigsending~feeder_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|process_2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|sigsending~regout\);

-- Location: LCCOMB_X31_Y17_N28
\kbd_ctrl|ps2_ctrl|TOPS2~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|TOPS2~1_combout\ = (\KEY~combout\(0) & \kbd_ctrl|ps2_ctrl|sigsending~regout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \KEY~combout\(0),
	datad => \kbd_ctrl|ps2_ctrl|sigsending~regout\,
	combout => \kbd_ctrl|ps2_ctrl|TOPS2~1_combout\);

-- Location: LCCOMB_X30_Y18_N24
\kbd_ctrl|ps2_ctrl|sigclkheld~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|sigclkheld~0_combout\ = (\kbd_ctrl|ps2_ctrl|count\(11) & (!\kbd_ctrl|ps2_ctrl|LessThan4~22_combout\ & \kbd_ctrl|ps2_ctrl|ps2_clk~4_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|count\(11),
	datac => \kbd_ctrl|ps2_ctrl|LessThan4~22_combout\,
	datad => \kbd_ctrl|ps2_ctrl|ps2_clk~4_combout\,
	combout => \kbd_ctrl|ps2_ctrl|sigclkheld~0_combout\);

-- Location: LCFF_X30_Y18_N25
\kbd_ctrl|ps2_ctrl|sigclkheld\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|sigclkheld~0_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|process_2~0_combout\,
	ena => \kbd_ctrl|ps2_ctrl|sigsending~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|sigclkheld~regout\);

-- Location: LCCOMB_X32_Y18_N2
\kbd_ctrl|ps2_ctrl|sigsendend~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|sigsendend~0_combout\ = (\kbd_ctrl|ps2_ctrl|sigclkheld~regout\) # (!\kbd_ctrl|ps2_ctrl|TOPS2~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \kbd_ctrl|ps2_ctrl|TOPS2~1_combout\,
	datad => \kbd_ctrl|ps2_ctrl|sigclkheld~regout\,
	combout => \kbd_ctrl|ps2_ctrl|sigsendend~0_combout\);

-- Location: LCFF_X32_Y18_N1
\kbd_ctrl|ps2_ctrl|TOPS2:count[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|TOPS2:count[0]~0_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|sigsendend~0_combout\,
	ena => \kbd_ctrl|ps2_ctrl|TOPS2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|TOPS2:count[0]~regout\);

-- Location: LCCOMB_X32_Y19_N16
\kbd_ctrl|ps2_ctrl|TOPS2:count[2]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|TOPS2:count[2]~0_combout\ = \kbd_ctrl|ps2_ctrl|TOPS2:count[2]~regout\ $ (((\kbd_ctrl|ps2_ctrl|TOPS2:count[0]~regout\ & (\kbd_ctrl|ps2_ctrl|TOPS2:count[1]~regout\ & \kbd_ctrl|ps2_ctrl|TOPS2~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|TOPS2:count[0]~regout\,
	datab => \kbd_ctrl|ps2_ctrl|TOPS2:count[1]~regout\,
	datac => \kbd_ctrl|ps2_ctrl|TOPS2:count[2]~regout\,
	datad => \kbd_ctrl|ps2_ctrl|TOPS2~0_combout\,
	combout => \kbd_ctrl|ps2_ctrl|TOPS2:count[2]~0_combout\);

-- Location: LCFF_X32_Y19_N17
\kbd_ctrl|ps2_ctrl|TOPS2:count[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|TOPS2:count[2]~0_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|sigsendend~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|TOPS2:count[2]~regout\);

-- Location: LCCOMB_X32_Y18_N26
\kbd_ctrl|ps2_ctrl|Add6~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add6~0_combout\ = (\kbd_ctrl|ps2_ctrl|TOPS2:count[1]~regout\ & (!\kbd_ctrl|ps2_ctrl|TOPS2:count[0]~regout\ & ((\kbd_ctrl|ps2_ctrl|TOPS2:count[2]~regout\) # (!\kbd_ctrl|ps2_ctrl|TOPS2:count[3]~regout\)))) # 
-- (!\kbd_ctrl|ps2_ctrl|TOPS2:count[1]~regout\ & (((\kbd_ctrl|ps2_ctrl|TOPS2:count[0]~regout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|TOPS2:count[3]~regout\,
	datab => \kbd_ctrl|ps2_ctrl|TOPS2:count[2]~regout\,
	datac => \kbd_ctrl|ps2_ctrl|TOPS2:count[1]~regout\,
	datad => \kbd_ctrl|ps2_ctrl|TOPS2:count[0]~regout\,
	combout => \kbd_ctrl|ps2_ctrl|Add6~0_combout\);

-- Location: LCFF_X32_Y18_N27
\kbd_ctrl|ps2_ctrl|TOPS2:count[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|Add6~0_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|sigsendend~0_combout\,
	ena => \kbd_ctrl|ps2_ctrl|TOPS2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|TOPS2:count[1]~regout\);

-- Location: LCCOMB_X32_Y18_N16
\kbd_ctrl|ps2_ctrl|Add6~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add6~1_combout\ = (\kbd_ctrl|ps2_ctrl|TOPS2:count[2]~regout\ & (\kbd_ctrl|ps2_ctrl|TOPS2:count[3]~regout\ $ (((\kbd_ctrl|ps2_ctrl|TOPS2:count[0]~regout\ & \kbd_ctrl|ps2_ctrl|TOPS2:count[1]~regout\))))) # 
-- (!\kbd_ctrl|ps2_ctrl|TOPS2:count[2]~regout\ & (\kbd_ctrl|ps2_ctrl|TOPS2:count[3]~regout\ & ((\kbd_ctrl|ps2_ctrl|TOPS2:count[0]~regout\) # (!\kbd_ctrl|ps2_ctrl|TOPS2:count[1]~regout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|TOPS2:count[2]~regout\,
	datab => \kbd_ctrl|ps2_ctrl|TOPS2:count[0]~regout\,
	datac => \kbd_ctrl|ps2_ctrl|TOPS2:count[3]~regout\,
	datad => \kbd_ctrl|ps2_ctrl|TOPS2:count[1]~regout\,
	combout => \kbd_ctrl|ps2_ctrl|Add6~1_combout\);

-- Location: LCFF_X32_Y18_N17
\kbd_ctrl|ps2_ctrl|TOPS2:count[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|Add6~1_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|sigsendend~0_combout\,
	ena => \kbd_ctrl|ps2_ctrl|TOPS2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|TOPS2:count[3]~regout\);

-- Location: LCCOMB_X33_Y18_N2
\kbd_ctrl|ps2_ctrl|Equal3~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Equal3~0_combout\ = (!\kbd_ctrl|ps2_ctrl|TOPS2:count[2]~regout\ & (\kbd_ctrl|ps2_ctrl|TOPS2:count[3]~regout\ & (!\kbd_ctrl|ps2_ctrl|TOPS2:count[0]~regout\ & !\kbd_ctrl|ps2_ctrl|TOPS2:count[1]~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|TOPS2:count[2]~regout\,
	datab => \kbd_ctrl|ps2_ctrl|TOPS2:count[3]~regout\,
	datac => \kbd_ctrl|ps2_ctrl|TOPS2:count[0]~regout\,
	datad => \kbd_ctrl|ps2_ctrl|TOPS2:count[1]~regout\,
	combout => \kbd_ctrl|ps2_ctrl|Equal3~0_combout\);

-- Location: LCCOMB_X31_Y17_N24
\kbd_ctrl|ps2_ctrl|send_rdy~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|send_rdy~1_combout\ = (!\kbd_ctrl|ps2_ctrl|TOPS2~1_combout\ & ((\kbd_ctrl|ps2_ctrl|send_rdy~1_combout\) # (!\KEY~combout\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \KEY~combout\(0),
	datac => \kbd_ctrl|ps2_ctrl|TOPS2~1_combout\,
	datad => \kbd_ctrl|ps2_ctrl|send_rdy~1_combout\,
	combout => \kbd_ctrl|ps2_ctrl|send_rdy~1_combout\);

-- Location: LCCOMB_X29_Y19_N12
\kbd_ctrl|ps2_ctrl|countclk[0]~55\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|countclk[0]~55_combout\ = \kbd_ctrl|ps2_ctrl|countclk\(0) $ (((!\kbd_ctrl|ps2_ctrl|sigsending~regout\ & ((!\kbd_ctrl|ps2_ctrl|Equal2~5_combout\) # (!\kbd_ctrl|ps2_ctrl|Equal2~0_combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000111000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|Equal2~0_combout\,
	datab => \kbd_ctrl|ps2_ctrl|sigsending~regout\,
	datac => \kbd_ctrl|ps2_ctrl|countclk\(0),
	datad => \kbd_ctrl|ps2_ctrl|Equal2~5_combout\,
	combout => \kbd_ctrl|ps2_ctrl|countclk[0]~55_combout\);

-- Location: LCCOMB_X30_Y18_N22
\kbd_ctrl|ps2_ctrl|FROMPS2~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|FROMPS2~1_combout\ = (\kbd_ctrl|ps2_ctrl|sigsending~regout\) # (!\KEY~combout\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111101010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \KEY~combout\(0),
	datad => \kbd_ctrl|ps2_ctrl|sigsending~regout\,
	combout => \kbd_ctrl|ps2_ctrl|FROMPS2~1_combout\);

-- Location: LCFF_X29_Y19_N13
\kbd_ctrl|ps2_ctrl|countclk[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|countclk[0]~55_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|FROMPS2~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|countclk\(0));

-- Location: LCCOMB_X29_Y19_N14
\kbd_ctrl|ps2_ctrl|countclk[1]~18\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|countclk[1]~18_combout\ = (\kbd_ctrl|ps2_ctrl|countclk\(1) & (\kbd_ctrl|ps2_ctrl|countclk\(0) $ (VCC))) # (!\kbd_ctrl|ps2_ctrl|countclk\(1) & (\kbd_ctrl|ps2_ctrl|countclk\(0) & VCC))
-- \kbd_ctrl|ps2_ctrl|countclk[1]~19\ = CARRY((\kbd_ctrl|ps2_ctrl|countclk\(1) & \kbd_ctrl|ps2_ctrl|countclk\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110011010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|countclk\(1),
	datab => \kbd_ctrl|ps2_ctrl|countclk\(0),
	datad => VCC,
	combout => \kbd_ctrl|ps2_ctrl|countclk[1]~18_combout\,
	cout => \kbd_ctrl|ps2_ctrl|countclk[1]~19\);

-- Location: LCCOMB_X29_Y19_N16
\kbd_ctrl|ps2_ctrl|countclk[2]~20\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|countclk[2]~20_combout\ = (\kbd_ctrl|ps2_ctrl|countclk\(2) & (!\kbd_ctrl|ps2_ctrl|countclk[1]~19\)) # (!\kbd_ctrl|ps2_ctrl|countclk\(2) & ((\kbd_ctrl|ps2_ctrl|countclk[1]~19\) # (GND)))
-- \kbd_ctrl|ps2_ctrl|countclk[2]~21\ = CARRY((!\kbd_ctrl|ps2_ctrl|countclk[1]~19\) # (!\kbd_ctrl|ps2_ctrl|countclk\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|countclk\(2),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|countclk[1]~19\,
	combout => \kbd_ctrl|ps2_ctrl|countclk[2]~20_combout\,
	cout => \kbd_ctrl|ps2_ctrl|countclk[2]~21\);

-- Location: LCCOMB_X29_Y19_N18
\kbd_ctrl|ps2_ctrl|countclk[3]~22\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|countclk[3]~22_combout\ = (\kbd_ctrl|ps2_ctrl|countclk\(3) & (\kbd_ctrl|ps2_ctrl|countclk[2]~21\ $ (GND))) # (!\kbd_ctrl|ps2_ctrl|countclk\(3) & (!\kbd_ctrl|ps2_ctrl|countclk[2]~21\ & VCC))
-- \kbd_ctrl|ps2_ctrl|countclk[3]~23\ = CARRY((\kbd_ctrl|ps2_ctrl|countclk\(3) & !\kbd_ctrl|ps2_ctrl|countclk[2]~21\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|countclk\(3),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|countclk[2]~21\,
	combout => \kbd_ctrl|ps2_ctrl|countclk[3]~22_combout\,
	cout => \kbd_ctrl|ps2_ctrl|countclk[3]~23\);

-- Location: LCCOMB_X29_Y18_N4
\kbd_ctrl|ps2_ctrl|countclk[12]~40\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|countclk[12]~40_combout\ = (\kbd_ctrl|ps2_ctrl|countclk\(12) & (!\kbd_ctrl|ps2_ctrl|countclk[11]~39\)) # (!\kbd_ctrl|ps2_ctrl|countclk\(12) & ((\kbd_ctrl|ps2_ctrl|countclk[11]~39\) # (GND)))
-- \kbd_ctrl|ps2_ctrl|countclk[12]~41\ = CARRY((!\kbd_ctrl|ps2_ctrl|countclk[11]~39\) # (!\kbd_ctrl|ps2_ctrl|countclk\(12)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|countclk\(12),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|countclk[11]~39\,
	combout => \kbd_ctrl|ps2_ctrl|countclk[12]~40_combout\,
	cout => \kbd_ctrl|ps2_ctrl|countclk[12]~41\);

-- Location: LCCOMB_X29_Y18_N6
\kbd_ctrl|ps2_ctrl|countclk[13]~42\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|countclk[13]~42_combout\ = (\kbd_ctrl|ps2_ctrl|countclk\(13) & (\kbd_ctrl|ps2_ctrl|countclk[12]~41\ $ (GND))) # (!\kbd_ctrl|ps2_ctrl|countclk\(13) & (!\kbd_ctrl|ps2_ctrl|countclk[12]~41\ & VCC))
-- \kbd_ctrl|ps2_ctrl|countclk[13]~43\ = CARRY((\kbd_ctrl|ps2_ctrl|countclk\(13) & !\kbd_ctrl|ps2_ctrl|countclk[12]~41\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|countclk\(13),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|countclk[12]~41\,
	combout => \kbd_ctrl|ps2_ctrl|countclk[13]~42_combout\,
	cout => \kbd_ctrl|ps2_ctrl|countclk[13]~43\);

-- Location: LCCOMB_X29_Y18_N8
\kbd_ctrl|ps2_ctrl|countclk[14]~44\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|countclk[14]~44_combout\ = (\kbd_ctrl|ps2_ctrl|countclk\(14) & (!\kbd_ctrl|ps2_ctrl|countclk[13]~43\)) # (!\kbd_ctrl|ps2_ctrl|countclk\(14) & ((\kbd_ctrl|ps2_ctrl|countclk[13]~43\) # (GND)))
-- \kbd_ctrl|ps2_ctrl|countclk[14]~45\ = CARRY((!\kbd_ctrl|ps2_ctrl|countclk[13]~43\) # (!\kbd_ctrl|ps2_ctrl|countclk\(14)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|countclk\(14),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|countclk[13]~43\,
	combout => \kbd_ctrl|ps2_ctrl|countclk[14]~44_combout\,
	cout => \kbd_ctrl|ps2_ctrl|countclk[14]~45\);

-- Location: LCFF_X29_Y18_N9
\kbd_ctrl|ps2_ctrl|countclk[14]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|countclk[14]~44_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|FROMPS2~1_combout\,
	ena => \kbd_ctrl|ps2_ctrl|countclk[18]~54_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|countclk\(14));

-- Location: LCCOMB_X29_Y18_N12
\kbd_ctrl|ps2_ctrl|countclk[16]~48\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|countclk[16]~48_combout\ = (\kbd_ctrl|ps2_ctrl|countclk\(16) & (!\kbd_ctrl|ps2_ctrl|countclk[15]~47\)) # (!\kbd_ctrl|ps2_ctrl|countclk\(16) & ((\kbd_ctrl|ps2_ctrl|countclk[15]~47\) # (GND)))
-- \kbd_ctrl|ps2_ctrl|countclk[16]~49\ = CARRY((!\kbd_ctrl|ps2_ctrl|countclk[15]~47\) # (!\kbd_ctrl|ps2_ctrl|countclk\(16)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|countclk\(16),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|countclk[15]~47\,
	combout => \kbd_ctrl|ps2_ctrl|countclk[16]~48_combout\,
	cout => \kbd_ctrl|ps2_ctrl|countclk[16]~49\);

-- Location: LCCOMB_X29_Y18_N14
\kbd_ctrl|ps2_ctrl|countclk[17]~50\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|countclk[17]~50_combout\ = (\kbd_ctrl|ps2_ctrl|countclk\(17) & (\kbd_ctrl|ps2_ctrl|countclk[16]~49\ $ (GND))) # (!\kbd_ctrl|ps2_ctrl|countclk\(17) & (!\kbd_ctrl|ps2_ctrl|countclk[16]~49\ & VCC))
-- \kbd_ctrl|ps2_ctrl|countclk[17]~51\ = CARRY((\kbd_ctrl|ps2_ctrl|countclk\(17) & !\kbd_ctrl|ps2_ctrl|countclk[16]~49\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|countclk\(17),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|countclk[16]~49\,
	combout => \kbd_ctrl|ps2_ctrl|countclk[17]~50_combout\,
	cout => \kbd_ctrl|ps2_ctrl|countclk[17]~51\);

-- Location: LCFF_X29_Y18_N15
\kbd_ctrl|ps2_ctrl|countclk[17]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|countclk[17]~50_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|FROMPS2~1_combout\,
	ena => \kbd_ctrl|ps2_ctrl|countclk[18]~54_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|countclk\(17));

-- Location: LCCOMB_X29_Y18_N16
\kbd_ctrl|ps2_ctrl|countclk[18]~52\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|countclk[18]~52_combout\ = \kbd_ctrl|ps2_ctrl|countclk\(18) $ (\kbd_ctrl|ps2_ctrl|countclk[17]~51\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|countclk\(18),
	cin => \kbd_ctrl|ps2_ctrl|countclk[17]~51\,
	combout => \kbd_ctrl|ps2_ctrl|countclk[18]~52_combout\);

-- Location: LCFF_X29_Y18_N17
\kbd_ctrl|ps2_ctrl|countclk[18]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|countclk[18]~52_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|FROMPS2~1_combout\,
	ena => \kbd_ctrl|ps2_ctrl|countclk[18]~54_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|countclk\(18));

-- Location: LCFF_X29_Y18_N13
\kbd_ctrl|ps2_ctrl|countclk[16]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|countclk[16]~48_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|FROMPS2~1_combout\,
	ena => \kbd_ctrl|ps2_ctrl|countclk[18]~54_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|countclk\(16));

-- Location: LCCOMB_X29_Y18_N30
\kbd_ctrl|ps2_ctrl|Equal2~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Equal2~0_combout\ = (!\kbd_ctrl|ps2_ctrl|countclk\(17) & (\kbd_ctrl|ps2_ctrl|countclk\(18) & !\kbd_ctrl|ps2_ctrl|countclk\(16)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|countclk\(17),
	datac => \kbd_ctrl|ps2_ctrl|countclk\(18),
	datad => \kbd_ctrl|ps2_ctrl|countclk\(16),
	combout => \kbd_ctrl|ps2_ctrl|Equal2~0_combout\);

-- Location: LCCOMB_X29_Y18_N26
\kbd_ctrl|ps2_ctrl|countclk[18]~54\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|countclk[18]~54_combout\ = (!\kbd_ctrl|ps2_ctrl|sigsending~regout\ & ((!\kbd_ctrl|ps2_ctrl|Equal2~5_combout\) # (!\kbd_ctrl|ps2_ctrl|Equal2~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|Equal2~0_combout\,
	datac => \kbd_ctrl|ps2_ctrl|sigsending~regout\,
	datad => \kbd_ctrl|ps2_ctrl|Equal2~5_combout\,
	combout => \kbd_ctrl|ps2_ctrl|countclk[18]~54_combout\);

-- Location: LCFF_X29_Y19_N19
\kbd_ctrl|ps2_ctrl|countclk[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|countclk[3]~22_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|FROMPS2~1_combout\,
	ena => \kbd_ctrl|ps2_ctrl|countclk[18]~54_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|countclk\(3));

-- Location: LCCOMB_X29_Y19_N22
\kbd_ctrl|ps2_ctrl|countclk[5]~26\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|countclk[5]~26_combout\ = (\kbd_ctrl|ps2_ctrl|countclk\(5) & (\kbd_ctrl|ps2_ctrl|countclk[4]~25\ $ (GND))) # (!\kbd_ctrl|ps2_ctrl|countclk\(5) & (!\kbd_ctrl|ps2_ctrl|countclk[4]~25\ & VCC))
-- \kbd_ctrl|ps2_ctrl|countclk[5]~27\ = CARRY((\kbd_ctrl|ps2_ctrl|countclk\(5) & !\kbd_ctrl|ps2_ctrl|countclk[4]~25\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|countclk\(5),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|countclk[4]~25\,
	combout => \kbd_ctrl|ps2_ctrl|countclk[5]~26_combout\,
	cout => \kbd_ctrl|ps2_ctrl|countclk[5]~27\);

-- Location: LCFF_X29_Y19_N23
\kbd_ctrl|ps2_ctrl|countclk[5]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|countclk[5]~26_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|FROMPS2~1_combout\,
	ena => \kbd_ctrl|ps2_ctrl|countclk[18]~54_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|countclk\(5));

-- Location: LCCOMB_X29_Y19_N24
\kbd_ctrl|ps2_ctrl|countclk[6]~28\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|countclk[6]~28_combout\ = (\kbd_ctrl|ps2_ctrl|countclk\(6) & (!\kbd_ctrl|ps2_ctrl|countclk[5]~27\)) # (!\kbd_ctrl|ps2_ctrl|countclk\(6) & ((\kbd_ctrl|ps2_ctrl|countclk[5]~27\) # (GND)))
-- \kbd_ctrl|ps2_ctrl|countclk[6]~29\ = CARRY((!\kbd_ctrl|ps2_ctrl|countclk[5]~27\) # (!\kbd_ctrl|ps2_ctrl|countclk\(6)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|countclk\(6),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|countclk[5]~27\,
	combout => \kbd_ctrl|ps2_ctrl|countclk[6]~28_combout\,
	cout => \kbd_ctrl|ps2_ctrl|countclk[6]~29\);

-- Location: LCCOMB_X29_Y19_N26
\kbd_ctrl|ps2_ctrl|countclk[7]~30\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|countclk[7]~30_combout\ = (\kbd_ctrl|ps2_ctrl|countclk\(7) & (\kbd_ctrl|ps2_ctrl|countclk[6]~29\ $ (GND))) # (!\kbd_ctrl|ps2_ctrl|countclk\(7) & (!\kbd_ctrl|ps2_ctrl|countclk[6]~29\ & VCC))
-- \kbd_ctrl|ps2_ctrl|countclk[7]~31\ = CARRY((\kbd_ctrl|ps2_ctrl|countclk\(7) & !\kbd_ctrl|ps2_ctrl|countclk[6]~29\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|countclk\(7),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|countclk[6]~29\,
	combout => \kbd_ctrl|ps2_ctrl|countclk[7]~30_combout\,
	cout => \kbd_ctrl|ps2_ctrl|countclk[7]~31\);

-- Location: LCFF_X29_Y19_N27
\kbd_ctrl|ps2_ctrl|countclk[7]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|countclk[7]~30_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|FROMPS2~1_combout\,
	ena => \kbd_ctrl|ps2_ctrl|countclk[18]~54_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|countclk\(7));

-- Location: LCCOMB_X29_Y19_N28
\kbd_ctrl|ps2_ctrl|countclk[8]~32\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|countclk[8]~32_combout\ = (\kbd_ctrl|ps2_ctrl|countclk\(8) & (!\kbd_ctrl|ps2_ctrl|countclk[7]~31\)) # (!\kbd_ctrl|ps2_ctrl|countclk\(8) & ((\kbd_ctrl|ps2_ctrl|countclk[7]~31\) # (GND)))
-- \kbd_ctrl|ps2_ctrl|countclk[8]~33\ = CARRY((!\kbd_ctrl|ps2_ctrl|countclk[7]~31\) # (!\kbd_ctrl|ps2_ctrl|countclk\(8)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|countclk\(8),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|countclk[7]~31\,
	combout => \kbd_ctrl|ps2_ctrl|countclk[8]~32_combout\,
	cout => \kbd_ctrl|ps2_ctrl|countclk[8]~33\);

-- Location: LCFF_X29_Y18_N19
\kbd_ctrl|ps2_ctrl|countclk[8]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	sdata => \kbd_ctrl|ps2_ctrl|countclk[8]~32_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|FROMPS2~1_combout\,
	sload => VCC,
	ena => \kbd_ctrl|ps2_ctrl|countclk[18]~54_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|countclk\(8));

-- Location: LCCOMB_X29_Y18_N0
\kbd_ctrl|ps2_ctrl|countclk[10]~36\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|countclk[10]~36_combout\ = (\kbd_ctrl|ps2_ctrl|countclk\(10) & (!\kbd_ctrl|ps2_ctrl|countclk[9]~35\)) # (!\kbd_ctrl|ps2_ctrl|countclk\(10) & ((\kbd_ctrl|ps2_ctrl|countclk[9]~35\) # (GND)))
-- \kbd_ctrl|ps2_ctrl|countclk[10]~37\ = CARRY((!\kbd_ctrl|ps2_ctrl|countclk[9]~35\) # (!\kbd_ctrl|ps2_ctrl|countclk\(10)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|countclk\(10),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|countclk[9]~35\,
	combout => \kbd_ctrl|ps2_ctrl|countclk[10]~36_combout\,
	cout => \kbd_ctrl|ps2_ctrl|countclk[10]~37\);

-- Location: LCFF_X29_Y18_N1
\kbd_ctrl|ps2_ctrl|countclk[10]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|countclk[10]~36_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|FROMPS2~1_combout\,
	ena => \kbd_ctrl|ps2_ctrl|countclk[18]~54_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|countclk\(10));

-- Location: LCCOMB_X29_Y18_N2
\kbd_ctrl|ps2_ctrl|countclk[11]~38\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|countclk[11]~38_combout\ = (\kbd_ctrl|ps2_ctrl|countclk\(11) & (\kbd_ctrl|ps2_ctrl|countclk[10]~37\ $ (GND))) # (!\kbd_ctrl|ps2_ctrl|countclk\(11) & (!\kbd_ctrl|ps2_ctrl|countclk[10]~37\ & VCC))
-- \kbd_ctrl|ps2_ctrl|countclk[11]~39\ = CARRY((\kbd_ctrl|ps2_ctrl|countclk\(11) & !\kbd_ctrl|ps2_ctrl|countclk[10]~37\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|countclk\(11),
	datad => VCC,
	cin => \kbd_ctrl|ps2_ctrl|countclk[10]~37\,
	combout => \kbd_ctrl|ps2_ctrl|countclk[11]~38_combout\,
	cout => \kbd_ctrl|ps2_ctrl|countclk[11]~39\);

-- Location: LCFF_X29_Y18_N3
\kbd_ctrl|ps2_ctrl|countclk[11]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|countclk[11]~38_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|FROMPS2~1_combout\,
	ena => \kbd_ctrl|ps2_ctrl|countclk[18]~54_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|countclk\(11));

-- Location: LCFF_X29_Y18_N5
\kbd_ctrl|ps2_ctrl|countclk[12]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|countclk[12]~40_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|FROMPS2~1_combout\,
	ena => \kbd_ctrl|ps2_ctrl|countclk[18]~54_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|countclk\(12));

-- Location: LCFF_X29_Y18_N7
\kbd_ctrl|ps2_ctrl|countclk[13]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|countclk[13]~42_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|FROMPS2~1_combout\,
	ena => \kbd_ctrl|ps2_ctrl|countclk[18]~54_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|countclk\(13));

-- Location: LCCOMB_X29_Y18_N22
\kbd_ctrl|ps2_ctrl|Equal2~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Equal2~4_combout\ = (!\kbd_ctrl|ps2_ctrl|countclk\(15) & (!\kbd_ctrl|ps2_ctrl|countclk\(12) & (!\kbd_ctrl|ps2_ctrl|countclk\(14) & !\kbd_ctrl|ps2_ctrl|countclk\(13))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|countclk\(15),
	datab => \kbd_ctrl|ps2_ctrl|countclk\(12),
	datac => \kbd_ctrl|ps2_ctrl|countclk\(14),
	datad => \kbd_ctrl|ps2_ctrl|countclk\(13),
	combout => \kbd_ctrl|ps2_ctrl|Equal2~4_combout\);

-- Location: LCFF_X29_Y19_N25
\kbd_ctrl|ps2_ctrl|countclk[6]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|countclk[6]~28_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|FROMPS2~1_combout\,
	ena => \kbd_ctrl|ps2_ctrl|countclk[18]~54_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|countclk\(6));

-- Location: LCCOMB_X29_Y19_N8
\kbd_ctrl|ps2_ctrl|Equal2~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Equal2~2_combout\ = (!\kbd_ctrl|ps2_ctrl|countclk\(4) & (!\kbd_ctrl|ps2_ctrl|countclk\(7) & (\kbd_ctrl|ps2_ctrl|countclk\(6) & !\kbd_ctrl|ps2_ctrl|countclk\(5))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|countclk\(4),
	datab => \kbd_ctrl|ps2_ctrl|countclk\(7),
	datac => \kbd_ctrl|ps2_ctrl|countclk\(6),
	datad => \kbd_ctrl|ps2_ctrl|countclk\(5),
	combout => \kbd_ctrl|ps2_ctrl|Equal2~2_combout\);

-- Location: LCFF_X29_Y19_N15
\kbd_ctrl|ps2_ctrl|countclk[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|countclk[1]~18_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|FROMPS2~1_combout\,
	ena => \kbd_ctrl|ps2_ctrl|countclk[18]~54_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|countclk\(1));

-- Location: LCFF_X29_Y19_N17
\kbd_ctrl|ps2_ctrl|countclk[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|countclk[2]~20_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|FROMPS2~1_combout\,
	ena => \kbd_ctrl|ps2_ctrl|countclk[18]~54_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|countclk\(2));

-- Location: LCCOMB_X29_Y19_N6
\kbd_ctrl|ps2_ctrl|Equal2~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Equal2~1_combout\ = (!\kbd_ctrl|ps2_ctrl|countclk\(0) & (!\kbd_ctrl|ps2_ctrl|countclk\(1) & (!\kbd_ctrl|ps2_ctrl|countclk\(2) & !\kbd_ctrl|ps2_ctrl|countclk\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|countclk\(0),
	datab => \kbd_ctrl|ps2_ctrl|countclk\(1),
	datac => \kbd_ctrl|ps2_ctrl|countclk\(2),
	datad => \kbd_ctrl|ps2_ctrl|countclk\(3),
	combout => \kbd_ctrl|ps2_ctrl|Equal2~1_combout\);

-- Location: LCCOMB_X29_Y18_N28
\kbd_ctrl|ps2_ctrl|Equal2~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Equal2~5_combout\ = (\kbd_ctrl|ps2_ctrl|Equal2~3_combout\ & (\kbd_ctrl|ps2_ctrl|Equal2~4_combout\ & (\kbd_ctrl|ps2_ctrl|Equal2~2_combout\ & \kbd_ctrl|ps2_ctrl|Equal2~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|Equal2~3_combout\,
	datab => \kbd_ctrl|ps2_ctrl|Equal2~4_combout\,
	datac => \kbd_ctrl|ps2_ctrl|Equal2~2_combout\,
	datad => \kbd_ctrl|ps2_ctrl|Equal2~1_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Equal2~5_combout\);

-- Location: LCCOMB_X31_Y17_N22
\kbd_ctrl|ps2_ctrl|send_rdy~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|send_rdy~3_combout\ = \kbd_ctrl|ps2_ctrl|send_rdy~1_combout\ $ (((\kbd_ctrl|ps2_ctrl|send_rdy~2_combout\) # ((\kbd_ctrl|ps2_ctrl|Equal2~0_combout\ & \kbd_ctrl|ps2_ctrl|Equal2~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111000111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|Equal2~0_combout\,
	datab => \kbd_ctrl|ps2_ctrl|send_rdy~2_combout\,
	datac => \kbd_ctrl|ps2_ctrl|send_rdy~1_combout\,
	datad => \kbd_ctrl|ps2_ctrl|Equal2~5_combout\,
	combout => \kbd_ctrl|ps2_ctrl|send_rdy~3_combout\);

-- Location: LCCOMB_X31_Y17_N14
\kbd_ctrl|ps2_ctrl|send_rdy~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|send_rdy~0_combout\ = (\kbd_ctrl|ps2_ctrl|TOPS2~1_combout\) # (!\KEY~combout\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \KEY~combout\(0),
	datad => \kbd_ctrl|ps2_ctrl|TOPS2~1_combout\,
	combout => \kbd_ctrl|ps2_ctrl|send_rdy~0_combout\);

-- Location: LCFF_X31_Y17_N23
\kbd_ctrl|ps2_ctrl|send_rdy~_emulated\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|send_rdy~3_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|send_rdy~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|send_rdy~_emulated_regout\);

-- Location: LCCOMB_X31_Y17_N0
\kbd_ctrl|ps2_ctrl|send_rdy~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|send_rdy~2_combout\ = (!\kbd_ctrl|ps2_ctrl|TOPS2~1_combout\ & ((\kbd_ctrl|ps2_ctrl|send_rdy~1_combout\ $ (\kbd_ctrl|ps2_ctrl|send_rdy~_emulated_regout\)) # (!\KEY~combout\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001111101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \KEY~combout\(0),
	datab => \kbd_ctrl|ps2_ctrl|send_rdy~1_combout\,
	datac => \kbd_ctrl|ps2_ctrl|send_rdy~_emulated_regout\,
	datad => \kbd_ctrl|ps2_ctrl|TOPS2~1_combout\,
	combout => \kbd_ctrl|ps2_ctrl|send_rdy~2_combout\);

-- Location: LCCOMB_X31_Y19_N4
\kbd_ctrl|ps2_ctrl|count~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|count~12_combout\ = (\kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~regout\ & (!\kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~regout\ & ((\kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~regout\) # (!\kbd_ctrl|ps2_ctrl|FROMPS2:count[3]~regout\)))) # 
-- (!\kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~regout\ & (((\kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~regout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|FROMPS2:count[3]~regout\,
	datab => \kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~regout\,
	datac => \kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~regout\,
	datad => \kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~regout\,
	combout => \kbd_ctrl|ps2_ctrl|count~12_combout\);

-- Location: LCFF_X31_Y19_N5
\kbd_ctrl|ps2_ctrl|FROMPS2:count[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|count~12_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|FROMPS2~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~regout\);

-- Location: LCCOMB_X31_Y19_N6
\kbd_ctrl|ps2_ctrl|count~13\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|count~13_combout\ = (!\kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~regout\ & (((\kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~regout\) # (!\kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~regout\)) # (!\kbd_ctrl|ps2_ctrl|FROMPS2:count[3]~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|FROMPS2:count[3]~regout\,
	datab => \kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~regout\,
	datac => \kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~regout\,
	datad => \kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~regout\,
	combout => \kbd_ctrl|ps2_ctrl|count~13_combout\);

-- Location: LCFF_X31_Y19_N7
\kbd_ctrl|ps2_ctrl|FROMPS2:count[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|count~13_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|FROMPS2~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~regout\);

-- Location: LCCOMB_X31_Y19_N18
\kbd_ctrl|ps2_ctrl|Add3~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Add3~0_combout\ = \kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~regout\ $ (((\kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~regout\ & \kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100001111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~regout\,
	datab => \kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~regout\,
	datac => \kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~regout\,
	combout => \kbd_ctrl|ps2_ctrl|Add3~0_combout\);

-- Location: LCFF_X31_Y19_N19
\kbd_ctrl|ps2_ctrl|FROMPS2:count[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|Add3~0_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|FROMPS2~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~regout\);

-- Location: LCCOMB_X31_Y19_N26
\kbd_ctrl|ps2_ctrl|FROMPS2~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|FROMPS2~0_combout\ = \kbd_ctrl|ps2_ctrl|FROMPS2:count[3]~regout\ $ (((\kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~regout\) # ((\kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~regout\) # (\kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~regout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010101010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|FROMPS2:count[3]~regout\,
	datab => \kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~regout\,
	datac => \kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~regout\,
	datad => \kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~regout\,
	combout => \kbd_ctrl|ps2_ctrl|FROMPS2~0_combout\);

-- Location: LCCOMB_X31_Y19_N12
\kbd_ctrl|ps2_ctrl|Decoder0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Decoder0~0_combout\ = (\kbd_ctrl|ps2_ctrl|sigsending~regout\) # (!\kbd_ctrl|ps2_ctrl|FROMPS2~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|FROMPS2~0_combout\,
	datad => \kbd_ctrl|ps2_ctrl|sigsending~regout\,
	combout => \kbd_ctrl|ps2_ctrl|Decoder0~0_combout\);

-- Location: LCCOMB_X31_Y19_N10
\kbd_ctrl|ps2_ctrl|Decoder0~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Decoder0~1_combout\ = (!\kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~regout\ & (!\kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~regout\ & (\kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~regout\ & !\kbd_ctrl|ps2_ctrl|Decoder0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~regout\,
	datab => \kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~regout\,
	datac => \kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~regout\,
	datad => \kbd_ctrl|ps2_ctrl|Decoder0~0_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Decoder0~1_combout\);

-- Location: LCCOMB_X32_Y19_N18
\kbd_ctrl|ps2_ctrl|sdata[0]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|sdata[0]~0_combout\ = (\kbd_ctrl|ps2_ctrl|Decoder0~1_combout\ & (\PS2_DAT~0\)) # (!\kbd_ctrl|ps2_ctrl|Decoder0~1_combout\ & ((\kbd_ctrl|ps2_ctrl|sdata\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \PS2_DAT~0\,
	datac => \kbd_ctrl|ps2_ctrl|sdata\(0),
	datad => \kbd_ctrl|ps2_ctrl|Decoder0~1_combout\,
	combout => \kbd_ctrl|ps2_ctrl|sdata[0]~0_combout\);

-- Location: LCFF_X32_Y19_N19
\kbd_ctrl|ps2_ctrl|sdata[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|sdata[0]~0_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|FROMPS2~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|sdata\(0));

-- Location: LCCOMB_X32_Y19_N30
\kbd_ctrl|ps2_ctrl|Decoder0~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Decoder0~3_combout\ = (\kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~regout\ & (!\kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~regout\ & (!\kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~regout\ & !\kbd_ctrl|ps2_ctrl|Decoder0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~regout\,
	datab => \kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~regout\,
	datac => \kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~regout\,
	datad => \kbd_ctrl|ps2_ctrl|Decoder0~0_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Decoder0~3_combout\);

-- Location: LCCOMB_X32_Y19_N8
\kbd_ctrl|ps2_ctrl|sdata[3]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|sdata[3]~2_combout\ = (\kbd_ctrl|ps2_ctrl|Decoder0~3_combout\ & (\PS2_DAT~0\)) # (!\kbd_ctrl|ps2_ctrl|Decoder0~3_combout\ & ((\kbd_ctrl|ps2_ctrl|sdata\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \PS2_DAT~0\,
	datac => \kbd_ctrl|ps2_ctrl|sdata\(3),
	datad => \kbd_ctrl|ps2_ctrl|Decoder0~3_combout\,
	combout => \kbd_ctrl|ps2_ctrl|sdata[3]~2_combout\);

-- Location: LCFF_X32_Y19_N9
\kbd_ctrl|ps2_ctrl|sdata[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|sdata[3]~2_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|FROMPS2~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|sdata\(3));

-- Location: LCCOMB_X31_Y19_N16
\kbd_ctrl|ps2_ctrl|Decoder0~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Decoder0~4_combout\ = (\kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~regout\ & (!\kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~regout\ & (\kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~regout\ & !\kbd_ctrl|ps2_ctrl|Decoder0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~regout\,
	datab => \kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~regout\,
	datac => \kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~regout\,
	datad => \kbd_ctrl|ps2_ctrl|Decoder0~0_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Decoder0~4_combout\);

-- Location: LCCOMB_X32_Y19_N2
\kbd_ctrl|ps2_ctrl|sdata[2]~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|sdata[2]~3_combout\ = (\kbd_ctrl|ps2_ctrl|Decoder0~4_combout\ & (\PS2_DAT~0\)) # (!\kbd_ctrl|ps2_ctrl|Decoder0~4_combout\ & ((\kbd_ctrl|ps2_ctrl|sdata\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \PS2_DAT~0\,
	datac => \kbd_ctrl|ps2_ctrl|sdata\(2),
	datad => \kbd_ctrl|ps2_ctrl|Decoder0~4_combout\,
	combout => \kbd_ctrl|ps2_ctrl|sdata[2]~3_combout\);

-- Location: LCFF_X32_Y19_N3
\kbd_ctrl|ps2_ctrl|sdata[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|sdata[2]~3_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|FROMPS2~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|sdata\(2));

-- Location: LCCOMB_X32_Y19_N4
\kbd_ctrl|Equal21~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal21~0_combout\ = ((\kbd_ctrl|ps2_ctrl|sdata\(0)) # ((!\kbd_ctrl|ps2_ctrl|sdata\(2)) # (!\kbd_ctrl|ps2_ctrl|sdata\(3)))) # (!\kbd_ctrl|ps2_ctrl|sdata\(1))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|sdata\(1),
	datab => \kbd_ctrl|ps2_ctrl|sdata\(0),
	datac => \kbd_ctrl|ps2_ctrl|sdata\(3),
	datad => \kbd_ctrl|ps2_ctrl|sdata\(2),
	combout => \kbd_ctrl|Equal21~0_combout\);

-- Location: LCCOMB_X31_Y19_N28
\kbd_ctrl|ps2_ctrl|Decoder0~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Decoder0~7_combout\ = (!\kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~regout\ & (!\kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~regout\ & (!\kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~regout\ & !\kbd_ctrl|ps2_ctrl|Decoder0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~regout\,
	datab => \kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~regout\,
	datac => \kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~regout\,
	datad => \kbd_ctrl|ps2_ctrl|Decoder0~0_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Decoder0~7_combout\);

-- Location: LCCOMB_X31_Y19_N0
\kbd_ctrl|ps2_ctrl|sdata[7]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|sdata[7]~6_combout\ = (\kbd_ctrl|ps2_ctrl|Decoder0~7_combout\ & (\PS2_DAT~0\)) # (!\kbd_ctrl|ps2_ctrl|Decoder0~7_combout\ & ((\kbd_ctrl|ps2_ctrl|sdata\(7))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \PS2_DAT~0\,
	datac => \kbd_ctrl|ps2_ctrl|sdata\(7),
	datad => \kbd_ctrl|ps2_ctrl|Decoder0~7_combout\,
	combout => \kbd_ctrl|ps2_ctrl|sdata[7]~6_combout\);

-- Location: LCFF_X31_Y19_N1
\kbd_ctrl|ps2_ctrl|sdata[7]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|sdata[7]~6_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|FROMPS2~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|sdata\(7));

-- Location: LCCOMB_X32_Y19_N22
\kbd_ctrl|ps2_ctrl|Decoder0~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Decoder0~6_combout\ = (\kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~regout\ & (\kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~regout\ & (!\kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~regout\ & !\kbd_ctrl|ps2_ctrl|Decoder0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~regout\,
	datab => \kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~regout\,
	datac => \kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~regout\,
	datad => \kbd_ctrl|ps2_ctrl|Decoder0~0_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Decoder0~6_combout\);

-- Location: LCCOMB_X32_Y19_N28
\kbd_ctrl|ps2_ctrl|sdata[4]~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|sdata[4]~5_combout\ = (\kbd_ctrl|ps2_ctrl|Decoder0~6_combout\ & (\PS2_DAT~0\)) # (!\kbd_ctrl|ps2_ctrl|Decoder0~6_combout\ & ((\kbd_ctrl|ps2_ctrl|sdata\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \PS2_DAT~0\,
	datac => \kbd_ctrl|ps2_ctrl|sdata\(4),
	datad => \kbd_ctrl|ps2_ctrl|Decoder0~6_combout\,
	combout => \kbd_ctrl|ps2_ctrl|sdata[4]~5_combout\);

-- Location: LCFF_X32_Y19_N29
\kbd_ctrl|ps2_ctrl|sdata[4]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|sdata[4]~5_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|FROMPS2~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|sdata\(4));

-- Location: LCCOMB_X31_Y19_N2
\kbd_ctrl|ps2_ctrl|Decoder0~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Decoder0~5_combout\ = (\kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~regout\ & (\kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~regout\ & (!\kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~regout\ & !\kbd_ctrl|ps2_ctrl|Decoder0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~regout\,
	datab => \kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~regout\,
	datac => \kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~regout\,
	datad => \kbd_ctrl|ps2_ctrl|Decoder0~0_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Decoder0~5_combout\);

-- Location: LCCOMB_X32_Y19_N6
\kbd_ctrl|ps2_ctrl|sdata[5]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|sdata[5]~4_combout\ = (\kbd_ctrl|ps2_ctrl|Decoder0~5_combout\ & (\PS2_DAT~0\)) # (!\kbd_ctrl|ps2_ctrl|Decoder0~5_combout\ & ((\kbd_ctrl|ps2_ctrl|sdata\(5))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \PS2_DAT~0\,
	datac => \kbd_ctrl|ps2_ctrl|sdata\(5),
	datad => \kbd_ctrl|ps2_ctrl|Decoder0~5_combout\,
	combout => \kbd_ctrl|ps2_ctrl|sdata[5]~4_combout\);

-- Location: LCFF_X32_Y19_N7
\kbd_ctrl|ps2_ctrl|sdata[5]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|sdata[5]~4_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|FROMPS2~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|sdata\(5));

-- Location: LCCOMB_X32_Y17_N28
\kbd_ctrl|Equal21~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal21~1_combout\ = (((!\kbd_ctrl|ps2_ctrl|sdata\(5)) # (!\kbd_ctrl|ps2_ctrl|sdata\(4))) # (!\kbd_ctrl|ps2_ctrl|sdata\(7))) # (!\kbd_ctrl|ps2_ctrl|sdata\(6))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|sdata\(6),
	datab => \kbd_ctrl|ps2_ctrl|sdata\(7),
	datac => \kbd_ctrl|ps2_ctrl|sdata\(4),
	datad => \kbd_ctrl|ps2_ctrl|sdata\(5),
	combout => \kbd_ctrl|Equal21~1_combout\);

-- Location: LCCOMB_X32_Y17_N12
\kbd_ctrl|Equal21~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal21~2_combout\ = (\kbd_ctrl|Equal21~0_combout\) # (\kbd_ctrl|Equal21~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \kbd_ctrl|Equal21~0_combout\,
	datad => \kbd_ctrl|Equal21~1_combout\,
	combout => \kbd_ctrl|Equal21~2_combout\);

-- Location: LCCOMB_X33_Y17_N20
\kbd_ctrl|Selector8~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Selector8~1_combout\ = (\kbd_ctrl|Selector8~0_combout\ & (\kbd_ctrl|Equal21~2_combout\ & ((\kbd_ctrl|ps2_ctrl|send_rdy~2_combout\) # (\kbd_ctrl|cmdstate.SETCMD~regout\)))) # (!\kbd_ctrl|Selector8~0_combout\ & 
-- ((\kbd_ctrl|ps2_ctrl|send_rdy~2_combout\) # ((\kbd_ctrl|cmdstate.SETCMD~regout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110001010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|Selector8~0_combout\,
	datab => \kbd_ctrl|ps2_ctrl|send_rdy~2_combout\,
	datac => \kbd_ctrl|cmdstate.SETCMD~regout\,
	datad => \kbd_ctrl|Equal21~2_combout\,
	combout => \kbd_ctrl|Selector8~1_combout\);

-- Location: LCCOMB_X30_Y17_N22
\dir~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \dir~0_combout\ = (lights(2)) # ((\dir~regout\ & lights(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => lights(2),
	datac => \dir~regout\,
	datad => lights(0),
	combout => \dir~0_combout\);

-- Location: LCFF_X30_Y17_N23
dir : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCKHZ~clkctrl_outclk\,
	datain => \dir~0_combout\,
	aclr => \ALT_INV_KEY~combout\(0),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \dir~regout\);

-- Location: LCCOMB_X30_Y17_N26
\lights~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \lights~1_combout\ = ((!lights(2) & ((!\dir~regout\) # (!lights(0))))) # (!lights(1))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011011101110111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => lights(2),
	datab => lights(1),
	datac => lights(0),
	datad => \dir~regout\,
	combout => \lights~1_combout\);

-- Location: LCCOMB_X32_Y19_N0
\kbd_ctrl|ps2_ctrl|Decoder0~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Decoder0~2_combout\ = (!\kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~regout\ & (!\kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~regout\ & (\kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~regout\ & !\kbd_ctrl|ps2_ctrl|Decoder0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~regout\,
	datab => \kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~regout\,
	datac => \kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~regout\,
	datad => \kbd_ctrl|ps2_ctrl|Decoder0~0_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Decoder0~2_combout\);

-- Location: LCCOMB_X32_Y19_N14
\kbd_ctrl|ps2_ctrl|sdata[1]~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|sdata[1]~1_combout\ = (\kbd_ctrl|ps2_ctrl|Decoder0~2_combout\ & (\PS2_DAT~0\)) # (!\kbd_ctrl|ps2_ctrl|Decoder0~2_combout\ & ((\kbd_ctrl|ps2_ctrl|sdata\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \PS2_DAT~0\,
	datac => \kbd_ctrl|ps2_ctrl|sdata\(1),
	datad => \kbd_ctrl|ps2_ctrl|Decoder0~2_combout\,
	combout => \kbd_ctrl|ps2_ctrl|sdata[1]~1_combout\);

-- Location: LCFF_X32_Y19_N15
\kbd_ctrl|ps2_ctrl|sdata[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|sdata[1]~1_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|FROMPS2~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|sdata\(1));

-- Location: LCCOMB_X32_Y19_N20
\kbd_ctrl|ps2_ctrl|FROMPS2~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|FROMPS2~2_combout\ = \PS2_DAT~0\ $ (\kbd_ctrl|ps2_ctrl|sdata\(0) $ (\kbd_ctrl|ps2_ctrl|sdata\(1) $ (\kbd_ctrl|ps2_ctrl|sdata\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \PS2_DAT~0\,
	datab => \kbd_ctrl|ps2_ctrl|sdata\(0),
	datac => \kbd_ctrl|ps2_ctrl|sdata\(1),
	datad => \kbd_ctrl|ps2_ctrl|sdata\(2),
	combout => \kbd_ctrl|ps2_ctrl|FROMPS2~2_combout\);

-- Location: LCCOMB_X32_Y19_N12
\kbd_ctrl|ps2_ctrl|FROMPS2~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|FROMPS2~3_combout\ = \kbd_ctrl|ps2_ctrl|sdata\(6) $ (\kbd_ctrl|ps2_ctrl|sdata\(4) $ (\kbd_ctrl|ps2_ctrl|sdata\(3) $ (\kbd_ctrl|ps2_ctrl|sdata\(5))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|sdata\(6),
	datab => \kbd_ctrl|ps2_ctrl|sdata\(4),
	datac => \kbd_ctrl|ps2_ctrl|sdata\(3),
	datad => \kbd_ctrl|ps2_ctrl|sdata\(5),
	combout => \kbd_ctrl|ps2_ctrl|FROMPS2~3_combout\);

-- Location: LCCOMB_X32_Y19_N26
\kbd_ctrl|ps2_ctrl|parchecked~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|parchecked~0_combout\ = \kbd_ctrl|ps2_ctrl|sdata\(7) $ (\kbd_ctrl|ps2_ctrl|FROMPS2~2_combout\ $ (\kbd_ctrl|ps2_ctrl|FROMPS2~3_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|sdata\(7),
	datac => \kbd_ctrl|ps2_ctrl|FROMPS2~2_combout\,
	datad => \kbd_ctrl|ps2_ctrl|FROMPS2~3_combout\,
	combout => \kbd_ctrl|ps2_ctrl|parchecked~0_combout\);

-- Location: LCCOMB_X32_Y19_N10
\kbd_ctrl|ps2_ctrl|parchecked~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|parchecked~2_combout\ = (\kbd_ctrl|ps2_ctrl|parchecked~1_combout\ & (\kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~regout\ & ((\kbd_ctrl|ps2_ctrl|parchecked~0_combout\)))) # (!\kbd_ctrl|ps2_ctrl|parchecked~1_combout\ & 
-- (((\kbd_ctrl|ps2_ctrl|parchecked~regout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|parchecked~1_combout\,
	datab => \kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~regout\,
	datac => \kbd_ctrl|ps2_ctrl|parchecked~regout\,
	datad => \kbd_ctrl|ps2_ctrl|parchecked~0_combout\,
	combout => \kbd_ctrl|ps2_ctrl|parchecked~2_combout\);

-- Location: LCFF_X32_Y19_N11
\kbd_ctrl|ps2_ctrl|parchecked\ : cycloneii_lcell_ff
PORT MAP (
	clk => \kbd_ctrl|ps2_ctrl|sigtrigger~regout\,
	datain => \kbd_ctrl|ps2_ctrl|parchecked~2_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|FROMPS2~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|parchecked~regout\);

-- Location: LCCOMB_X32_Y17_N6
\kbd_ctrl|ps2_ctrl|odata_rdy\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|odata_rdy~combout\ = LCELL((\kbd_ctrl|ps2_ctrl|parchecked~regout\ & \KEY~combout\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|parchecked~regout\,
	datad => \KEY~combout\(1),
	combout => \kbd_ctrl|ps2_ctrl|odata_rdy~combout\);

-- Location: LCCOMB_X33_Y17_N18
\kbd_ctrl|cmdstate.CLEAR~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|cmdstate.CLEAR~0_combout\ = (\kbd_ctrl|cmdstate.CLEAR~regout\) # ((\kbd_ctrl|cmdstate.WAITACK1~regout\ & (\kbd_ctrl|ps2_ctrl|odata_rdy~combout\ & \kbd_ctrl|Equal21~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|cmdstate.WAITACK1~regout\,
	datab => \kbd_ctrl|ps2_ctrl|odata_rdy~combout\,
	datac => \kbd_ctrl|cmdstate.CLEAR~regout\,
	datad => \kbd_ctrl|Equal21~2_combout\,
	combout => \kbd_ctrl|cmdstate.CLEAR~0_combout\);

-- Location: LCFF_X33_Y17_N19
\kbd_ctrl|cmdstate.CLEAR\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|cmdstate.CLEAR~0_combout\,
	aclr => \kbd_ctrl|process_15~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|cmdstate.CLEAR~regout\);

-- Location: LCFF_X33_Y17_N17
\kbd_ctrl|sigsending\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	sdata => \kbd_ctrl|cmdstate.CLEAR~regout\,
	aclr => \kbd_ctrl|process_15~0_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|sigsending~regout\);

-- Location: LCCOMB_X33_Y17_N8
\kbd_ctrl|process_0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|process_0~0_combout\ = (!\kbd_ctrl|sigsending~regout\) # (!\KEY~combout\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101111101011111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \KEY~combout\(0),
	datac => \kbd_ctrl|sigsending~regout\,
	combout => \kbd_ctrl|process_0~0_combout\);

-- Location: LCFF_X33_Y17_N3
\kbd_ctrl|state.DECODE\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	sdata => \kbd_ctrl|state.FETCH~regout\,
	aclr => \kbd_ctrl|process_0~0_combout\,
	sload => VCC,
	ena => \KEY~combout\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|state.DECODE~regout\);

-- Location: LCFF_X24_Y17_N31
\kbd_ctrl|fetchdata[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	sdata => \kbd_ctrl|ps2_ctrl|sdata\(0),
	aclr => \ALT_INV_KEY~combout\(0),
	sload => VCC,
	ena => \kbd_ctrl|state.FETCH~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|fetchdata\(0));

-- Location: LCCOMB_X33_Y17_N24
\kbd_ctrl|state.CODE~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|state.CODE~0_combout\ = (\kbd_ctrl|state.DECODE~regout\ & (((\kbd_ctrl|fetchdata\(4) & \kbd_ctrl|fetchdata\(0))) # (!\kbd_ctrl|Equal0~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100010001000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|Equal0~1_combout\,
	datab => \kbd_ctrl|state.DECODE~regout\,
	datac => \kbd_ctrl|fetchdata\(4),
	datad => \kbd_ctrl|fetchdata\(0),
	combout => \kbd_ctrl|state.CODE~0_combout\);

-- Location: LCFF_X33_Y17_N25
\kbd_ctrl|state.CODE\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|state.CODE~0_combout\,
	aclr => \kbd_ctrl|process_0~0_combout\,
	ena => \KEY~combout\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|state.CODE~regout\);

-- Location: LCFF_X33_Y17_N15
\kbd_ctrl|state.CLRDP\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	sdata => \kbd_ctrl|state.CODE~regout\,
	aclr => \kbd_ctrl|process_0~0_combout\,
	sload => VCC,
	ena => \KEY~combout\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|state.CLRDP~regout\);

-- Location: LCFF_X24_Y17_N1
\kbd_ctrl|fetchdata[5]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	sdata => \kbd_ctrl|ps2_ctrl|sdata\(5),
	aclr => \ALT_INV_KEY~combout\(0),
	sload => VCC,
	ena => \kbd_ctrl|state.FETCH~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|fetchdata\(5));

-- Location: LCFF_X24_Y17_N21
\kbd_ctrl|fetchdata[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	sdata => \kbd_ctrl|ps2_ctrl|sdata\(2),
	aclr => \ALT_INV_KEY~combout\(0),
	sload => VCC,
	ena => \kbd_ctrl|state.FETCH~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|fetchdata\(2));

-- Location: LCFF_X24_Y17_N17
\kbd_ctrl|fetchdata[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	sdata => \kbd_ctrl|ps2_ctrl|sdata\(3),
	aclr => \ALT_INV_KEY~combout\(0),
	sload => VCC,
	ena => \kbd_ctrl|state.FETCH~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|fetchdata\(3));

-- Location: LCFF_X24_Y17_N29
\kbd_ctrl|fetchdata[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	sdata => \kbd_ctrl|ps2_ctrl|sdata\(1),
	aclr => \ALT_INV_KEY~combout\(0),
	sload => VCC,
	ena => \kbd_ctrl|state.FETCH~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|fetchdata\(1));

-- Location: LCCOMB_X24_Y17_N16
\kbd_ctrl|Equal0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal0~0_combout\ = (\kbd_ctrl|fetchdata\(6) & (!\kbd_ctrl|fetchdata\(2) & (!\kbd_ctrl|fetchdata\(3) & !\kbd_ctrl|fetchdata\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|fetchdata\(6),
	datab => \kbd_ctrl|fetchdata\(2),
	datac => \kbd_ctrl|fetchdata\(3),
	datad => \kbd_ctrl|fetchdata\(1),
	combout => \kbd_ctrl|Equal0~0_combout\);

-- Location: LCCOMB_X32_Y17_N16
\kbd_ctrl|Equal0~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal0~2_combout\ = (\kbd_ctrl|fetchdata\(7) & (\kbd_ctrl|fetchdata\(5) & (\kbd_ctrl|Equal0~0_combout\ & !\kbd_ctrl|fetchdata\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|fetchdata\(7),
	datab => \kbd_ctrl|fetchdata\(5),
	datac => \kbd_ctrl|Equal0~0_combout\,
	datad => \kbd_ctrl|fetchdata\(0),
	combout => \kbd_ctrl|Equal0~2_combout\);

-- Location: LCCOMB_X33_Y17_N22
\kbd_ctrl|nstate.EXT0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|nstate.EXT0~0_combout\ = (\kbd_ctrl|state.DECODE~regout\ & (!\kbd_ctrl|fetchdata\(4) & \kbd_ctrl|Equal0~2_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|state.DECODE~regout\,
	datac => \kbd_ctrl|fetchdata\(4),
	datad => \kbd_ctrl|Equal0~2_combout\,
	combout => \kbd_ctrl|nstate.EXT0~0_combout\);

-- Location: LCFF_X33_Y17_N23
\kbd_ctrl|state.EXT0\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|nstate.EXT0~0_combout\,
	aclr => \kbd_ctrl|process_0~0_combout\,
	ena => \KEY~combout\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|state.EXT0~regout\);

-- Location: LCCOMB_X33_Y17_N12
\kbd_ctrl|nstate.RELEASE~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|nstate.RELEASE~0_combout\ = (\kbd_ctrl|state.DECODE~regout\ & (\kbd_ctrl|fetchdata\(4) & \kbd_ctrl|Equal0~2_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|state.DECODE~regout\,
	datac => \kbd_ctrl|fetchdata\(4),
	datad => \kbd_ctrl|Equal0~2_combout\,
	combout => \kbd_ctrl|nstate.RELEASE~0_combout\);

-- Location: LCFF_X33_Y17_N13
\kbd_ctrl|state.RELEASE\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|nstate.RELEASE~0_combout\,
	aclr => \kbd_ctrl|process_0~0_combout\,
	ena => \KEY~combout\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|state.RELEASE~regout\);

-- Location: LCCOMB_X32_Y17_N30
\kbd_ctrl|newdata~feeder\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|newdata~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	combout => \kbd_ctrl|newdata~feeder_combout\);

-- Location: LCCOMB_X33_Y17_N14
\kbd_ctrl|process_13~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|process_13~0_combout\ = ((\kbd_ctrl|state.DECODE~regout\) # (!\KEY~combout\(0))) # (!\kbd_ctrl|sigsending~regout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111101110111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|sigsending~regout\,
	datab => \KEY~combout\(0),
	datad => \kbd_ctrl|state.DECODE~regout\,
	combout => \kbd_ctrl|process_13~0_combout\);

-- Location: LCFF_X32_Y17_N31
\kbd_ctrl|newdata\ : cycloneii_lcell_ff
PORT MAP (
	clk => \kbd_ctrl|ps2_ctrl|odata_rdy~combout\,
	datain => \kbd_ctrl|newdata~feeder_combout\,
	aclr => \kbd_ctrl|process_13~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|newdata~regout\);

-- Location: LCCOMB_X32_Y17_N10
\kbd_ctrl|Selector0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Selector0~0_combout\ = (\kbd_ctrl|state.EXT0~regout\) # ((\kbd_ctrl|state.RELEASE~regout\) # ((!\kbd_ctrl|state.IDLE~regout\ & !\kbd_ctrl|newdata~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110011111101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|state.IDLE~regout\,
	datab => \kbd_ctrl|state.EXT0~regout\,
	datac => \kbd_ctrl|state.RELEASE~regout\,
	datad => \kbd_ctrl|newdata~regout\,
	combout => \kbd_ctrl|Selector0~0_combout\);

-- Location: LCCOMB_X33_Y17_N26
\kbd_ctrl|Selector0~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Selector0~1_combout\ = (!\kbd_ctrl|state.EXT1~regout\ & (!\kbd_ctrl|state.CLRDP~regout\ & !\kbd_ctrl|Selector0~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|state.EXT1~regout\,
	datac => \kbd_ctrl|state.CLRDP~regout\,
	datad => \kbd_ctrl|Selector0~0_combout\,
	combout => \kbd_ctrl|Selector0~1_combout\);

-- Location: LCFF_X33_Y17_N27
\kbd_ctrl|state.IDLE\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|Selector0~1_combout\,
	aclr => \kbd_ctrl|process_0~0_combout\,
	ena => \KEY~combout\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|state.IDLE~regout\);

-- Location: LCCOMB_X33_Y17_N6
\kbd_ctrl|nstate.FETCH~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|nstate.FETCH~0_combout\ = (!\kbd_ctrl|state.IDLE~regout\ & \kbd_ctrl|newdata~regout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|state.IDLE~regout\,
	datac => \kbd_ctrl|newdata~regout\,
	combout => \kbd_ctrl|nstate.FETCH~0_combout\);

-- Location: LCFF_X33_Y17_N7
\kbd_ctrl|state.FETCH\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|nstate.FETCH~0_combout\,
	aclr => \kbd_ctrl|process_0~0_combout\,
	ena => \KEY~combout\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|state.FETCH~regout\);

-- Location: LCFF_X24_Y17_N27
\kbd_ctrl|fetchdata[4]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	sdata => \kbd_ctrl|ps2_ctrl|sdata\(4),
	aclr => \ALT_INV_KEY~combout\(0),
	sload => VCC,
	ena => \kbd_ctrl|state.FETCH~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|fetchdata\(4));

-- Location: LCCOMB_X29_Y17_N24
\kbd_ctrl|process_4~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|process_4~0_combout\ = (\kbd_ctrl|state.CLRDP~regout\) # (!\KEY~combout\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \KEY~combout\(0),
	datad => \kbd_ctrl|state.CLRDP~regout\,
	combout => \kbd_ctrl|process_4~0_combout\);

-- Location: LCCOMB_X29_Y17_N30
\kbd_ctrl|relbt\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|relbt~combout\ = (!\kbd_ctrl|process_4~0_combout\ & ((\kbd_ctrl|state.RELEASE~regout\) # (\kbd_ctrl|relbt~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|state.RELEASE~regout\,
	datac => \kbd_ctrl|process_4~0_combout\,
	datad => \kbd_ctrl|relbt~combout\,
	combout => \kbd_ctrl|relbt~combout\);

-- Location: LCCOMB_X29_Y17_N28
\kbd_ctrl|selbt\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|selbt~combout\ = (!\kbd_ctrl|process_4~0_combout\ & ((\kbd_ctrl|state.CODE~regout\) # (\kbd_ctrl|selbt~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|state.CODE~regout\,
	datac => \kbd_ctrl|process_4~0_combout\,
	datad => \kbd_ctrl|selbt~combout\,
	combout => \kbd_ctrl|selbt~combout\);

-- Location: LCCOMB_X29_Y17_N8
\kbd_ctrl|KEY0~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|KEY0~2_combout\ = (\kbd_ctrl|relbt~combout\ & \kbd_ctrl|selbt~combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|relbt~combout\,
	datad => \kbd_ctrl|selbt~combout\,
	combout => \kbd_ctrl|KEY0~2_combout\);

-- Location: LCCOMB_X29_Y17_N12
\kbd_ctrl|selE0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|selE0~combout\ = (!\kbd_ctrl|process_4~0_combout\ & ((\kbd_ctrl|state.EXT0~regout\) # (\kbd_ctrl|selE0~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|state.EXT0~regout\,
	datac => \kbd_ctrl|process_4~0_combout\,
	datad => \kbd_ctrl|selE0~combout\,
	combout => \kbd_ctrl|selE0~combout\);

-- Location: LCFF_X25_Y17_N23
\kbd_ctrl|key0code[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	sdata => \kbd_ctrl|fetchdata\(3),
	aclr => \kbd_ctrl|KEY0~4_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key0en~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|key0code\(3));

-- Location: LCFF_X26_Y17_N17
\kbd_ctrl|key0code[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	sdata => \kbd_ctrl|fetchdata\(1),
	aclr => \kbd_ctrl|KEY0~4_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key0en~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|key0code\(1));

-- Location: LCFF_X26_Y17_N7
\kbd_ctrl|key0code[4]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	sdata => \kbd_ctrl|fetchdata\(4),
	aclr => \kbd_ctrl|KEY0~4_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key0en~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|key0code\(4));

-- Location: LCCOMB_X26_Y17_N16
\kbd_ctrl|Equal17~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal17~0_combout\ = (!\kbd_ctrl|key0code\(6) & (!\kbd_ctrl|key0code\(3) & (!\kbd_ctrl|key0code\(1) & !\kbd_ctrl|key0code\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(6),
	datab => \kbd_ctrl|key0code\(3),
	datac => \kbd_ctrl|key0code\(1),
	datad => \kbd_ctrl|key0code\(4),
	combout => \kbd_ctrl|Equal17~0_combout\);

-- Location: LCCOMB_X29_Y17_N26
\kbd_ctrl|key2en~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|key2en~0_combout\ = (!\kbd_ctrl|relbt~combout\ & \kbd_ctrl|selbt~combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|relbt~combout\,
	datad => \kbd_ctrl|selbt~combout\,
	combout => \kbd_ctrl|key2en~0_combout\);

-- Location: LCCOMB_X25_Y17_N26
\kbd_ctrl|key1en~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|key1en~0_combout\ = (!\kbd_ctrl|Equal18~5_combout\ & (\kbd_ctrl|Equal17~1_combout\ & \kbd_ctrl|key2en~1_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|Equal18~5_combout\,
	datab => \kbd_ctrl|Equal17~1_combout\,
	datad => \kbd_ctrl|key2en~1_combout\,
	combout => \kbd_ctrl|key1en~0_combout\);

-- Location: LCFF_X25_Y17_N9
\kbd_ctrl|key1code[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	sdata => \kbd_ctrl|fetchdata\(3),
	aclr => \kbd_ctrl|KEY1~4_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key1en~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|key1code\(3));

-- Location: LCFF_X25_Y17_N15
\kbd_ctrl|key1code[4]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	sdata => \kbd_ctrl|fetchdata\(4),
	aclr => \kbd_ctrl|KEY1~4_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key1en~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|key1code\(4));

-- Location: LCFF_X25_Y17_N3
\kbd_ctrl|key1code[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	sdata => \kbd_ctrl|fetchdata\(1),
	aclr => \kbd_ctrl|KEY1~4_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key1en~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|key1code\(1));

-- Location: LCCOMB_X25_Y17_N4
\kbd_ctrl|Equal8~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal8~2_combout\ = ((!\kbd_ctrl|key1code\(1)) # (!\kbd_ctrl|key1code\(4))) # (!\kbd_ctrl|key1code\(13))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key1code\(13),
	datab => \kbd_ctrl|key1code\(4),
	datad => \kbd_ctrl|key1code\(1),
	combout => \kbd_ctrl|Equal8~2_combout\);

-- Location: LCFF_X25_Y17_N19
\kbd_ctrl|key1code[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	sdata => \kbd_ctrl|fetchdata\(0),
	aclr => \kbd_ctrl|KEY1~4_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key1en~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|key1code\(0));

-- Location: LCFF_X24_Y17_N23
\kbd_ctrl|fetchdata[7]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	sdata => \kbd_ctrl|ps2_ctrl|sdata\(7),
	aclr => \ALT_INV_KEY~combout\(0),
	sload => VCC,
	ena => \kbd_ctrl|state.FETCH~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|fetchdata\(7));

-- Location: LCFF_X25_Y17_N11
\kbd_ctrl|key1code[7]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	sdata => \kbd_ctrl|fetchdata\(7),
	aclr => \kbd_ctrl|KEY1~4_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key1en~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|key1code\(7));

-- Location: LCFF_X25_Y17_N17
\kbd_ctrl|key1code[5]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	sdata => \kbd_ctrl|fetchdata\(5),
	aclr => \kbd_ctrl|KEY1~4_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key1en~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|key1code\(5));

-- Location: LCCOMB_X25_Y17_N10
\kbd_ctrl|Equal18~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal18~2_combout\ = (!\kbd_ctrl|key1code\(2) & (!\kbd_ctrl|key1code\(0) & (!\kbd_ctrl|key1code\(7) & !\kbd_ctrl|key1code\(5))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key1code\(2),
	datab => \kbd_ctrl|key1code\(0),
	datac => \kbd_ctrl|key1code\(7),
	datad => \kbd_ctrl|key1code\(5),
	combout => \kbd_ctrl|Equal18~2_combout\);

-- Location: LCCOMB_X26_Y17_N22
\kbd_ctrl|Equal8~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal8~3_combout\ = (\kbd_ctrl|key1code\(6)) # ((\kbd_ctrl|key1code\(3)) # ((\kbd_ctrl|Equal8~2_combout\) # (!\kbd_ctrl|Equal18~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key1code\(6),
	datab => \kbd_ctrl|key1code\(3),
	datac => \kbd_ctrl|Equal8~2_combout\,
	datad => \kbd_ctrl|Equal18~2_combout\,
	combout => \kbd_ctrl|Equal8~3_combout\);

-- Location: LCCOMB_X24_Y17_N0
\kbd_ctrl|Equal6~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal6~0_combout\ = (!\kbd_ctrl|fetchdata\(7) & (!\kbd_ctrl|fetchdata\(2) & (!\kbd_ctrl|fetchdata\(5) & \kbd_ctrl|fetchdata\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|fetchdata\(7),
	datab => \kbd_ctrl|fetchdata\(2),
	datac => \kbd_ctrl|fetchdata\(5),
	datad => \kbd_ctrl|fetchdata\(4),
	combout => \kbd_ctrl|Equal6~0_combout\);

-- Location: LCCOMB_X31_Y19_N14
\kbd_ctrl|ps2_ctrl|Decoder0~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Decoder0~8_combout\ = (\kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~regout\ & (\kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~regout\ & (\kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~regout\ & !\kbd_ctrl|ps2_ctrl|Decoder0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|FROMPS2:count[1]~regout\,
	datab => \kbd_ctrl|ps2_ctrl|FROMPS2:count[2]~regout\,
	datac => \kbd_ctrl|ps2_ctrl|FROMPS2:count[0]~regout\,
	datad => \kbd_ctrl|ps2_ctrl|Decoder0~0_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Decoder0~8_combout\);

-- Location: LCCOMB_X32_Y19_N24
\kbd_ctrl|ps2_ctrl|sdata[6]~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|sdata[6]~7_combout\ = (\kbd_ctrl|ps2_ctrl|Decoder0~8_combout\ & (\PS2_DAT~0\)) # (!\kbd_ctrl|ps2_ctrl|Decoder0~8_combout\ & ((\kbd_ctrl|ps2_ctrl|sdata\(6))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \PS2_DAT~0\,
	datac => \kbd_ctrl|ps2_ctrl|sdata\(6),
	datad => \kbd_ctrl|ps2_ctrl|Decoder0~8_combout\,
	combout => \kbd_ctrl|ps2_ctrl|sdata[6]~7_combout\);

-- Location: LCFF_X32_Y19_N25
\kbd_ctrl|ps2_ctrl|sdata[6]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|sdata[6]~7_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|FROMPS2~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|sdata\(6));

-- Location: LCFF_X24_Y17_N7
\kbd_ctrl|fetchdata[6]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	sdata => \kbd_ctrl|ps2_ctrl|sdata\(6),
	aclr => \ALT_INV_KEY~combout\(0),
	sload => VCC,
	ena => \kbd_ctrl|state.FETCH~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|fetchdata\(6));

-- Location: LCCOMB_X24_Y17_N28
\kbd_ctrl|Equal10~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal10~0_combout\ = (\kbd_ctrl|fetchdata\(3) & (\kbd_ctrl|fetchdata\(0) & (!\kbd_ctrl|fetchdata\(1) & \kbd_ctrl|fetchdata\(6))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|fetchdata\(3),
	datab => \kbd_ctrl|fetchdata\(0),
	datac => \kbd_ctrl|fetchdata\(1),
	datad => \kbd_ctrl|fetchdata\(6),
	combout => \kbd_ctrl|Equal10~0_combout\);

-- Location: LCCOMB_X25_Y17_N24
\kbd_ctrl|Equal10~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal10~1_combout\ = (!\kbd_ctrl|selE0~combout\ & (\kbd_ctrl|Equal6~0_combout\ & \kbd_ctrl|Equal10~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|selE0~combout\,
	datac => \kbd_ctrl|Equal6~0_combout\,
	datad => \kbd_ctrl|Equal10~0_combout\,
	combout => \kbd_ctrl|Equal10~1_combout\);

-- Location: LCFF_X24_Y17_N25
\kbd_ctrl|key0code[7]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	sdata => \kbd_ctrl|fetchdata\(7),
	aclr => \kbd_ctrl|KEY0~4_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key0en~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|key0code\(7));

-- Location: LCFF_X24_Y17_N13
\kbd_ctrl|key0code[6]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	sdata => \kbd_ctrl|fetchdata\(6),
	aclr => \kbd_ctrl|KEY0~4_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key0en~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|key0code\(6));

-- Location: LCCOMB_X24_Y17_N12
\Equal2~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Equal2~0_combout\ = (\kbd_ctrl|key0code\(6) & \kbd_ctrl|key0code\(3))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \kbd_ctrl|key0code\(6),
	datad => \kbd_ctrl|key0code\(3),
	combout => \Equal2~0_combout\);

-- Location: LCCOMB_X24_Y17_N8
\Equal4~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Equal4~0_combout\ = (\kbd_ctrl|key0code\(0) & (!\kbd_ctrl|key0code\(7) & (!\kbd_ctrl|key0code\(2) & \Equal2~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(0),
	datab => \kbd_ctrl|key0code\(7),
	datac => \kbd_ctrl|key0code\(2),
	datad => \Equal2~0_combout\,
	combout => \Equal4~0_combout\);

-- Location: LCCOMB_X24_Y17_N6
\kbd_ctrl|Equal6~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal6~1_combout\ = (!\kbd_ctrl|fetchdata\(3) & (!\kbd_ctrl|fetchdata\(0) & (!\kbd_ctrl|fetchdata\(6) & \kbd_ctrl|fetchdata\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|fetchdata\(3),
	datab => \kbd_ctrl|fetchdata\(0),
	datac => \kbd_ctrl|fetchdata\(6),
	datad => \kbd_ctrl|fetchdata\(1),
	combout => \kbd_ctrl|Equal6~1_combout\);

-- Location: LCCOMB_X25_Y17_N2
\kbd_ctrl|Equal6~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal6~2_combout\ = (!\kbd_ctrl|selE0~combout\ & (\kbd_ctrl|Equal6~0_combout\ & \kbd_ctrl|Equal6~1_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|selE0~combout\,
	datab => \kbd_ctrl|Equal6~0_combout\,
	datad => \kbd_ctrl|Equal6~1_combout\,
	combout => \kbd_ctrl|Equal6~2_combout\);

-- Location: LCCOMB_X26_Y17_N18
\kbd_ctrl|KEY1~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|KEY1~2_combout\ = (\kbd_ctrl|Equal10~1_combout\ & (!\kbd_ctrl|Equal6~2_combout\ & ((\kbd_ctrl|Equal11~0_combout\) # (!\Equal4~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|Equal11~0_combout\,
	datab => \kbd_ctrl|Equal10~1_combout\,
	datac => \Equal4~0_combout\,
	datad => \kbd_ctrl|Equal6~2_combout\,
	combout => \kbd_ctrl|KEY1~2_combout\);

-- Location: LCCOMB_X26_Y17_N8
\kbd_ctrl|KEY1~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|KEY1~3_combout\ = (\kbd_ctrl|Equal12~2_combout\ & (!\kbd_ctrl|Equal8~3_combout\ & (\kbd_ctrl|key0clearn~1_combout\))) # (!\kbd_ctrl|Equal12~2_combout\ & ((\kbd_ctrl|KEY1~2_combout\) # ((!\kbd_ctrl|Equal8~3_combout\ & 
-- \kbd_ctrl|key0clearn~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111010100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|Equal12~2_combout\,
	datab => \kbd_ctrl|Equal8~3_combout\,
	datac => \kbd_ctrl|key0clearn~1_combout\,
	datad => \kbd_ctrl|KEY1~2_combout\,
	combout => \kbd_ctrl|KEY1~3_combout\);

-- Location: LCCOMB_X26_Y17_N24
\kbd_ctrl|KEY1~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|KEY1~5_combout\ = (!\kbd_ctrl|Equal14~5_combout\ & (\kbd_ctrl|relbt~combout\ & (\kbd_ctrl|Equal4~5_combout\ & \kbd_ctrl|selbt~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|Equal14~5_combout\,
	datab => \kbd_ctrl|relbt~combout\,
	datac => \kbd_ctrl|Equal4~5_combout\,
	datad => \kbd_ctrl|selbt~combout\,
	combout => \kbd_ctrl|KEY1~5_combout\);

-- Location: LCCOMB_X25_Y17_N0
\kbd_ctrl|KEY1~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|KEY1~4_combout\ = ((\kbd_ctrl|KEY1~5_combout\) # ((\kbd_ctrl|KEY0~2_combout\ & \kbd_ctrl|KEY1~3_combout\))) # (!\KEY~combout\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \KEY~combout\(0),
	datab => \kbd_ctrl|KEY0~2_combout\,
	datac => \kbd_ctrl|KEY1~3_combout\,
	datad => \kbd_ctrl|KEY1~5_combout\,
	combout => \kbd_ctrl|KEY1~4_combout\);

-- Location: LCFF_X25_Y17_N7
\kbd_ctrl|key1code[13]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	sdata => \kbd_ctrl|selE0~combout\,
	aclr => \kbd_ctrl|KEY1~4_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key1en~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|key1code\(13));

-- Location: LCCOMB_X25_Y17_N18
\kbd_ctrl|Equal4~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal4~0_combout\ = (\kbd_ctrl|fetchdata\(1) & (\kbd_ctrl|key1code\(1) & (\kbd_ctrl|key1code\(0) $ (!\kbd_ctrl|fetchdata\(0))))) # (!\kbd_ctrl|fetchdata\(1) & (!\kbd_ctrl|key1code\(1) & (\kbd_ctrl|key1code\(0) $ (!\kbd_ctrl|fetchdata\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001000000001001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|fetchdata\(1),
	datab => \kbd_ctrl|key1code\(1),
	datac => \kbd_ctrl|key1code\(0),
	datad => \kbd_ctrl|fetchdata\(0),
	combout => \kbd_ctrl|Equal4~0_combout\);

-- Location: LCFF_X25_Y17_N31
\kbd_ctrl|key1code[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	sdata => \kbd_ctrl|fetchdata\(2),
	aclr => \kbd_ctrl|KEY1~4_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key1en~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|key1code\(2));

-- Location: LCCOMB_X25_Y17_N8
\kbd_ctrl|Equal4~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal4~1_combout\ = (\kbd_ctrl|fetchdata\(3) & (\kbd_ctrl|key1code\(3) & (\kbd_ctrl|fetchdata\(2) $ (!\kbd_ctrl|key1code\(2))))) # (!\kbd_ctrl|fetchdata\(3) & (!\kbd_ctrl|key1code\(3) & (\kbd_ctrl|fetchdata\(2) $ (!\kbd_ctrl|key1code\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000010000100001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|fetchdata\(3),
	datab => \kbd_ctrl|fetchdata\(2),
	datac => \kbd_ctrl|key1code\(3),
	datad => \kbd_ctrl|key1code\(2),
	combout => \kbd_ctrl|Equal4~1_combout\);

-- Location: LCFF_X25_Y17_N13
\kbd_ctrl|key1code[6]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	sdata => \kbd_ctrl|fetchdata\(6),
	aclr => \kbd_ctrl|KEY1~4_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key1en~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|key1code\(6));

-- Location: LCCOMB_X25_Y17_N12
\kbd_ctrl|Equal4~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal4~3_combout\ = (\kbd_ctrl|fetchdata\(7) & (\kbd_ctrl|key1code\(7) & (\kbd_ctrl|fetchdata\(6) $ (!\kbd_ctrl|key1code\(6))))) # (!\kbd_ctrl|fetchdata\(7) & (!\kbd_ctrl|key1code\(7) & (\kbd_ctrl|fetchdata\(6) $ (!\kbd_ctrl|key1code\(6)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000001001000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|fetchdata\(7),
	datab => \kbd_ctrl|fetchdata\(6),
	datac => \kbd_ctrl|key1code\(6),
	datad => \kbd_ctrl|key1code\(7),
	combout => \kbd_ctrl|Equal4~3_combout\);

-- Location: LCCOMB_X25_Y17_N28
\kbd_ctrl|Equal4~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal4~4_combout\ = (\kbd_ctrl|Equal4~2_combout\ & (\kbd_ctrl|Equal4~0_combout\ & (\kbd_ctrl|Equal4~1_combout\ & \kbd_ctrl|Equal4~3_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|Equal4~2_combout\,
	datab => \kbd_ctrl|Equal4~0_combout\,
	datac => \kbd_ctrl|Equal4~1_combout\,
	datad => \kbd_ctrl|Equal4~3_combout\,
	combout => \kbd_ctrl|Equal4~4_combout\);

-- Location: LCCOMB_X25_Y17_N6
\kbd_ctrl|Equal4~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal4~5_combout\ = (\kbd_ctrl|Equal4~4_combout\ & (\kbd_ctrl|selE0~combout\ $ (!\kbd_ctrl|key1code\(13))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|selE0~combout\,
	datac => \kbd_ctrl|key1code\(13),
	datad => \kbd_ctrl|Equal4~4_combout\,
	combout => \kbd_ctrl|Equal4~5_combout\);

-- Location: LCFF_X27_Y17_N5
\kbd_ctrl|key2code[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	sdata => \kbd_ctrl|fetchdata\(1),
	aclr => \kbd_ctrl|KEY2~7_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key2en~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|key2code\(1));

-- Location: LCFF_X27_Y17_N11
\kbd_ctrl|key2code[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	sdata => \kbd_ctrl|fetchdata\(0),
	aclr => \kbd_ctrl|KEY2~7_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key2en~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|key2code\(0));

-- Location: LCCOMB_X27_Y17_N4
\kbd_ctrl|Equal5~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal5~0_combout\ = (\kbd_ctrl|fetchdata\(1) & (\kbd_ctrl|key2code\(1) & (\kbd_ctrl|fetchdata\(0) $ (!\kbd_ctrl|key2code\(0))))) # (!\kbd_ctrl|fetchdata\(1) & (!\kbd_ctrl|key2code\(1) & (\kbd_ctrl|fetchdata\(0) $ (!\kbd_ctrl|key2code\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000010000100001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|fetchdata\(1),
	datab => \kbd_ctrl|fetchdata\(0),
	datac => \kbd_ctrl|key2code\(1),
	datad => \kbd_ctrl|key2code\(0),
	combout => \kbd_ctrl|Equal5~0_combout\);

-- Location: LCFF_X27_Y17_N3
\kbd_ctrl|key2code[6]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	sdata => \kbd_ctrl|fetchdata\(6),
	aclr => \kbd_ctrl|KEY2~7_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key2en~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|key2code\(6));

-- Location: LCFF_X27_Y17_N9
\kbd_ctrl|key2code[7]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	sdata => \kbd_ctrl|fetchdata\(7),
	aclr => \kbd_ctrl|KEY2~7_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key2en~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|key2code\(7));

-- Location: LCCOMB_X27_Y17_N8
\kbd_ctrl|Equal5~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal5~3_combout\ = (\kbd_ctrl|fetchdata\(7) & (\kbd_ctrl|key2code\(7) & (\kbd_ctrl|key2code\(6) $ (!\kbd_ctrl|fetchdata\(6))))) # (!\kbd_ctrl|fetchdata\(7) & (!\kbd_ctrl|key2code\(7) & (\kbd_ctrl|key2code\(6) $ (!\kbd_ctrl|fetchdata\(6)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000010000100001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|fetchdata\(7),
	datab => \kbd_ctrl|key2code\(6),
	datac => \kbd_ctrl|key2code\(7),
	datad => \kbd_ctrl|fetchdata\(6),
	combout => \kbd_ctrl|Equal5~3_combout\);

-- Location: LCFF_X27_Y17_N27
\kbd_ctrl|key2code[5]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	sdata => \kbd_ctrl|fetchdata\(5),
	aclr => \kbd_ctrl|KEY2~7_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key2en~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|key2code\(5));

-- Location: LCCOMB_X27_Y17_N26
\kbd_ctrl|Equal5~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal5~2_combout\ = (\kbd_ctrl|key2code\(4) & (\kbd_ctrl|fetchdata\(4) & (\kbd_ctrl|fetchdata\(5) $ (!\kbd_ctrl|key2code\(5))))) # (!\kbd_ctrl|key2code\(4) & (!\kbd_ctrl|fetchdata\(4) & (\kbd_ctrl|fetchdata\(5) $ (!\kbd_ctrl|key2code\(5)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000001001000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key2code\(4),
	datab => \kbd_ctrl|fetchdata\(5),
	datac => \kbd_ctrl|key2code\(5),
	datad => \kbd_ctrl|fetchdata\(4),
	combout => \kbd_ctrl|Equal5~2_combout\);

-- Location: LCCOMB_X27_Y17_N18
\kbd_ctrl|Equal5~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal5~4_combout\ = (\kbd_ctrl|Equal5~1_combout\ & (\kbd_ctrl|Equal5~0_combout\ & (\kbd_ctrl|Equal5~3_combout\ & \kbd_ctrl|Equal5~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|Equal5~1_combout\,
	datab => \kbd_ctrl|Equal5~0_combout\,
	datac => \kbd_ctrl|Equal5~3_combout\,
	datad => \kbd_ctrl|Equal5~2_combout\,
	combout => \kbd_ctrl|Equal5~4_combout\);

-- Location: LCCOMB_X27_Y17_N22
\kbd_ctrl|Equal5~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal5~5_combout\ = (\kbd_ctrl|Equal5~4_combout\ & (\kbd_ctrl|key2code\(13) $ (!\kbd_ctrl|selE0~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key2code\(13),
	datac => \kbd_ctrl|selE0~combout\,
	datad => \kbd_ctrl|Equal5~4_combout\,
	combout => \kbd_ctrl|Equal5~5_combout\);

-- Location: LCCOMB_X26_Y17_N0
\kbd_ctrl|key2en~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|key2en~1_combout\ = (!\kbd_ctrl|Equal14~5_combout\ & (\kbd_ctrl|key2en~0_combout\ & (!\kbd_ctrl|Equal4~5_combout\ & !\kbd_ctrl|Equal5~5_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|Equal14~5_combout\,
	datab => \kbd_ctrl|key2en~0_combout\,
	datac => \kbd_ctrl|Equal4~5_combout\,
	datad => \kbd_ctrl|Equal5~5_combout\,
	combout => \kbd_ctrl|key2en~1_combout\);

-- Location: LCCOMB_X26_Y17_N26
\kbd_ctrl|key0en~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|key0en~2_combout\ = (\Equal2~1_combout\ & (\kbd_ctrl|Equal17~0_combout\ & (!\kbd_ctrl|key0code\(13) & \kbd_ctrl|key2en~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal2~1_combout\,
	datab => \kbd_ctrl|Equal17~0_combout\,
	datac => \kbd_ctrl|key0code\(13),
	datad => \kbd_ctrl|key2en~1_combout\,
	combout => \kbd_ctrl|key0en~2_combout\);

-- Location: LCFF_X24_Y17_N9
\kbd_ctrl|key0code[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	sdata => \kbd_ctrl|fetchdata\(2),
	aclr => \kbd_ctrl|KEY0~4_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key0en~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|key0code\(2));

-- Location: LCFF_X25_Y17_N5
\kbd_ctrl|key0code[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	sdata => \kbd_ctrl|fetchdata\(0),
	aclr => \kbd_ctrl|KEY0~4_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key0en~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|key0code\(0));

-- Location: LCCOMB_X24_Y17_N24
\Equal2~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \Equal2~1_combout\ = (!\kbd_ctrl|key0code\(5) & (!\kbd_ctrl|key0code\(2) & (!\kbd_ctrl|key0code\(7) & !\kbd_ctrl|key0code\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(5),
	datab => \kbd_ctrl|key0code\(2),
	datac => \kbd_ctrl|key0code\(7),
	datad => \kbd_ctrl|key0code\(0),
	combout => \Equal2~1_combout\);

-- Location: LCCOMB_X26_Y17_N4
\kbd_ctrl|key0clearn~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|key0clearn~1_combout\ = (\kbd_ctrl|Equal6~2_combout\ & (((!\Equal2~1_combout\) # (!\kbd_ctrl|key0code\(13))) # (!\kbd_ctrl|key0clearn~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0clearn~0_combout\,
	datab => \kbd_ctrl|key0code\(13),
	datac => \Equal2~1_combout\,
	datad => \kbd_ctrl|Equal6~2_combout\,
	combout => \kbd_ctrl|key0clearn~1_combout\);

-- Location: LCCOMB_X26_Y17_N20
\kbd_ctrl|KEY0~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|KEY0~3_combout\ = (\kbd_ctrl|Equal6~2_combout\) # ((!\kbd_ctrl|Equal11~0_combout\ & (\kbd_ctrl|Equal10~1_combout\ & \Equal4~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111101000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|Equal11~0_combout\,
	datab => \kbd_ctrl|Equal10~1_combout\,
	datac => \Equal4~0_combout\,
	datad => \kbd_ctrl|Equal6~2_combout\,
	combout => \kbd_ctrl|KEY0~3_combout\);

-- Location: LCCOMB_X24_Y17_N2
\kbd_ctrl|Equal14~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal14~3_combout\ = (\kbd_ctrl|key0code\(7) & (\kbd_ctrl|fetchdata\(7) & (\kbd_ctrl|key0code\(6) $ (!\kbd_ctrl|fetchdata\(6))))) # (!\kbd_ctrl|key0code\(7) & (!\kbd_ctrl|fetchdata\(7) & (\kbd_ctrl|key0code\(6) $ (!\kbd_ctrl|fetchdata\(6)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001000000001001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(7),
	datab => \kbd_ctrl|fetchdata\(7),
	datac => \kbd_ctrl|key0code\(6),
	datad => \kbd_ctrl|fetchdata\(6),
	combout => \kbd_ctrl|Equal14~3_combout\);

-- Location: LCFF_X26_Y17_N13
\kbd_ctrl|key0code[5]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	sdata => \kbd_ctrl|fetchdata\(5),
	aclr => \kbd_ctrl|KEY0~4_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key0en~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|key0code\(5));

-- Location: LCCOMB_X24_Y17_N14
\kbd_ctrl|Equal14~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal14~2_combout\ = (\kbd_ctrl|fetchdata\(4) & (\kbd_ctrl|key0code\(4) & (\kbd_ctrl|fetchdata\(5) $ (!\kbd_ctrl|key0code\(5))))) # (!\kbd_ctrl|fetchdata\(4) & (!\kbd_ctrl|key0code\(4) & (\kbd_ctrl|fetchdata\(5) $ (!\kbd_ctrl|key0code\(5)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000010000100001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|fetchdata\(4),
	datab => \kbd_ctrl|fetchdata\(5),
	datac => \kbd_ctrl|key0code\(4),
	datad => \kbd_ctrl|key0code\(5),
	combout => \kbd_ctrl|Equal14~2_combout\);

-- Location: LCCOMB_X24_Y17_N30
\kbd_ctrl|Equal14~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal14~0_combout\ = (\kbd_ctrl|key0code\(0) & (\kbd_ctrl|fetchdata\(0) & (\kbd_ctrl|fetchdata\(1) $ (!\kbd_ctrl|key0code\(1))))) # (!\kbd_ctrl|key0code\(0) & (!\kbd_ctrl|fetchdata\(0) & (\kbd_ctrl|fetchdata\(1) $ (!\kbd_ctrl|key0code\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000010000100001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(0),
	datab => \kbd_ctrl|fetchdata\(1),
	datac => \kbd_ctrl|fetchdata\(0),
	datad => \kbd_ctrl|key0code\(1),
	combout => \kbd_ctrl|Equal14~0_combout\);

-- Location: LCCOMB_X24_Y17_N18
\kbd_ctrl|Equal14~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal14~4_combout\ = (\kbd_ctrl|Equal14~1_combout\ & (\kbd_ctrl|Equal14~3_combout\ & (\kbd_ctrl|Equal14~2_combout\ & \kbd_ctrl|Equal14~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|Equal14~1_combout\,
	datab => \kbd_ctrl|Equal14~3_combout\,
	datac => \kbd_ctrl|Equal14~2_combout\,
	datad => \kbd_ctrl|Equal14~0_combout\,
	combout => \kbd_ctrl|Equal14~4_combout\);

-- Location: LCCOMB_X25_Y17_N20
\kbd_ctrl|Equal14~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal14~5_combout\ = (\kbd_ctrl|Equal14~4_combout\ & (\kbd_ctrl|key0code\(13) $ (!\kbd_ctrl|selE0~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(13),
	datac => \kbd_ctrl|selE0~combout\,
	datad => \kbd_ctrl|Equal14~4_combout\,
	combout => \kbd_ctrl|Equal14~5_combout\);

-- Location: LCCOMB_X26_Y17_N30
\kbd_ctrl|KEY0~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|KEY0~5_combout\ = ((\kbd_ctrl|relbt~combout\ & (\kbd_ctrl|Equal14~5_combout\ & \kbd_ctrl|selbt~combout\))) # (!\KEY~combout\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101010101010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \KEY~combout\(0),
	datab => \kbd_ctrl|relbt~combout\,
	datac => \kbd_ctrl|Equal14~5_combout\,
	datad => \kbd_ctrl|selbt~combout\,
	combout => \kbd_ctrl|KEY0~5_combout\);

-- Location: LCCOMB_X26_Y17_N2
\kbd_ctrl|KEY0~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|KEY0~4_combout\ = (\kbd_ctrl|KEY0~5_combout\) # ((\kbd_ctrl|KEY0~2_combout\ & (!\kbd_ctrl|key0clearn~1_combout\ & \kbd_ctrl|KEY0~3_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|KEY0~2_combout\,
	datab => \kbd_ctrl|key0clearn~1_combout\,
	datac => \kbd_ctrl|KEY0~3_combout\,
	datad => \kbd_ctrl|KEY0~5_combout\,
	combout => \kbd_ctrl|KEY0~4_combout\);

-- Location: LCFF_X26_Y17_N27
\kbd_ctrl|key0code[13]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	sdata => \kbd_ctrl|selE0~combout\,
	aclr => \kbd_ctrl|KEY0~4_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key0en~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|key0code\(13));

-- Location: LCCOMB_X25_Y17_N22
\kbd_ctrl|Equal17~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal17~1_combout\ = ((\kbd_ctrl|key0code\(13)) # (!\Equal2~1_combout\)) # (!\kbd_ctrl|Equal17~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101110111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|Equal17~0_combout\,
	datab => \kbd_ctrl|key0code\(13),
	datad => \Equal2~1_combout\,
	combout => \kbd_ctrl|Equal17~1_combout\);

-- Location: LCCOMB_X27_Y17_N30
\kbd_ctrl|key2en~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|key2en~2_combout\ = (\kbd_ctrl|Equal18~5_combout\ & (\kbd_ctrl|Equal17~1_combout\ & (\kbd_ctrl|key2en~1_combout\ & !\kbd_ctrl|Equal19~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|Equal18~5_combout\,
	datab => \kbd_ctrl|Equal17~1_combout\,
	datac => \kbd_ctrl|key2en~1_combout\,
	datad => \kbd_ctrl|Equal19~2_combout\,
	combout => \kbd_ctrl|key2en~2_combout\);

-- Location: LCFF_X27_Y17_N7
\kbd_ctrl|key2code[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	sdata => \kbd_ctrl|fetchdata\(2),
	aclr => \kbd_ctrl|KEY2~7_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key2en~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|key2code\(2));

-- Location: LCCOMB_X27_Y17_N6
\kbd_ctrl|Equal19~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal19~0_combout\ = (!\kbd_ctrl|key2code\(7) & (!\kbd_ctrl|key2code\(2) & !\kbd_ctrl|key2code\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key2code\(7),
	datac => \kbd_ctrl|key2code\(2),
	datad => \kbd_ctrl|key2code\(5),
	combout => \kbd_ctrl|Equal19~0_combout\);

-- Location: LCCOMB_X27_Y17_N24
\kbd_ctrl|KEY2~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|KEY2~6_combout\ = (\kbd_ctrl|key2code\(13) & (\kbd_ctrl|KEY0~2_combout\ & (\kbd_ctrl|key2code\(4) & \kbd_ctrl|Equal19~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key2code\(13),
	datab => \kbd_ctrl|KEY0~2_combout\,
	datac => \kbd_ctrl|key2code\(4),
	datad => \kbd_ctrl|Equal19~0_combout\,
	combout => \kbd_ctrl|KEY2~6_combout\);

-- Location: LCFF_X27_Y17_N1
\kbd_ctrl|key2code[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	sdata => \kbd_ctrl|fetchdata\(3),
	aclr => \kbd_ctrl|KEY2~7_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key2en~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|key2code\(3));

-- Location: LCCOMB_X27_Y17_N10
\kbd_ctrl|KEY2~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|KEY2~1_combout\ = (!\kbd_ctrl|key2code\(1) & (\kbd_ctrl|key2code\(3) & (\kbd_ctrl|key2code\(0) & \kbd_ctrl|key2code\(6))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key2code\(1),
	datab => \kbd_ctrl|key2code\(3),
	datac => \kbd_ctrl|key2code\(0),
	datad => \kbd_ctrl|key2code\(6),
	combout => \kbd_ctrl|KEY2~1_combout\);

-- Location: LCCOMB_X25_Y17_N14
\kbd_ctrl|Equal12~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal12~1_combout\ = ((\kbd_ctrl|key1code\(1)) # ((!\kbd_ctrl|key1code\(3)) # (!\kbd_ctrl|key1code\(4)))) # (!\kbd_ctrl|key1code\(13))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key1code\(13),
	datab => \kbd_ctrl|key1code\(1),
	datac => \kbd_ctrl|key1code\(4),
	datad => \kbd_ctrl|key1code\(3),
	combout => \kbd_ctrl|Equal12~1_combout\);

-- Location: LCCOMB_X26_Y17_N28
\kbd_ctrl|KEY2~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|KEY2~2_combout\ = (\kbd_ctrl|KEY2~1_combout\ & ((\kbd_ctrl|Equal12~0_combout\) # ((\kbd_ctrl|Equal12~1_combout\) # (!\kbd_ctrl|key1code\(6)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|Equal12~0_combout\,
	datab => \kbd_ctrl|key1code\(6),
	datac => \kbd_ctrl|KEY2~1_combout\,
	datad => \kbd_ctrl|Equal12~1_combout\,
	combout => \kbd_ctrl|KEY2~2_combout\);

-- Location: LCCOMB_X27_Y17_N14
\kbd_ctrl|KEY2~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|KEY2~5_combout\ = (\kbd_ctrl|KEY2~4_combout\ & ((\kbd_ctrl|key0clearn~1_combout\) # ((\kbd_ctrl|KEY2~2_combout\ & \kbd_ctrl|KEY1~2_combout\)))) # (!\kbd_ctrl|KEY2~4_combout\ & (\kbd_ctrl|KEY2~2_combout\ & ((\kbd_ctrl|KEY1~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|KEY2~4_combout\,
	datab => \kbd_ctrl|KEY2~2_combout\,
	datac => \kbd_ctrl|key0clearn~1_combout\,
	datad => \kbd_ctrl|KEY1~2_combout\,
	combout => \kbd_ctrl|KEY2~5_combout\);

-- Location: LCCOMB_X26_Y17_N14
\kbd_ctrl|KEY2~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|KEY2~0_combout\ = (\kbd_ctrl|KEY0~2_combout\ & (!\kbd_ctrl|Equal4~5_combout\ & (!\kbd_ctrl|Equal14~5_combout\ & \kbd_ctrl|Equal5~5_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|KEY0~2_combout\,
	datab => \kbd_ctrl|Equal4~5_combout\,
	datac => \kbd_ctrl|Equal14~5_combout\,
	datad => \kbd_ctrl|Equal5~5_combout\,
	combout => \kbd_ctrl|KEY2~0_combout\);

-- Location: LCCOMB_X27_Y17_N20
\kbd_ctrl|KEY2~7\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|KEY2~7_combout\ = ((\kbd_ctrl|KEY2~0_combout\) # ((\kbd_ctrl|KEY2~6_combout\ & \kbd_ctrl|KEY2~5_combout\))) # (!\KEY~combout\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \KEY~combout\(0),
	datab => \kbd_ctrl|KEY2~6_combout\,
	datac => \kbd_ctrl|KEY2~5_combout\,
	datad => \kbd_ctrl|KEY2~0_combout\,
	combout => \kbd_ctrl|KEY2~7_combout\);

-- Location: LCFF_X27_Y17_N25
\kbd_ctrl|key2code[4]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	sdata => \kbd_ctrl|fetchdata\(4),
	aclr => \kbd_ctrl|KEY2~7_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key2en~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|key2code\(4));

-- Location: LCFF_X27_Y17_N17
\kbd_ctrl|key2code[13]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	sdata => \kbd_ctrl|selE0~combout\,
	aclr => \kbd_ctrl|KEY2~7_combout\,
	sload => VCC,
	ena => \kbd_ctrl|key2en~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|key2code\(13));

-- Location: LCCOMB_X27_Y17_N16
\kbd_ctrl|Equal19~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal19~2_combout\ = ((\kbd_ctrl|key2code\(4)) # ((\kbd_ctrl|key2code\(13)) # (!\kbd_ctrl|Equal19~0_combout\))) # (!\kbd_ctrl|Equal19~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|Equal19~1_combout\,
	datab => \kbd_ctrl|key2code\(4),
	datac => \kbd_ctrl|key2code\(13),
	datad => \kbd_ctrl|Equal19~0_combout\,
	combout => \kbd_ctrl|Equal19~2_combout\);

-- Location: LCCOMB_X29_Y17_N4
\kbd_ctrl|key_on[2]~feeder\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|key_on[2]~feeder_combout\ = \kbd_ctrl|Equal19~2_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \kbd_ctrl|Equal19~2_combout\,
	combout => \kbd_ctrl|key_on[2]~feeder_combout\);

-- Location: LCFF_X29_Y17_N5
\kbd_ctrl|key_on[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|key_on[2]~feeder_combout\,
	aclr => \ALT_INV_KEY~combout\(0),
	ena => \kbd_ctrl|state.CLRDP~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|key_on\(2));

-- Location: LCFF_X29_Y17_N9
\kbd_ctrl|key_on[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	sdata => \kbd_ctrl|Equal17~1_combout\,
	aclr => \ALT_INV_KEY~combout\(0),
	sload => VCC,
	ena => \kbd_ctrl|state.CLRDP~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|key_on\(0));

-- Location: LCCOMB_X24_Y17_N26
\kbd_ctrl|Equal18~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal18~4_combout\ = (!\kbd_ctrl|key1code\(1) & (!\kbd_ctrl|key1code\(4) & !\kbd_ctrl|key1code\(13)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000010001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key1code\(1),
	datab => \kbd_ctrl|key1code\(4),
	datad => \kbd_ctrl|key1code\(13),
	combout => \kbd_ctrl|Equal18~4_combout\);

-- Location: LCCOMB_X24_Y17_N4
\kbd_ctrl|Equal18~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal18~5_combout\ = (\kbd_ctrl|key1code\(6)) # ((\kbd_ctrl|key1code\(3)) # ((!\kbd_ctrl|Equal18~4_combout\) # (!\kbd_ctrl|Equal18~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key1code\(6),
	datab => \kbd_ctrl|key1code\(3),
	datac => \kbd_ctrl|Equal18~2_combout\,
	datad => \kbd_ctrl|Equal18~4_combout\,
	combout => \kbd_ctrl|Equal18~5_combout\);

-- Location: LCCOMB_X29_Y17_N2
\kbd_ctrl|key_on[1]~feeder\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|key_on[1]~feeder_combout\ = \kbd_ctrl|Equal18~5_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \kbd_ctrl|Equal18~5_combout\,
	combout => \kbd_ctrl|key_on[1]~feeder_combout\);

-- Location: LCFF_X29_Y17_N3
\kbd_ctrl|key_on[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|key_on[1]~feeder_combout\,
	aclr => \ALT_INV_KEY~combout\(0),
	ena => \kbd_ctrl|state.CLRDP~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|key_on\(1));

-- Location: LCCOMB_X29_Y17_N14
\Equal7~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Equal7~0_combout\ = (!\kbd_ctrl|key_on\(2) & (!\kbd_ctrl|key_on\(0) & !\kbd_ctrl|key_on\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|key_on\(2),
	datac => \kbd_ctrl|key_on\(0),
	datad => \kbd_ctrl|key_on\(1),
	combout => \Equal7~0_combout\);

-- Location: LCFF_X30_Y17_N27
\lights[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCKHZ~clkctrl_outclk\,
	datain => \lights~1_combout\,
	aclr => \ALT_INV_KEY~combout\(0),
	ena => \Equal7~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => lights(0));

-- Location: LCCOMB_X30_Y17_N28
\lights~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \lights~2_combout\ = (lights(2)) # (!lights(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => lights(2),
	datad => lights(0),
	combout => \lights~2_combout\);

-- Location: LCFF_X30_Y17_N29
\lights[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCKHZ~clkctrl_outclk\,
	datain => \lights~2_combout\,
	aclr => \ALT_INV_KEY~combout\(0),
	ena => \Equal7~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => lights(1));

-- Location: LCFF_X34_Y17_N9
\kbd_ctrl|laststate[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	sdata => lights(1),
	sload => VCC,
	ena => \kbd_ctrl|Equal20~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|laststate\(2));

-- Location: LCCOMB_X30_Y17_N12
\lights~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \lights~0_combout\ = (lights(2) & (((!lights(0))))) # (!lights(2) & (lights(1) & ((!\dir~regout\) # (!lights(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001000111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => lights(1),
	datab => lights(0),
	datac => lights(2),
	datad => \dir~regout\,
	combout => \lights~0_combout\);

-- Location: LCFF_X30_Y17_N13
\lights[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCKHZ~clkctrl_outclk\,
	datain => \lights~0_combout\,
	aclr => \ALT_INV_KEY~combout\(0),
	ena => \Equal7~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => lights(2));

-- Location: LCFF_X34_Y17_N1
\kbd_ctrl|laststate[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	sdata => lights(2),
	sload => VCC,
	ena => \kbd_ctrl|Equal20~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|laststate\(1));

-- Location: LCCOMB_X34_Y17_N2
\kbd_ctrl|laststate[0]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|laststate[0]~0_combout\ = !lights(0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => lights(0),
	combout => \kbd_ctrl|laststate[0]~0_combout\);

-- Location: LCFF_X34_Y17_N3
\kbd_ctrl|laststate[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|laststate[0]~0_combout\,
	ena => \kbd_ctrl|Equal20~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|laststate\(0));

-- Location: LCCOMB_X34_Y17_N0
\kbd_ctrl|Equal20~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal20~0_combout\ = (lights(2) & (\kbd_ctrl|laststate\(1) & (lights(0) $ (\kbd_ctrl|laststate\(0))))) # (!lights(2) & (!\kbd_ctrl|laststate\(1) & (lights(0) $ (\kbd_ctrl|laststate\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000110000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => lights(2),
	datab => lights(0),
	datac => \kbd_ctrl|laststate\(1),
	datad => \kbd_ctrl|laststate\(0),
	combout => \kbd_ctrl|Equal20~0_combout\);

-- Location: LCCOMB_X34_Y17_N8
\kbd_ctrl|Equal20~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Equal20~1_combout\ = (lights(1) $ (\kbd_ctrl|laststate\(2))) # (!\kbd_ctrl|Equal20~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => lights(1),
	datac => \kbd_ctrl|laststate\(2),
	datad => \kbd_ctrl|Equal20~0_combout\,
	combout => \kbd_ctrl|Equal20~1_combout\);

-- Location: LCFF_X33_Y17_N31
\kbd_ctrl|siguplights\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	sdata => \kbd_ctrl|Equal20~1_combout\,
	aclr => \kbd_ctrl|process_0~0_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|siguplights~regout\);

-- Location: LCCOMB_X33_Y17_N30
\kbd_ctrl|process_15~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|process_15~0_combout\ = ((\kbd_ctrl|siguplights~regout\) # (!\KEY~combout\(1))) # (!\KEY~combout\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \KEY~combout\(0),
	datac => \kbd_ctrl|siguplights~regout\,
	datad => \KEY~combout\(1),
	combout => \kbd_ctrl|process_15~0_combout\);

-- Location: LCFF_X33_Y17_N21
\kbd_ctrl|cmdstate.SETCMD\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|Selector8~1_combout\,
	aclr => \kbd_ctrl|process_15~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|cmdstate.SETCMD~regout\);

-- Location: LCCOMB_X34_Y18_N4
\kbd_ctrl|hdata[3]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|hdata[3]~0_combout\ = !\kbd_ctrl|cmdstate.SETCMD~regout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \kbd_ctrl|cmdstate.SETCMD~regout\,
	combout => \kbd_ctrl|hdata[3]~0_combout\);

-- Location: LCCOMB_X33_Y17_N0
\kbd_ctrl|Selector13~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Selector13~4_combout\ = (\kbd_ctrl|cmdstate.SETLIGHTS~regout\ & \kbd_ctrl|ps2_ctrl|send_rdy~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|cmdstate.SETLIGHTS~regout\,
	datad => \kbd_ctrl|ps2_ctrl|send_rdy~2_combout\,
	combout => \kbd_ctrl|Selector13~4_combout\);

-- Location: LCFF_X33_Y17_N1
\kbd_ctrl|cmdstate.SENDVAL\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|Selector13~4_combout\,
	aclr => \kbd_ctrl|process_15~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|cmdstate.SENDVAL~regout\);

-- Location: LCCOMB_X32_Y17_N4
\kbd_ctrl|Selector13~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Selector13~3_combout\ = (\kbd_ctrl|cmdstate.SENDVAL~regout\) # ((!\kbd_ctrl|cmdstate.SEND~regout\ & (\kbd_ctrl|cmdstate.WAITACK1~regout\ & !\kbd_ctrl|Selector13~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011011100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|cmdstate.SEND~regout\,
	datab => \kbd_ctrl|cmdstate.SENDVAL~regout\,
	datac => \kbd_ctrl|cmdstate.WAITACK1~regout\,
	datad => \kbd_ctrl|Selector13~2_combout\,
	combout => \kbd_ctrl|Selector13~3_combout\);

-- Location: LCFF_X32_Y17_N5
\kbd_ctrl|cmdstate.WAITACK1\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|Selector13~3_combout\,
	aclr => \kbd_ctrl|process_15~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|cmdstate.WAITACK1~regout\);

-- Location: LCCOMB_X32_Y17_N18
\kbd_ctrl|Selector13~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Selector13~1_combout\ = (\kbd_ctrl|ps2_ctrl|odata_rdy~combout\ & ((\kbd_ctrl|cmdstate.WAITACK1~regout\) # (\kbd_ctrl|cmdstate.WAITACK~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|cmdstate.WAITACK1~regout\,
	datac => \kbd_ctrl|cmdstate.WAITACK~regout\,
	datad => \kbd_ctrl|ps2_ctrl|odata_rdy~combout\,
	combout => \kbd_ctrl|Selector13~1_combout\);

-- Location: LCCOMB_X32_Y17_N22
\kbd_ctrl|Selector13~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Selector13~2_combout\ = (\kbd_ctrl|Selector13~0_combout\ & ((\kbd_ctrl|Selector13~1_combout\) # ((\kbd_ctrl|WideOr2~0_combout\ & \kbd_ctrl|ps2_ctrl|send_rdy~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|Selector13~0_combout\,
	datab => \kbd_ctrl|WideOr2~0_combout\,
	datac => \kbd_ctrl|ps2_ctrl|send_rdy~2_combout\,
	datad => \kbd_ctrl|Selector13~1_combout\,
	combout => \kbd_ctrl|Selector13~2_combout\);

-- Location: LCCOMB_X32_Y17_N14
\kbd_ctrl|Selector10~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Selector10~0_combout\ = (!\kbd_ctrl|cmdstate.SENDVAL~regout\ & ((\kbd_ctrl|cmdstate.SEND~regout\) # ((\kbd_ctrl|cmdstate.WAITACK~regout\ & !\kbd_ctrl|Selector13~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010001000110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|cmdstate.SEND~regout\,
	datab => \kbd_ctrl|cmdstate.SENDVAL~regout\,
	datac => \kbd_ctrl|cmdstate.WAITACK~regout\,
	datad => \kbd_ctrl|Selector13~2_combout\,
	combout => \kbd_ctrl|Selector10~0_combout\);

-- Location: LCFF_X32_Y17_N15
\kbd_ctrl|cmdstate.WAITACK\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|Selector10~0_combout\,
	aclr => \kbd_ctrl|process_15~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|cmdstate.WAITACK~regout\);

-- Location: LCCOMB_X32_Y17_N26
\kbd_ctrl|Selector11~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Selector11~0_combout\ = (\kbd_ctrl|ps2_ctrl|odata_rdy~combout\ & ((\kbd_ctrl|Equal21~2_combout\ & ((\kbd_ctrl|cmdstate.WAITACK~regout\))) # (!\kbd_ctrl|Equal21~2_combout\ & (\kbd_ctrl|cmdstate.WAITACK1~regout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|cmdstate.WAITACK1~regout\,
	datab => \kbd_ctrl|cmdstate.WAITACK~regout\,
	datac => \kbd_ctrl|ps2_ctrl|odata_rdy~combout\,
	datad => \kbd_ctrl|Equal21~2_combout\,
	combout => \kbd_ctrl|Selector11~0_combout\);

-- Location: LCCOMB_X32_Y17_N24
\kbd_ctrl|Selector11~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Selector11~1_combout\ = (\kbd_ctrl|Selector11~0_combout\) # ((!\kbd_ctrl|ps2_ctrl|send_rdy~2_combout\ & \kbd_ctrl|cmdstate.SETLIGHTS~regout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111101010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|send_rdy~2_combout\,
	datac => \kbd_ctrl|cmdstate.SETLIGHTS~regout\,
	datad => \kbd_ctrl|Selector11~0_combout\,
	combout => \kbd_ctrl|Selector11~1_combout\);

-- Location: LCFF_X32_Y17_N25
\kbd_ctrl|cmdstate.SETLIGHTS\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|Selector11~1_combout\,
	aclr => \kbd_ctrl|process_15~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|cmdstate.SETLIGHTS~regout\);

-- Location: LCCOMB_X33_Y17_N2
\kbd_ctrl|WideOr2~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|WideOr2~0_combout\ = (\kbd_ctrl|cmdstate.SETLIGHTS~regout\) # (!\kbd_ctrl|cmdstate.SETCMD~regout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111101010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|cmdstate.SETCMD~regout\,
	datad => \kbd_ctrl|cmdstate.SETLIGHTS~regout\,
	combout => \kbd_ctrl|WideOr2~0_combout\);

-- Location: LCFF_X34_Y18_N5
\kbd_ctrl|hdata[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|hdata[3]~0_combout\,
	ena => \kbd_ctrl|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|hdata\(3));

-- Location: LCCOMB_X33_Y18_N14
\kbd_ctrl|ps2_ctrl|hdata[3]~feeder\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|hdata[3]~feeder_combout\ = \kbd_ctrl|hdata\(3)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \kbd_ctrl|hdata\(3),
	combout => \kbd_ctrl|ps2_ctrl|hdata[3]~feeder_combout\);

-- Location: LCFF_X33_Y18_N15
\kbd_ctrl|ps2_ctrl|hdata[3]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \kbd_ctrl|sigsend~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|hdata[3]~feeder_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|process_2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|hdata\(3));

-- Location: LCCOMB_X33_Y18_N20
\kbd_ctrl|ps2_ctrl|ps2_data~8\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|ps2_data~8_combout\ = (\kbd_ctrl|ps2_ctrl|TOPS2:count[2]~regout\ & (\kbd_ctrl|ps2_ctrl|hdata\(3) & ((\kbd_ctrl|ps2_ctrl|TOPS2:count[0]~regout\) # (\kbd_ctrl|ps2_ctrl|TOPS2:count[1]~regout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|TOPS2:count[2]~regout\,
	datab => \kbd_ctrl|ps2_ctrl|hdata\(3),
	datac => \kbd_ctrl|ps2_ctrl|TOPS2:count[0]~regout\,
	datad => \kbd_ctrl|ps2_ctrl|TOPS2:count[1]~regout\,
	combout => \kbd_ctrl|ps2_ctrl|ps2_data~8_combout\);

-- Location: LCCOMB_X34_Y18_N22
\kbd_ctrl|Selector5~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Selector5~0_combout\ = (lights(1)) # (!\kbd_ctrl|cmdstate.SETLIGHTS~regout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => lights(1),
	datad => \kbd_ctrl|cmdstate.SETLIGHTS~regout\,
	combout => \kbd_ctrl|Selector5~0_combout\);

-- Location: LCFF_X34_Y18_N23
\kbd_ctrl|hdata[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|Selector5~0_combout\,
	ena => \kbd_ctrl|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|hdata\(2));

-- Location: LCCOMB_X33_Y18_N26
\kbd_ctrl|ps2_ctrl|hdata[2]~feeder\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|hdata[2]~feeder_combout\ = \kbd_ctrl|hdata\(2)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \kbd_ctrl|hdata\(2),
	combout => \kbd_ctrl|ps2_ctrl|hdata[2]~feeder_combout\);

-- Location: LCFF_X33_Y18_N27
\kbd_ctrl|ps2_ctrl|hdata[2]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \kbd_ctrl|sigsend~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|hdata[2]~feeder_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|process_2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|hdata\(2));

-- Location: LCCOMB_X34_Y18_N24
\kbd_ctrl|Selector6~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Selector6~0_combout\ = (lights(2) & \kbd_ctrl|cmdstate.SETLIGHTS~regout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => lights(2),
	datad => \kbd_ctrl|cmdstate.SETLIGHTS~regout\,
	combout => \kbd_ctrl|Selector6~0_combout\);

-- Location: LCFF_X34_Y18_N25
\kbd_ctrl|hdata[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|Selector6~0_combout\,
	ena => \kbd_ctrl|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|hdata\(1));

-- Location: LCCOMB_X33_Y18_N4
\kbd_ctrl|ps2_ctrl|hdata[1]~feeder\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|hdata[1]~feeder_combout\ = \kbd_ctrl|hdata\(1)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \kbd_ctrl|hdata\(1),
	combout => \kbd_ctrl|ps2_ctrl|hdata[1]~feeder_combout\);

-- Location: LCFF_X33_Y18_N5
\kbd_ctrl|ps2_ctrl|hdata[1]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \kbd_ctrl|sigsend~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|hdata[1]~feeder_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|process_2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|hdata\(1));

-- Location: LCCOMB_X34_Y18_N30
\kbd_ctrl|Selector7~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|Selector7~0_combout\ = (!\kbd_ctrl|cmdstate.SETLIGHTS~regout\) # (!lights(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => lights(0),
	datad => \kbd_ctrl|cmdstate.SETLIGHTS~regout\,
	combout => \kbd_ctrl|Selector7~0_combout\);

-- Location: LCFF_X34_Y18_N31
\kbd_ctrl|hdata[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|Selector7~0_combout\,
	ena => \kbd_ctrl|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|hdata\(0));

-- Location: LCCOMB_X33_Y18_N22
\kbd_ctrl|ps2_ctrl|hdata[0]~feeder\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|hdata[0]~feeder_combout\ = \kbd_ctrl|hdata\(0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \kbd_ctrl|hdata\(0),
	combout => \kbd_ctrl|ps2_ctrl|hdata[0]~feeder_combout\);

-- Location: LCFF_X33_Y18_N23
\kbd_ctrl|ps2_ctrl|hdata[0]\ : cycloneii_lcell_ff
PORT MAP (
	clk => \kbd_ctrl|sigsend~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|hdata[0]~feeder_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|process_2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|hdata\(0));

-- Location: LCCOMB_X33_Y18_N28
\kbd_ctrl|ps2_ctrl|Mux0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Mux0~0_combout\ = (\kbd_ctrl|ps2_ctrl|TOPS2:count[0]~regout\ & ((\kbd_ctrl|ps2_ctrl|TOPS2:count[1]~regout\) # ((\kbd_ctrl|ps2_ctrl|hdata\(1))))) # (!\kbd_ctrl|ps2_ctrl|TOPS2:count[0]~regout\ & (!\kbd_ctrl|ps2_ctrl|TOPS2:count[1]~regout\ 
-- & ((\kbd_ctrl|ps2_ctrl|hdata\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100110101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|TOPS2:count[0]~regout\,
	datab => \kbd_ctrl|ps2_ctrl|TOPS2:count[1]~regout\,
	datac => \kbd_ctrl|ps2_ctrl|hdata\(1),
	datad => \kbd_ctrl|ps2_ctrl|hdata\(0),
	combout => \kbd_ctrl|ps2_ctrl|Mux0~0_combout\);

-- Location: LCCOMB_X33_Y18_N6
\kbd_ctrl|ps2_ctrl|Mux0~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Mux0~1_combout\ = (\kbd_ctrl|ps2_ctrl|TOPS2:count[1]~regout\ & ((\kbd_ctrl|ps2_ctrl|Mux0~0_combout\ & ((\kbd_ctrl|ps2_ctrl|hdata\(3)))) # (!\kbd_ctrl|ps2_ctrl|Mux0~0_combout\ & (\kbd_ctrl|ps2_ctrl|hdata\(2))))) # 
-- (!\kbd_ctrl|ps2_ctrl|TOPS2:count[1]~regout\ & (((\kbd_ctrl|ps2_ctrl|Mux0~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|TOPS2:count[1]~regout\,
	datab => \kbd_ctrl|ps2_ctrl|hdata\(2),
	datac => \kbd_ctrl|ps2_ctrl|hdata\(3),
	datad => \kbd_ctrl|ps2_ctrl|Mux0~0_combout\,
	combout => \kbd_ctrl|ps2_ctrl|Mux0~1_combout\);

-- Location: LCCOMB_X33_Y18_N12
\kbd_ctrl|ps2_ctrl|ps2_data~9\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|ps2_data~9_combout\ = (!\kbd_ctrl|ps2_ctrl|TOPS2:count[2]~regout\ & \kbd_ctrl|ps2_ctrl|Mux0~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|TOPS2:count[2]~regout\,
	datad => \kbd_ctrl|ps2_ctrl|Mux0~1_combout\,
	combout => \kbd_ctrl|ps2_ctrl|ps2_data~9_combout\);

-- Location: LCCOMB_X33_Y18_N0
\kbd_ctrl|ps2_ctrl|ps2_data~11\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|ps2_data~11_combout\ = (\kbd_ctrl|ps2_ctrl|ps2_data~8_combout\) # ((\kbd_ctrl|ps2_ctrl|Equal3~0_combout\ & (!\kbd_ctrl|ps2_ctrl|ps2_data~10_combout\)) # (!\kbd_ctrl|ps2_ctrl|Equal3~0_combout\ & 
-- ((\kbd_ctrl|ps2_ctrl|ps2_data~9_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111011111110100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|ps2_data~10_combout\,
	datab => \kbd_ctrl|ps2_ctrl|Equal3~0_combout\,
	datac => \kbd_ctrl|ps2_ctrl|ps2_data~8_combout\,
	datad => \kbd_ctrl|ps2_ctrl|ps2_data~9_combout\,
	combout => \kbd_ctrl|ps2_ctrl|ps2_data~11_combout\);

-- Location: LCCOMB_X32_Y18_N8
\kbd_ctrl|ps2_ctrl|ps2_data~12\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|ps2_data~12_combout\ = ((!\kbd_ctrl|ps2_ctrl|TOPS2:count[2]~regout\ & ((!\kbd_ctrl|ps2_ctrl|TOPS2:count[0]~regout\) # (!\kbd_ctrl|ps2_ctrl|TOPS2:count[1]~regout\)))) # (!\kbd_ctrl|ps2_ctrl|TOPS2:count[3]~regout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101011101011111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|TOPS2:count[3]~regout\,
	datab => \kbd_ctrl|ps2_ctrl|TOPS2:count[1]~regout\,
	datac => \kbd_ctrl|ps2_ctrl|TOPS2:count[2]~regout\,
	datad => \kbd_ctrl|ps2_ctrl|TOPS2:count[0]~regout\,
	combout => \kbd_ctrl|ps2_ctrl|ps2_data~12_combout\);

-- Location: LCCOMB_X32_Y18_N30
\kbd_ctrl|ps2_ctrl|ps2_data~13\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|ps2_data~13_combout\ = (\kbd_ctrl|ps2_ctrl|sigsending~regout\ & (\kbd_ctrl|ps2_ctrl|ps2_data~12_combout\ & !\kbd_ctrl|ps2_ctrl|sigclkreleased~regout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|sigsending~regout\,
	datac => \kbd_ctrl|ps2_ctrl|ps2_data~12_combout\,
	datad => \kbd_ctrl|ps2_ctrl|sigclkreleased~regout\,
	combout => \kbd_ctrl|ps2_ctrl|ps2_data~13_combout\);

-- Location: LCFF_X33_Y18_N1
\kbd_ctrl|ps2_ctrl|ps2_data~reg0\ : cycloneii_lcell_ff
PORT MAP (
	clk => \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|ps2_data~11_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|sigclkheld~regout\,
	ena => \kbd_ctrl|ps2_ctrl|ps2_data~13_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|ps2_data~reg0_regout\);

-- Location: LCCOMB_X32_Y18_N28
\kbd_ctrl|ps2_ctrl|ps2_data~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|ps2_data~1_combout\ = (\kbd_ctrl|ps2_ctrl|TOPS2~1_combout\ & ((\kbd_ctrl|ps2_ctrl|sigclkheld~regout\) # (\kbd_ctrl|ps2_ctrl|ps2_data~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|TOPS2~1_combout\,
	datab => \kbd_ctrl|ps2_ctrl|sigclkheld~regout\,
	datad => \kbd_ctrl|ps2_ctrl|ps2_data~1_combout\,
	combout => \kbd_ctrl|ps2_ctrl|ps2_data~1_combout\);

-- Location: LCCOMB_X32_Y18_N14
\kbd_ctrl|ps2_ctrl|Equal5~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|Equal5~0_combout\ = (\kbd_ctrl|ps2_ctrl|TOPS2:count[3]~regout\ & !\kbd_ctrl|ps2_ctrl|TOPS2:count[2]~regout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|TOPS2:count[3]~regout\,
	datac => \kbd_ctrl|ps2_ctrl|TOPS2:count[2]~regout\,
	combout => \kbd_ctrl|ps2_ctrl|Equal5~0_combout\);

-- Location: LCCOMB_X32_Y18_N20
\kbd_ctrl|ps2_ctrl|ps2_data~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|ps2_data~3_combout\ = \kbd_ctrl|ps2_ctrl|ps2_data~1_combout\ $ (((\kbd_ctrl|ps2_ctrl|TOPS2:count[1]~regout\ $ (!\kbd_ctrl|ps2_ctrl|TOPS2:count[0]~regout\)) # (!\kbd_ctrl|ps2_ctrl|Equal5~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110010110010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|ps2_data~1_combout\,
	datab => \kbd_ctrl|ps2_ctrl|TOPS2:count[1]~regout\,
	datac => \kbd_ctrl|ps2_ctrl|Equal5~0_combout\,
	datad => \kbd_ctrl|ps2_ctrl|TOPS2:count[0]~regout\,
	combout => \kbd_ctrl|ps2_ctrl|ps2_data~3_combout\);

-- Location: LCCOMB_X32_Y18_N4
\kbd_ctrl|ps2_ctrl|ps2_data~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|ps2_data~0_combout\ = (\kbd_ctrl|ps2_ctrl|sigclkheld~regout\) # (!\kbd_ctrl|ps2_ctrl|TOPS2~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|sigclkheld~regout\,
	datad => \kbd_ctrl|ps2_ctrl|TOPS2~1_combout\,
	combout => \kbd_ctrl|ps2_ctrl|ps2_data~0_combout\);

-- Location: LCFF_X32_Y18_N21
\kbd_ctrl|ps2_ctrl|ps2_data~en_emulated\ : cycloneii_lcell_ff
PORT MAP (
	clk => \kbd_ctrl|ps2_ctrl|sigtrigger~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|ps2_data~3_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|ps2_data~0_combout\,
	ena => \kbd_ctrl|ps2_ctrl|ps2_data~13_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|ps2_data~en_emulated_regout\);

-- Location: LCCOMB_X32_Y18_N18
\kbd_ctrl|ps2_ctrl|ps2_data~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|ps2_data~2_combout\ = (\kbd_ctrl|ps2_ctrl|TOPS2~1_combout\ & ((\kbd_ctrl|ps2_ctrl|sigclkheld~regout\) # (\kbd_ctrl|ps2_ctrl|ps2_data~1_combout\ $ (\kbd_ctrl|ps2_ctrl|ps2_data~en_emulated_regout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101111000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|ps2_ctrl|ps2_data~1_combout\,
	datab => \kbd_ctrl|ps2_ctrl|sigclkheld~regout\,
	datac => \kbd_ctrl|ps2_ctrl|ps2_data~en_emulated_regout\,
	datad => \kbd_ctrl|ps2_ctrl|TOPS2~1_combout\,
	combout => \kbd_ctrl|ps2_ctrl|ps2_data~2_combout\);

-- Location: PIN_B12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\CLOCK_24[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_CLOCK_24(0),
	combout => \CLOCK_24~combout\(0));

-- Location: CLKCTRL_G10
\CLOCK_24[0]~clkctrl\ : cycloneii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \CLOCK_24[0]~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \CLOCK_24[0]~clkctrl_outclk\);

-- Location: LCCOMB_X30_Y18_N18
\kbd_ctrl|ps2_ctrl|ps2_clk~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \kbd_ctrl|ps2_ctrl|ps2_clk~2_combout\ = (\kbd_ctrl|ps2_ctrl|ps2_clk~4_combout\) # (!\kbd_ctrl|ps2_ctrl|count\(11))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|ps2_ctrl|count\(11),
	datad => \kbd_ctrl|ps2_ctrl|ps2_clk~4_combout\,
	combout => \kbd_ctrl|ps2_ctrl|ps2_clk~2_combout\);

-- Location: LCFF_X30_Y18_N19
\kbd_ctrl|ps2_ctrl|ps2_clk~en\ : cycloneii_lcell_ff
PORT MAP (
	clk => \CLOCK_24[0]~clkctrl_outclk\,
	datain => \kbd_ctrl|ps2_ctrl|ps2_clk~2_combout\,
	aclr => \kbd_ctrl|ps2_ctrl|process_2~0_combout\,
	ena => \kbd_ctrl|ps2_ctrl|sigsending~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \kbd_ctrl|ps2_ctrl|ps2_clk~en_regout\);

-- Location: LCCOMB_X8_Y17_N24
\hexseg0|Mux6~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \hexseg0|Mux6~0_combout\ = (\kbd_ctrl|key0code\(3) & (\kbd_ctrl|key0code\(0) & (\kbd_ctrl|key0code\(2) $ (\kbd_ctrl|key0code\(1))))) # (!\kbd_ctrl|key0code\(3) & (!\kbd_ctrl|key0code\(1) & (\kbd_ctrl|key0code\(0) $ (\kbd_ctrl|key0code\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100010010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(3),
	datab => \kbd_ctrl|key0code\(0),
	datac => \kbd_ctrl|key0code\(2),
	datad => \kbd_ctrl|key0code\(1),
	combout => \hexseg0|Mux6~0_combout\);

-- Location: LCCOMB_X8_Y17_N6
\hexseg0|Mux5~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \hexseg0|Mux5~0_combout\ = (\kbd_ctrl|key0code\(3) & ((\kbd_ctrl|key0code\(0) & ((\kbd_ctrl|key0code\(1)))) # (!\kbd_ctrl|key0code\(0) & (\kbd_ctrl|key0code\(2))))) # (!\kbd_ctrl|key0code\(3) & (\kbd_ctrl|key0code\(2) & (\kbd_ctrl|key0code\(0) $ 
-- (\kbd_ctrl|key0code\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100001100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(3),
	datab => \kbd_ctrl|key0code\(0),
	datac => \kbd_ctrl|key0code\(2),
	datad => \kbd_ctrl|key0code\(1),
	combout => \hexseg0|Mux5~0_combout\);

-- Location: LCCOMB_X8_Y17_N4
\hexseg0|Mux4~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \hexseg0|Mux4~0_combout\ = (\kbd_ctrl|key0code\(3) & (\kbd_ctrl|key0code\(2) & ((\kbd_ctrl|key0code\(1)) # (!\kbd_ctrl|key0code\(0))))) # (!\kbd_ctrl|key0code\(3) & (!\kbd_ctrl|key0code\(0) & (!\kbd_ctrl|key0code\(2) & \kbd_ctrl|key0code\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000100100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(3),
	datab => \kbd_ctrl|key0code\(0),
	datac => \kbd_ctrl|key0code\(2),
	datad => \kbd_ctrl|key0code\(1),
	combout => \hexseg0|Mux4~0_combout\);

-- Location: LCCOMB_X8_Y17_N22
\hexseg0|Mux3~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \hexseg0|Mux3~0_combout\ = (\kbd_ctrl|key0code\(1) & ((\kbd_ctrl|key0code\(0) & ((\kbd_ctrl|key0code\(2)))) # (!\kbd_ctrl|key0code\(0) & (\kbd_ctrl|key0code\(3) & !\kbd_ctrl|key0code\(2))))) # (!\kbd_ctrl|key0code\(1) & (!\kbd_ctrl|key0code\(3) & 
-- (\kbd_ctrl|key0code\(0) $ (\kbd_ctrl|key0code\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001000010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(3),
	datab => \kbd_ctrl|key0code\(0),
	datac => \kbd_ctrl|key0code\(2),
	datad => \kbd_ctrl|key0code\(1),
	combout => \hexseg0|Mux3~0_combout\);

-- Location: LCCOMB_X8_Y17_N0
\hexseg0|Mux2~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \hexseg0|Mux2~0_combout\ = (\kbd_ctrl|key0code\(1) & (!\kbd_ctrl|key0code\(3) & (\kbd_ctrl|key0code\(0)))) # (!\kbd_ctrl|key0code\(1) & ((\kbd_ctrl|key0code\(2) & (!\kbd_ctrl|key0code\(3))) # (!\kbd_ctrl|key0code\(2) & ((\kbd_ctrl|key0code\(0))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010001011100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(3),
	datab => \kbd_ctrl|key0code\(0),
	datac => \kbd_ctrl|key0code\(2),
	datad => \kbd_ctrl|key0code\(1),
	combout => \hexseg0|Mux2~0_combout\);

-- Location: LCCOMB_X8_Y17_N18
\hexseg0|Mux1~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \hexseg0|Mux1~0_combout\ = (\kbd_ctrl|key0code\(0) & (\kbd_ctrl|key0code\(3) $ (((\kbd_ctrl|key0code\(1)) # (!\kbd_ctrl|key0code\(2)))))) # (!\kbd_ctrl|key0code\(0) & (!\kbd_ctrl|key0code\(3) & (!\kbd_ctrl|key0code\(2) & \kbd_ctrl|key0code\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010110000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(3),
	datab => \kbd_ctrl|key0code\(0),
	datac => \kbd_ctrl|key0code\(2),
	datad => \kbd_ctrl|key0code\(1),
	combout => \hexseg0|Mux1~0_combout\);

-- Location: LCCOMB_X8_Y17_N12
\hexseg0|Mux0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \hexseg0|Mux0~0_combout\ = (\kbd_ctrl|key0code\(0) & ((\kbd_ctrl|key0code\(3)) # (\kbd_ctrl|key0code\(2) $ (\kbd_ctrl|key0code\(1))))) # (!\kbd_ctrl|key0code\(0) & ((\kbd_ctrl|key0code\(1)) # (\kbd_ctrl|key0code\(3) $ (\kbd_ctrl|key0code\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011111111011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(3),
	datab => \kbd_ctrl|key0code\(0),
	datac => \kbd_ctrl|key0code\(2),
	datad => \kbd_ctrl|key0code\(1),
	combout => \hexseg0|Mux0~0_combout\);

-- Location: LCCOMB_X1_Y21_N8
\hexseg1|Mux6~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \hexseg1|Mux6~0_combout\ = (\kbd_ctrl|key0code\(7) & (\kbd_ctrl|key0code\(4) & (\kbd_ctrl|key0code\(5) $ (\kbd_ctrl|key0code\(6))))) # (!\kbd_ctrl|key0code\(7) & (!\kbd_ctrl|key0code\(5) & (\kbd_ctrl|key0code\(4) $ (\kbd_ctrl|key0code\(6)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100110000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(7),
	datab => \kbd_ctrl|key0code\(4),
	datac => \kbd_ctrl|key0code\(5),
	datad => \kbd_ctrl|key0code\(6),
	combout => \hexseg1|Mux6~0_combout\);

-- Location: LCCOMB_X1_Y21_N14
\hexseg1|Mux5~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \hexseg1|Mux5~0_combout\ = (\kbd_ctrl|key0code\(7) & ((\kbd_ctrl|key0code\(4) & (\kbd_ctrl|key0code\(5))) # (!\kbd_ctrl|key0code\(4) & ((\kbd_ctrl|key0code\(6)))))) # (!\kbd_ctrl|key0code\(7) & (\kbd_ctrl|key0code\(6) & (\kbd_ctrl|key0code\(4) $ 
-- (\kbd_ctrl|key0code\(5)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011011010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(7),
	datab => \kbd_ctrl|key0code\(4),
	datac => \kbd_ctrl|key0code\(5),
	datad => \kbd_ctrl|key0code\(6),
	combout => \hexseg1|Mux5~0_combout\);

-- Location: LCCOMB_X1_Y21_N28
\hexseg1|Mux4~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \hexseg1|Mux4~0_combout\ = (\kbd_ctrl|key0code\(7) & (\kbd_ctrl|key0code\(6) & ((\kbd_ctrl|key0code\(5)) # (!\kbd_ctrl|key0code\(4))))) # (!\kbd_ctrl|key0code\(7) & (!\kbd_ctrl|key0code\(4) & (\kbd_ctrl|key0code\(5) & !\kbd_ctrl|key0code\(6))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010001000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(7),
	datab => \kbd_ctrl|key0code\(4),
	datac => \kbd_ctrl|key0code\(5),
	datad => \kbd_ctrl|key0code\(6),
	combout => \hexseg1|Mux4~0_combout\);

-- Location: LCCOMB_X1_Y21_N30
\hexseg1|Mux3~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \hexseg1|Mux3~0_combout\ = (\kbd_ctrl|key0code\(5) & ((\kbd_ctrl|key0code\(4) & ((\kbd_ctrl|key0code\(6)))) # (!\kbd_ctrl|key0code\(4) & (\kbd_ctrl|key0code\(7) & !\kbd_ctrl|key0code\(6))))) # (!\kbd_ctrl|key0code\(5) & (!\kbd_ctrl|key0code\(7) & 
-- (\kbd_ctrl|key0code\(4) $ (\kbd_ctrl|key0code\(6)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000100100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(7),
	datab => \kbd_ctrl|key0code\(4),
	datac => \kbd_ctrl|key0code\(5),
	datad => \kbd_ctrl|key0code\(6),
	combout => \hexseg1|Mux3~0_combout\);

-- Location: LCCOMB_X1_Y21_N16
\hexseg1|Mux2~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \hexseg1|Mux2~0_combout\ = (\kbd_ctrl|key0code\(5) & (!\kbd_ctrl|key0code\(7) & (\kbd_ctrl|key0code\(4)))) # (!\kbd_ctrl|key0code\(5) & ((\kbd_ctrl|key0code\(6) & (!\kbd_ctrl|key0code\(7))) # (!\kbd_ctrl|key0code\(6) & ((\kbd_ctrl|key0code\(4))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010101001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(7),
	datab => \kbd_ctrl|key0code\(4),
	datac => \kbd_ctrl|key0code\(5),
	datad => \kbd_ctrl|key0code\(6),
	combout => \hexseg1|Mux2~0_combout\);

-- Location: LCCOMB_X1_Y21_N18
\hexseg1|Mux1~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \hexseg1|Mux1~0_combout\ = (\kbd_ctrl|key0code\(4) & (\kbd_ctrl|key0code\(7) $ (((\kbd_ctrl|key0code\(5)) # (!\kbd_ctrl|key0code\(6)))))) # (!\kbd_ctrl|key0code\(4) & (!\kbd_ctrl|key0code\(7) & (\kbd_ctrl|key0code\(5) & !\kbd_ctrl|key0code\(6))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100100001010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(7),
	datab => \kbd_ctrl|key0code\(4),
	datac => \kbd_ctrl|key0code\(5),
	datad => \kbd_ctrl|key0code\(6),
	combout => \hexseg1|Mux1~0_combout\);

-- Location: LCCOMB_X1_Y21_N0
\hexseg1|Mux0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \hexseg1|Mux0~0_combout\ = (\kbd_ctrl|key0code\(4) & ((\kbd_ctrl|key0code\(7)) # (\kbd_ctrl|key0code\(5) $ (\kbd_ctrl|key0code\(6))))) # (!\kbd_ctrl|key0code\(4) & ((\kbd_ctrl|key0code\(5)) # (\kbd_ctrl|key0code\(7) $ (\kbd_ctrl|key0code\(6)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011110111111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(7),
	datab => \kbd_ctrl|key0code\(4),
	datac => \kbd_ctrl|key0code\(5),
	datad => \kbd_ctrl|key0code\(6),
	combout => \hexseg1|Mux0~0_combout\);

-- Location: LCCOMB_X22_Y17_N14
\Equal0~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \Equal0~1_combout\ = (!\kbd_ctrl|key0code\(6) & (!\kbd_ctrl|key0code\(1) & (\kbd_ctrl|key0code\(2) & \kbd_ctrl|key0code\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(6),
	datab => \kbd_ctrl|key0code\(1),
	datac => \kbd_ctrl|key0code\(2),
	datad => \kbd_ctrl|key0code\(3),
	combout => \Equal0~1_combout\);

-- Location: LCCOMB_X22_Y17_N28
\Equal0~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Equal0~0_combout\ = (\kbd_ctrl|key0code\(4) & (!\kbd_ctrl|key0code\(7) & \kbd_ctrl|key0code\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(4),
	datab => \kbd_ctrl|key0code\(7),
	datac => \kbd_ctrl|key0code\(5),
	combout => \Equal0~0_combout\);

-- Location: LCCOMB_X22_Y17_N16
\Equal0~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Equal0~2_combout\ = (!\kbd_ctrl|key0code\(0) & (\Equal0~1_combout\ & \Equal0~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \kbd_ctrl|key0code\(0),
	datac => \Equal0~1_combout\,
	datad => \Equal0~0_combout\,
	combout => \Equal0~2_combout\);

-- Location: LCCOMB_X24_Y17_N10
\Equal2~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \Equal2~2_combout\ = (\Equal2~1_combout\ & (\kbd_ctrl|key0code\(1) & (\kbd_ctrl|key0code\(4) & \Equal2~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal2~1_combout\,
	datab => \kbd_ctrl|key0code\(1),
	datac => \kbd_ctrl|key0code\(4),
	datad => \Equal2~0_combout\,
	combout => \Equal2~2_combout\);

-- Location: LCCOMB_X22_Y17_N10
\Equal3~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Equal3~0_combout\ = (\kbd_ctrl|key0code\(6) & (\Equal0~0_combout\ & !\kbd_ctrl|key0code\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(6),
	datab => \Equal0~0_combout\,
	datad => \kbd_ctrl|key0code\(3),
	combout => \Equal3~0_combout\);

-- Location: LCCOMB_X22_Y17_N20
\Equal6~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Equal6~0_combout\ = (!\kbd_ctrl|key0code\(1) & (\kbd_ctrl|key0code\(2) & (\kbd_ctrl|key0code\(0) & \Equal3~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(1),
	datab => \kbd_ctrl|key0code\(2),
	datac => \kbd_ctrl|key0code\(0),
	datad => \Equal3~0_combout\,
	combout => \Equal6~0_combout\);

-- Location: LCCOMB_X22_Y17_N26
\Equal4~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \Equal4~1_combout\ = (!\kbd_ctrl|key0code\(4) & (\kbd_ctrl|key0code\(5) & (\kbd_ctrl|key0code\(1) & \Equal4~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \kbd_ctrl|key0code\(4),
	datab => \kbd_ctrl|key0code\(5),
	datac => \kbd_ctrl|key0code\(1),
	datad => \Equal4~0_combout\,
	combout => \Equal4~1_combout\);

-- Location: LCCOMB_X22_Y17_N0
\KEY_PRESS~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \KEY_PRESS~0_combout\ = (\Equal0~2_combout\) # ((\Equal2~2_combout\) # ((\Equal6~0_combout\) # (\Equal4~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~2_combout\,
	datab => \Equal2~2_combout\,
	datac => \Equal6~0_combout\,
	datad => \Equal4~1_combout\,
	combout => \KEY_PRESS~0_combout\);

-- Location: LCCOMB_X22_Y17_N4
\KEY_PRESS~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \KEY_PRESS~2_combout\ = (\Equal2~2_combout\) # ((\Equal0~3_combout\ & (!\kbd_ctrl|key0code\(0) & \Equal3~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~3_combout\,
	datab => \Equal2~2_combout\,
	datac => \kbd_ctrl|key0code\(0),
	datad => \Equal3~0_combout\,
	combout => \KEY_PRESS~2_combout\);

-- Location: LCCOMB_X22_Y17_N22
\Equal5~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \Equal5~0_combout\ = (!\kbd_ctrl|key0code\(0) & \kbd_ctrl|key0code\(1))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \kbd_ctrl|key0code\(0),
	datad => \kbd_ctrl|key0code\(1),
	combout => \Equal5~0_combout\);

-- Location: LCCOMB_X22_Y17_N24
\KEY_PRESS~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \KEY_PRESS~1_combout\ = (\Equal0~2_combout\) # ((\Equal5~0_combout\ & (\kbd_ctrl|key0code\(2) & \Equal3~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal0~2_combout\,
	datab => \Equal5~0_combout\,
	datac => \kbd_ctrl|key0code\(2),
	datad => \Equal3~0_combout\,
	combout => \KEY_PRESS~1_combout\);

-- Location: LCCOMB_X22_Y17_N30
\KEY_PRESS~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \KEY_PRESS~3_combout\ = (!\Equal4~1_combout\ & (((\kbd_ctrl|key0code\(2)) # (!\Equal5~0_combout\)) # (!\Equal3~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000100110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal3~0_combout\,
	datab => \Equal4~1_combout\,
	datac => \kbd_ctrl|key0code\(2),
	datad => \Equal5~0_combout\,
	combout => \KEY_PRESS~3_combout\);

-- Location: LCCOMB_X22_Y17_N12
\KEY_PRESS~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \KEY_PRESS~4_combout\ = (!\Equal6~0_combout\ & (((!\KEY_PRESS~2_combout\ & \KEY_PRESS~1_combout\)) # (!\KEY_PRESS~3_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000001010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal6~0_combout\,
	datab => \KEY_PRESS~2_combout\,
	datac => \KEY_PRESS~1_combout\,
	datad => \KEY_PRESS~3_combout\,
	combout => \KEY_PRESS~4_combout\);

-- Location: LCCOMB_X22_Y17_N18
\KEY_PRESS~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \KEY_PRESS~5_combout\ = (!\Equal6~0_combout\ & (\KEY_PRESS~3_combout\ & ((\KEY_PRESS~2_combout\) # (\KEY_PRESS~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Equal6~0_combout\,
	datab => \KEY_PRESS~2_combout\,
	datac => \KEY_PRESS~1_combout\,
	datad => \KEY_PRESS~3_combout\,
	combout => \KEY_PRESS~5_combout\);

-- Location: PIN_A12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\CLOCK_24[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_CLOCK_24(1));

-- Location: PIN_D12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\CLOCK_27[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_CLOCK_27(0));

-- Location: PIN_E12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\CLOCK_27[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_CLOCK_27(1));

-- Location: PIN_L1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\CLOCK_50~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_CLOCK_50);

-- Location: PIN_T22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\KEY[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_KEY(2));

-- Location: PIN_T21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\KEY[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_KEY(3));

-- Location: PIN_L22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(0));

-- Location: PIN_L21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(1));

-- Location: PIN_M22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(2));

-- Location: PIN_V12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(3));

-- Location: PIN_W12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(4));

-- Location: PIN_U12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(5));

-- Location: PIN_U11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(6));

-- Location: PIN_M2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[7]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(7));

-- Location: PIN_M1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[8]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(8));

-- Location: PIN_L2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[9]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(9));

-- Location: PIN_J2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \hexseg0|Mux6~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(0));

-- Location: PIN_J1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \hexseg0|Mux5~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(1));

-- Location: PIN_H2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \hexseg0|Mux4~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(2));

-- Location: PIN_H1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \hexseg0|Mux3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(3));

-- Location: PIN_F2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \hexseg0|Mux2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(4));

-- Location: PIN_F1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \hexseg0|Mux1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(5));

-- Location: PIN_E2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \hexseg0|ALT_INV_Mux0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(6));

-- Location: PIN_E1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \hexseg1|Mux6~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(0));

-- Location: PIN_H6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \hexseg1|Mux5~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(1));

-- Location: PIN_H5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \hexseg1|Mux4~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(2));

-- Location: PIN_H4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \hexseg1|Mux3~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(3));

-- Location: PIN_G3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \hexseg1|Mux2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(4));

-- Location: PIN_D2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \hexseg1|Mux1~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(5));

-- Location: PIN_D1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \hexseg1|ALT_INV_Mux0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(6));

-- Location: PIN_G5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(0));

-- Location: PIN_G6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(1));

-- Location: PIN_C2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(2));

-- Location: PIN_C1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(3));

-- Location: PIN_E3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(4));

-- Location: PIN_E4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(5));

-- Location: PIN_D3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(6));

-- Location: PIN_F4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(0));

-- Location: PIN_D5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \kbd_ctrl|key0code\(13),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(1));

-- Location: PIN_D6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \kbd_ctrl|key0code\(13),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(2));

-- Location: PIN_J4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(3));

-- Location: PIN_L8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(4));

-- Location: PIN_F3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(5));

-- Location: PIN_D4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \kbd_ctrl|ALT_INV_key0code\(13),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(6));

-- Location: PIN_E11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\KEY_PRESS[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \KEY_PRESS~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_KEY_PRESS(0));

-- Location: PIN_D11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\KEY_PRESS[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \KEY_PRESS~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_KEY_PRESS(1));

-- Location: PIN_AA10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\KEY_PRESS[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \KEY_PRESS~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_KEY_PRESS(2));

-- Location: PIN_P15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\IS_PRESSED~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \KEY~combout\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_IS_PRESSED);

-- Location: PIN_U22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDG[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDG(0));

-- Location: PIN_U21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDG[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDG(1));

-- Location: PIN_V22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDG[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDG(2));

-- Location: PIN_V21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDG[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDG(3));

-- Location: PIN_W22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDG[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDG(4));

-- Location: PIN_W21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDG[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \kbd_ctrl|key_on\(0),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDG(5));

-- Location: PIN_Y22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDG[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \kbd_ctrl|key_on\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDG(6));

-- Location: PIN_Y21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDG[7]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \kbd_ctrl|key_on\(2),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDG(7));

-- Location: PIN_R20,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDR[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDR(0));

-- Location: PIN_R19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDR[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDR(1));

-- Location: PIN_U19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDR[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDR(2));

-- Location: PIN_Y19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDR[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDR(3));

-- Location: PIN_T18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDR[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDR(4));

-- Location: PIN_V19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDR[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDR(5));

-- Location: PIN_Y18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDR[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDR(6));

-- Location: PIN_U18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDR[7]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDR(7));

-- Location: PIN_R18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDR[8]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDR(8));

-- Location: PIN_R17,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDR[9]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDR(9));
END structure;


